#!/bin/bash

extract_texture_tool="../tools/nif_texture_extractor"

shopt -s globstar
${extract_texture_tool} ./public/**/*.nif