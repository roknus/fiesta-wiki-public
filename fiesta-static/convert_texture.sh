#!/bin/bash

convert_texture_tool="../tools/nconvert"

shopt -s globstar
# -D delete origin DDS file
# ${convert_texture_tool} -out png ./public/**/*.dds
${convert_texture_tool} -out tga -D ./public/**/*.dds