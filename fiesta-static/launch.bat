CMD /c mvn compile war:war
CMD /c docker-compose stop fiesta-static-dev
CMD /c docker-compose rm -s -f fiesta-static-dev
CMD /c docker-compose build fiesta-static-dev
CMD /c docker-compose up fiesta-static-dev