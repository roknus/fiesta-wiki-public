package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class FiestaWikiStaticApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(FiestaWikiStaticApplication.class, args);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry
				.addResourceHandler("/**")
				.addResourceLocations("file:/opt/")
				.addResourceLocations("classpath:/static/");
	}
}
