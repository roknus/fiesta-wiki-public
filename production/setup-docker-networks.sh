#!/bin/bash

docker network create -d bridge fiesta-backend-dev
docker network create -d bridge fiesta-backend-staging
docker network create -d bridge fiesta-backend-staging-de
docker network create -d bridge fiesta-backend-production
docker network create -d bridge fiesta-backend-production-de

docker network create -d bridge fiesta-frontend-staging
docker network create -d bridge fiesta-frontend-staging-de
docker network create -d bridge fiesta-frontend-production
docker network create -d bridge fiesta-frontend-production-de

docker network create -d bridge fiesta-maintenance