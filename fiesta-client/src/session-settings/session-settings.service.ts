import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject, Observable, Subject, empty } from 'rxjs';
import { defaultIfEmpty } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class SessionSettingsService {

    settings = new Map<string, Subject<string>>();

    isBrowser = false;

    constructor(
        @Inject(PLATFORM_ID) platformId: any
    ) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    hasSetting(key: string): boolean {
        return this.settings.has(key);
    }

    getSettingObservable(key: string, defaultValue: string): Observable<string> {
        return this.getSettingSubject(key)
            .pipe(
                defaultIfEmpty(defaultValue)
            );
    }

    private getSettingSubject(key: string): Observable<string> {

        if (this.isBrowser) {
            if (this.hasSetting(key)) {
                return this.settings.get(key);
            } else if (sessionStorage.getItem(key) !== null) {
                const value = sessionStorage.getItem(key);
                const newObs = new BehaviorSubject<string>(value);
                this.settings.set(key, newObs);
                return newObs;
            }
        }

        return empty();
    }

    setSettingValue(key: string, value: string) {
        if (this.hasSetting(key)) {
            return this.settings.get(key).next(value);
        } else if (this.isBrowser) {
            sessionStorage.setItem(key, value);
            this.settings.set(key, new BehaviorSubject<string>(value));
        }
    }
}
