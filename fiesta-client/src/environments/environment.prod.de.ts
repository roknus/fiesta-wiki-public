import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'de',
        language: 'de',
        title: 'Fiesta Wiki (EU)',
        flag: 'flag-de'
    },
    production: true,
    debug: false,
    domain: '//de.fiesta-wiki.com/',
    api: '//api-de.fiesta-wiki.com/',
    static_assets: '//static-de.fiesta-wiki.com/'
};
