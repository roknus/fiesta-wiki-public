import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'de',
        language: 'de',
        title: 'Fiesta Wiki (EU)',
        flag: 'flag-de'
    },
    production: false,
    debug: false,
    domain: '//beta-de.fiesta-wiki.com/',
    api: '//api-beta-de.fiesta-wiki.com/',
    static_assets: '//static-beta-de.fiesta-wiki.com/'
};
