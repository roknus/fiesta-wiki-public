import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'na',
        language: 'en',
        title: 'Fiesta Wiki (NA)',
        flag: 'flag-us'
    },
    production: true,
    debug: false,
    domain: 'http://fiesta-wiki.com/',
    api: 'http://api.fiesta-wiki.com/',
    static_assets: 'http://static.fiesta-wiki.com/'
};
