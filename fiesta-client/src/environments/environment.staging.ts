import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'na',
        language: 'en',
        title: 'Fiesta Wiki (NA)',
        flag: 'flag-us'
    },
    production: false,
    debug: false,
    domain: '//beta.fiesta-wiki.com/',
    api: '//api-beta.fiesta-wiki.com/',
    static_assets: '//static-beta.fiesta-wiki.com/'
};
