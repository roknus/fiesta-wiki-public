import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'na',
        language: 'en',
        title: 'Fiesta Wiki (NA)',
        flag: 'flag-us'
    },
    production: false,
    debug: true,
    api: 'http://192.168.1.34:8080/',
    static_assets: 'http://localhost:8081/',
    domain: 'http://192.168.1.34/'
};
