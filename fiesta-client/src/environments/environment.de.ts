import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'de',
        language: 'de',
        title: 'Fiesta Wiki (EU)',
        flag: 'flag-de'
    },
    production: false,
    debug: true,
    api: 'http://localhost:8080/',
    static_assets: 'http://localhost:8081/',
    domain: 'http://192.168.1.34/'
};
