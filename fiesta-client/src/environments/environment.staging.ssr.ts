import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'na',
        language: 'en',
        title: 'Fiesta Wiki (NA)',
        flag: 'flag-us'
    },
    production: false,
    debug: false,
    domain: 'http://beta.fiesta-wiki.com/',
    api: 'http://api-beta.fiesta-wiki.com/',
    static_assets: 'http://static-beta.fiesta-wiki.com/'
};
