import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'de',
        language: 'de',
        title: 'Fiesta Wiki (EU)',
        flag: 'flag-de'
    },
    production: true,
    debug: false,
    domain: 'http://de.fiesta-wiki.com/',
    api: 'http://api-de.fiesta-wiki.com/',
    static_assets: 'http://static-de.fiesta-wiki.com/'
};
