/***************************************************************************************************
 * Load `$localize` onto the global scope - used if i18n tags appear in Angular templates.
 */
import '@angular/localize/init';
import 'zone.js/node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import * as compression from 'compression';
import { join } from 'path';

import { AppServerModule } from './main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
import * as redis from 'redis';

// The Express app is exported so that it can be used by serverless Functions.
export function app() {
    const server = express();
    const distFolder = join(process.cwd(), `${process.env.FOLDER || '/dist/browser'}`);
    const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

    server.use(compression());

    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    server.engine('html', ngExpressEngine({
        bootstrap: AppServerModule,
    }));

    server.set('view engine', 'html');
    server.set('views', distFolder);

    // Example Express Rest API endpoints
    // server.get('/api/**', (req, res) => { });
    // Serve static files from /browser
    server.get('*.*', express.static(distFolder, {
        maxAge: '1y'
    }));

    // Creates a cache key using the request URL
    const cacheKey: (req: express.Request) => string = (req) =>
        `ssr_${req.originalUrl}`;

    // Redis cache client
    const redisClient = redis.createClient(`redis://${process.env.REDIS || 'localhost:6379'}`);

    // All regular routes use the Universal engine
    server.get('*',
        // Middleware to check if cached response exists
        (req, res, next) =>
            redisClient.get(cacheKey(req), (error: Error, reply: string) => {
                if (reply?.length) {
                    // Cache exists. Send the response.
                    res.send(reply);
                } else {
                    // Use the Universal engine to render a response.
                    next();
                }
            }),
        // Angular SSR rendering
        (req, res) => {
            res.render(
                indexHtml,
                {
                    req,
                    providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }],
                },
                (error: Error, html: string) => {
                    if (error) {
                        return req.next(error);
                    }
                    if (res.statusCode === 200) {
                        // Cache the rendered HTML
                        redisClient.set(cacheKey(req), html);
                    }
                    res.send(html);
                }
            );
        }
    );

    return server;
}

function run() {
    const port = process.env.PORT || 4000;

    // Start up the Node server
    const server = app();
    server.listen(port, () => {
        console.log(`Node Express server listening on http://localhost:${port}`);
    });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
    run();
}

export * from './main.server';

export { renderModule, renderModuleFactory } from '@angular/platform-server';
