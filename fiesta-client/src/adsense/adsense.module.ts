import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdsenseComponent } from './components/adsense/adsense.component';

@NgModule({
    declarations: [
        AdsenseComponent
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        AdsenseComponent
    ]
})
export class AdsenseModule { }
