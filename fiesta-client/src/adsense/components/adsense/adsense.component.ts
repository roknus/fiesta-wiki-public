import { isPlatformBrowser } from '@angular/common';
import { AfterViewInit, Component, Inject, PLATFORM_ID } from '@angular/core';
import { environment } from '@environments/environment';

@Component({
    selector: 'app-adsense',
    templateUrl: './adsense.component.html',
    styleUrls: ['./adsense.component.scss']
})
export class AdsenseComponent implements AfterViewInit {

    readonly CLIENT_ID = environment.common.adsense.CaPub;
    readonly AD_SLOT = environment.common.adsense.AdSlot;

    constructor(@Inject(PLATFORM_ID) private platformId: any) { }

    ngAfterViewInit() {
        if (isPlatformBrowser(this.platformId)) {
            setTimeout(() => {
                try {
                    (window["adsbygoogle"] = window["adsbygoogle"] || []).push({});
                } catch (e) {
                    console.error(e);
                }
            }, 2000);
        }
    }
}