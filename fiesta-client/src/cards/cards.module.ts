import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FiestaCommonModule } from '@fiesta-common/fiesta-common.module';
import { CardsPageComponent } from './components/cards-page/cards-page.component';
import { CardDescriptionPageComponent } from './components/card-description-page/card-description-page.component';
import { FlexLayoutModule } from '@angular/flex-layout';

const routes: Routes = [
    { path: ':id', component: CardDescriptionPageComponent },
    { path: '', component: CardsPageComponent },
];

@NgModule({
    declarations: [
        CardDescriptionPageComponent,
        CardsPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FiestaCommonModule,
        FlexLayoutModule,
    ]
})
export class CardsModule { }
