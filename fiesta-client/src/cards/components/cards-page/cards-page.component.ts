import { Component, OnInit } from '@angular/core';
import { LoadingStatus } from '@enums/loading-return';
import { PageDescriptionService } from '@services/page-description.service';
import { CardsService } from '@fiesta-common/services/cards.service';
import { Card } from '@fiesta-common/dto/cards/card';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-cards-page',
    templateUrl: './cards-page.component.html',
    styleUrls: ['./cards-page.component.scss']
})
export class CardsPageComponent implements OnInit {

    readonly pageTitle = PAGES_NAME.collectionCards

    cards: Card[];

    cardsS: Card[];
    cardsA: Card[];
    cardsB: Card[];
    cardsC: Card[];

    loadingErr: LoadingStatus = 'loading';

    constructor(
        protected pageDescriptionService: PageDescriptionService,
        private cardService: CardsService,
    ) { }

    ngOnInit(): void {
        this.pageDescriptionService.setPageDescription({
            title: PAGES_NAME.collectionCards,
            description: 'Cards can be obtained by defeating monster and then added to your collection, some cards gives bonus titles which improves your stats.'
        });

        this.cardService.getCards()
            .subscribe(
                cards => {

                    this.loadingErr = 'ok';

                    this.cards = cards;
                    this.cardsS = cards.filter(c => c.grade_type === 0);
                    this.cardsA = cards.filter(c => c.grade_type === 1);
                    this.cardsB = cards.filter(c => c.grade_type === 2);
                    this.cardsC = cards.filter(c => c.grade_type === 3);
                },
                err => {
                    this.loadingErr = 'error';
                });
    }
}
