import { Component, OnInit } from '@angular/core';
import { Card } from '@fiesta-common/dto/cards/card';
import { CardsService } from '@fiesta-common/services/cards.service';
import { PageDescriptionService } from '@services/page-description.service';
import { ActivatedRoute } from '@angular/router';
import { map, mergeMap, publishLast, refCount, switchMap } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { ROUTES_URL } from '@app/routing/routes';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-card-description-page',
    templateUrl: './card-description-page.component.html',
    styleUrls: ['./card-description-page.component.scss']
})
export class CardDescriptionPageComponent implements OnInit {

    ROUTES = ROUTES_URL;

    readonly pageTitle = PAGES_NAME.collectionCards;

    card: Card;

    get cardRank(): string {
        return this.cardService.cardRank(this.card);
    }


    cardGroup: Observable<any[]>;

    constructor(
        private route: ActivatedRoute,
        private cardService: CardsService,
        protected pageDescriptionService: PageDescriptionService
    ) { }

    ngOnInit() {
        this.pageDescriptionService.setPageDescription({
            title: PAGES_NAME.collectionCards,
            description: 'Cards can be obtained by defeating monster and then added to your collection, some cards gives bonus titles which improves your stats.'
        });

        this.route.paramMap.pipe(
            switchMap(params => {

                let id = 0;

                if (params.has('id')) {
                    id = +params.get('id');
                }

                return this.cardService.getCard(id);
            })
        ).subscribe(
            card => {
                this.card = card;
                this.pageDescriptionService.setTitle(card.view.name, 'Collection Cards');
                this.cardGroup =
                    this.card.group.pipe(
                        mergeMap(group => forkJoin(group.cards).pipe(
                            map(cards => {
                                const total = cards.map(c => c.get_rate)
                                    .reduce((sum, current) => sum + current, 0);

                                return cards
                                    .map(c => ({ rate: c.get_rate / total * 100, card: c }))
                                    .sort((c1, c2) => c2.rate - c1.rate);
                            }),
                            publishLast(),
                            refCount()
                        ))
                    );
            }
        );
    }

}
