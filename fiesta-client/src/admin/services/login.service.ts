import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

export interface Credentials {
   username: string;
   password: string;
}

export interface AuthenticationResponse {
   name: string;
}

export class Authenticated {
   ok: boolean;
}

@Injectable({
   providedIn: 'root'
})
export class LoginService {

   get username(): string {
      const user = sessionStorage.getItem('username');
      if (user === null) {
         return 'Unknown';
      }

      return user;
   }

   constructor(private http: HttpClient) {
   }

   authenticate(credentials: Credentials): Observable<Authenticated> {

      console.log('authenticating : ' + credentials.username);

      const authString = btoa(credentials.username + ':' + credentials.password);

      const headers = new HttpHeaders(credentials ? {
         authorization: 'Basic ' + authString
      } : {});

      return this.http.get<AuthenticationResponse>(environment.api + 'user/login', { headers }).pipe(
         map(response => {
            const auth = new Authenticated();
            auth.ok = response.name ? true : false;

            console.log('auth ' + response.name);

            if (auth.ok) {
               console.log('session start');
               sessionStorage.setItem('username', credentials.username);
               sessionStorage.setItem('basicauth', authString);
            }

            return auth;
         }));
   }

   isLoggedIn() {
      const user = sessionStorage.getItem('username');
      return !(user === null);
   }

   logout() {
      sessionStorage.removeItem('username');
   }
}
