import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
   providedIn: 'root'
})
export class BasicAuthHttpInterceptorService implements HttpInterceptor {

   constructor(private loginService: LoginService) { }

   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      if (this.loginService.isLoggedIn()) {
         req = req.clone({
            setHeaders: {
               Authorization: 'Basic ' + sessionStorage.getItem('basicauth')
            }
         });
      }

      return next.handle(req);
   }
}
