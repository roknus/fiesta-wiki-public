import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ROUTES_URL } from '@app/routing/routes';
import { LoadingStatus } from '@enums/loading-return';
import { environment } from '@environments/environment';
import { LevelRangeFilterComponent } from '@fiesta-common/components/level-range-filter/level-range-filter.component';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { MobSpecies } from '@fiesta-common/dto/mobs/mob-info';
import { MobService } from '@fiesta-common/services/mob.service';
import { map, switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-mobs-page',
    templateUrl: './mobs-page.component.html',
    styleUrls: ['./mobs-page.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0', borderBottomWidth: '0px' })),
            state('expanded', style({ height: '*', borderBottomWidth: '1px' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class MobsPageComponent implements OnInit {

    readonly ROUTES = ROUTES_URL;

    readonly STATIC_ASSETS = environment.static_assets;

    @ViewChild(LevelRangeFilterComponent, { static: true }) levelRangeForm: LevelRangeFilterComponent;

    dataSource: MatTableDataSource<MobSpecies>;
    expandedElement: MobSpecies | null;

    page = new Page<MobSpecies>();

    loadingStatus: LoadingStatus = 'loading';

    displayedColumns: string[] =
        [
            'icon'
            , 'name'
            , 'mobs'
            /*,'level'
            ,'exp'
            ,'location'*/
        ];

    readonly defaultPageSize = 20;
    readonly defaultMin = 0;
    readonly defaultMax = 150;

    pageSizeOptions: number[] = [10, 20, 50, 100];

    mobsForm: FormGroup;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private mobService: MobService) { }

    ngOnInit(): void {

        this.mobsForm = this.formBuilder.group({
            levelRange: this.levelRangeForm.createForm({min: this.defaultMin, max: this.defaultMax}),
            search: ''
        });

        this.route.queryParamMap.pipe(
            switchMap(queryParam => {

                let page = 0;
                let size = this.defaultPageSize;
                let min = this.defaultMin;
                let max = this.defaultMax;
                let search: string;

                if (queryParam.has('page')) {
                    page = +queryParam.get('page');
                }

                if (queryParam.has('size')) {
                    size = +queryParam.get('size');
                }

                if (queryParam.has('search')) {
                    search = queryParam.get('search');
                }
                if (queryParam.has('min')) {
                    min = +queryParam.get('min');
                }
                if (queryParam.has('max')) {
                    max = +queryParam.get('max');
                }

                return this.mobService.getSpeciesPage(
                    {
                        page,
                        size,
                        search,
                        min,
                        max,
                        npc: false,
                        sort: ['level', 'name']
                    }).pipe(
                        map(pageResult => {
                            return {
                                pageResult,
                                min,
                                max
                            };
                        })
                    );
            })
        ).subscribe(
            res => {
                this.mobsForm.setValue({ search: '', levelRange: { min: res.min, max: res.max } });
                this.page = res.pageResult;
                this.dataSource = new MatTableDataSource(res.pageResult.content);
                this.loadingStatus = 'ok';
            },
            err => {
                this.loadingStatus = 'error';
            });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(
            [],
            {
                queryParams:
                {
                    ...this.route.snapshot.queryParams,
                    ...{
                        page: pageEvent.pageIndex !== 0 ? pageEvent.pageIndex : undefined,
                        size: pageEvent.pageSize !== this.defaultPageSize ? pageEvent.pageSize : undefined
                    }
                },
                queryParamsHandling: 'merge'
            });
    }

    onSubmit(value: any) {

        this.router.navigate(
            [],
            {
                queryParams:
                {
                    ...this.route.snapshot.queryParams,
                    ...{
                        min: value.levelRange.min !== this.defaultMin ? value.levelRange.min : undefined,
                        max: value.levelRange.max !== this.defaultMax ? value.levelRange.max : undefined,
                        search: value.search?.length ? value.search : undefined,
                        page: undefined
                    }
                },
                queryParamsHandling: 'merge'
            });
    }
}
