import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobEditionComponent } from './mob-edition.component';

describe('MobEditionComponent', () => {
  let component: MobEditionComponent;
  let fixture: ComponentFixture<MobEditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobEditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
