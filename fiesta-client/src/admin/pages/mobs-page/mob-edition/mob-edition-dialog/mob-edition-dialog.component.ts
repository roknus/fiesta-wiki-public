import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MobInfo } from '@fiesta-common/dto/mobs/mob-info';
import { MobService } from '@fiesta-common/services/mob.service';

@Component({
    selector: 'app-mob-edition-dialog',
    templateUrl: './mob-edition-dialog.component.html',
    styleUrls: ['./mob-edition-dialog.component.scss']
})
export class MobEditionDialogComponent {

    mob: MobInfo;

    mobUpdateForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<MobEditionDialogComponent, MobInfo>,
        private formBuilder: FormBuilder,
        private mobService: MobService,
        @Inject(MAT_DIALOG_DATA) public data: { mob: MobInfo }
    ) {
        this.mob = data.mob;

        this.mobUpdateForm = this.formBuilder.group({
            location: this.mob.location,
            active: this.mob.active
        });
    }

    onSubmit(value: any) {

        let mobCopy: MobInfo = { ...this.mob };
        mobCopy.location = value.location;
        mobCopy.active = value.active;

        this.mobService.updateMobInfo(mobCopy).subscribe(
            result => {
                this.dialogRef.close(result);
            }
        );
    }
}
