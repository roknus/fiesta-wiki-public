import { Component, OnInit } from '@angular/core';
import { LoginService } from 'admin/services/login.service';
import { Router } from '@angular/router';

@Component({
   selector: 'app-admin-page',
   templateUrl: './admin-page.component.html',
   styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {

   constructor(
      private router: Router,
      private loginService: LoginService
      ) { }

   ngOnInit() {
   }


   onLogout() {
      this.loginService.logout();

      this.router.navigate(['/admin/login']);
   }
}
