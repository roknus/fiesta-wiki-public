import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { StoreCategory } from '@fiesta-common/dto/store-items/store-item';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

interface CategoryType {
   value: number;
   name: string;
}

@Component({
   selector: 'app-item-category-edition-dialog',
   templateUrl: './item-category-edition-dialog.component.html',
   styleUrls: ['./item-category-edition-dialog.component.scss']
})
export class ItemCategoryEditionDialogComponent implements OnInit {

   storeCategoryForm: FormGroup;
   nameControl = new FormControl({ value: '' });
   typeControl = new FormControl({ value: 0 });

   categorieTypes: CategoryType[] = [
      { name: 'Time', value: 0},
      { name: 'Stats', value: 1},
      { name: 'Slot', value: 2},
      { name: 'Designer', value: 3},
      { name: 'Bundle', value: 4},
   ];

   category: StoreCategory;

   validationButtonName = 'Create';

   constructor(
      public dialogRef: MatDialogRef<ItemCategoryEditionDialogComponent, StoreCategory>,
      private formBuilder: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: any
   ) {
      this.storeCategoryForm = this.formBuilder.group({
         name: this.nameControl,
         type: this.typeControl
      });

      if (data && data.mode) {
         if (data.mode === 'edit') {
            this.validationButtonName = 'Edit';
         }
      }

      if (data && data.category) {
         this.category = data.category;
      } else {
         this.category = new StoreCategory();
      }

      this.nameControl.setValue(this.category.name);
      this.typeControl.setValue(this.category.type);
   }

   ngOnInit() {
      this.nameControl.valueChanges.subscribe(name => {
         this.category.name = name;
      });
      this.typeControl.valueChanges.subscribe(type => {
         this.category.type = type;
      });
   }

}
