import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl, AbstractControl, Validators } from '@angular/forms';
import { StoreItemView, StoreItemImage } from '@fiesta-common/dto/store-items/store-item';
import { StoreItemAdminService } from 'admin/services/store-item-admin.service';
import { switchMap, map } from 'rxjs/operators';
import { timer } from 'rxjs';

@Component({
   selector: 'app-store-item-view-edition-dialog',
   templateUrl: './store-item-view-edition-dialog.component.html',
   styleUrls: ['./store-item-view-edition-dialog.component.scss']
})
export class StoreItemViewEditionDialogComponent implements OnInit {

   storeItemViewForm: FormGroup;
   nameControl = new FormControl({ value: ''}, [Validators.required] , this.validateName.bind(this) );
   iconControl = new FormControl({ value: '' });
   // imagesControl = new FormControl({ value: {} });
   descriptionControl = new FormControl({ value: '' });

   item: StoreItemView;

   stringList: string[] = [];

   validationButtonName = 'Create';

   constructor(
      public dialogRef: MatDialogRef<StoreItemViewEditionDialogComponent, StoreItemView>,
      private formBuilder: FormBuilder,
      private storeItemAdminService: StoreItemAdminService,
      //private storeItemService: StoreItemService
      @Inject(MAT_DIALOG_DATA) public data: any
   ) {
      this.storeItemViewForm = this.formBuilder.group({
         name: this.nameControl,
         icon: this.iconControl,
         // image: this.imagesControl,
         description: this.descriptionControl
      });

      if (data && data.mode) {
         if (data.mode === 'edit') {
            this.validationButtonName = 'Edit';
         }
      }

      if (data && data.item) {
         this.item =  data.item;
      } else {
         this.item = new StoreItemView();
      }

      this.nameControl.setValue(this.item.name);
      this.iconControl.setValue(this.item.icon);
      // this.imagesControl.setValue(this.item.image_previews);
      this.descriptionControl.setValue(this.item.description);
   }

   ngOnInit() {
      this.nameControl.valueChanges.subscribe(name => {
         this.item.name = name;
      });
      this.iconControl.valueChanges.subscribe(icon => {
         this.item.icon = icon;
      });
     /* this.imagesControl.valueChanges.subscribe(images => {
         this.item.image_previews = images;
      });*/
      this.descriptionControl.valueChanges.subscribe(description => {
         this.item.description = description;
      });
   }

   onAddImage(event: any) {
      this.item.image_previews.push(new StoreItemImage());
   }

   onRemoveImage(image: StoreItemImage) {
      const index = this.item.image_previews.indexOf(image);

      if (index >= 0) {
         this.item.image_previews.splice(index, 1);
      }
   }

   validateName(input: AbstractControl) {
      return timer(500).pipe(
         switchMap(() => this.storeItemAdminService.checkStoreItemView(input.value)),
         map(storeItems => {
            return storeItems.find(i => i.name === input.value) != null ? { itemExist: true } : null;
         })
      );
   }
}
