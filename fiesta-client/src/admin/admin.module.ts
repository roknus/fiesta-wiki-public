import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { LoginService } from './services/login.service';
import { FiestaCommonModule } from '@fiesta-common/fiesta-common.module';
import { AngularMaterialModule } from 'angular-material/angular-material-module.module';
import { StoreItemsPageComponent } from './pages/store-items-page/store-items-page.component';
import { AdminLoginPageComponent } from './pages/admin-login-page/admin-login-page.component';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { AuthGuard } from './guards/auth.guard';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { BrowserModule } from '@angular/platform-browser';
import {
   ItemCategoryEditionDialogComponent
} from './pages/store-items-page/item-category-edition-dialog/item-category-edition-dialog.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { BasicAuthHttpInterceptorService } from './services/basic-auth-http-interceptor.service';
import {
   StoreItemViewEditionDialogComponent
} from './pages/store-items-page/store-item-view-edition-dialog/store-item-view-edition-dialog.component';
import { StoreItemEditionDialogComponent } from './pages/store-items-page/store-item-edition-dialog/store-item-edition-dialog.component';
import {
   StoreItemSetEditionDialogComponent
} from './pages/store-items-page/store-item-set-edition-dialog/store-item-set-edition-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { MobsPageComponent } from './pages/mobs-page/mobs-page.component';
import { MobEditionComponent } from './pages/mobs-page/mob-edition/mob-edition.component';
import { MobEditionDialogComponent } from './pages/mobs-page/mob-edition/mob-edition-dialog/mob-edition-dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule } from '@angular/material/table';


const routes: Routes = [
   { path: 'admin/login', component: AdminLoginPageComponent },
   {
      path: 'admin',
      component: AdminPageComponent,
      canActivate: [AuthGuard],
      children: [
         { path: 'premium', redirectTo: 'premium/0' },
         { path: 'premium/:page', component: StoreItemsPageComponent },
         { path: 'mobs', redirectTo: 'mobs/0' },
         { path: 'mobs/:page', component: MobsPageComponent }
      ]
   }
];

@NgModule({
   declarations: [
      AdminLoginPageComponent,
      AdminPageComponent,
      StoreItemsPageComponent,
      TopBarComponent,
      StoreItemViewEditionDialogComponent,
      StoreItemEditionDialogComponent,
      ItemCategoryEditionDialogComponent,
      StoreItemSetEditionDialogComponent,
      MobsPageComponent,
      MobEditionComponent,
      MobEditionDialogComponent
   ],
   imports: [
      RouterModule.forChild(routes),
      BrowserModule,
      CommonModule,
      AngularMaterialModule,
      MatDialogModule,
      MatAutocompleteModule,
      MatChipsModule,
      MatCheckboxModule,
      MatIconModule,
      MatTableModule,
      FormsModule,
      HttpClientModule,
      ReactiveFormsModule,
      FiestaCommonModule,
      FlexLayoutModule
   ],
   providers: [
      LoginService,
      {
         provide: HTTP_INTERCEPTORS,
         useClass: BasicAuthHttpInterceptorService,
         multi: true
      }
   ],
   entryComponents: [
      StoreItemViewEditionDialogComponent,
      ItemCategoryEditionDialogComponent,
      StoreItemEditionDialogComponent,
      StoreItemSetEditionDialogComponent
   ]
})
export class AdminModule { }
