import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableNoResultsFoundComponent } from './table-no-results-found.component';

describe('TableNoResultsFoundComponent', () => {
  let component: TableNoResultsFoundComponent;
  let fixture: ComponentFixture<TableNoResultsFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableNoResultsFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableNoResultsFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
