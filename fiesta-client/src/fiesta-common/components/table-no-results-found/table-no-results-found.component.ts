import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-no-results-found',
  templateUrl: './table-no-results-found.component.html',
  styleUrls: ['./table-no-results-found.component.scss']
})
export class TableNoResultsFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
