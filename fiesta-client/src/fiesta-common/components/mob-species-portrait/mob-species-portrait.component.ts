import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { MobSpecies } from '@fiesta-common/dto/mobs/mob-info';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-mob-species-portrait',
    templateUrl: './mob-species-portrait.component.html',
    styleUrls: ['./mob-species-portrait.component.scss']
})
export class MobSpeciesPortraitComponent implements OnInit {

    readonly ROUTES = ROUTES_URL;

    @Input()
    species: MobSpecies = null;

    @Input()
    showName = false;

    @HostBinding('style.height.px')
    @Input()
    iconSize = 32;

    constructor() { }

    ngOnInit() {
    }
}
