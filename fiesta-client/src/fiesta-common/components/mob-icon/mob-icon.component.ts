import { Component, OnInit, Input } from '@angular/core';
import { environment } from '@environments/environment';

@Component({
    selector: 'app-mob-icon',
    templateUrl: './mob-icon.component.html',
    styleUrls: ['./mob-icon.component.scss']
})
export class MobIconComponent implements OnInit {

    STATIC_ASSETS = environment.static_assets;

    @Input()
    iconSize = 32;

    @Input()
    mobPortrait: string;

    constructor() { }

    ngOnInit(): void {
    }

}
