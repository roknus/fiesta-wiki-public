import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobIconComponent } from './mob-icon.component';

describe('MobIconComponent', () => {
  let component: MobIconComponent;
  let fixture: ComponentFixture<MobIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MobIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
