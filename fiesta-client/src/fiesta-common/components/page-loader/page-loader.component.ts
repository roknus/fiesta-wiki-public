import { Component, OnInit, Input } from '@angular/core';
import { LoadingStatus } from '@enums/loading-return';

@Component({
    selector: 'app-page-loader',
    templateUrl: './page-loader.component.html',
    styleUrls: ['./page-loader.component.scss']
})
export class PageLoaderComponent implements OnInit {

    @Input()
    status: LoadingStatus = 'loading';

    constructor() { }

    ngOnInit(): void { }
}
