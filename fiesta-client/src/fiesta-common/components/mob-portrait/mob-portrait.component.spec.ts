import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobPortraitComponent } from './mob-portrait.component';

describe('MobPortraitComponent', () => {
  let component: MobPortraitComponent;
  let fixture: ComponentFixture<MobPortraitComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MobPortraitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobPortraitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
