import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { MobInfo } from '@fiesta-common/dto/mobs/mob-info';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-mob-portrait',
    templateUrl: './mob-portrait.component.html',
    styleUrls: ['./mob-portrait.component.scss']
})
export class MobPortraitComponent implements OnInit {

    readonly ROUTES = ROUTES_URL;

    @Input()
    mob: MobInfo = null;

    @Input()
    showName = false;

    @Input()
    showLevel = false;

    @Input()
    showLocation = false;

    @HostBinding('style.height.px')
    @Input()
    iconSize = 32;

    constructor() { }

    ngOnInit() {
    }
}
