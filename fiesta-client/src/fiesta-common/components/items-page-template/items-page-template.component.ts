import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Item } from '@fiesta-common/dto/items/item';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { ItemsTableComponent } from '../items-table/items-table.component';


export interface ItemFilterParams {
    class?: string;
    rarity?: number[];
    search?: string;
    min?: number;
    max?: number;
}

@Component({
    selector: 'app-items-page-template',
    template: '',
    styles: ['']
})
export abstract class ItemsPageTemplateComponent {

    page$: Observable<Page<Item>>;

    enhancement = 0;

    @ViewChild(ItemsTableComponent)
    itemTable: ItemsTableComponent;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute
    ) { }

    onEnhancementChange(e: number) {
        this.enhancement = e;
    }

    filter(data: ItemFilterParams) {

        // ../0 to reset the page
        this.router.navigate(['../0'], { queryParams: data, relativeTo: this.route });
    }
}
