import { Component, OnInit, Input, HostListener } from '@angular/core';
import { environment } from '@environments/environment';
import { Card } from '@fiesta-common/dto/cards/card';
import { TooltipService } from 'tooltip/tooltip.service';
import { TooltipComponent } from 'tooltip/tooltip/tooltip.component';
import { ComponentWithTooltipComponent } from '@fiesta-common/components/component-with-tooltip/component-with-tooltip.component';
import { CardTooltipComponent } from './card-tooltip/card-tooltip.component';

@Component({
   selector: 'app-collection-card',
   templateUrl: './collection-card.component.html',
   styleUrls: ['./collection-card.component.scss']
})
export class CollectionCardComponent extends ComponentWithTooltipComponent implements OnInit {

   @Input()
   card: Card;

   @Input()
   width = 80;

   @Input()
   minImage = true;

   @Input()
   hasTitle = false;

   get cardPath(): string {
      let path = environment.static_assets + 'cards/';

      path += this.minImage ? 'min/' : 'max/';

      return path;
   }

   get cardDecoration(): string {
      switch (this.card.grade_type) {
         case 0:
            return 'card_s.png';
         case 1:
            return 'card_a.png';
         case 2:
            return 'card_b.png';
         case 3:
            return 'card_c.png';
         default:
            return 'card_c.png';
      }
   }

   get cardBack(): string {
      switch (this.card.grade_type) {
         case 0:
            return 'card_s_back.png';
         case 1:
            return 'card_a_back.png';
         case 2:
            return 'card_b_back.png';
         case 3:
            return 'card_c_back.png';
         default:
            return 'card_c_back.png';
      }
   }

   tooltip: TooltipComponent<CardTooltipComponent>;

   @HostListener('mouseenter', ['$event'])
   mouseEnter(event): void {
      if (!this.minImage) {
         return;
      }

      if (!this.tooltip) {
         this.tooltipRef = this.tooltipService.appendComponentToBody(CardTooltipComponent, event.target);
         this.tooltip = this.tooltipRef.instance;
         this.tooltip.view.subscribe(comp => {
            if (comp) {
               comp.card = this.card;
            }
         });
      }

      this.tooltip.fadeIn();
   }

   @HostListener('mouseleave', ['$event'])
   mouseLeave(event): void {
      if (this.tooltip) {
         this.tooltip.fadeOut(event);
      }
   }

   constructor(
      private tooltipService: TooltipService
   ) { super(); }

   ngOnInit() {
   }
}
