import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MapInfo } from '@fiesta-common/dto/maps/map-info';
import { MapService } from '@fiesta-common/services/map.service';

@Component({
    selector: 'app-map-picker',
    templateUrl: './map-picker.component.html',
    styleUrls: ['./map-picker.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MapPickerComponent),
            multi: true
        }
    ]
})
export class MapPickerComponent implements ControlValueAccessor, OnInit 
{
    @Input()
    map: number;

    @Output()
    mapChange: EventEmitter<number> = new EventEmitter();

    maps: MapInfo[] = null;

    mapLoaded = false;

    constructor(
        private mapService: MapService
    ) { }

    writeValue(obj: any): void {
        this.map = obj;
    }
    registerOnChange(fn: any): void {
        this.mapChange.subscribe(fn);
    }
    registerOnTouched(fn: any): void {}
    setDisabledState?(isDisabled: boolean): void {}

    ngOnInit(): void 
    {
        this.mapService.getAllMapInfo().subscribe(
            maps => {
                this.maps = maps;
                this.mapLoaded = true;
            }
        )
    }
}
