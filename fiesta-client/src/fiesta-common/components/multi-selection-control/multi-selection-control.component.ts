import {
    Component,
    OnInit,
    ContentChildren,
    QueryList,
    ViewContainerRef,
    ViewChild,
    TemplateRef,
    Input,
    Output,
    EventEmitter,
    Inject,
    PLATFORM_ID,
    HostListener
} from '@angular/core';
import { MultiSelectionOptionComponent } from './multi-selection-option/multi-selection-option.component';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatSelectChange } from '@angular/material/select';
import { isPlatformBrowser } from '@angular/common';

@Component({
    selector: 'app-multi-selection',
    templateUrl: './multi-selection-control.component.html',
    styleUrls: ['./multi-selection-control.component.scss']
})
export class MultiSelectionControlComponent implements OnInit {

    @ViewChild('desktopView', { read: TemplateRef, static: true })
    desktopView: TemplateRef<any>;

    @ViewChild('mobileView', { read: TemplateRef, static: true })
    mobileView: TemplateRef<any>;

    @ViewChild('vc', { read: ViewContainerRef, static: true })
    vc: ViewContainerRef;

    @ContentChildren(MultiSelectionOptionComponent)
    options: QueryList<MultiSelectionOptionComponent>;

    @Input()
    title: string;

    @Input()
    value: any;

    @Output()
    valueChange: EventEmitter<any> = new EventEmitter<any>();

    get multiple() { return Array.isArray(this.value); }

    itemChecked(value: any): boolean {
        if(this.multiple)
        {
            return this.value.indexOf(value) >= 0;
        }
        else
        {
            return this.value == value;
        }
    }

    private _isMobileResolution = false;
    get isMobileResolution(): boolean { return this._isMobileResolution; }

    set isMobileResolution(isMobile: boolean) {
        if (this._isMobileResolution != isMobile) {
            this._isMobileResolution = isMobile;
            this.create();
        }
    }

    isBrowser = false;
    @HostListener('window:resize')
    onResize() {
        if (this.isBrowser) {
            if (window.innerWidth < 960) {
                this.isMobileResolution = true;
            } else {
                this.isMobileResolution = false;
            }
        }
    }

    constructor(
        @Inject(PLATFORM_ID) platformId: any
    ) {
        this.isBrowser = isPlatformBrowser(platformId);

        if (this.isBrowser) {
            if (window.innerWidth < 960) {
                this._isMobileResolution = true;
            } else {
                this._isMobileResolution = false;
            }
        }
    }

    ngOnInit() {
        this.create();
    }

    create() {
        this.vc.remove();

        if (this.isMobileResolution) {
            this.vc.createEmbeddedView(this.mobileView);
        } else {
            this.vc.createEmbeddedView(this.desktopView);
        }
    }

    onSelectChange(change: MatSelectChange) {
        this.value = change.value;
    }

    mobileDropdownChanged(change: boolean) {
        if (!change) {
            this.valueChange.emit(this.value);
        }
    }

    onCheckChange(change: MatCheckboxChange) {
        if (this.multiple) {
            if (change.checked) {
                this.value.push(change.source.value);
            } else {
                const foundIndex = this.value.indexOf(change.source.value);
                if (foundIndex > -1) {
                    this.value.splice(foundIndex, 1);
                }
            }
        }
        else {
            this.value = change.source.value;
        }
        this.valueChange.emit(this.value);
    }
}
