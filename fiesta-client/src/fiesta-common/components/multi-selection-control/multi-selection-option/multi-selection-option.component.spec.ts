import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MultiSelectionOptionComponent } from './multi-selection-option.component';

describe('MultiSelectionOptionComponent', () => {
  let component: MultiSelectionOptionComponent;
  let fixture: ComponentFixture<MultiSelectionOptionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectionOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectionOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
