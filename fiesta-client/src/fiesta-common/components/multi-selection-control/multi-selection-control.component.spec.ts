import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MultiSelectionControlComponent } from './multi-selection-control.component';

describe('MultiSelectionControlComponent', () => {
  let component: MultiSelectionControlComponent;
  let fixture: ComponentFixture<MultiSelectionControlComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectionControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectionControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
