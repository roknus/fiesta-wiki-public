import { Component, OnInit, Input } from '@angular/core';

@Component({
   selector: 'app-money',
   templateUrl: './money.component.html',
   styleUrls: ['./money.component.scss']
})
export class MoneyComponent implements OnInit {

   @Input()
   money = 0;

   get copper() {
      return this.money % 1000;
   }

   get silver() {
      return Math.trunc(this.money / 1000) % 1000;
   }

   get gold() {
      return Math.trunc(this.money / 1000000) % 100;
   }

   get gem() {
      return Math.trunc(this.money / 100000000);
   }

   constructor() { }

   ngOnInit() {
   }

}
