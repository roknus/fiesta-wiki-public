import { Component, HostBinding, Input } from '@angular/core';
import { ComponentWithTooltipComponent } from '@fiesta-common/components/component-with-tooltip/component-with-tooltip.component';

@Component({
    template: '',
    selector: 'app-icon'
})
export class IconComponent extends ComponentWithTooltipComponent {

    @HostBinding('style.width.px')
    @Input()
    width = 34;

    @HostBinding('style.height.px')
    @Input()
    height = 34;

    constructor() { super(); }

}
