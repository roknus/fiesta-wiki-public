import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AbStateIconComponent } from './ab-state-icon.component';

describe('AbStateIconComponent', () => {
  let component: AbStateIconComponent;
  let fixture: ComponentFixture<AbStateIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AbStateIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbStateIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
