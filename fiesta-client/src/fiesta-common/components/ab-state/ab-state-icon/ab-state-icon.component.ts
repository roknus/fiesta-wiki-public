import { Component, OnInit, Input } from '@angular/core';
import { environment } from '@environments/environment';
import { AbState } from '@fiesta-common/dto/skills/ab-state';

@Component({
   selector: 'app-ab-state-icon',
   templateUrl: './ab-state-icon.component.html',
   styleUrls: ['./ab-state-icon.component.scss']
})
export class AbStateIconComponent implements OnInit {

   STATIC_ASSETS = environment.static_assets + 'icons/';

   @Input()
   abState: AbState;

   constructor() {
   }

   ngOnInit() {
   }

}
