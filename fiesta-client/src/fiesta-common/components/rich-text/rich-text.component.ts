import { Component, ComponentFactoryResolver, Input, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ItemService } from '@fiesta-common/services/item.service';
import { Parser } from 'simple-text-parser';

@Component({
    selector: 'app-rich-text',
    templateUrl: './rich-text.component.html',
    styleUrls: ['./rich-text.component.scss']
})
export class RichTextComponent implements OnInit {

    private _text: string;
    @Input()
    set text(text: string) {
        this._text = text;
        this.updateContent();
    }
    get text(): string { return this._text; }

    @ViewChild('item', { static: true }) itemTpl: TemplateRef<any>;
    @ViewChild('text', { static: true }) textTpl: TemplateRef<any>;
    @ViewChild('p', { static: true }) pTpl: TemplateRef<any>;

    constructor(
        private itemService: ItemService,
        private vcr: ViewContainerRef,
        private componentFactoryResolver: ComponentFactoryResolver
    ) { }

    ngOnInit(): void {
        this.updateContent();
    }

    private clear() {
        this.vcr.clear();
    }

    private updateContent() {
        this.clear();

        const parser = new Parser();

        parser.addRule(/<p>(.*?)<\/p>/gi, function (capture, content) {

            // return a node describing this tag
            return { type: "p", content: content, text: capture };
          });

        parser.addRule(/\${ITEM:([\d]+)}/gi, (capture, itemID) => {

            // return a node describing this tag
            return { type: 'item', itemID: +itemID, text: capture };
        });

        const nodes = parser.toTree(this.text);

        nodes.forEach(n => {
            switch (n.type) {
                case 'item':
                    {
                        const item$ = this.itemService.getItem(n.itemID as number);
                        this.vcr.createEmbeddedView(this.itemTpl, { $implicit: item$ });
                        break;
                    }
                case 'p':
                    {
                        this.vcr.createEmbeddedView(this.pTpl, { $implicit: n.content });
                        break;
                    }
                default:
                    this.vcr.createEmbeddedView(this.textTpl, { $implicit: n.text });
                    break;
            }
        });
    }


}
