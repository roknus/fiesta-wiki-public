import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Item } from '@fiesta-common/dto/items/item';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

    @Input()
    item: Item;

    @Input()
    enhancement = 0;

    @HostBinding('style.height.px')
    @Input()
    size = 34;

    constructor() { }

    ngOnInit(): void {
    }
}
