import { Component, OnInit, Input } from '@angular/core';
import { FiestaSettingsService } from '@fiesta-common/services/fiesta-settings.service';
import { Item } from '@fiesta-common/dto/items/item';
import { environment } from '@environments/environment';

@Component({
   selector: 'app-item-tooltip',
   templateUrl: './item-tooltip.component.html',
   styleUrls: ['./item-tooltip.component.scss']
})
export class ItemTooltipComponent implements OnInit {
   STATIC_ASSETS = environment.static_assets;

   @Input()
   item: Item;

   @Input()
   enhancement = 0;

   showMaxStats = false;

   constructor(private fiestaSettings: FiestaSettingsService) { }

   ngOnInit() {
      this.fiestaSettings.showRandomStats.subscribe(value => this.showMaxStats = value);
   }
}
