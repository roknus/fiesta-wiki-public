import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-rarity-combobox',
    templateUrl: './rarity-combobox.component.html',
    styleUrls: ['./rarity-combobox.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RarityComboboxComponent),
            multi: true
        }
    ],
    encapsulation: ViewEncapsulation.None
})
export class RarityComboboxComponent implements ControlValueAccessor, OnInit {

    @Input()
    value: number[] = [];

    @Output()
    valueChanged = new EventEmitter<number[]>();

    constructor() { }

    ngOnInit() { }

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: any): void {
        this.valueChanged.subscribe(fn);
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState?(isDisabled: boolean): void {
    }

    onRarityChanged(value: number[]) {
        this.value = value;
        this.valueChanged.emit(value);
    }
}
