import { Component, Input } from '@angular/core';
import { environment } from '@environments/environment';

@Component({
    selector: 'app-mob-class-icon',
    templateUrl: './mob-class-icon.component.html',
    styleUrls: ['./mob-class-icon.component.scss']
})
export class MobClassIconComponent {

    iconSize = 16;

    STATIC_ASSETS = environment.static_assets;

    @Input()
    name: string;

    constructor() { }

}
