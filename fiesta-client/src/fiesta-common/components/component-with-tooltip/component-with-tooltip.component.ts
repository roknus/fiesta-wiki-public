import { Component, OnDestroy, ComponentRef } from '@angular/core';

@Component({
   selector: 'app-component-with-tooltip',
   template: ''
})
export class ComponentWithTooltipComponent implements OnDestroy {

   tooltipRef: ComponentRef<any>;

   constructor() { }

   ngOnDestroy(): void {
      if (this.tooltipRef) {
         this.tooltipRef.destroy();
      }
   }
}
