import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LevelRangeFilterComponent } from './level-range-filter.component';

describe('LevelRangeFilterComponent', () => {
  let component: LevelRangeFilterComponent;
  let fixture: ComponentFixture<LevelRangeFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LevelRangeFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelRangeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
