import { Component, OnInit, Input, EventEmitter, Output, forwardRef } from '@angular/core';
import { Validators, FormBuilder, ControlValueAccessor, NG_VALUE_ACCESSOR, FormGroup } from '@angular/forms';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';

@Component({
    selector: 'app-level-range-filter',
    templateUrl: './level-range-filter.component.html',
    styleUrls: ['./level-range-filter.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => LevelRangeFilterComponent),
            multi: true
        }
    ]
})
export class LevelRangeFilterComponent implements ControlValueAccessor, OnInit {

    @Input()
    set minLevel(value: number) {
        this.formGroup.patchValue({ min: value });
    }
    @Output()
    minLevelChanged: EventEmitter<number> = new EventEmitter();

    @Input()
    set maxLevel(value: number) {
        this.formGroup.patchValue({ max: value });
    }
    @Output()
    maxLevelChanged: EventEmitter<number> = new EventEmitter();

    formGroup: FormGroup;

    constructor(private formBuilder: FormBuilder) {
    }

    createForm(defaultValue: { min?: number, max?: number } = { min: 0, max: fiestaMaxLevel }): FormGroup {
        this.formGroup = this.formBuilder.group({
            min: [defaultValue.min ? defaultValue.min : 0, [
                Validators.min(0),
                Validators.max(fiestaMaxLevel),
                Validators.pattern('[0-9]*')
            ]],
            max: [defaultValue.max ? defaultValue.max : fiestaMaxLevel, [
                Validators.min(0),
                Validators.max(fiestaMaxLevel),
                Validators.pattern('[0-9]*')
            ]]
        });

        return this.formGroup;
    }

    writeValue(obj: any): void {
        this.formGroup.setValue(obj);
    }

    registerOnChange(fn: any): void {
        this.formGroup.valueChanges.subscribe(fn);
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState?(isDisabled: boolean): void {

    }

    ngOnInit(): void {
        this.formGroup.controls.min.valueChanges.subscribe(val => {
            this.minLevelChanged.emit(val);
        });
        this.formGroup.controls.max.valueChanges.subscribe(val => {
            this.maxLevelChanged.emit(val);
        });
    }
}
