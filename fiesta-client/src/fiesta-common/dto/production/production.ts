import { Item, ItemDTO } from '@fiesta-common/dto/items/item';

export interface ProductionDTO {
   id: number;
   recipe: ItemDTO;
   name: string;
   product: ItemDTO;
   quantity: number;
   ingredient1: ItemDTO;
   quantity1: number;
   ingredient2: ItemDTO;
   quantity2: number;
   ingredient3: ItemDTO;
   quantity3: number;
   ingredient4: ItemDTO;
   quantity4: number;
   mastery_type: number;
   mastery_gain: number;
   needed_mastery: number;
}

export class Production 
{
   id: number;
   recipe: Item;
   name: string;
   product: Item;
   quantity: number;
   ingredient1: Item;
   quantity1: number;
   ingredient2: Item;
   quantity2: number;
   ingredient3: Item;
   quantity3: number;
   ingredient4: Item;
   quantity4: number;
   mastery_type: number;
   mastery_gain: number;
   needed_mastery: number;
}
