export interface SkillView {
   id: number;
   icon_index: number;
   icon_file: string;
   red: number;
   green: number;
   blue: number;
   description: string;
   required_level: number;
}
