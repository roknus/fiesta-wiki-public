import { Observable } from 'rxjs';
import { ActiveSkill } from './active-skill';

export interface SkillGroupDTO {
    id: number;
    skills: number[];
}

export class SkillGroup {
    id: number;
    skills: Observable<ActiveSkill[]>;
}
