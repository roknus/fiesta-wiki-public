import { SkillView } from './skill-view';
import { AbState } from './ab-state';

export interface ActiveSkillView extends SkillView {
    function: string;
}

export interface SkillEmpowerment {
    power: number[];
    sp: number[];
    duration: number[];
    cooldown: number[];
}

export class SkillEmpowerement {
    power = -1;
    sp = -1;
    duration = -1;
    cooldown = -1;

    reset() {
        this.power = -1;
        this.sp = -1;
        this.duration = -1;
        this.cooldown = -1;
    }
}

export interface ActiveSkillEffect {
    effect_id: number;
    effect_value: number;
}

// tslint:disable: variable-name
export class ActiveSkill {
    id: number;
    inxname: string;
    name: string;
    view: ActiveSkillView;
    step: number;
    max_step: number;
    required_weapon: number;
    sp: number;
    sp_rate: number;
    hp: number;
    hp_rate: number;
    lp: number;
    range: number;
    casted_on: number;
    affected_target: number;
    cast_time: number;
    cooldown: number;
    min_dmg: number;
    max_dmg: number;
    max_dmg_rate: number;
    min_mdmg: number;
    max_mdmg: number;
    max_mdmg_rate: number;
    use_class: number;
    ab_states: AbState[];
    empowerment: SkillEmpowerment;
    skill_type: number;
    effects: ActiveSkillEffect[];
    soul_required: number;
    group_name: string;
    groups_id: number[];

    // Custom data
    skill_empowerment: SkillEmpowerement;

    get healingEffect(): ActiveSkillEffect {
        return this.effects.find(e => e.effect_id === 1);
    }

    get hasHealingEffect(): boolean {
        return this.healingEffect !== undefined;
    }

    static clone(other: ActiveSkill) {
        const _ = new ActiveSkill();

        _.id = other.id;
        _.inxname = other.inxname;
        _.name = other.name;
        _.view = other.view;
        _.step = other.step;
        _.max_step = other.max_step;
        _.required_weapon = other.required_weapon;
        _.sp = other.sp;
        _.sp_rate = other.sp_rate;
        _.hp = other.hp;
        _.hp_rate = other.hp_rate;
        _.lp = other.lp;
        _.range = other.range;
        _.casted_on = other.casted_on;
        _.affected_target = other.affected_target;
        _.cast_time = other.cast_time;
        _.cooldown = other.cooldown;
        _.min_dmg = other.min_dmg;
        _.max_dmg = other.max_dmg;
        _.max_dmg_rate = other.max_dmg_rate;
        _.min_mdmg = other.min_mdmg;
        _.max_mdmg = other.max_mdmg;
        _.max_mdmg_rate = other.max_mdmg_rate;
        _.use_class = other.use_class;
        _.ab_states = [...other.ab_states];
        _.empowerment = other.empowerment;
        _.skill_type = other.skill_type;
        _.effects = [...other.effects];
        _.soul_required = other.soul_required;
        _.group_name = other.group_name;
        _.groups_id = [...other.groups_id];

        return _;
    }
}
// tslint:enable: variable-name
