import { SkillView } from './skill-view';
import { PassiveAbState } from './ab-state';

export interface PassiveSkillView extends SkillView {
   required_class: number;
}

export interface PassiveSkill {
   id: number;
   inxname: string;
   name: string;
   view: PassiveSkillView;
   passive_ab_state: PassiveAbState;
   mastery_sword_dmg_rate: number;
   mastery_hammer_dmg_rate: number;
   mastery_two_hand_dmg_rate: number;
   mastery_axe_dmg_rate: number;
   mastery_mace_dmg_rate: number;
   mastery_bow_dmg_rate: number;
   mastery_crossbow_dmg_rate: number;
   mastery_wand_dmg_rate: number;
   mastery_staff_dmg_rate: number;
   mastery_claw_dmg_rate: number;
   mastery_dsword_dmg_rate: number;
   mastery_sword_dmg: number;
   mastery_hammer_dmg: number;
   mastery_two_hand_dmg: number;
   mastery_axe_dmg: number;
   mastery_mace_dmg: number;
   mastery_bow_dmg: number;
   mastery_crossbow_dmg: number;
   mastery_wand_dmg: number;
   mastery_staff_dmg: number;
   mastery_claw_dmg: number;
   mastery_dsword_dmg: number;
   evasion: number;
   sp_increase: number;
   lp_increase: number;
   phy_atk_up_rate: number;
   mag_atk_up_rate: number;
   dmg_up_per_hp_down: number;
   dmg_up_per_hp_down_ratio: number;
   def_up_per_hp_down: number;
   def_up_per_hp_down_ratio: number;
   movement_increase_evasion_up: number;
   heal_up_rate: number;
   buff_duration_up_rate: number;
   crit_dmg_up_rate: number;
   active_skill_group_index: number;
   dmg_receive_minus_rate: number;
   group_name: string;
}
