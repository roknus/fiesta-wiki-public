export interface MapInfo {
   id: number;
   map_name: string;
   name: string;
}
