import { Title } from '@dto/titles/title';
import { MobSpecies } from '@fiesta-common/dto/mobs/mob-info';
import { Observable } from 'rxjs';

export interface CardView {
   filename: string;
   name: string;
   description: string;
   mob_inx: string;
   season: number;
}

export interface CardGroupDTO {
   cards: number[];
}

export class CardGroup {
   cards: Observable<Card>[];
}

export interface CardDTO {
   id: number;
   itemInx: string;
   grade_type: number;
   mob_species: number[];
   get_rate: number;
   view: CardView;
   title: Title;
   card_group: number;
}

export class Card {
   id: number;
   itemInx: string;
   grade_type: number;
   mob_species: Observable<MobSpecies>[];
   get_rate: number;
   view: CardView;
   title: Title;
   group: Observable<CardGroup>;
}

export interface CardMinimal {
   id: number;
   itemInx: string;
   grade_type: number;
   view: CardView;
}

