import { Observable } from 'rxjs';
import { Item } from '../items/item';

export interface MobDropInfoDTO {
    drop_rate: number;
    item_drops: number[];
}

export class MobDropInfo {
    drop_rate: number;
    item_drops: Observable<Item>[] = [];
}