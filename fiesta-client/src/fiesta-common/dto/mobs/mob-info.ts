import { Observable } from 'rxjs';
import { MapInfo } from '../maps/map-info';

export interface MobViewInfoDTO {
    inxname: string;
    filename: string;
    attack_type: number;
    mob_portrait: string;
    minimap_icon: number;
    npc_view_id: number;
}
export class MobViewInfo {
    inxname: string;
    filename: string;
    attack_type: number;
    mob_portrait: string;
    minimap_icon: number;
    npc_view_id: number;
}

export interface MobCoordinateDTO {
    id: number;
    map_id: number;
    center_x: number;
    center_y: number;
    width: number;
    height: number;
    range_degree: number;
}

export class MobCoordinate {
    id: number;
    map: Observable<MapInfo>;
    center_x: number;
    center_y: number;
    width: number;
    height: number;
    range_degree: number;
}

export interface MobSpeciesDTO {
    id: number;
    name: string;
    mob_portrait: string;
    mobs: number[];
}

export class MobSpecies {
    id: number;
    name: string;
    mob_portrait: string;
    mobs: Observable<MobInfo>[];
}

export class MobInfoDTO {
    id: number;
    name: string;
    view: MobViewInfoDTO;
    level: number;
    max_hp: number;
    walk_speed: number;
    run_speed: number;
    npc: boolean;
    weapon_type: number;
    armor_type: number;
    grade_type: number;
    type: number;
    location: number;
    active: boolean;
    coordinates: MobCoordinateDTO[];
    related_quests: number[];
    species: number;

    defense: number;
    evasion: number;
    magical_defense: number;
    aggro_type: number;
    aggro_radius: number;
    aggro_range: number;
    exp: number;
    STR: number;
    DEX: number;
    END: number;
    INT: number;
    SPR: number;
    blooding_resistance: number;
    stun_resistance: number;
    move_speed_resistance: number;
    fear_resistance: number;
}
export class MobInfo {
    id: number;
    name: string;
    view: MobViewInfo;
    level: number;
    max_hp: number;
    walk_speed: number;
    run_speed: number;
    npc: boolean;
    weapon_type: number;
    armor_type: number;
    grade_type: number;
    type: number;
    location: number;
    active: boolean;
    coordinates: MobCoordinate[];
    related_quests: number[];
    species: number;

    defense: number;
    evasion: number;
    magical_defense: number;
    aggro_type: number;
    aggro_radius: number;
    aggro_range: number;
    exp: number;
    STR: number;
    DEX: number;
    END: number;
    INT: number;
    SPR: number;
    blooding_resistance: number;
    stun_resistance: number;
    move_speed_resistance: number;
    fear_resistance: number;
}
