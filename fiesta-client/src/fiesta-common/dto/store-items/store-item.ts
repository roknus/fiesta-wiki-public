import { Item, ItemDTO } from '@fiesta-common/dto/items/item';

export interface StoreItemDTO {
   id: number;
   item: ItemDTO;
   sc: number;
   duration: number;
   link: string;
   categories: StoreCategoryDTO[];
   set: StoreItemSetDTO;
}

// tslint:disable
export class StoreItem {
   id: number;
   item: Item;
   sc: number = 0;
   duration: number = 0;
   link: string = null;
   categories: StoreCategory[] = [];
   set: StoreItemSet = null;
}

export interface StoreItemSetDTO {
   id: number;
   name: string;
}
export class StoreItemSet {
   id: number;
   name: string;
}

export interface StoreCategoryDTO {
   id: number;
   name: string;
   type: number;
}

export class StoreCategory {
   id: number;
   name: string;
   type: number;
}

export interface StoreItemImageDTO {
   id: number;
   filename: string;
}
export class StoreItemImage {
   id: number;
   filename: string;
}

export interface StoreItemViewDTO {
   id: number;
   name: string;
   icon: string;
   image_previews: StoreItemImageDTO[];
   store_items: StoreItemDTO[];
   description: string;
}

export class StoreItemView {
   id: number;
   name: string;
   icon: string;
   image_previews: StoreItemImage[] = [];
   store_items: StoreItem[];
   description: string;
}
// tslint:enable
