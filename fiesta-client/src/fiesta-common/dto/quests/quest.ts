import { MobInfo } from '../mobs/mob-info';
import { Item } from '../items/item';
import { Observable } from 'rxjs';

export interface QuestLineDTO {
    id: number;
    name: string;
    next_quests: QuestLineDTO[];
    previous_quests: QuestLineDTO[];
}

export class QuestLine {
    quest: Observable<Quest>;
    next_quests: QuestLine[] = [];
    previous_quests: QuestLine[] = [];

    hasQuestLine(): boolean {
        return this.next_quests.length > 0 || this.previous_quests.length > 0;
    }

    previousQuest(): QuestLine {
        return this.previous_quests.length > 0 ? this.previous_quests[this.previous_quests.length - 1] : null;
    }
}

export interface QuestItemDropInfoDTO {
    mob_id: number;
    percent: number;
    count_min: number;
    count_max: number;
}

export class QuestItemDropInfo {
    mob: Observable<MobInfo>;
    percent: number;
    count_min: number;
    count_max: number;
}

export interface QuestItemGatheringDTO {
    item_id: number;
    count: number;
    drop_info: QuestItemDropInfoDTO[];
}

export class QuestItemGathering {
    item: Observable<Item>;
    count: number;
    drop_info: QuestItemDropInfo[];
}

export interface ItemRewardDTO {
    selectable: number;
    quantity: number;
    item_id: number;
}

export class ItemReward {
    selectable: number;
    quantity: number;
    item: Observable<Item>;
}

export interface QuestMobKillDTO {
    kill_count: number;
    mob_id: number;
}

export class QuestMobKill {
    kill_count: number;
    mob: Observable<MobInfo>;
}

export interface QuestDataDTO {
    id: number;
    quest_type: number;
    title: string;
    description: string;
    type: number;
    min_level: number;
    max_level: number;
    starting_npc_id: number;
    end_npc_id: number;
    starting_item_id: number;
    starting_item_quantity: number;
    exp: number;
    money: number;
    fame: number;
    item_rewards: ItemRewardDTO[];
    selectable_item_rewards: ItemRewardDTO[];
    start_class: number;
    previous_quest: QuestDTO;
    next_quests: QuestDTO[];
    mob_kill: QuestMobKillDTO[];
    item_gathering: QuestItemGatheringDTO[];
    repeat: boolean;
    remote_turn_in: boolean;
    remote_pick_up: boolean;
}

export class QuestData {
    id: number;
    quest_type: number;
    title: string;
    description: string;
    type: number;
    min_level: number;
    max_level: number;
    starting_npc: Observable<MobInfo>;
    end_npc: Observable<MobInfo>;
    starting_item: Observable<Item>;
    starting_item_quantity: number;
    exp: number;
    money: number;
    fame: number;
    item_rewards: ItemReward[];
    selectable_item_rewards: ItemReward[];
    start_class: number;
    previous_quest: Quest;
    next_quests: Quest[];
    mob_kill: QuestMobKill[];
    item_gathering: QuestItemGathering[];
    repeat: boolean;
    remote_turn_in: boolean;
    remote_pick_up: boolean;
}

export interface QuestDTO {
    id: number;
    title: string;
    quest_type: number;
    repeat: boolean;
    remote_turn_in: boolean;
    remote_pick_up: boolean;
    start_class: number;
    min_level: number;
    max_level: number;
    exp: number;
    money: number;
    item_rewards: ItemRewardDTO[];
    selectable_item_rewards: ItemRewardDTO[];
}

export class Quest {
    id: number;
    title: string;
    quest_type: number;
    repeat: boolean;
    remote_turn_in: boolean;
    remote_pick_up: boolean;
    start_class: number;
    min_level: number;
    max_level: number;
    exp: number;
    money: number;
    item_rewards: ItemReward[];
    selectable_item_rewards: ItemReward[];
    data: Observable<QuestData>;
}

export interface QuestLevelInfoDTO {
    level: number;
    xp_required: number;
    total_xp: number;
    quests: number[];
}

export class QuestLevelInfo {
    level: number;
    xp_required: number;
    total_xp: number;
    quests: Quest[];
}
