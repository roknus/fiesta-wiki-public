import { ItemStats } from './item-stats';
import { ItemSet } from './set-effect';
import { Observable } from 'rxjs';

export interface RandomStats {
   STR: number;
   DEX: number;
   END: number;
   INT: number;
   SPR: number;
   aim: number;
   dmg: number;
   def: number;
   mdmg: number;
   mdef: number;
   eva: number;
   crit: number;
   minus_level: number;
   HP: number;
}

export interface ItemInfoServer {
   random_stats: RandomStats;
}

export interface ItemViewDTO {
   id: number;
   icon_index: number;
   icon_file: string;
   sub_icon_index: number;
   sub_icon_file: string;
   period_icon_index: number;
   period_icon_file: string;
   border_color: string;
   background_color: string;
   description: string;
}

export class ItemView {
   id: number;
   icon_index: number;
   icon_file: string;
   sub_icon_index: number;
   sub_icon_file: string;
   period_icon_index: number;
   period_icon_file: string;
   border_color: string;
   background_color: string;
   description: string;
}

export interface ItemUpgradeInfo {
   stat_upgrade: number;
   upgrades: number[];
}

export interface ItemInfoDTO {
   id: number;
   name: string;
   item_category: number;
   two_hand: number;
   attack_speed: number;
   required_level: number;
   grade: number;
   min_phy_atk: number;
   max_phy_atk: number;
   defense: number;
   min_mag_atk: number;
   max_mag_atk: number;
   magical_defense: number;
   aim: number;
   evasion: number;
   phy_atk_rate: number;
   mag_atk_rate: number;
   defense_rate: number;
   magical_defense_rate: number;
   critical_rate: number;
   buy_price: number;
   sell_price: number;
   buy_lh_coin: number;
   weapon_type: number;
   use_class: number;
   upgrade_max: number;
   upgrade_info: ItemUpgradeInfo;
   block_rate: number;
   stats: ItemStats;
   random_stats: RandomStats;
   item_set_id: number;
   production_skill_required: number;
   description: string;
   stat_values_unsure: boolean;
}

// tslint:disable
export class ItemInfo {
   id: number;
   name: string;
   item_category: number;
   two_hand: number;
   attack_speed: number;
   required_level: number;
   grade: number;
   min_phy_atk: number;
   max_phy_atk: number;
   defense: number;
   min_mag_atk: number;
   max_mag_atk: number;
   magical_defense: number;
   aim: number;
   evasion: number;
   phy_atk_rate: number;
   mag_atk_rate: number;
   defense_rate: number;
   magical_defense_rate: number;
   critical_rate: number;
   buy_price: number;
   sell_price: number;
   buy_lh_coin: number;
   weapon_type: number;
   use_class: number;
   upgrade_max: number;
   upgrade_info: ItemUpgradeInfo;
   block_rate: number;
   stats: ItemStats;
   random_stats: RandomStats;
   item_set: Observable<ItemSet>;
   production_skill_required: number;
   description: string;
   stat_values_unsure: boolean;
   
   get hasStats(): boolean { return this.stats != null; }

   get hasRandomSTR(): boolean { return this.random_stats.STR > 0; }
   get hasSTR(): boolean { return this.hasStats && this.stats.strength > 0; }
   get STR(): number { return this.hasStats ? this.stats.strength : null; }

   get hasRandomEND(): boolean { return this.random_stats.END > 0; }
   get hasEND(): boolean { return this.hasStats && this.stats.endurance > 0; }
   get END(): number { return this.hasStats ? this.stats.endurance : null; }

   get hasRandomDEX(): boolean { return this.random_stats.DEX > 0; }
   get hasDEX(): boolean { return this.hasStats && this.stats.dexterity > 0; }
   get DEX(): number { return this.hasStats ? this.stats.dexterity : null; }

   get hasRandomINT(): boolean { return this.random_stats.INT > 0; }
   get hasINT(): boolean { return this.hasStats && this.stats.intelligence > 0; }
   get INT(): number { return this.hasStats ? this.stats.intelligence : null; }

   get hasRandomSPR(): boolean { return this.random_stats.SPR > 0; }
   get hasSPR(): boolean { return this.hasStats && this.stats.mentality > 0; }
   get SPR(): number { return this.hasStats ? this.stats.mentality : null; }

   get hasPoisonResist(): boolean { return this.hasStats && this.stats.poison_resist > 0; }
   get hasDiseaseResist(): boolean { return this.hasStats && this.stats.disease_resist > 0; }
   get hasCurseResist(): boolean { return this.hasStats && this.stats.curse_resist > 0; }
   get hasMoveSpeedReductionResist(): boolean { return this.hasStats && this.stats.move_speed_reduction_resist > 0; }

   get hasMaxHP(): boolean { return this.hasStats && this.stats.max_hp > 0; }
   get hasMaxSP(): boolean { return this.hasStats && this.stats.max_sp > 0; }
   
   get hasDmgPlus(): boolean { return this.hasStats && this.stats.dmg_plus > 0; }
   get hasMDmgPlus(): boolean { return this.hasStats && this.stats.mdmg_plus > 0; }
   
   get hasRandomAim(): boolean { return this.random_stats.aim > 0; }
   get hasRandomDmg(): boolean { return this.random_stats.dmg > 0; }
   get hasRandomDef(): boolean { return this.random_stats.def > 0; }
   get hasRandomMdmg(): boolean { return this.random_stats.mdmg > 0; }
   get hasRandomMdef(): boolean { return this.random_stats.mdef > 0; }
   get hasRandomEva(): boolean { return this.random_stats.eva > 0; }
   get hasRandomCrit(): boolean { return this.random_stats.crit > 0; }
   get hasRandomMinusLevel(): boolean { return this.random_stats?.minus_level > 0; }
   get hasRandomHp(): boolean { return this.random_stats.HP > 0; }

   get minLevel(): number { return this.required_level - this.random_stats?.minus_level; }
}
// tslint:enable

export interface ItemDTO {
   id: number;
   name: string;
   item_grade_type: number;
}

export class Item {
   id: number;
   name: string;
   item_grade_type: number;
   view: Observable<ItemView>;
   item_info: Observable<ItemInfo>
}
