import { Observable } from 'rxjs';
import { SkillGroup, SkillGroupDTO } from '../skills/skill-group';
import { Item } from './item';

export interface ItemActionEffectDesc {
    item_action_id: number;
    use_item: number;
    item_action_description: string;
}

export interface SetEffectDTO {
    count: number;
    item_set: number;
    item_actions: number[];
    item_action_effect_desc: ItemActionEffectDesc[];
}

export class SetEffect {
    count: number;
    item_set: Observable<ItemSet>;
    item_actions: Observable<ItemAction[]>;
    item_action_effect_desc: ItemActionEffectDesc[];
}

export interface ItemSetDTO {
    id: number;
    set_name: string;
    set_items: number[];
    set_effects: number[];
}

export class ItemSet {
    id: number;
    set_name: string;
    set_items: Observable<Item[]>;
    set_effects: Observable<SetEffect[]>;
}

export interface ItemActionEffect {
    unique_id: number;
    effect_id: number;
    effect_target: number[];
    effect_activity: number[];
    value: number;
    area: number;
}

export interface ItemActionConditionDTO {
    condition_id: number;
    subject_target: number[];
    object_target: number[];
    activity_rate: number;
    range: number;
    skill_group: SkillGroupDTO;
}

export class ItemActionCondition {
    condition_id: number;
    subject_target: number[];
    object_target: number[];
    activity_rate: number;
    range: number;
    skill_group: SkillGroup;
}

export interface ItemActionDTO {
    item_action_id: number;
    condition: ItemActionConditionDTO;
    effect: ItemActionEffect;
    cooltime: number;
    cooltime_group_action_id: number;
}

export class ItemAction {
    item_action_id: number;
    condition: ItemActionCondition;
    effect: ItemActionEffect;
    cooltime: number;
    cooltime_group_action_id: number;
}
