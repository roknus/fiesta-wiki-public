export interface ItemStats {
    strength: number;
    endurance: number;
    dexterity: number;
    intelligence: number;
    mentality: number;
    poison_resist: number;
    disease_resist: number;
    curse_resist: number;
    move_speed_reduction_resist: number;
    aim_rate: number;
    eva_rate: number;
    max_hp: number;
    max_sp: number;
    dmg_plus: number;
    mdmg_plus: number;
}
