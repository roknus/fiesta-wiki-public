import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { environment } from '@environments/environment';
import { ItemAction } from '@fiesta-common/dto/items/set-effect';

@Pipe({ name: 'itemActionEffect' })
export class ItemActionEffectPipe implements PipeTransform {

    constructor(private decimalPipe: DecimalPipe) { }

    transform(itemAction: ItemAction): string {

        const effectActivity = itemAction.effect.effect_activity;

        switch (effectActivity[1]) {
            case 0:
                return `Increase skill's damage by ${(itemAction.effect.value - 1000) / 10}%`;
            case 1:
                return `Reduce skill's cooldown time by ${(1000 - itemAction.effect.value) / 10}%`;
            case 2:
                return `Increase skill's effect duration by ${(itemAction.effect.value - 1000) / 10}%`;
            case 3:
                return `Decrease skill's SP consumption by ${(1000 - itemAction.effect.value) / 10}%`;
            case 4:
                return `Decrease enemy's defense by ${(1000 - itemAction.effect.value) / 10}%`;
            case 4:
                return `Decrease enemy's defense by ${(1000 - itemAction.effect.value) / 10}%`;
            case 5:
                {
                    // Shared by Nature's protection and Mock (one reduce and one increase agro)
                    // the effect is actually just a dmg modifier but its better displayed like that
                    if (itemAction.effect.value < 1000) {
                        return `Decrease aggro by ${(1000 - itemAction.effect.value) / 10}%`;
                    } else {
                        return `Increase aggro by ${(itemAction.effect.value - 1000) / 10}%`;
                    }
                }
            case 6:
                return `Decrease enemy's DEX by ${(1000 - itemAction.effect.value) / 10}%`;
            case 7:
                return `Increase skill's vampiric effect by ${(itemAction.effect.value - 1000) / 10}%`;
            case 8: // Cleric buff
                return `Increase skill's buff effect by ${(itemAction.effect.value - 1000) / 10}%`;
            // case 9 : not used
            case 11: // HoT
            case 12: // Heal
                return `Increase skill's healing by ${(itemAction.effect.value - 1000) / 10}%`;
            case 13:
                {
                    // Shared by demoralizing shout and vitality (one reduce and one increase dmg)
                    // the effect is actually just a dmg modifier but its better displayed like that
                    if (itemAction.effect.value < 1000) {
                        return `Decrease enemy's damage by ${(1000 - itemAction.effect.value) / 10}%`;
                    } else {
                        return `Increase damage by ${(itemAction.effect.value - 1000) / 10}%`;
                    }
                }
            case 10: // Arena Swing Set
            case 14: // Swing Set
                return `Increase skill's block rate by ${(itemAction.effect.value - 1000) / 10}%`;
        }

        if (environment.debug) {
            return `effect activity : [${effectActivity[0]},${effectActivity[1]}]`;
        }

        return '';
    }
}


@Pipe({ name: 'itemActionEffectGroup' })
export class ItemActionEffectGroupPipe implements PipeTransform {

    constructor(private decimalPipe: DecimalPipe) { }

    transform(itemAction: ItemAction): string {

        const effectActivity = itemAction.effect.effect_activity;

        switch (effectActivity[1]) {
            case 0:
                return `Increase skill's damage`;
            case 1:
                return `Reduce skill's cooldown time`;
            case 2:
                return `Increase skill's effect duration`;
            case 3:
                return `Decrease skill's SP consumption`;
            case 4:
                return `Decrease enemy's defense`;
            case 4:
                return `Decrease enemy's defense`;
            case 5:
                {
                    // Shared by Nature's protection and Mock (one reduce and one increase agro)
                    // the effect is actually just a dmg modifier but its better displayed like that
                    if (itemAction.effect.value < 1000) {
                        return `Decrease aggro  effect`;
                    } else {
                        return `Increase aggro effect`;
                    }
                }
            case 6:
                return `Increase debuff effect`;
            case 7:
                return `Increase skill's vampiric effect`;
            case 8: // Cleric buff
                return `Increase skill's buff effect`;
            // case 9 : not used
            case 11: // HoT
            case 12: // Heal
                return `Increase skill's healing`;
            case 13:
                {
                    // Shared by demoralizing shout and vitality (one reduce and one increase dmg)
                    // the effect is actually just a dmg modifier but its better displayed like that
                    if (itemAction.effect.value < 1000) {
                        return `Decrease enemy's damage`;
                    } else {
                        return `Increase damage`;
                    }
                }
            case 10: // Arena Swing Set
            case 14: // Swing Set
                return `Increase skill's block rate`;
        }

        if (environment.debug) {
            return `effect activity : [${effectActivity[0]},${effectActivity[1]}]`;
        }

        return '';
    }
}
