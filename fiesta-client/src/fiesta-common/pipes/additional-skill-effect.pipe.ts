import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { ActiveSkillEffect } from '../dto/skills/active-skill';
import { environment } from '@environments/environment';

@Pipe({ name: 'additionalSkillEffect' })
export class AdditionalSkillEffectPipe implements PipeTransform {

    constructor(private decimalPipe: DecimalPipe) { }

    transform(effect: ActiveSkillEffect): string {
        switch (effect.effect_id) {
            case 1:
                return ''; // Healing
            case 2:
                return 'Remove Poison and Illness Tier ' + effect.effect_value;
            case 10:
                return 'Life leech : ' + effect.effect_value + 'HP';
            case 12:
                return ''; // Resurrect
            case 13:
                return 'Tick every : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 14:
                return 'Duration : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 15:
                return 'Chance of success : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 17:
                return 'Chance of success : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 32:
                return ''; // Summons
            case 40:
                return ''; // Diminish
            case 41:
                return 'Increase fire effect duration : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 44:
                return 'Increase poison effect duration : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 46:
                return 'Cooldown time reduced when getting hit : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 49:
                return 'Duration : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 57:
                return 'Damage increase : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 58:
                return 'Target HP below : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 77:
                return `Increase damage by ${this.decimalPipe.transform(effect.effect_value / 10, '1.0')}% when casting from behind target`;
        }

        if (environment.debug) {
            return 'effect id : ' + effect.effect_id + '\n' +
                'effect value :' + effect.effect_value;
        }

        return undefined;
    }
}
