import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'monsterCategory'
})
export class MonsterCategoryPipe implements PipeTransform {

   transform(mobCategory: number): unknown {
      switch (mobCategory) {
         case 0:
            return 'monster-icon-human';
         case 1:
            return 'monster-icon-elemental';
         case 2:
            return 'monster-icon-spirit';
         case 3:
            return 'monster-icon-beast';
         case 4:
            return 'monster-icon-summoned-beast';
         case 5:
            return 'monster-icon-undead';
         case 6:
            return 'monster-icon-npc';
         case 8:
         case 9:
         case 10:
            return 'monster-icon-collectible';
         case 28:
            return 'monster-icon-devildom';
         default:
            return 'monster-icon-npc';
      }
   }
}

@Pipe({
   name: 'monsterCategoryName'
})
export class MonsterCategoryNamePipe implements PipeTransform {

   transform(mobCategory: number): unknown {
      switch (mobCategory) {
         case 0:
            return 'Human';
         case 1:
            return 'Elemental';
         case 2:
            return 'Spirit';
         case 3:
            return 'Beast';
         case 4:
            return 'Summoned Beast';
         case 5:
            return 'Undead';
         case 6:
            return 'NPC';
         case 8:
         case 9:
         case 10:
            return 'Collectible';
         case 28:
            return 'Devildom';
         default:
            return 'NPC';
      }
   }
}
