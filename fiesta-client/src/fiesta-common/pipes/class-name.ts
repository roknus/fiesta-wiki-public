import { Pipe, PipeTransform } from '@angular/core';
import { CLASSES_NAME } from '@app/global-strings';

/*
0 : All
1 : Fighter
2 : CleverFighter
3 : Warrior
4 : Gladiator
5 : Knight
6 : Cleric
7 : High Cleric
8 : Paladin
9 : Holy Knight
10 : Guardian
11 : Archer
12 : HawkArcher
13 : Scout
14 : SharpShooter
15 : Ranger
16 : Mage
17 : WizMage
18 : Enchanter
19 : Warlock
20 : Wizard
21 : Trickster
22 : Gambit
23 : Renegade
24 : Spectre
25 : Reaper
26 : Crusader
27 : Templar
*/

@Pipe({ name: 'className' })
export class ClassPipe implements PipeTransform {
  transform(classID: number): string {
    switch (classID) {
        case 0 :
            return $localize`All`;
        case 1:
            return CLASSES_NAME.fighter;
        case 2:
            return CLASSES_NAME.cleverFighter;
        case 3:
            return CLASSES_NAME.warrior;
        case 4:
            return CLASSES_NAME.gladiator;
        case 5:
            return CLASSES_NAME.knight;
        case 6:
            return CLASSES_NAME.cleric;
        case 7:
            return CLASSES_NAME.highCleric;
        case 8:
            return CLASSES_NAME.paladin;
        case 9:
            return CLASSES_NAME.holyKnight;
        case 10:
            return CLASSES_NAME.guardian;
        case 11:
            return CLASSES_NAME.archer;
        case 12:
            return CLASSES_NAME.hawkArcher;
        case 13:
            return CLASSES_NAME.scout;
        case 14:
            return CLASSES_NAME.sharpShooter;
        case 15:
            return CLASSES_NAME.ranger;
        case 16:
            return CLASSES_NAME.mage;
        case 17:
            return CLASSES_NAME.wizMage;
        case 18:
            return CLASSES_NAME.enchanter;
        case 19:
            return CLASSES_NAME.warlock;
        case 20:
            return CLASSES_NAME.wizard;
        case 21:
            return CLASSES_NAME.trickster;
        case 22:
            return CLASSES_NAME.gambit;
        case 23:
            return CLASSES_NAME.renegade;
        case 24:
            return CLASSES_NAME.spectre;
        case 25:
            return CLASSES_NAME.reaper;
        case 26:
            return CLASSES_NAME.crusader;
        case 27:
            return CLASSES_NAME.templar;
        default :
            return $localize`Unknown`;
    }
  }
}
