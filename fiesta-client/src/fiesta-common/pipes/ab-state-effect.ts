import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { environment } from '@environments/environment';
import { SubAbStateEffect } from '@fiesta-common/dto/skills/ab-state';

@Pipe({ name: 'abStateEffect' })
export class AbStateEffectPipe implements PipeTransform {

    constructor(private decimalPipe: DecimalPipe) { }

    transform(effect: SubAbStateEffect): string {

        switch (effect.effect_id) {
            case 3:
                return 'Increase damage by ' + effect.effect_value;
            case 4:
                return 'Increase damage by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 5:
                return 'Increase defense by ' + effect.effect_value;
            case 6:
                return 'Increase defense by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 7:
                return 'Increase DEX by ' + effect.effect_value;
            case 10:
                return 'Increase aim by ' + effect.effect_value;
            case 13:
                return 'Increase magic damage by ' + effect.effect_value;
            case 15:
                return 'Increase magic defense by ' + effect.effect_value;
            case 16:
                return 'Increase gears magic defense by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 17:
                return 'Absorb ' + effect.effect_value + ' damage';
            case 18:
                return 'Increase Shield Block Rate by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 19:
                return 'Unable to move';
            case 20:
                return 'Movement increased by : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 21:
                return 'Decrease attack speed : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 24:
                return 'HP/SP recovery : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 25:
                return 'Unable to use skills';
            case 26:
                return 'Tick every ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 27:
                return 'DoT damage : ' + effect.effect_value;
            case 29:
                return 'Increase casting time : ' + this.decimalPipe.transform(effect.effect_value / 1000, '1.1-1') + 's';
            case 30:
                return 'Healing : ' + effect.effect_value;
            case 31:
                return 'Increase Poison & Illness Resistance : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 34:
                return 'Increase critical rate : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 35:
                return 'Increase HP : ' + effect.effect_value;
            case 36:
                return 'Increase SP : ' + effect.effect_value;
            case 38:
                return 'Feared';
            case 40:
                return 'Health recovered after rebirth : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 41:
                return 'Long range attack blocked : ' + effect.effect_value;
            case 46:
                return 'Increase weapon magic damage : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 49:
                return 'Knock back : ' + this.decimalPipe.transform(effect.effect_value / 100, '1.1') + 'm';
            case 50:
                return 'Increase damage by : ' + this.decimalPipe.transform((effect.effect_value - 1000) / 10, '1.0') + '%';
            case 53:
                return 'Apply the effect on surrounding enemies';
            case 55:
                return 'Effect radius : ' + this.decimalPipe.transform(effect.effect_value / 100, '1.1') + 'm';
            case 57:
                return 'Consume max HP ' + this.decimalPipe.transform(effect.effect_value / 10, '1.1') + '%';
            case 68:
                return 'Increase DoT damage by ' + effect.effect_value;
            case 70:
                return 'Increase poison damage by ' + effect.effect_value;
            case 71:
                return 'Increase evasion against aoe by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 72:
                return 'Increase SP consumption by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 73:
                return 'Decrease defense by ' + effect.effect_value;
            case 74:
                return 'Decrease defense by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 75:
                return 'Decrease DoT damage by ' + effect.effect_value;
            case 76:
                return 'Decrease bleeding damage by ' + effect.effect_value;
            case 77:
                return 'Decrease poison damage by ' + effect.effect_value;
            case 78:
                return 'Increase attack speed by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 81:
                return 'Decrease DEX by ' + effect.effect_value;
            case 88:
                return 'Decrease movement speed by ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 89:
                return 'Decrease STR by ' + effect.effect_value;
            case 90:
                return 'Decrease evasion by ' + effect.effect_value;
            case 92:
                return 'Decrease aim by ' + effect.effect_value;
            case 94:
                return 'Decrease damage by ' + effect.effect_value;
            case 96:
                return 'Inflict ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '% damage of caster';
            case 97:
                return 'Apply the effect to ' + effect.effect_value + ' enemies';
            case 99:
                return 'Decrease SPR by ' + effect.effect_value;
            case 100:
                return 'Decrease SP consumption by ' + effect.effect_value;
            case 111:
                return 'Reduce damage taken : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 112:
                return 'Absorb ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '% of max HP as damage';
            case 113:
                return 'LP recovery rate : ' + effect.effect_value;
            case 114:
                return 'HP can\'t go below : ' + effect.effect_value;
            case 115:
                return 'Reduce magic damage taken : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 121:
                return 'Increase received damage by : ' + this.decimalPipe.transform(effect.effect_value / 10, '1.0') + '%';
            case 122:
                return `Ignore the next ${effect.effect_value} attacks`;
        }

        if (environment.debug) {
            return 'effect id : ' + effect.effect_id + '\n' +
                'effect value :' + effect.effect_value;
        }

        return '';
    }
}
