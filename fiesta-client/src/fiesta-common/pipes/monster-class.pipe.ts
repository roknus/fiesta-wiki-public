import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'monsterClass'
})
export class MonsterClassPipe implements PipeTransform {

   transform(mobClass: number): string {
      switch (mobClass) {
         case 0:
            return 'monster-icon-general';
         case 1:
            return 'monster-icon-boss';
         case 2:
            return 'monster-icon-worldboss';
         case 3:
            return 'monster-icon-field-boss';
         case 4:
            return 'monster-icon-elite';
         default:
            return 'monster-icon-general';
      }
   }
}

@Pipe({
   name: 'monsterClassName'
})
export class MonsterClassNamePipe implements PipeTransform {

   transform(mobClass: number): string {
      switch (mobClass) {
         case 0:
            return 'General Monster';
         case 1:
            return 'Boss Monster';
         case 2:
            return 'World Boss Monster';
         case 3:
            return 'Hero Monster';
         case 4:
            return 'Elite Monster';
         default:
            return 'General Monster';
      }
   }
}