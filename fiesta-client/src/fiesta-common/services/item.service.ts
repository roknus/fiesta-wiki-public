import { _COALESCED_STYLE_SCHEDULER } from '@angular/cdk/table';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UseClassFlagUtils } from '@enums/useclass-flag';
import { environment } from '@environments/environment';
import { Item, ItemDTO, ItemInfoDTO, ItemInfo, ItemView, ItemViewDTO } from '@fiesta-common/dto/items/item';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { ItemAction, ItemActionCondition, ItemActionConditionDTO, ItemActionDTO, ItemSet, ItemSetDTO, SetEffect, SetEffectDTO } from '@fiesta-common/dto/items/set-effect';
import { SkillsService } from '@fiesta-common/services/skills.service';
import { EMPTY, forkJoin, Observable } from 'rxjs';
import { defaultIfEmpty, map, publishLast, refCount, shareReplay, switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ItemService {

    itemMap = new Map<number, Observable<Item>>();
    itemViewMap = new Map<number, Observable<ItemView>>();
    itemInfoMap = new Map<number, Observable<ItemInfo>>();
    itemSetMap = new Map<number, Observable<ItemSet>>();
    setEffectMap = new Map<number, Observable<SetEffect>>();
    itemActionMap = new Map<number, Observable<ItemAction>>();

    constructor(
        private http: HttpClient,
        private skillService: SkillsService
    ) { }

    getItem(itemID: number): Observable<Item> {

        if (itemID == null) {
            return null;
        }

        if (this.itemMap.has(itemID)) {
            return this.itemMap.get(itemID);
        }

        const obs$ = this.http.get<ItemDTO>(environment.api + 'item/' + itemID).pipe(
            shareReplay(1),
            map(results => this.convertItem(results))
        );

        this.itemMap.set(itemID, obs$);

        return obs$;
    }

    private getView(itemID: number): Observable<ItemView> {

        if (itemID == null) {
            return null;
        }

        if (this.itemViewMap.has(itemID)) {
            return this.itemViewMap.get(itemID);
        }

        const obs$ = this.http.get<ItemViewDTO>(environment.api + 'item-view/' + itemID).pipe(
            shareReplay(1),
            map(result => this.convertItemView(result))
        );

        this.itemViewMap.set(itemID, obs$);

        return obs$;
    }

    private getItemInfo(itemID: number): Observable<ItemInfo> {

        if (itemID == null) {
            return null;
        }

        if (this.itemInfoMap.has(itemID)) {
            return this.itemInfoMap.get(itemID);
        }

        const obs$ = this.http.get<ItemInfoDTO>(environment.api + 'item-info/' + itemID).pipe(
            shareReplay(1),
            map(result => this.convertItemInfo(result))
        );

        this.itemInfoMap.set(itemID, obs$);

        return obs$;
    }

    private getItemSet(itemSetID: number): Observable<ItemSet> {

        if (itemSetID == null) {
            return null;
        }

        if (itemSetID === -1) {
            return EMPTY;
        }

        if (this.itemSetMap.has(itemSetID)) {
            return this.itemSetMap.get(itemSetID);
        }

        const obs$ = this.http.get<ItemSetDTO>(environment.api + 'item-set/' + itemSetID).pipe(
            shareReplay(1),
            map(result => this.convertItemSet(result))
        );

        this.itemSetMap.set(itemSetID, obs$);

        return obs$;
    }

    getSetEffect(id: number): Observable<SetEffect> {

        if (this.setEffectMap.has(id)) {
            return this.setEffectMap.get(id);
        }

        const obs$ = this.http.get<SetEffectDTO>(environment.api + 'set-effect/' + id).pipe(
            map(result => this.convertSetEffect(result)),
            publishLast(),
            refCount()
        );

        this.setEffectMap.set(id, obs$);

        return obs$;
    }

    getItems(page: number): Observable<Page<ItemInfo>> {
        return this.http.get<Page<ItemInfo>>(environment.api + 'items/' + page);
    }

    findItem(name: string): Observable<Item[]> {

        const params =
            new HttpParams()
                .set('search', name);

        return this.http.get<ItemDTO[]>(environment.api + 'find-item', { params }).pipe(
            map(result => result.map(i => this.convertItem(i)))
        );
    }

    getWeapons(data:
        {
            page: number,
            weaponType: number,
            rarities?: number[],
            order?: number,
            search?: string,
            min?: number,
            max?: number
        }
    ): Observable<Page<Item>> {

        let params = new HttpParams();

        if (data.rarities && data.rarities.length > 0) {
            for (const r of data.rarities) {
                params = params.append('rarity', r.toString());
            }
        }
        if (data.search && data.search.length > 0) {
            params = params.set('search', data.search.toLocaleLowerCase());
        }
        if (data.order) {
            params = params.set('order', data.order.toString());
        }
        if (data.min) {
            params = params.set('min', data.min.toString());
        }
        if (data.max) {
            params = params.set('max', data.max.toString());
        }

        return this.http.get<Page<ItemDTO>>(environment.api + 'weapons/' + data.weaponType + '/' + data.page, { params }).pipe(
            map(results => this.convertItemInfoPage(results))
        );
    }

    getShields(
        shieldType: number,
        rarities: number[],
        page: number,
        order: number,
        search: string,
        min: number,
        max: number
    ): Observable<Page<Item>> {
        let params = new HttpParams();

        if (rarities.length > 0) {
            for (const r of rarities) {
                params = params.append('rarity', r.toString());
            }
        }
        if (search.length > 0) {
            params = params.set('search', search);
        }
        if (order) {
            params = params.set('order', order.toString());
        }
        if (min) {
            params = params.set('min', min.toString());
        }
        if (max) {
            params = params.set('max', max.toString());
        }

        return this.http.get<Page<ItemDTO>>(environment.api + 'shields/' + shieldType + '/' + page, { params }).pipe(
            map(results => this.convertItemInfoPage(results))
        );
    }

    getGears(
        classType: number,
        rarities: number[],
        page: number,
        order: number,
        search: string,
        min: number,
        max: number
    ): Observable<Page<Item>> {

        let params = new HttpParams();

        if (rarities.length > 0) {
            for (const r of rarities) {
                params = params.append('rarity', r.toString());
            }
        }
        if (search.length > 0) {
            params = params.set('search', search);
        }
        if (order) {
            params = params.set('order', order.toString());
        }
        if (min) {
            params = params.set('min', min.toString());
        }
        if (max) {
            params = params.set('max', max.toString());
        }

        return this.http.get<Page<ItemDTO>>(environment.api + 'gears/' + classType + '/' + page, { params }).pipe(
            map(results => this.convertItemInfoPage(results))
        );
    }

    getJewels(
        jewelType: number,
        page: number,
        rarities: number[],
        order: number,
        search: string,
        min: number,
        max: number
    ): Observable<Page<Item>> {

        let params = new HttpParams();

        if (rarities.length > 0) {
            for (const r of rarities) {
                params = params.append('rarity', r.toString());
            }
        }
        if (search.length > 0) {
            params = params.set('search', search);
        }
        if (order) {
            params = params.set('order', order.toString());
        }
        if (min) {
            params = params.set('min', min.toString());
        }
        if (max) {
            params = params.set('max', max.toString());
        }

        return this.http.get<Page<ItemDTO>>(environment.api + 'jewels/' + jewelType + '/' + page, { params }).pipe(
            map(results => this.convertItemInfoPage(results))
        );
    }

    getBracelets(
        page: number,
        order: any,
        search: string,
        min: number,
        max: number
    ): Observable<Page<Item>> {

        let params = new HttpParams();

        if (search.length > 0) {
            params = params.set('search', search);
        }
        if (order) {
            params = params.set('order', order.toString());
        }
        if (min) {
            params = params.set('min', min.toString());
        }
        if (max) {
            params = params.set('max', max.toString());
        }

        return this.http.get<Page<ItemDTO>>(environment.api + 'bracelets/' + page, { params }).pipe(
            map(results => this.convertItemInfoPage(results))
        );
    }

    getTalismans(
        page: number,
        order: any,
        search: string,
        min: number,
        max: number
    ): Observable<Page<Item>> {

        let params = new HttpParams();

        if (search.length > 0) {
            params = params.set('search', search);
        }
        if (order) {
            params = params.set('order', order.toString());
        }
        if (min) {
            params = params.set('min', min.toString());
        }
        if (max) {
            params = params.set('max', max.toString());
        }

        return this.http.get<Page<ItemDTO>>(environment.api + 'talismans/' + page, { params }).pipe(
            map(results => this.convertItemInfoPage(results))
        );
    }

    getItemAction(id: number): Observable<ItemAction> {

        if (this.itemActionMap.has(id)) {
            return this.itemActionMap.get(id);
        }

        const obs$ = this.http.get<ItemActionDTO>(environment.api + 'item-action/' + id).pipe(
            map(result => this.convertItemAction(result)),
            publishLast(),
            refCount()
        );

        this.itemActionMap.set(id, obs$);

        return obs$;
    }

    getSkillItemSet(useClass: number, id: number): Observable<SetEffect[]> {
        const useClassFlag = UseClassFlagUtils.fromUseClass(useClass);
        const completeSubClasses = UseClassFlagUtils.completeWithSubclass(useClassFlag);

        return this.http.get<number[]>(environment.api + 'skill-item-set/' + completeSubClasses + '/' + id).pipe(
            switchMap(result => forkJoin(result.map(effectId => this.getSetEffect(effectId))).pipe(
                defaultIfEmpty<SetEffect[]>([])
            ))
        );
    }

    private convertItemInfo(itemInfo: ItemInfoDTO): ItemInfo {
        if (!itemInfo) {
            return null;
        }

        const _ = new ItemInfo();

        _.id = itemInfo.id;
        _.name = itemInfo.name;
        _.item_category = itemInfo.item_category;
        _.two_hand = itemInfo.two_hand;
        _.attack_speed = itemInfo.attack_speed;
        _.required_level = itemInfo.required_level;
        _.grade = itemInfo.grade;
        _.min_phy_atk = itemInfo.min_phy_atk;
        _.max_phy_atk = itemInfo.max_phy_atk;
        _.defense = itemInfo.defense;
        _.min_mag_atk = itemInfo.min_mag_atk;
        _.max_mag_atk = itemInfo.max_mag_atk;
        _.magical_defense = itemInfo.magical_defense;
        _.aim = itemInfo.aim;
        _.evasion = itemInfo.evasion;
        _.phy_atk_rate = itemInfo.phy_atk_rate;
        _.mag_atk_rate = itemInfo.mag_atk_rate;
        _.defense_rate = itemInfo.defense_rate;
        _.magical_defense_rate = itemInfo.magical_defense_rate;
        _.critical_rate = itemInfo.critical_rate;
        _.buy_price = itemInfo.buy_price;
        _.sell_price = itemInfo.sell_price;
        _.buy_lh_coin = itemInfo.buy_lh_coin;
        _.weapon_type = itemInfo.weapon_type;
        _.use_class = itemInfo.use_class;
        _.upgrade_max = itemInfo.upgrade_max;
        _.upgrade_info = itemInfo.upgrade_info;
        _.block_rate = itemInfo.block_rate;
        _.stats = itemInfo.stats;
        _.random_stats = itemInfo.random_stats;
        _.item_set = this.getItemSet(itemInfo.item_set_id);
        _.production_skill_required = itemInfo.production_skill_required;
        _.description = itemInfo.description;
        _.stat_values_unsure = itemInfo.stat_values_unsure;

        return _;
    }

    private convertItemView(itemView: ItemViewDTO): ItemView {
        if (!itemView) {
            return null;
        }

        const _ = new ItemView();

        _.id = itemView.id;
        _.icon_index = itemView.icon_index;
        _.icon_file = itemView.icon_file;
        _.sub_icon_index = itemView.sub_icon_index;
        _.sub_icon_file = itemView.sub_icon_file;
        _.period_icon_index = itemView.period_icon_index;
        _.period_icon_file = itemView.period_icon_file;
        _.border_color = itemView.border_color;
        _.background_color = itemView.background_color;
        _.description = itemView.description;

        return _;
    }

    convertItem(item: ItemDTO): Item {
        if (!item) {
            return null;
        }

        const _ = new Item();

        _.id = item.id;
        _.name = item.name;
        _.item_grade_type = item.item_grade_type;
        _.view = this.getView(item.id);
        _.item_info = this.getItemInfo(item.id);

        return _;
    }

    convertItemSet(itemSet: ItemSetDTO): ItemSet {
        if (!itemSet) {
            return null;
        }

        const _ = new ItemSet();

        _.id = itemSet.id;
        _.set_name = itemSet.set_name;
        _.set_items = forkJoin(itemSet.set_items.map(item => this.getItem(item)));
        _.set_effects = forkJoin(itemSet.set_effects.map(effect => this.getSetEffect(effect)));

        return _;
    }

    convertSetEffect(setEffect: SetEffectDTO): SetEffect {
        if (!setEffect) {
            return null;
        }

        const _ = new SetEffect();

        _.count = setEffect.count;
        _.item_set = this.getItemSet(setEffect.item_set);
        _.item_actions = forkJoin(setEffect.item_actions.map(id => this.getItemAction(id)));
        _.item_action_effect_desc = setEffect.item_action_effect_desc;

        return _;
    }

    convertItemAction(itemAction: ItemActionDTO): any {
        if (!itemAction) {
            return null;
        }

        const _ = new ItemAction();

        _.item_action_id = itemAction.item_action_id;
        _.condition = this.convertItemActionCondition(itemAction.condition);
        _.effect = itemAction.effect;
        _.cooltime = itemAction.cooltime;
        _.cooltime_group_action_id = itemAction.cooltime_group_action_id;

        return _;
    }

    convertItemActionCondition(itemActionCondition: ItemActionConditionDTO): any {
        if (!itemActionCondition) {
            return null;
        }

        const _ = new ItemActionCondition();

        _.condition_id = itemActionCondition.condition_id;
        _.subject_target = [...itemActionCondition.subject_target];
        _.object_target = [...itemActionCondition.object_target];
        _.activity_rate = itemActionCondition.activity_rate;
        _.range = itemActionCondition.range;
        _.skill_group = this.skillService.convertSkillGroup(itemActionCondition.skill_group);

        return _;
    }

    convertItemInfoList(items: ItemDTO[]): Item[] {
        return items.map(i => this.convertItem(i));
    }

    convertItemInfoPage(pageDTO: Page<ItemDTO>): Page<Item> {
        const pageResult = new Page<Item>();

        pageResult.totalElements = pageDTO.totalElements;
        pageResult.totalPages = pageDTO.totalPages;
        pageResult.size = pageDTO.size;
        pageResult.number = pageDTO.number;

        pageResult.content = this.convertItemInfoList(pageDTO.content);

        return pageResult;
    }
}
