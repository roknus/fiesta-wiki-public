import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { defaultIfEmpty, map, mergeMap, publishLast, refCount } from 'rxjs/operators';
import { ActiveSkill, ActiveSkillEffect } from '@fiesta-common/dto/skills/active-skill';
import { environment } from '@environments/environment';
import { UseClassFlagUtils } from '@enums/useclass-flag';
import { PassiveSkill } from '@fiesta-common/dto/skills/passive-skill';
import { AdditionalSkillEffectPipe } from '@fiesta-common/pipes/additional-skill-effect.pipe';
import { SkillGroup, SkillGroupDTO } from '@fiesta-common/dto/skills/skill-group';

@Injectable({
    providedIn: 'root'
})
export class SkillsService {

    activeSkillMap = new Map<number, Observable<ActiveSkill>>();
    passiveSkillMap = new Map<number, Observable<PassiveSkill>>();

    constructor(
        private http: HttpClient,
        private additionalSkillEffectPipe: AdditionalSkillEffectPipe
    ) { }

    private filterUnknownEffects(effects: ActiveSkillEffect[]): ActiveSkillEffect[] {
        return effects.filter(e => this.additionalSkillEffectPipe.transform(e) !== undefined);
    }

    getActiveSkill(id: number): Observable<ActiveSkill> {

        if (this.activeSkillMap.has(id)) {
            return this.activeSkillMap.get(id);
        }

        const obs$ = this.http.get<ActiveSkill>(environment.api + 'active-skill/' + id).pipe(
            map(skill => {
                const newSkill = ActiveSkill.clone(skill);

                newSkill.effects = this.filterUnknownEffects(newSkill.effects);

                return newSkill;
            }),
            publishLast(),
            refCount()
        );

        this.activeSkillMap.set(id, obs$);

        return obs$;
    }

    getPassiveSkill(id: number): Observable<PassiveSkill> {

        if (this.passiveSkillMap.has(id)) {
            return this.passiveSkillMap.get(id);
        }

        const obs$ = this.http.get<PassiveSkill>(environment.api + 'passive-skill/' + id).pipe(
            publishLast(),
            refCount()
        );

        this.passiveSkillMap.set(id, obs$);

        return obs$;
    }

    getActiveSkillList(useClass: number, level: number): Observable<ActiveSkill[]> {

        const useClassFlag = UseClassFlagUtils.fromUseClass(useClass);
        const completeSubClasses = UseClassFlagUtils.completeWithSubclass(useClassFlag);

        const params =
            new HttpParams()
                .set('level', level.toString());

        return this.http.get<number[]>(environment.api + 'active-skill-list/' + completeSubClasses, { params }).pipe(
            mergeMap(result => forkJoin(result.map(i => this.getActiveSkill(i))).pipe(
                defaultIfEmpty<ActiveSkill[]>([])
            ))
        );
    }

    getPassiveSkillList(useClass: number, level: number): Observable<PassiveSkill[]> {

        const useClassFlag = UseClassFlagUtils.fromUseClass(useClass);
        const completeSubClasses = UseClassFlagUtils.completeWithSubclass(useClassFlag);

        const params =
            new HttpParams()
                .set('level', level.toString());

        return this.http.get<number[]>(environment.api + 'passive-skill-list/' + completeSubClasses, { params }).pipe(
            mergeMap(result => forkJoin(result.map(i => this.getPassiveSkill(i))).pipe(
                defaultIfEmpty<PassiveSkill[]>([])
            ))
        );
    }

    getActiveSkillGroup(useClass: number, inxname: string, level: number): Observable<ActiveSkill[]> {

        const useClassFlag = UseClassFlagUtils.fromUseClass(useClass);
        const completeSubClasses = UseClassFlagUtils.completeWithSubclass(useClassFlag);

        const params =
            new HttpParams()
                .set('level', level.toString());

        return this.http.get<number[]>(environment.api + 'active-skill-group/' + completeSubClasses + '/' + inxname, { params }).pipe(
            mergeMap(result => forkJoin(result.map(i => this.getActiveSkill(i))).pipe(
                defaultIfEmpty<ActiveSkill[]>([])
            ))
        );
    }

    getPassiveSkillGroup(useClass: number, inxname: string, level: number): Observable<PassiveSkill[]> {

        const useClassFlag = UseClassFlagUtils.fromUseClass(useClass);
        const completeSubClasses = UseClassFlagUtils.completeWithSubclass(useClassFlag);

        const params =
            new HttpParams()
                .set('level', level.toString());

        return this.http.get<number[]>(environment.api + 'passive-skill-group/' + completeSubClasses + '/' + inxname, { params }).pipe(
            mergeMap(result => forkJoin(result.map(i => this.getPassiveSkill(i))).pipe(
                defaultIfEmpty<PassiveSkill[]>([])
            ))
        );
    }

    convertSkillGroup(skillGroup: SkillGroupDTO): any {
        if (!skillGroup) {
            return null;
        }

        const _ = new SkillGroup();

        _.id = skillGroup.id;
        _.skills = forkJoin(skillGroup.skills.map(id => this.getActiveSkill(id)));

        return _;
    }
}
