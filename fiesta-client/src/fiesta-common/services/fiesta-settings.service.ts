import { Injectable } from '@angular/core';
import { SessionSettingsService } from 'session-settings/session-settings.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
   providedIn: 'root'
})
export class FiestaSettingsService {

   get showRandomStats(): Observable<boolean> {
      return this.sessionSettings.getSettingObservable('showRandomStats', 'true').pipe(
         map(value => value === 'true')
      );
   }

   constructor(private sessionSettings: SessionSettingsService) {
      this.sessionSettings.setSettingValue('showRandomStats', 'true');
   }

   setShowRandomStats(value: boolean) {
      this.sessionSettings.setSettingValue('showRandomStats', value.toString());
   }
}
