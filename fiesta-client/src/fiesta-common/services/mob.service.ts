import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MobInfo, MobInfoDTO, MobCoordinateDTO, MobCoordinate, MobViewInfo, MobViewInfoDTO, MobSpecies, MobSpeciesDTO } from '@fiesta-common/dto/mobs/mob-info';
import { MobDropInfo, MobDropInfoDTO } from '@fiesta-common/dto/mobs/mob-drop-info';
import { Observable, forkJoin, ReplaySubject } from 'rxjs';
import { environment } from '@environments/environment';
import { shareReplay, map, defaultIfEmpty, mergeMap, publishLast, refCount, switchMap } from 'rxjs/operators';
import { MapService } from '@fiesta-common/services/map.service';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { ItemService } from './item.service';

@Injectable({
    providedIn: 'root'
})
export class MobService {

    mobMap = new Map<number, Observable<MobInfo>>();
    speciesMap = new Map<number, Observable<MobSpecies>>();
    mobDropMap = new Map<number, Observable<MobDropInfo[]>>();

    constructor(
        private http: HttpClient,
        private mapService: MapService,
        private itemService: ItemService
    ) {
    }

    getMobInfo(mobId: number): Observable<MobInfo> {

        if (this.mobMap.has(mobId)) {
            return this.mobMap.get(mobId);
        }

        const obs$ = this.http.get<MobInfoDTO>(environment.api + 'mob/' + mobId).pipe(
            map(mob => this.convertMobInfo(mob)),
            publishLast(),
            refCount()
        );

        this.mobMap.set(mobId, obs$);

        return obs$;
    }

    getMobSpecies(speciesId: number): Observable<MobSpecies> {
        if (this.speciesMap.has(speciesId)) {
            return this.speciesMap.get(speciesId);
        }

        const obs$ = this.http.get<MobSpeciesDTO>(environment.api + 'species/' + speciesId).pipe(
            map(species => this.convertMobSpecies(species)),
            publishLast(),
            refCount()
        );

        this.speciesMap.set(speciesId, obs$);

        return obs$;
    }

    updateMobInfo(mob: MobInfo): Observable<MobInfo> {
        return this.http.put<MobInfoDTO>(environment.api + 'mob', this.convertMobInfoDTO(mob)).pipe(
            map(m => this.convertMobInfo(m))
        );
    }

    getMobDropInfo(mobId: number): Observable<MobDropInfo[]> {

        if (this.mobDropMap.has(mobId)) {
            return this.mobDropMap.get(mobId);
        }

        const obs$ = this.http.get<MobDropInfoDTO[]>(environment.api + 'mob-drops/' + mobId).pipe(
            shareReplay(1),
            map(drops => drops.map(d => this.convertMobDropInfo(d)))
        );

        this.mobDropMap.set(mobId, obs$);

        return obs$;
    }

    getItemSources(id: number): Observable<MobInfo[]> {
        return this.http.get<number[]>(environment.api + 'item-sources/' + id).pipe(
            switchMap(result => forkJoin(result.map(i => this.getMobInfo(i))).pipe(
                defaultIfEmpty<MobInfo[]>([])
            ))
        );
    }

    getSpeciesPage(data: {
        page: number,
        size?: number,
        search?: string,
        min?: number,
        max?: number,
        npc?: boolean,
        sort?: string[],
        active?: boolean,
        location?: number
    }): Observable<Page<MobSpecies>> {

        let params = new HttpParams();

        if (data.page) {
            params = params.set('page', data.page.toString());
        }
        if (data.size) {
            params = params.set('size', data.size.toString());
        }
        if (data.sort) {
            params = params.set('sort', data.sort.join(','));
        }
        if (data.search) {
            params = params.set('search', data.search);
        }
        if (data.location) {
            params = params.set('location', data.location.toString());
        }
        if (data.active) {
            params = params.set('active', data.active.toString());
        }
        if (data.min) {
            params = params.set('min', data.min.toString());
        }
        if (data.max) {
            params = params.set('max', data.max.toString());
        }
        if (data.npc !== undefined) {
            params = params.set('npc', data.npc.toString());
        }

        return this.http.get<Page<number>>(environment.api + 'species', { params }).pipe(
            mergeMap(page => forkJoin(page.content.map(id => this.getMobSpecies(id))).pipe(
                defaultIfEmpty<MobSpecies[]>([]),
                map(mobs => {
                    const mobPage = new Page<MobSpecies>();
                    mobPage.number = page.number;
                    mobPage.size = page.size;
                    mobPage.totalElements = page.totalElements;
                    mobPage.totalPages = page.totalPages;
                    mobPage.content = mobs;

                    return mobPage;
                })
            )
            )
        );
    }

    convertMobView(mobView: MobViewInfoDTO): MobViewInfo {
        if (!mobView) {
            return null;
        }

        const _ = new MobViewInfo();

        _.inxname = mobView.inxname;
        _.filename = mobView.filename;
        _.attack_type = mobView.attack_type;
        _.mob_portrait = mobView.mob_portrait;
        _.minimap_icon = mobView.minimap_icon;
        _.npc_view_id = mobView.npc_view_id;

        return _;
    }

    convertMobSpecies(dto: MobSpeciesDTO): MobSpecies {
        if (!dto) {
            return null;
        }

        const _ = new MobSpecies();

        _.id = dto.id;
        _.name = dto.name;
        _.mob_portrait = dto.mob_portrait;
        _.mobs = dto.mobs.map(m => this.getMobInfo(m));

        return _;
    }

    convertMobInfo(mobInfo: MobInfoDTO): MobInfo {
        if (!mobInfo) {
            return null;
        }

        const newMob = new MobInfo();

        newMob.id = mobInfo.id;
        newMob.name = mobInfo.name;
        newMob.view = this.convertMobView(mobInfo.view);
        newMob.level = mobInfo.level;
        newMob.max_hp = mobInfo.max_hp;
        newMob.walk_speed = mobInfo.walk_speed;
        newMob.run_speed = mobInfo.run_speed;
        newMob.npc = mobInfo.npc;
        newMob.weapon_type = mobInfo.weapon_type;
        newMob.armor_type = mobInfo.armor_type;
        newMob.grade_type = mobInfo.grade_type;
        newMob.type = mobInfo.type;
        newMob.location = mobInfo.location;
        newMob.active = mobInfo.active;
        newMob.coordinates = mobInfo.coordinates.map(x => this.convertMobCoordinate(x));
        newMob.related_quests = mobInfo.related_quests;
        newMob.species = mobInfo.species;

        newMob.defense = mobInfo.defense;
        newMob.evasion = mobInfo.evasion;
        newMob.magical_defense = mobInfo.magical_defense;
        newMob.aggro_type = mobInfo.aggro_type;
        newMob.aggro_radius = mobInfo.aggro_radius;
        newMob.aggro_range = mobInfo.aggro_range;
        newMob.exp = mobInfo.exp;
        newMob.STR = mobInfo.STR;
        newMob.DEX = mobInfo.DEX;
        newMob.END = mobInfo.END;
        newMob.INT = mobInfo.INT;
        newMob.SPR = mobInfo.SPR;
        newMob.blooding_resistance = mobInfo.blooding_resistance;
        newMob.stun_resistance = mobInfo.stun_resistance;
        newMob.move_speed_resistance = mobInfo.move_speed_resistance;
        newMob.fear_resistance = mobInfo.fear_resistance;

        return newMob;
    }

    convertMobInfoDTO(mobInfo: MobInfo): MobInfoDTO {
        if (!mobInfo) {
            return null;
        }

        const newMobDTO = new MobInfoDTO();

        newMobDTO.id = mobInfo.id;
        newMobDTO.name = mobInfo.name;
        newMobDTO.level = mobInfo.level;
        newMobDTO.location = mobInfo.location;
        newMobDTO.active = mobInfo.active;

        return newMobDTO;
    }

    convertMobDropInfo(mobDropInfo: MobDropInfoDTO): MobDropInfo {
        if (!mobDropInfo) {
            return null;
        }

        const newMobDropInfo = new MobDropInfo();

        newMobDropInfo.drop_rate = mobDropInfo.drop_rate;
        newMobDropInfo.item_drops = mobDropInfo.item_drops.map(i => this.itemService.getItem(i));

        return newMobDropInfo;
    }

    convertMobCoordinate(mobCoordinate: MobCoordinateDTO): MobCoordinate {
        if (!mobCoordinate) {
            return null;
        }

        const newMobCoordinate = new MobCoordinate();

        newMobCoordinate.id = mobCoordinate.id;
        newMobCoordinate.map = this.mapService.getMapInfo(mobCoordinate.map_id);
        newMobCoordinate.center_x = mobCoordinate.center_x;
        newMobCoordinate.center_y = mobCoordinate.center_y;
        newMobCoordinate.width = mobCoordinate.width;
        newMobCoordinate.height = mobCoordinate.height;
        newMobCoordinate.range_degree = mobCoordinate.range_degree;

        return newMobCoordinate;
    }
}
