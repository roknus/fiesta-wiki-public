import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import {
    ItemReward,
    ItemRewardDTO,
    Quest,
    QuestData,
    QuestDataDTO,
    QuestDTO,
    QuestItemDropInfo,
    QuestItemDropInfoDTO,
    QuestItemGathering,
    QuestItemGatheringDTO,
    QuestLevelInfo,
    QuestLevelInfoDTO,
    QuestLine,
    QuestLineDTO,
    QuestMobKill,
    QuestMobKillDTO
} from '@fiesta-common/dto/quests/quest';
import { ItemService } from '@fiesta-common/services/item.service';
import { forkJoin, Observable } from 'rxjs';
import { defaultIfEmpty, map, shareReplay, switchMap } from 'rxjs/operators';
import { MobService } from './mob.service';

@Injectable({
    providedIn: 'root'
})
export class QuestService {

    questMap = new Map<number, Observable<Quest>>();

    constructor(
        private http: HttpClient,
        private itemInfoService: ItemService,
        private mobService: MobService
    ) { }

    getQuests(page: number, iClass: number, hideEvents: boolean): Observable<QuestLevelInfo[]> {

        let params =
            new HttpParams()
                .set('start', (page * 10).toString())
                .set('range', (10).toString());

        if (iClass > 0) {
            params = params.set('class', iClass.toString());
        }

        if (hideEvents) {
            params = params.set('hideEvents', hideEvents.toString());
        }

        return this.http.get<QuestLevelInfoDTO[]>(environment.api + 'quests/page', { params })
            .pipe(
                // switch map to avoid observable of observable
                switchMap(levels =>
                    // Observable<QuestLevelInfo>[]> -> Observable<QuestLevelInfo[]>
                    forkJoin(levels
                        .map(level =>
                            // Observable<Quest>[] -> Observable<Quest[]>
                            forkJoin(level.quests
                                .map(i => this.getQuest(i))
                            )
                                .pipe(
                                    defaultIfEmpty<Quest[]>([]),
                                    map(quests => {
                                        const _ = this.convertQuestLevelInfo(level);
                                        _.quests = quests;
                                        return _;
                                    })
                                )
                        )
                    )
                )
            );
    }

    getQuest(questId: number): Observable<Quest> {
        if (questId == null) {
            return null;
        }

        if (this.questMap.has(questId)) {
            return this.questMap.get(questId);
        }

        const obs$ = this.http.get<QuestDTO>(environment.api + 'quest/' + questId).pipe(
            shareReplay(1),
            map(q => this.convertQuest(q))
        );

        this.questMap.set(questId, obs$);

        return obs$;
    }

    private getQuestData(questId: number): Observable<QuestData> {

        if (questId == null) {
            return null;
        }

        return this.http.get<QuestDataDTO>(environment.api + 'quest-data/' + questId).pipe(
            map(q => this.convertQuestData(q))
        );
    }

    getAvailableQuestsPerLevel(level: number, minLevel: number): Observable<QuestLevelInfo> {
        if (level == null) {
            return null;
        }

        const params =
            new HttpParams()
                .set('minLevel', minLevel.toString());

        return this.http.get<QuestLevelInfoDTO>(environment.api + 'quest/level/' + level, { params })
            .pipe(
                // switch map to avoid observable of observable
                switchMap(result =>
                    // Observable<Quest>[] -> Observable<Quest[]>
                    forkJoin(result.quests
                        .map(i => this.getQuest(i))
                    )
                        .pipe(
                            defaultIfEmpty<Quest[]>([]),
                            map(quests => {
                                const _ = this.convertQuestLevelInfo(result);
                                _.quests = quests;
                                return _;
                            })
                        ))
            );
    }

    convertQuestLevelInfo(levelQuestInfo: QuestLevelInfoDTO): QuestLevelInfo {
        if (!levelQuestInfo) {
            return null;
        }

        const newLevelQuestInfo = new QuestLevelInfo();

        newLevelQuestInfo.level = levelQuestInfo.level;
        newLevelQuestInfo.xp_required = levelQuestInfo.xp_required;
        newLevelQuestInfo.total_xp = levelQuestInfo.total_xp;

        return newLevelQuestInfo;
    }

    convertQuest(quest: QuestDTO): Quest {
        if (!quest) {
            return null;
        }

        const _ = new Quest();

        _.id = quest.id;
        _.title = quest.title;
        _.quest_type = quest.quest_type;
        _.repeat = quest.repeat;
        _.remote_turn_in = quest.remote_turn_in;
        _.remote_pick_up = quest.remote_pick_up;
        _.start_class = quest.start_class;
        _.min_level = quest.min_level;
        _.max_level = quest.max_level;
        _.exp = quest.exp;
        _.money = quest.money;
        _.item_rewards = quest.item_rewards.map(x => this.convertItemReward(x));
        _.selectable_item_rewards = quest.selectable_item_rewards.map(x => this.convertItemReward(x));
        _.data = this.getQuestData(quest.id);

        return _;
    }

    convertQuestData(data: QuestDataDTO): QuestData {
        if (!data) {
            return null;
        }

        const _ = new QuestData();

        _.id = data.id;
        _.quest_type = data.quest_type;
        _.title = data.title;
        _.description = data.description;
        _.type = data.type;
        _.min_level = data.min_level;
        _.max_level = data.max_level;
        _.starting_npc = this.mobService.getMobInfo(data.starting_npc_id);
        _.end_npc = this.mobService.getMobInfo(data.end_npc_id);
        _.starting_item = this.itemInfoService.getItem(data.starting_item_id);
        _.starting_item_quantity = data.starting_item_quantity;
        _.exp = data.exp;
        _.money = data.money;
        _.fame = data.fame;
        _.item_rewards = data.item_rewards.map(x => this.convertItemReward(x));
        _.selectable_item_rewards = data.selectable_item_rewards.map(x => this.convertItemReward(x));
        _.start_class = data.start_class;
        _.previous_quest = this.convertQuest(data.previous_quest);
        _.next_quests = data.next_quests.map(q => this.convertQuest(q));
        _.mob_kill = data.mob_kill.map(x => this.convertQuestMobKill(x));
        _.item_gathering = data.item_gathering.map(x => this.convertItemGathering(x));
        _.repeat = data.repeat;
        _.remote_turn_in = data.remote_turn_in;
        _.remote_pick_up = data.remote_pick_up;

        return _;
    }

    convertItemGathering(itemGathering: QuestItemGatheringDTO): QuestItemGathering {
        if (!itemGathering) {
            return null;
        }

        const newQuestItemGathering = new QuestItemGathering();

        newQuestItemGathering.item = this.itemInfoService.getItem(itemGathering.item_id);
        newQuestItemGathering.count = itemGathering.count;
        newQuestItemGathering.drop_info = itemGathering.drop_info.map(x => this.convertItemDropInfo(x));

        return newQuestItemGathering;
    }

    convertItemReward(itemReward: ItemRewardDTO): ItemReward {
        if (!itemReward) {
            return null;
        }

        const newItemReward = new ItemReward();

        newItemReward.selectable = itemReward.selectable;
        newItemReward.quantity = itemReward.quantity;
        newItemReward.item = this.itemInfoService.getItem(itemReward.item_id);

        return newItemReward;
    }

    convertQuestMobKill(questMobKill: QuestMobKillDTO): QuestMobKill {
        if (!questMobKill) {
            return null;
        }

        const newQuestMobKill = new QuestMobKill();

        newQuestMobKill.kill_count = questMobKill.kill_count;
        newQuestMobKill.mob = this.mobService.getMobInfo(questMobKill.mob_id);

        return newQuestMobKill;
    }

    convertItemDropInfo(itemDropInfo: QuestItemDropInfoDTO): QuestItemDropInfo {
        if (!itemDropInfo) {
            return null;
        }

        const newQuestItemDropInfo = new QuestItemDropInfo();

        newQuestItemDropInfo.mob = this.mobService.getMobInfo(itemDropInfo.mob_id);
        newQuestItemDropInfo.percent = itemDropInfo.percent;
        newQuestItemDropInfo.count_min = itemDropInfo.count_min;
        newQuestItemDropInfo.count_max = itemDropInfo.count_max;

        return newQuestItemDropInfo;
    }

    convertQuestLine(questLine: QuestLineDTO): QuestLine {
        if (!questLine) {
            return null;
        }

        const newQuestLine = new QuestLine();

        newQuestLine.quest = this.getQuest(questLine.id);
        if (questLine.next_quests) {
            newQuestLine.next_quests = questLine.next_quests.map(q => this.convertQuestLine(q));
        }
        if (questLine.previous_quests) {
            newQuestLine.previous_quests = questLine.previous_quests.map(q => this.convertQuestLine(q));
        }

        return newQuestLine;
    }
}
