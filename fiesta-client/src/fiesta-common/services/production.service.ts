import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Production, ProductionDTO } from '@fiesta-common/dto/production/production';
import { environment } from '@environments/environment';
import { ItemService } from './item.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProductionService {

    constructor(
        private http: HttpClient,
        private itemService: ItemService
    ) { }

    getProduction(id: number): Observable<Production[]> {
        return this.http.get<ProductionDTO[]>(environment.api + 'production/' + id).pipe(
            map(productions => productions.map(p => this.convertProduction(p)))
        );
    }

    convertProduction(prod: ProductionDTO): Production {
        const _ = new Production();

        _.id = prod.id;
        _.recipe = this.itemService.convertItem(prod.recipe);
        _.name = prod.name;
        _.product = this.itemService.convertItem(prod.product);
        _.quantity = prod.quantity;
        _.ingredient1 = this.itemService.convertItem(prod.ingredient1);
        _.quantity1 = prod.quantity1;
        _.ingredient2 = this.itemService.convertItem(prod.ingredient2);
        _.quantity2 = prod.quantity2;
        _.ingredient3 = this.itemService.convertItem(prod.ingredient3);
        _.quantity3 = prod.quantity3;
        _.ingredient4 = this.itemService.convertItem(prod.ingredient4);
        _.quantity4 = prod.quantity4;
        _.mastery_type = prod.mastery_type;
        _.mastery_gain = prod.mastery_gain;
        _.needed_mastery = prod.needed_mastery;

        return _;
    }
}
