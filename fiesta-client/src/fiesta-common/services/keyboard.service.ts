import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
   providedIn: 'root'
})
export class KeyboardService {

   isCtrlPressed$: BehaviorSubject<boolean> = new BehaviorSubject(false);

   constructor() { }

   onCtrl(pressed) {
      if (this.isCtrlPressed$.value !== pressed) {
         this.isCtrlPressed$.next(pressed);
      }
   }
}
