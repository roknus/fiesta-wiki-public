import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { ItemService } from './services/item.service';
import { PageLoaderComponent } from './components/page-loader/page-loader.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { ItemsTableComponent } from './components/items-table/items-table.component';
import { MoneyComponent } from './components/money/money.component';
import { ItemGradeColorPipe } from './pipes/item-grade-color';
import { StoreItemService } from './services/store-item.service';
import { UseClassCompatiblePipe, UseClassPipe } from './pipes/class';
import { ItemStatListComponent } from './components/item-stat-list/item-stat-list.component';
import { ComponentWithTooltipComponent } from './components/component-with-tooltip/component-with-tooltip.component';
import { SessionSettingsModule } from 'session-settings/session-settings.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AbStateComponent } from './components/ab-state/ab-state.component';
import { AbStateIconComponent } from './components/ab-state/ab-state-icon/ab-state-icon.component';
import { AbStateEffectPipe } from './pipes/ab-state-effect';
import { FiestaMinutesTimePipe, FiestaSecondsTimePipe } from './pipes/fiesta-time';
import { AdditionalSkillEffectPipe } from './pipes/additional-skill-effect.pipe';
import { PictureComponent } from './components/picture/picture.component';
import { FloorPipe } from './pipes/floor.pipe';
import { MobPortraitComponent } from './components/mob-portrait/mob-portrait.component';
import { MapService } from './services/map.service';
import { MultiSelectionControlComponent } from './components/multi-selection-control/multi-selection-control.component';
import {
    MultiSelectionOptionComponent
} from './components/multi-selection-control/multi-selection-option/multi-selection-option.component';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { LevelRangeFilterComponent } from './components/level-range-filter/level-range-filter.component';
import { MobService } from './services/mob.service';
import { QuestService } from './services/quest.service';
import { RouterModule } from '@angular/router';
import { MobClassIconComponent } from './components/mob-class-icon/mob-class-icon.component';
import { MonsterClassPipe, MonsterClassNamePipe } from './pipes/monster-class.pipe';
import { MonsterCategoryPipe, MonsterCategoryNamePipe } from './pipes/monster-category.pipe';
import { MobIconComponent } from './components/mob-icon/mob-icon.component';
import { ItemComponent } from './components/item/item.component';
import { ItemIconComponent } from './components/item/item-icon/item-icon.component';
import { ItemTooltipComponent } from './components/item/item-tooltip/item-tooltip.component';
import { ItemUpgradeComponent } from './components/item-upgrade/item-upgrade.component';
import { TableNoResultsFoundComponent } from './components/table-no-results-found/table-no-results-found.component';
import { MonsterLocationPipe } from './pipes/monster-location';
import { ClassPipe } from './pipes/class-name';
import { MapPickerComponent } from './components/map-picker/map-picker.component';
import { RichTextComponent } from './components/rich-text/rich-text.component';
import { RarityComboboxComponent } from './components/rarity-combobox/rarity-combobox.component';
import { MobSpeciesPortraitComponent } from './components/mob-species-portrait/mob-species-portrait.component';
import { ItemActionEffectPipe, ItemActionEffectGroupPipe } from './pipes/item-action-effect';
import { CardsService } from './services/cards.service';
import { ProductionService } from './services/production.service';
import { CardTooltipComponent } from './components/collection-card/card-tooltip/card-tooltip.component';
import { CollectionCardComponent } from './components/collection-card/collection-card.component';
import { IconComponent } from './components/icon/icon.component';
import { AdsenseModule } from '../adsense/adsense.module'
import { MatInputModule } from '@angular/material/input';

@NgModule({
    declarations: [
        // Pipes
        ItemGradeColorPipe,
        UseClassPipe,
        AbStateEffectPipe,
        FiestaSecondsTimePipe,
        AdditionalSkillEffectPipe,
        FloorPipe,
        ClassPipe,
        UseClassCompatiblePipe,
        MonsterClassPipe,
        MonsterClassNamePipe,
        MonsterCategoryPipe,
        MonsterCategoryNamePipe,
        MonsterLocationPipe,
        ItemActionEffectPipe,
        ItemActionEffectGroupPipe,
        FiestaMinutesTimePipe,
        // Components
        PageHeaderComponent,
        PageLoaderComponent,
        ItemsTableComponent,
        IconComponent,
        ItemIconComponent,
        MoneyComponent,
        ItemTooltipComponent,
        ItemStatListComponent,
        ComponentWithTooltipComponent,
        AbStateComponent,
        AbStateIconComponent,
        PictureComponent,
        MobPortraitComponent,
        MultiSelectionControlComponent,
        MultiSelectionOptionComponent,
        LevelRangeFilterComponent,
        MobClassIconComponent,
        MobIconComponent,
        ItemComponent,
        ItemUpgradeComponent,
        TableNoResultsFoundComponent,
        MapPickerComponent,
        RichTextComponent,
        RarityComboboxComponent,
        MobSpeciesPortraitComponent,
        CollectionCardComponent,
        CardTooltipComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SessionSettingsModule,
        RouterModule,
        // Angular Material Modules
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatTableModule,
        MatSortModule,
        MatSelectModule,
        MatCheckboxModule,
        MatInputModule,
        AdsenseModule
    ],
    exports: [
        PageHeaderComponent,
        PageLoaderComponent,
        ItemsTableComponent,
        ItemIconComponent,
        AbStateComponent,
        MoneyComponent,
        ItemTooltipComponent,
        ItemStatListComponent,
        MobPortraitComponent,
        MobSpeciesPortraitComponent,
        PictureComponent,
        MultiSelectionControlComponent,
        MultiSelectionOptionComponent,
        LevelRangeFilterComponent,
        MobClassIconComponent,
        MobIconComponent,
        ItemComponent,
        CollectionCardComponent,
        TableNoResultsFoundComponent,
        MapPickerComponent,
        RichTextComponent,
        RarityComboboxComponent,
        ItemGradeColorPipe,
        UseClassPipe,
        FiestaSecondsTimePipe,
        AdditionalSkillEffectPipe,
        FloorPipe,
        ClassPipe,
        UseClassCompatiblePipe,
        MonsterClassPipe,
        MonsterClassNamePipe,
        MonsterCategoryPipe,
        MonsterCategoryNamePipe,
        MonsterLocationPipe,
        ItemActionEffectPipe,
        ItemActionEffectGroupPipe,
    ],
    providers: [
        ItemService,
        CardsService,
        ProductionService,
        StoreItemService,
        UseClassPipe,
        AbStateEffectPipe,
        FiestaSecondsTimePipe,
        FiestaMinutesTimePipe,
        AdditionalSkillEffectPipe,
        MapService,
        MobService,
        QuestService,
        DecimalPipe,
        ClassPipe,
    ],
    entryComponents: [
        ItemTooltipComponent,
        CardTooltipComponent,
    ]
})
export class FiestaCommonModule { }
