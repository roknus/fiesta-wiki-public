import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NifviewerPageComponent } from './nifviewer-page.component';

describe('NifviewerPageComponent', () => {
  let component: NifviewerPageComponent;
  let fixture: ComponentFixture<NifviewerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NifviewerPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NifviewerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
