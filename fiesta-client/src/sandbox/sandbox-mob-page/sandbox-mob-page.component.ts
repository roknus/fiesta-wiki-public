import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MobDropInfo } from '@fiesta-common/dto/mobs/mob-drop-info';
import { MobInfo } from '@fiesta-common/dto/mobs/mob-info';
import { MobService } from '@fiesta-common/services/mob.service';
import { asapScheduler, forkJoin, scheduled } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-sandbox-mob-page',
    templateUrl: './sandbox-mob-page.component.html',
    styleUrls: ['./sandbox-mob-page.component.scss']
})
export class SandboxMobPageComponent implements OnInit {

    mob: MobInfo;
    mobDrop: MobDropInfo[];

    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        private mobService: MobService
    ) { }

    ngOnInit(): void {

        this.route.paramMap.pipe(
            switchMap(param => {
                let index = 0;

                if (param.has('index'))
                    index = +param.get('index');

                return forkJoin( 
                    {
                        mobInfo: this.mobService.getMobInfo(index),
                        mobDropInfo: this.mobService.getMobDropInfo(index)
                    }
                );
            }),
            catchError(err => {
                return scheduled([null], asapScheduler);
            })
        )
        .subscribe(
            res => {
                this.mob = res.mobInfo;
                this.mobDrop = res.mobDropInfo;
            }
        );
    }

}
