import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SandboxMobPageComponent } from './sandbox-mob-page.component';

describe('SandboxMobPageComponent', () => {
  let component: SandboxMobPageComponent;
  let fixture: ComponentFixture<SandboxMobPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SandboxMobPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SandboxMobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
