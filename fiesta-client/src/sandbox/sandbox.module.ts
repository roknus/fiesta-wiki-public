import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiestaCommonModule } from '@fiesta-common/fiesta-common.module';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { SandboxPageComponent } from './sandbox-page/sandbox-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { SandboxItemPageComponent } from './sandbox-item-page/sandbox-item-page.component';
import { SandboxMobPageComponent } from './sandbox-mob-page/sandbox-mob-page.component';
import { NifviewerPageComponent } from './nifviewer-page/nifviewer-page.component';

const routes: Routes = [
    {
        path: 'sandbox', component: SandboxPageComponent, children: [
            {
                path: 'item/:index', component : SandboxItemPageComponent
            },
            {
                path: 'item', redirectTo: '/sandbox'
            },
            {
                path: 'mob/:index', component : SandboxMobPageComponent
            },
            {
                path: 'mob', redirectTo: '/sandbox'
            },
            {
                path: 'nifviewer/:index', component : NifviewerPageComponent
            },
            {
                path: 'nifviewer', redirectTo: '/sandbox'
            }
        ]
    }
];

@NgModule({
    declarations: [
        SandboxPageComponent,
        SandboxItemPageComponent,
        SandboxMobPageComponent,
        NifviewerPageComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatRadioModule,
        FiestaCommonModule,
    ],
    exports: [RouterModule]
})
export class SandboxModule { }
