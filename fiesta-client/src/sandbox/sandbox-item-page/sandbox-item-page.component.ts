import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Item } from '@fiesta-common/dto/items/item';
import { MobInfo } from '@fiesta-common/dto/mobs/mob-info';
import { ItemService } from '@fiesta-common/services/item.service';
import { MobService } from '@fiesta-common/services/mob.service';
import { asapScheduler, forkJoin, Observable, scheduled } from 'rxjs';
import { catchError, defaultIfEmpty, switchMap } from 'rxjs/operators';
@Component({
    selector: 'app-sandbox-item-page',
    templateUrl: './sandbox-item-page.component.html',
    styleUrls: ['./sandbox-item-page.component.scss']
})
export class SandboxItemPageComponent implements OnInit {

    obs$: Observable<[Item, MobInfo[]]>;

    item: Item;

    sources: MobInfo[];

    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        private itemService: ItemService,
        private mobService: MobService
    ) { }

    ngOnInit(): void {

        this.route.paramMap.pipe(
            switchMap(param => {
                let index = 0;

                if (param.has('index')) {
                    index = +param.get('index');
                }

                return forkJoin([
                    this.itemService.getItem(index),
                    this.mobService.getItemSources(index)
                ]).pipe(
                    defaultIfEmpty([])
                );
            }),
            catchError(err => {
                return scheduled([null], asapScheduler);
            })
        ).subscribe(
            ([item, mobs]) => {
                this.item = item;
                this.sources = mobs;
            }
        );
    }
}
