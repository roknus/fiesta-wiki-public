import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SandboxItemPageComponent } from './sandbox-item-page.component';

describe('SandboxItemPageComponent', () => {
  let component: SandboxItemPageComponent;
  let fixture: ComponentFixture<SandboxItemPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SandboxItemPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SandboxItemPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
