import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'fiestaUnitToMeter' })
export class FiestaUnitToMeterPipe implements PipeTransform {
  transform(range: number): number {
      if (range === 0) {
          return 1.5;
      } else {
        return range * 0.03;
      }

  }
}
