import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';
import { combineLatest, EMPTY, forkJoin } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { SkillsService } from '@fiesta-common/services/skills.service';
import { PageDescriptionService } from '@services/page-description.service';
import { ActiveSkill, SkillEmpowerement } from '@fiesta-common/dto/skills/active-skill';
import { UseClassUtils } from '@app/enums/useclass';
import { PassiveSkill } from '@fiesta-common/dto/skills/passive-skill';
import { UseClassPipe } from '@fiesta-common/pipes/class';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { LoadingStatus } from '@enums/loading-return';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-skills-page',
    templateUrl: './skills-page.component.html',
    styleUrls: ['./skills-page.component.scss']
})
export class SkillsPageComponent implements OnInit {

    readonly pageTitle = PAGES_NAME.skills;

    empMap = new Map<string, SkillEmpowerement>();

    // reduce brightness
    inactiveColorMatrix =
        `0.08 0 0 0 0 ` +
        `0 0.08 0 0 0 ` +
        `0 0 0.08 0 0 ` +
        `0 0 0 1 0`;

    pageLoaded = false;

    activeSkillList: ActiveSkill[] = [];
    passiveSkillList: PassiveSkill[] = [];

    selectedSkills: {
        skills: ActiveSkill[] | PassiveSkill[],
        type: string
    } = { skills: [], type: '' };

    loadingErr: LoadingStatus = 'loading';

    filteredClass = 1;

    get tierMinLevel(): number {
        return this.getMinLevelForClass(this.filteredClass);
    }
    get tierMaxLevel(): number {
        return this.getMaxLevelForClass(this.filteredClass);
    }

    get maxLevel(): number {
        return fiestaMaxLevel;
    }

    level = 0;

    skillPoints = 0;
    totalSkillPoints = 0;

    levelFormControl = new FormControl('');

    // tslint:disable-next-line: variable-name
    _powerUpgrade: number[] = [0, 0, 0, 0, 0];
    get powerUpgrade(): Array<number> {
        if (this.selectedSkills && this.selectedSkills.skills.length) {
            const s = this.selectedSkills.skills[0] as ActiveSkill;
            this._powerUpgrade = this._powerUpgrade.map((x, i) => s.skill_empowerment.power < i ? 0 : 1);
        }
        return this._powerUpgrade;
    }

    // tslint:disable-next-line: variable-name
    _spUpgrade: number[] = [0, 0, 0, 0, 0];
    get spUpgrade(): Array<number> {
        if (this.selectedSkills && this.selectedSkills.skills.length) {
            const s = this.selectedSkills.skills[0] as ActiveSkill;
            this._spUpgrade = this._spUpgrade.map((x, i) => s.skill_empowerment.sp < i ? 0 : 1);
        }
        return this._spUpgrade;
    }

    // tslint:disable-next-line: variable-name
    _durationUpgrade: number[] = [0, 0, 0, 0, 0];
    get durationUpgrade(): Array<number> {
        if (this.selectedSkills && this.selectedSkills.skills.length) {
            const s = this.selectedSkills.skills[0] as ActiveSkill;
            this._durationUpgrade = this._durationUpgrade.map((x, i) => s.skill_empowerment.duration < i ? 0 : 1);
        }
        return this._durationUpgrade;
    }

    // tslint:disable-next-line: variable-name
    _cooldownUpgrade: number[] = [0, 0, 0, 0, 0];
    get cooldownUpgrade(): Array<number> {
        if (this.selectedSkills && this.selectedSkills.skills.length) {
            const s = this.selectedSkills.skills[0] as ActiveSkill;
            this._cooldownUpgrade = this._cooldownUpgrade.map((x, i) => s.skill_empowerment.cooldown < i ? 0 : 1);
        }
        return this._cooldownUpgrade;
    }

    constructor(
        protected pageDescriptionService: PageDescriptionService,
        private router: Router,
        private route: ActivatedRoute,
        private skillsService: SkillsService,
        private classPipe: UseClassPipe
    ) { }

    ngOnInit() {
        this.pageDescriptionService.setPageDescription({
            title: PAGES_NAME.skills,
            description: 'List of the Fiesta Online skills per class and level. You can use skill points to see the upgrades of each skill.'
        });

        this.route.paramMap.pipe(
            map(results => {
                if (results.has('class')) {
                    return +results.get('class');
                }

                return 2;
            })).subscribe(
                useClass => {
                    this.filteredClass = useClass;

                    const className = this.classPipe.transform(useClass);

                    this.pageDescriptionService.setTitle(className, PAGES_NAME.skills);
                }
            );

        this.route.paramMap.pipe(
            switchMap(results => {

                this.loadingErr = 'loading';

                let selectedClass = 2;
                if (results.has('class')) {
                    selectedClass = +results.get('class');
                }

                let level = this.getMaxLevelForClass(selectedClass);
                if (results.has('level')) {
                    level = +results.get('level');
                }

                level = Math.max(this.getMinLevelForClass(selectedClass), Math.min(level, fiestaMaxLevel));

                return forkJoin(
                    {
                        active: this.skillsService.getActiveSkillList(selectedClass, level),
                        passive: this.skillsService.getPassiveSkillList(selectedClass, level)
                    }
                ).pipe(
                    map(res => {
                        return {
                            level,
                            active: res.active,
                            passive: res.passive
                        };
                    })
                );
            })
        ).subscribe(
            res => {

                this.activeSkillList = res.active;
                this.passiveSkillList = res.passive;
                this.level = res.level;

                this.activeSkillList.forEach(s => {
                    if (!this.empMap.has(s.group_name)) {
                        this.empMap.set(s.group_name, new SkillEmpowerement());
                    }
                    s.skill_empowerment = this.empMap.get(s.group_name);
                });

                this.levelFormControl.setValidators([
                    Validators.min(this.tierMinLevel),
                    Validators.max(this.maxLevel)
                ]);

                this.onReset();

                if (this.activeSkillList && this.route.snapshot.queryParamMap.keys.length === 0) {
                    this.router.navigate([], {
                        queryParams: { 'active-skill': this.activeSkillList[0].group_name }
                    });
                }

                this.loadingErr = 'ok';
            },
            err => {
                this.loadingErr = 'error';
            }
        );

        combineLatest([this.route.queryParamMap, this.route.paramMap]).pipe(
            switchMap(([querParams, params]: [ParamMap, ParamMap]) => {

                let selectedClass = 2;
                if (params.has('class')) {
                    selectedClass = +params.get('class');
                }

                let level = this.getMaxLevelForClass(selectedClass);
                if (params.has('level')) {
                    level = +params.get('level');
                }

                level = Math.max(this.getMinLevelForClass(selectedClass), Math.min(level, fiestaMaxLevel));

                let skillGroup = '';

                if (querParams.has('active-skill')) {
                    skillGroup = querParams.get('active-skill');
                    return this.skillsService.getActiveSkillGroup(selectedClass, skillGroup, level).pipe(
                        map(group => {
                            return {
                                group,
                                type: 'active'
                            };
                        })
                    );
                }
                else if (querParams.has('passive-skill')) {
                    skillGroup = querParams.get('passive-skill');
                    return this.skillsService.getPassiveSkillGroup(selectedClass, skillGroup, level).pipe(
                        map(group => {
                            return {
                                group,
                                type: 'passive'
                            };
                        })
                    );
                }

                return EMPTY;
            })
        ).subscribe(result => {
            if (result) {
                if (result.group.length) {
                    result.group.forEach(s => {
                        if (!this.empMap.has(s.group_name)) {
                            this.empMap.set(s.group_name, new SkillEmpowerement());
                        }
                        s.skill_empowerment = this.empMap.get(s.group_name);
                    });

                    this.selectedSkills.skills = result.group;
                    this.selectedSkills.type = result.type;
                } else {
                    this.selectedSkills.type = 'none';
                }
            }
        });
    }

    getMinLevelForClass(useClass: number) {
        switch (UseClassUtils.classTier(useClass)) {
            case 0:
                return 1;
            case 1:
                return 20;
            case 2:
                return 60;
            case 3:
                return 100;
        }

        return 1;
    }

    getMaxLevelForClass(useClass: number) {
        switch (UseClassUtils.classTier(useClass)) {
            case 0:
                return 19;
            case 1:
                return 59;
            case 2:
                return 99;
            case 3:
                return fiestaMaxLevel;
        }

        return 1;
    }

    onClassChange(c: number) {
        this.filteredClass = c;
    }

    onFilter() {
        this.router.navigate(['/skills', this.filteredClass]);
    }

    onLevelChange() {
        this.router.navigate(['/skills', this.filteredClass, this.level], {
            queryParamsHandling: 'merge'
        });
    }

    onReset() {
        this.resetSkillPoints();

        this.activeSkillList.forEach(s =>
            s.skill_empowerment.reset()
        );
    }

    resetSkillPoints() {
        // Untill 135 we get one skill point for each odd level except level 1
        // After 135 we get a skill point for each even level (136-138-140)
        this.totalSkillPoints = Math.trunc((this.level - (this.level <= 135 ? 1 : 0)) / 2);
        this.skillPoints = this.totalSkillPoints;
    }

    onEmpowerUp(index: number) {
        if (this.selectedSkills == null || this.skillPoints <= 0) {
            return;
        }

        const s = this.selectedSkills.skills[0] as ActiveSkill;
        switch (index) {
            case 0:
                if (s.empowerment.power.length > 0 && s.skill_empowerment.power < 4) {
                    s.skill_empowerment.power++;
                    this.skillPoints--;
                }
                break;
            case 1:
                if (s.empowerment.sp.length > 0 && s.skill_empowerment.sp < 4) {
                    s.skill_empowerment.sp++;
                    this.skillPoints--;
                }
                break;
            case 2:
                if (s.empowerment.duration.length > 0 && s.skill_empowerment.duration < 4) {
                    s.skill_empowerment.duration++;
                    this.skillPoints--;
                }
                break;
            case 3:
                if (s.empowerment.cooldown.length > 0 && s.skill_empowerment.cooldown < 4) {
                    s.skill_empowerment.cooldown++;
                    this.skillPoints--;
                }
                break;
            default:
                return;
        }
    }

    onEmpowerDown(index: number) {

        const s = this.selectedSkills.skills[0] as ActiveSkill;

        switch (index) {
            case 0:
                if (s.skill_empowerment.power > -1) {
                    s.skill_empowerment.power--;
                    this.skillPoints++;
                }
                break;
            case 1:
                if (s.skill_empowerment.sp > -1) {
                    s.skill_empowerment.sp--;
                    this.skillPoints++;
                }
                break;
            case 2:
                if (s.skill_empowerment.duration > -1) {
                    s.skill_empowerment.duration--;
                    this.skillPoints++;
                }
                break;
            case 3:
                if (s.skill_empowerment.cooldown > -1) {
                    s.skill_empowerment.cooldown--;
                    this.skillPoints++;
                }
                break;
        }
    }
}
