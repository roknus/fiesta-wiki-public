import { Component, Input, OnInit } from '@angular/core';
import { ItemAction, SetEffect } from '@fiesta-common/dto/items/set-effect';
import { ActiveSkill } from '@fiesta-common/dto/skills/active-skill';
import { ItemService } from '@fiesta-common/services/item.service';
import { combineLatest, concat, forkJoin } from 'rxjs';
import { defaultIfEmpty, filter, flatMap, map, mergeMap, switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-skill-info',
    templateUrl: './skill-info.component.html',
    styleUrls: ['./skill-info.component.scss']
})
export class SkillInfoComponent implements OnInit {

    // tslint:disable-next-line: variable-name
    _skill: ActiveSkill = null;
    @Input()
    get skill(): ActiveSkill { return this._skill; }
    set skill(s: ActiveSkill) {
        this._skill = s;
        this.fetchSkillItemSet(s);
    }

    @Input()
    useClass: number;

    setEffects: SetEffect[] = [];

    itemActions = new Map<number, { action: ItemAction, effects: SetEffect[] }>();

    constructor(private itemService: ItemService) { }

    ngOnInit(): void {
    }

    fetchSkillItemSet(s: ActiveSkill) {
        console.log(s.groups_id);

        this.setEffects = [];
        forkJoin(s.groups_id.map(group => this.itemService.getSkillItemSet(this.useClass, group))).pipe(
            map(result => [].concat(...result) as SetEffect[])
        ).subscribe(result => {
            this.setEffects = result;
        });

        this.itemActions = new Map<number, { action: ItemAction, effects: SetEffect[] }>();
        forkJoin(s.groups_id.map(group => this.itemService.getSkillItemSet(this.useClass, group))).pipe(
            map(result => [].concat(...result) as SetEffect[]),
            switchMap(res => {
                return forkJoin(res.map(e => e.item_actions.pipe(
                    map(itemActions => {
                        const actions = itemActions.filter(itemAction => {
                            return this.skill.groups_id.includes(itemAction.condition.skill_group.id);
                        });
                        console.log(actions);
                        return { actions, effect: e };
                    })
                )));
            }),
            defaultIfEmpty<{ actions: ItemAction[], effect: SetEffect }[]>([])
        ).subscribe(result => {
            result.forEach(r => {
                r.actions.forEach(itemAction => {
                    const itemActionEffectActivityId = itemAction.effect.effect_activity[1];
                    if (!this.itemActions.has(itemActionEffectActivityId)) {
                        this.itemActions.set(itemActionEffectActivityId, { action: itemAction, effects: [] });
                    }
                    this.itemActions.get(itemActionEffectActivityId).effects.push(r.effect);
                });
            });
            console.log(this.itemActions);
        });
    }
}
