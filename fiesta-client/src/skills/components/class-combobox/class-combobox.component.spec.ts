import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassComboboxComponent } from './class-combobox.component';

describe('ClassComboboxComponent', () => {
  let component: ClassComboboxComponent;
  let fixture: ComponentFixture<ClassComboboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassComboboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassComboboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
