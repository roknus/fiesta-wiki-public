import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PassiveSkillTiersComponent } from './passive-skill-tiers.component';

describe('PassiveSkillTiersComponent', () => {
  let component: PassiveSkillTiersComponent;
  let fixture: ComponentFixture<PassiveSkillTiersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PassiveSkillTiersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassiveSkillTiersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
