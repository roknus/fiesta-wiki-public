import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PassiveSkill } from '@fiesta-common/dto/skills/passive-skill';

@Component({
   selector: 'app-passive-skill-tiers',
   templateUrl: './passive-skill-tiers.component.html',
   styleUrls: ['./passive-skill-tiers.component.scss']
})
export class PassiveSkillTiersComponent implements OnInit {

   displayedColumns: string[] = [];
   dataSource: MatTableDataSource<PassiveSkill>;

   skills: PassiveSkill[];

   expandedRows: PassiveSkill[] = [];

   @Input('skillList')
   set setSkillList(list: PassiveSkill[]) {
      this.skills = list;
      const s = this.skills[0];
      this.dataSource = new MatTableDataSource<PassiveSkill>(this.skills);

      this.displayedColumns = ['icon', 'name'];

      if (s.mastery_sword_dmg_rate > 0) { this.displayedColumns.push('mastery1HDmgRate'); }
      if (s.mastery_hammer_dmg_rate > 0) { this.displayedColumns.push('masteryHammerDmgRate'); }
      if (s.mastery_two_hand_dmg_rate > 0) { this.displayedColumns.push('mastery2HDmgRate'); }
      if (s.mastery_axe_dmg_rate > 0) { this.displayedColumns.push('masteryAxeDmgRate'); }
      if (s.mastery_mace_dmg_rate > 0) { this.displayedColumns.push('masteryMaceDmgRate'); }
      if (s.mastery_bow_dmg_rate > 0) { this.displayedColumns.push('masteryBowDmgRate'); }
      if (s.mastery_crossbow_dmg_rate > 0) { this.displayedColumns.push('masteryCrossbowDmgRate'); }
      if (s.mastery_wand_dmg_rate > 0) { this.displayedColumns.push('masteryWandDmgRate'); }
      if (s.mastery_staff_dmg_rate > 0) { this.displayedColumns.push('masteryStaffDmgRate'); }
      if (s.mastery_claw_dmg_rate > 0) { this.displayedColumns.push('masteryClawDmgRate'); }
      if (s.mastery_dsword_dmg_rate > 0) { this.displayedColumns.push('masteryDSDmgRate'); }

      if (s.mastery_sword_dmg > 0) { this.displayedColumns.push('mastery1HDmg'); }
      if (s.mastery_hammer_dmg > 0) { this.displayedColumns.push('masteryHammerDmg'); }
      if (s.mastery_two_hand_dmg > 0) { this.displayedColumns.push('mastery2HDmg'); }
      if (s.mastery_axe_dmg > 0) { this.displayedColumns.push('masteryAxeDmg'); }
      if (s.mastery_mace_dmg > 0) { this.displayedColumns.push('masteryMaceDmg'); }
      if (s.mastery_bow_dmg > 0) { this.displayedColumns.push('masteryBowDmg'); }
      if (s.mastery_crossbow_dmg > 0) { this.displayedColumns.push('masteryCrossbowDmg'); }
      if (s.mastery_wand_dmg > 0) { this.displayedColumns.push('masteryWandDmg'); }
      if (s.mastery_staff_dmg > 0) { this.displayedColumns.push('masteryStaffDmg'); }
      if (s.mastery_claw_dmg > 0) { this.displayedColumns.push('masteryClawDmg'); }
      if (s.mastery_dsword_dmg > 0) { this.displayedColumns.push('masteryDSDmg'); }

      if (s.evasion > 0) { this.displayedColumns.push('evasion'); }
      if (s.sp_increase > 0) { this.displayedColumns.push('SPIncrease'); }
      if (s.lp_increase > 0) { this.displayedColumns.push('LPIncrease'); }
      if (s.phy_atk_up_rate > 0) { this.displayedColumns.push('phyAtkUpRate'); }
      if (s.mag_atk_up_rate > 0) { this.displayedColumns.push('magAtkUpRate'); }
      if (s.dmg_up_per_hp_down > 0) { this.displayedColumns.push('dmgUpPerHPDown'); }
      if (s.def_up_per_hp_down > 0) { this.displayedColumns.push('defUpPerHPDown'); }
      if (s.movement_increase_evasion_up > 0) { this.displayedColumns.push('movementIncreaseEvaUp'); }
      if (s.heal_up_rate > 0) { this.displayedColumns.push('healUpRate'); }
      if (s.buff_duration_up_rate > 0) { this.displayedColumns.push('buffDurationUpRate'); }
      if (s.crit_dmg_up_rate > 0) { this.displayedColumns.push('critDmgUpRate'); }

      if (s.dmg_receive_minus_rate > 0) { this.displayedColumns.push('dmgReceiveMinusRate'); }

   }

   constructor() { }

   ngOnInit() {
   }

   hasAbStates(skill: PassiveSkill) {
      return skill.passive_ab_state && skill.passive_ab_state.ab_states.length > 0;
   }
}
