import { Component, Input, HostListener } from '@angular/core';
import { ActiveSkill } from '@fiesta-common/dto/skills/active-skill';
import { SkillIconComponent } from '../skill-icon.component';
import { TooltipService } from 'tooltip/tooltip.service';
import { TooltipComponent } from 'tooltip/tooltip/tooltip.component';
import { ActiveSkillTooltipComponent } from '../../skill-tooltip/active/active-skill-tooltip.component';

@Component({
   selector: 'app-active-skill-icon',
   templateUrl: './active-skill-icon.component.html',
   styleUrls: ['./active-skill-icon.component.scss']
})
export class ActiveSkillIconComponent extends SkillIconComponent {

   activeSkill: ActiveSkill;

   @Input('skill')
   set skill(skill: ActiveSkill) {
      this.activeSkill = skill;
      this.skillView = skill.view;
   }

   get powerUpgrade(): Array<number> {
      return new Array<number>(this.activeSkill.skill_empowerment.power + 1).fill(0).map((x, i) => i);
   }

   get spUpgrade(): Array<number> {
      return new Array<number>(this.activeSkill.skill_empowerment.sp + 1).fill(0).map((x, i) => i);
   }

   get durationUpgrade(): Array<number> {
      return new Array<number>(this.activeSkill.skill_empowerment.duration + 1).fill(0).map((x, i) => i);
   }

   get cooldownUpgrade(): Array<number> {
      return new Array<number>(this.activeSkill.skill_empowerment.cooldown + 1).fill(0).map((x, i) => i);
   }

   tooltip: TooltipComponent<ActiveSkillTooltipComponent>;

   @HostListener('mouseenter', ['$event'])
   mouseEnter(event): void {

      if (!this.tooltip) {
         this.tooltipRef = this.tooltipService.appendComponentToBody(ActiveSkillTooltipComponent, event.target);
         this.tooltip = this.tooltipRef.instance;
         this.tooltip.view.subscribe(comp => {
            if (comp) {
               comp.skill = this.activeSkill;
            }
         });
      }

      this.tooltip.fadeIn();
   }

   @HostListener('mouseleave', ['$event'])
   mouseLeave(event): void {
      this.tooltip.fadeOut(event);
   }

   constructor(
      private tooltipService: TooltipService
      ) {
         super();
      }
}
