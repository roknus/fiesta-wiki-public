import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActiveSkillIconComponent } from './active-skill-icon.component';

describe('ActiveSkillIconComponent', () => {
  let component: ActiveSkillIconComponent;
  let fixture: ComponentFixture<ActiveSkillIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveSkillIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveSkillIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
