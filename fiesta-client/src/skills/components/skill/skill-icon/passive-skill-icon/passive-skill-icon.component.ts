import { Component, Input, HostListener } from '@angular/core';
import { PassiveSkill } from '@fiesta-common/dto/skills/passive-skill';
import { TooltipService } from 'tooltip/tooltip.service';
import { TooltipComponent } from 'tooltip/tooltip/tooltip.component';
import { PassiveSkillTooltipComponent } from '../../skill-tooltip/passive/passive-skill-tooltip.component';
import { SkillIconComponent } from '../skill-icon.component';

@Component({
    selector: 'app-passive-skill-icon',
    templateUrl: './passive-skill-icon.component.html',
    styleUrls: ['./passive-skill-icon.component.scss']
})
export class PassiveSkillIconComponent extends SkillIconComponent {

    passiveSkill: PassiveSkill;

    @Input()
    set skill(skill: PassiveSkill) {
        this.passiveSkill = skill;
        this.skillView = skill.view;
    }

    tooltip: TooltipComponent<PassiveSkillTooltipComponent>;

    @HostListener('mouseenter', ['$event'])
    mouseEnter(event): void {

        if (!this.tooltip) {
            this.tooltipRef = this.tooltipService.appendComponentToBody(PassiveSkillTooltipComponent, event.target);
            this.tooltip = this.tooltipRef.instance;
            this.tooltip.view.subscribe(comp => {
                if (comp) {
                    comp.skill = this.passiveSkill;
                }
            });
        }

        this.tooltip.fadeIn();
    }

    @HostListener('mouseleave', ['$event'])
    mouseLeave(event): void {
        this.tooltip.fadeOut(event);
    }

    constructor(
        private tooltipService: TooltipService
    ) {
        super();
    }
}
