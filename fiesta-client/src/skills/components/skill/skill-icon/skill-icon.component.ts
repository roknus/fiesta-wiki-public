import { Component, OnInit } from '@angular/core';
import { environment } from '@environments/environment';
import { SkillView } from '@fiesta-common/dto/skills/skill-view';
import { ComponentWithTooltipComponent } from '@fiesta-common/components/component-with-tooltip/component-with-tooltip.component';

@Component({
   selector: 'app-skill-icon',
   template: '',
   styles: ['']
})
export class SkillIconComponent extends ComponentWithTooltipComponent implements OnInit {

   STATIC_ASSETS = environment.static_assets + 'icons/';

   skillView: SkillView;

   get colorMatrix(): string {

      const r = 1 + (this.skillView.red / 255);
      const g = 1 + (this.skillView.green / 255);
      const b = 1 + (this.skillView.blue / 255);

      const matrix =
         `${r} 0 0 0 0 ` +
         `0 ${g} 0 0 0 ` +
         `0 0 ${b} 0 0 ` +
         `0 0 0 1 0`;

      return matrix;
   }

   constructor(
   ) {
      super();
   }

   ngOnInit(): void {
   }
}
