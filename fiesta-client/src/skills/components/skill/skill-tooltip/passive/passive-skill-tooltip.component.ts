import { Component, OnInit } from '@angular/core';
import { PassiveSkill } from '@fiesta-common/dto/skills/passive-skill';

@Component({
   selector: 'app-passive-skill-tooltip',
   templateUrl: './passive-skill-tooltip.component.html',
   styleUrls: ['./passive-skill-tooltip.component.scss']
})
export class PassiveSkillTooltipComponent implements OnInit {

   skill: PassiveSkill;

   constructor() { }

   ngOnInit() {
   }

}
