import { Component, Input } from '@angular/core';
import { ActiveSkill } from '@fiesta-common/dto/skills/active-skill';

@Component({
    selector: 'app-skill-tooltip',
    templateUrl: './active-skill-tooltip.component.html',
    styleUrls: ['./active-skill-tooltip.component.scss']
})
export class ActiveSkillTooltipComponent {

    @Input()
    skill: ActiveSkill;

    constructor() { }
}
