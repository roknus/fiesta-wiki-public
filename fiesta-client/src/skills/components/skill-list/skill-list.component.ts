import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ActiveSkill } from '@fiesta-common/dto/skills/active-skill';
import { PassiveSkill } from '@fiesta-common/dto/skills/passive-skill';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-skill-list',
    templateUrl: './skill-list.component.html',
    styleUrls: ['./skill-list.component.scss']
})
export class SkillListComponent implements OnInit {

    // tslint:disable-next-line: variable-name
    _activeSkills: ActiveSkill[] = [];

    @Input()
    get activeSkills(): ActiveSkill[] { return this._activeSkills; }
    set activeSkills(skills: ActiveSkill[]) {
        this._activeSkills = skills;
        this.updateSelectedSkill();
    }

    // tslint:disable-next-line: variable-name
    _passiveSkills: PassiveSkill[] = [];

    @Input()
    get passiveSkills(): PassiveSkill[] { return this._passiveSkills; }
    set passiveSkills(skills: PassiveSkill[]) {
        this._passiveSkills = skills;
        this.updateSelectedSkill();
    }

    selectedSkill: ActiveSkill | PassiveSkill = null;

    constructor(
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.queryParamMap.pipe(
            map(queryparams => this.parseQuerySkill(queryparams))
        ).subscribe(skill => {
            this.selectedSkill = skill;
        });
    }

    updateSelectedSkill() {
        this.selectedSkill = this.parseQuerySkill(this.route.snapshot.queryParamMap);
    }

    parseQuerySkill(paramMap: ParamMap): ActiveSkill | PassiveSkill {
        if (paramMap.has('active-skill')) {
            return this.activeSkills.find(s => s.group_name === paramMap.get('active-skill'));
        } else if (paramMap.has('passive-skill')) {
            return this.passiveSkills.find(s => s.group_name === paramMap.get('passive-skill'));
        }
        return null;
    }

    onActiveSkillSelected(s: ActiveSkill) {
        this.router.navigate([], {
            queryParams: { 'active-skill': s.group_name }
        });
    }

    onPassiveSkillSelected(s: PassiveSkill) {
        this.router.navigate([], {
            queryParams: { 'passive-skill': s.group_name }
        });
    }
}
