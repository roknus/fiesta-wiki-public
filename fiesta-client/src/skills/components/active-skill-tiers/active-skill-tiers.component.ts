import { Component, OnInit, Input } from '@angular/core';
import { ActiveSkill } from '@fiesta-common/dto/skills/active-skill';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
   selector: 'app-active-skill-tiers',
   templateUrl: './active-skill-tiers.component.html',
   styleUrls: ['./active-skill-tiers.component.scss'],
   animations: [
      trigger('detailExpand', [
         state('collapsed', style({ height: '0px', minHeight: '0', borderBottomWidth: '0px'})),
         state('expanded', style({ borderBottomWidth: '1px' })),
         transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      ]),
   ],
})
export class ActiveSkillTiersComponent implements OnInit {

   displayedColumns: string[] = [];
   dataSource: MatTableDataSource<ActiveSkill>;

   skills: ActiveSkill[];

   expandedRows: ActiveSkill[] = [];

   @Input('skillList')
   set setSkillList(list: ActiveSkill[]) {
      this.skills = list;
      this.dataSource = new MatTableDataSource<ActiveSkill>(this.skills);

      this.displayedColumns = ['icon', 'name'];
      if (this.hasPowerEmpowerment(list)) { this.displayedColumns.push('power'); }
      if (this.hasSPEmpowerment(list)) { this.displayedColumns.push('sp'); }
      if (this.hasDurationEmpowerment(list)) { this.displayedColumns.push('duration'); }
      this.displayedColumns.push('cooldown');
   }

   constructor() { }

   ngOnInit() {
   }

   hasPowerEmpowerment(skills: ActiveSkill[]) {
      for (const s of skills) {
         switch (s.skill_type) {
            case 0:
               if (s.min_dmg > 0) { return true; }
               break;
            case 1:
               if (s.min_mdmg > 0) { return true; }
               break;
         }

         if (s.hasHealingEffect) {
            return true;
         }
      }
      return false;
   }

   hasSPEmpowerment(skills: ActiveSkill[]) {
      for (const s of skills) {
         if (s.sp > 0) {
            return true;
         }
      }
      return false;
   }

   hasDurationEmpowerment(skills: ActiveSkill[]) {
      for (const s of skills) {
         if (s.ab_states.length > 0 && s.ab_states[0].sub_ab_state.duration > 0) {
            return true;
         }
      }
      return false;
   }

   onRowClick(skill: ActiveSkill) {
      const index = this.expandedRows.indexOf(skill, 0);
      if (index > -1) {
         this.expandedRows.splice(index, 1);
         return;
      }

      if (this.isRowClickable(skill)) {
         this.expandedRows.push(skill);
      }
   }

   isRowClickable(skill: ActiveSkill) {
      return skill.ab_states.length > 0 || skill.effects.length !== 0;
   }
}
