import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { License, LicenseDTO, LicenseLevel, LicenseLevelDTO } from 'license/dto/licenses';
import { ItemService } from '@fiesta-common/services/item.service';
import { MobService } from '@fiesta-common/services/mob.service';
import { combineLatest, Observable, ReplaySubject } from 'rxjs';
import { defaultIfEmpty, map, switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LicensesService {

    licenseMap = new Map<number, ReplaySubject<License>>();

    constructor(
        private http: HttpClient,
        private mobService: MobService,
        private itemService: ItemService
    ) { }

    getLicensePage(data: {
        page: number,
        size: number,
        sort?: string[],
        active?: boolean
    }): Observable<Page<License>> {
        let params = new HttpParams();

        if (data.page) {
            params = params.set('page', data.page.toString());
        }
        if (data.size) {
            params = params.set('size', data.size.toString());
        }
        if (data.sort) {
            params = params.set('sort', data.sort.join(','));
        }
        if (data.active != null) {
            params = params.set('active', data.active.toString());
        }

        return this.http.get<Page<number>>(environment.api + 'licenses', { params }).pipe(
            switchMap(page => combineLatest(page.content.map(id => this.getLicense(id))).pipe(
                defaultIfEmpty<License[]>([]),
                map(lic => {
                    let licPage = new Page<License>();
                    licPage.number = page.number;
                    licPage.size = page.size;
                    licPage.totalElements = page.totalElements;
                    licPage.totalPages = page.totalPages;
                    licPage.content = lic;

                    return licPage;
                })
            )
            )
        )
    }

    getLicense(id: number): ReplaySubject<License> {

        if (this.licenseMap.has(id)) {
            return this.licenseMap.get(id);
        }

        const obs$ = new ReplaySubject<License>(1);

        this.http.get<LicenseDTO>(environment.api + 'license/' + id).pipe(
            switchMap(res => this.convertLicense(res))
        ).subscribe(
            lic => { obs$.next(lic); },
            err => { obs$.error(err); }
        );

        this.licenseMap.set(id, obs$);

        return obs$;
    }

    private convertLicenseLevel(licenseLevelDTO: LicenseLevelDTO): LicenseLevel {
        if (!licenseLevelDTO) {
            return null;
        }

        const _ = new LicenseLevel();

        _.level = licenseLevelDTO.level;
        _.suffix = licenseLevelDTO.suffix;
        _.mob_kill_count = licenseLevelDTO.mob_kill_count;
        _.dmg_ratio = licenseLevelDTO.dmg_ratio;
        _.crit_ratio = licenseLevelDTO.crit_ratio;

        return _;
    }

    private convertLicense(licenseDTO: LicenseDTO): Observable<License> {
        if (!licenseDTO) {
            return null;
        }

        const _ = new License();

        return combineLatest([
            this.mobService.getMobSpecies(licenseDTO.species_id),
            this.itemService.getItem(licenseDTO.item_id)
        ]).pipe(
            map(result => {
                _.id = licenseDTO.id;
                _.species = result[0];
                _.item = result[1];
                _.levels = licenseDTO.levels.map(l => this.convertLicenseLevel(l));

                return _;
            })
        )
    }
}
