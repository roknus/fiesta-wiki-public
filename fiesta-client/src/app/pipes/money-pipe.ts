import { PipeTransform, Pipe } from '@angular/core';


@Pipe({
   name: 'fiestaMoney'
})
export class FiestaMoneyPipe implements PipeTransform {

   transform(input: number): string {

      let ret = '';

      if (this.gem(input) > 0) {
         ret += '<span class="gem">' + this.gem(input) + '</span>';
      }
      if (this.gold(input) > 0) {
         ret += '<span class="gold">' + this.gold(input) + '</span>';
      }
      if (this.silver(input) > 0) {
         ret += '<span class="silver">' + this.silver(input) + '</span>';
      }

      return ret;
   }

   silver(money: number) {
      return money % 1000;
   }

   gold(money: number) {
      return Math.trunc(money / 1000) % 100;
   }

   gem(money: number) {
      return Math.trunc(money / 100000);
   }
}
