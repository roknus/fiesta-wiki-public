import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'numberSpacer' })
export class NumberSpacerPipe implements PipeTransform {
  transform(num: number): string {
    return num + '';
  }
}
