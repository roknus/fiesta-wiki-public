import { Component, Output, EventEmitter } from '@angular/core';
import { PAGES_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';
import { environment } from '@environments/environment';

@Component({
    selector: 'app-navigation-bar',
    templateUrl: './navigation-bar.component.html',
    styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent {

    readonly LOCALE = environment.locale;

    readonly ROUTES = ROUTES_URL;

    readonly PAGES = PAGES_NAME;

    @Output()
    toggleSidenav = new EventEmitter<boolean>();
}
