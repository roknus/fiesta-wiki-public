import { Component } from '@angular/core';
import { CLASSES_NAME, WEAPONS_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-weapons-menu',
    templateUrl: './weapons-menu.component.html',
    styleUrls: ['./weapons-menu.component.scss']
})
export class WeaponsMenuComponent {

    readonly ROUTES = ROUTES_URL;

    readonly CLASSES = CLASSES_NAME;

    readonly WEAPONS = WEAPONS_NAME;

    constructor() { }
}
