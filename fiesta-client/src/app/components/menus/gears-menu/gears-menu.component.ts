import { Component } from '@angular/core';
import { CLASSES_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-gears-menu',
    templateUrl: './gears-menu.component.html',
    styleUrls: ['./gears-menu.component.scss']
})
export class GearsMenuComponent {

    readonly CLASSES = CLASSES_NAME;

    readonly ROUTES = ROUTES_URL;

    constructor() { }
}
