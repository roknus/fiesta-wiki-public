import { Component } from '@angular/core';
import { PAGES_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-jewels-menu',
    templateUrl: './jewels-menu.component.html',
    styleUrls: ['./jewels-menu.component.scss']
})
export class JewelsMenuComponent {

    readonly ROUTES = ROUTES_URL;

    readonly PAGES = PAGES_NAME;

    constructor() { }
}
