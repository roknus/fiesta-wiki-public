import { Component } from '@angular/core';
import { CLASSES_NAME, PAGES_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-skills-menu',
    templateUrl: './skills-menu.component.html',
    styleUrls: ['./skills-menu.component.scss']
})
export class SkillsMenuComponent {

    readonly ROUTES = ROUTES_URL;

    readonly PAGES = PAGES_NAME;

    readonly CLASSES = CLASSES_NAME;

    constructor() { }
}
