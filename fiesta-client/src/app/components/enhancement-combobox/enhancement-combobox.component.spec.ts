import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EnhancementComboboxComponent } from './enhancement-combobox.component';

describe('EnhancementComboboxComponent', () => {
  let component: EnhancementComboboxComponent;
  let fixture: ComponentFixture<EnhancementComboboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EnhancementComboboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnhancementComboboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
