import { Component, OnInit, Input } from '@angular/core';
import { QuestData } from '@fiesta-common/dto/quests/quest';

@Component({
    selector: 'app-quest-description',
    templateUrl: './quest-description.component.html',
    styleUrls: ['./quest-description.component.scss']
})
export class QuestDescriptionComponent implements OnInit {

    @Input()
    quest: QuestData;

    constructor() { }

    ngOnInit() { }

    hasReward(): boolean {
        return this.quest.exp > 0 ||
            this.quest.money > 0 ||
            this.quest.fame > 0 ||
            this.quest.item_rewards.length > 0 ||
            this.quest.selectable_item_rewards.length > 0;
    }
}
