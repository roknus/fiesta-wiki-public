import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestDescriptionComponent } from './quest-description.component';

describe('QuestDescriptionComponent', () => {
  let component: QuestDescriptionComponent;
  let fixture: ComponentFixture<QuestDescriptionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
