import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestNameComponent } from './quest-name.component';

describe('QuestNameComponent', () => {
  let component: QuestNameComponent;
  let fixture: ComponentFixture<QuestNameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
