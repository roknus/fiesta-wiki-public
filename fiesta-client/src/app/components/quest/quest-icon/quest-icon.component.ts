import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quest-icon',
  templateUrl: './quest-icon.component.html',
  styleUrls: ['./quest-icon.component.scss']
})
export class QuestIconComponent implements OnInit {

  // normal = 0 - 5 - 8
  // epic = 1 - 6 - 7
  // class = 2
  // event = 3
  // chaos = 4
  // event = 10
  @Input()
  questType: number;

  constructor() { }

  ngOnInit() {
  }

}
