import { Component, OnInit, Input } from '@angular/core';
import { QuestLine } from '@fiesta-common/dto/quests/quest';

@Component({
  selector: 'app-quest-line',
  templateUrl: './quest-line.component.html',
  styleUrls: ['./quest-line.component.scss']
})
export class QuestLineComponent implements OnInit {

  @Input()
  questLine: QuestLine;

  @Input()
  current = false;

  constructor() { }

  ngOnInit() {
  }

}
