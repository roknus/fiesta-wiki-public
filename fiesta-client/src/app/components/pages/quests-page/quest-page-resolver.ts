import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { QuestService } from '@fiesta-common/services/quest.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class QuestPageResolver implements Resolve<any> {

    constructor(
        private questService: QuestService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

        let hideEvents = false;
        let filteredClass = 1;

        const page = +route.paramMap.get('page');
        if (route.queryParamMap.has('class')) {
            filteredClass = +route.queryParamMap.get('class');
        }
        if (route.queryParamMap.has('hideEvents')) {
            hideEvents = route.queryParamMap.get('hideEvents') === 'true';
        }

        return this.questService.getQuests(page, filteredClass, hideEvents).pipe(
            map(quests => {
                return {
                    hideEvents,
                    filteredClass,
                    quests
                };
            }));
    }
}
