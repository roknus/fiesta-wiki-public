import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestLevelInfo } from '@fiesta-common/dto/quests/quest';
import { map } from 'rxjs/operators';
import { PageDescriptionService } from '@services/page-description.service';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { CLASSES_NAME, PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-quests',
    templateUrl: './quests.component.html',
    styleUrls: ['./quests.component.scss'],
})
export class QuestsComponent implements OnInit {

    questLevelInfos: QuestLevelInfo[] = [];

    levelRange = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

    filteredClass = 1;
    hideEvents = false;
    currentPage = 0;

    readonly PAGE_TITLE = PAGES_NAME.quests;

    readonly CLASSES = CLASSES_NAME;

    constructor(
        protected pageDescriptionService: PageDescriptionService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            description: 'Quests in fiesta online give players a way to earn experience and money by killing monsters or gathering items.'
        });

        this.route.paramMap.pipe(
            map(params => +params.get('page'))
        ).subscribe(
            page => {
                // Change the current page early to change selected tab style early
                this.currentPage = page;
            }
        );

        this.route.data.subscribe(data => {
            this.hideEvents = data.pageData.hideEvents;
            this.filteredClass = data.pageData.filteredClass;
            this.questLevelInfos = data.pageData.quests;
        });
    }

    getQuestLevels(level: number): Array<number> {

        // Find a clever way to do that
        const nbLevels = level < fiestaMaxLevel ? 10 : 6;
        return Array(nbLevels).fill(0).map((x, i) => level + i);
    }

    onFilter() {
        this.router.navigate([], { queryParams: { class: this.filteredClass, hideEvents: this.hideEvents } });
    }
}
