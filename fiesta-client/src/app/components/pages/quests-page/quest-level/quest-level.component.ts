import { Component, Input } from '@angular/core';
import { Quest, QuestLevelInfo } from '@fiesta-common/dto/quests/quest';

@Component({
    selector: 'app-quest-level',
    templateUrl: './quest-level.component.html',
    styleUrls: ['./quest-level.component.scss']
})
export class QuestLevelComponent {

    @Input()
    levelInfo: QuestLevelInfo;

    @Input()
    quests: Quest[] = [];

    constructor(
    ) { }
}
