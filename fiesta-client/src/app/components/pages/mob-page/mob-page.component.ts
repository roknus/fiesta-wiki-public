import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PageDescriptionService } from '@services/page-description.service';
import { MobInfo, MobSpecies } from '@fiesta-common/dto/mobs/mob-info';
import { Quest } from '@fiesta-common/dto/quests/quest';
import { MobDropInfo } from '@fiesta-common/dto/mobs/mob-drop-info';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-mob-page',
    templateUrl: './mob-page.component.html',
    styleUrls: ['./mob-page.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0', borderBottomWidth: '0px' })),
            state('expanded', style({ height: '*', borderBottomWidth: '1px' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class MobPageComponent implements OnInit {

    species: MobSpecies;
    mobs: MobInfo[];
    relatedQuests: Quest[];
    dropTable: MobDropInfo[];

    displayedColumns: string[] = ['drops-preview', 'drop-rate'];

    dataSource = new MatTableDataSource<MobDropInfo>();
    expandedElement: MobDropInfo | null;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        protected pageDescriptionService: PageDescriptionService
    ) { }

    ngOnInit(): void {
        this.pageDescriptionService.setPageDescription({
            title: PAGES_NAME.mobs,
        });

        this.route.data.subscribe(data => {
            this.species = data.pageData.species;
            this.mobs = data.pageData.mobs;
            this.relatedQuests = data.pageData.quests;
            this.dataSource.data = data.pageData.drops;

            this.pageDescriptionService.setTitle(this.mobs[0].name, PAGES_NAME.mobs);
        });
    }
}
