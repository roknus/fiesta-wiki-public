import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { QuestService } from '@fiesta-common/services/quest.service';
import { MobService } from '@fiesta-common/services/mob.service';
import { defaultIfEmpty, map, mergeMap, switchMap, take } from 'rxjs/operators';
import { Quest } from '@fiesta-common/dto/quests/quest';

@Injectable({
    providedIn: 'root'
})
export class MobPageResolver implements Resolve<any> {

    constructor(
        private mobService: MobService,
        private questService: QuestService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

        let id = 0;

        if (route.paramMap.has('id')) {
            id = +route.paramMap.get('id');
        }

        const species$ = this.mobService.getMobSpecies(id);

        return species$.pipe(
            switchMap(species => {
                return combineLatest(species.mobs).pipe(
                    map(mobs => {
                        return { mobs, species };
                    }));
            }),
            mergeMap(res => {
                const quests$ = forkJoin(res.mobs[0].related_quests.map(q => this.questService.getQuest(q)))
                    .pipe(defaultIfEmpty<Quest[]>([]));

                const drops$ = this.mobService.getMobDropInfo(res.mobs[0].id);

                return combineLatest([quests$, drops$]).pipe(
                    map(([quests, drops]) => {
                        return {
                            species: res.species,
                            mobs: res.mobs,
                            quests,
                            drops
                        };
                    })
                );
            }),
            // take one to return first result and complete the observable, resolver won't finish if observable don't complete
            take(1)
        );
    }
}
