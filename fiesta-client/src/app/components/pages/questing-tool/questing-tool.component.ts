import { Component, OnInit } from '@angular/core';
import { QuestService } from '@fiesta-common/services/quest.service';
import { ActivatedRoute } from '@angular/router';
import { PageDescriptionService } from '@services/page-description.service';
import { switchMap } from 'rxjs/operators';
import { QuestLevelInfo } from '@fiesta-common/dto/quests/quest';
import { LoadingStatus } from '@enums/loading-return';
import { combineLatest } from 'rxjs';

@Component({
   selector: 'app-questing-tool',
   templateUrl: './questing-tool.component.html',
   styleUrls: ['./questing-tool.component.scss']
})
export class QuestingToolComponent implements OnInit {

   levelData: QuestLevelInfo;

   loadingErr: LoadingStatus = 'loading';

   values = [6];

   constructor(
      private route: ActivatedRoute,
      private questService: QuestService,
      protected pageDescriptionService: PageDescriptionService
   ) { }

   ngOnInit(): void {
      this.pageDescriptionService.setPageDescription({
         title: 'Questing Tool'
      });

      combineLatest(
         [this.route.paramMap,
         this.route.queryParamMap]
      ).pipe(
         switchMap(([params, queryParams]) => {

            this.loadingErr = 'loading';

            const level = +params.get('level');

            let minLevel: number = level - 10;

            if (queryParams.has('minLevel')) {
               minLevel = +queryParams.get('minLevel');
            }

            return this.questService.getAvailableQuestsPerLevel(+level, minLevel);
         })
      ).subscribe(
         levelData => {
            this.levelData = levelData;
            this.loadingErr = 'ok';
         },
         err => {
            this.levelData = null;
            this.loadingErr = 'error';
         }
      );
   }

}
