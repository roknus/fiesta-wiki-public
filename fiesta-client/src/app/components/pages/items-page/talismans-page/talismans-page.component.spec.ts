import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TalismansPageComponent } from './talismans-page.component';

describe('TalismansPageComponent', () => {
  let component: TalismansPageComponent;
  let fixture: ComponentFixture<TalismansPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TalismansPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalismansPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
