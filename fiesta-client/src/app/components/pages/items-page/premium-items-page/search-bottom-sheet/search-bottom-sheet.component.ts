import { Component, OnInit, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { StoreCategory } from '@fiesta-common/dto/store-items/store-item';
import { StoreItemSearch } from '@fiesta-common/services/store-item.service';

@Component({
   selector: 'app-search-bottom-sheet',
   templateUrl: './search-bottom-sheet.component.html',
   styleUrls: ['./search-bottom-sheet.component.scss']
})
export class SearchBottomSheetComponent implements OnInit {

   categories: StoreCategory[];

   constructor(
      private bottomSheetRef: MatBottomSheetRef<SearchBottomSheetComponent>,
      @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
   ) {
      this.categories = data.categories;
   }

   ngOnInit() {

   }

   onFilter(search: StoreItemSearch) {
      this.bottomSheetRef.dismiss(search);
   }
}
