import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SearchBottomSheetComponent } from './search-bottom-sheet.component';

describe('SearchBottomSheetComponent', () => {
  let component: SearchBottomSheetComponent;
  let fixture: ComponentFixture<SearchBottomSheetComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
