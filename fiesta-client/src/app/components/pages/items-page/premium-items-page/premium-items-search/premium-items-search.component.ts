import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { StoreCategory } from '@fiesta-common/dto/store-items/store-item';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { StoreItemSearch } from '@fiesta-common/services/store-item.service';

@Component({
   selector: 'app-premium-items-search',
   templateUrl: './premium-items-search.component.html',
   styleUrls: ['./premium-items-search.component.scss']
})
export class PremiumItemsSearchComponent implements OnInit {

   @Input()
   categories: StoreCategory[] = [];

   @Output()
   filter = new EventEmitter<StoreItemSearch>();

   search = '';

   searchItemForm: FormGroup;
   searchControl = new FormControl({ value: '' });
   selectedStatCategoriesControl = new FormControl({ value: {} });
   selectedSlotCategoriesControl = new FormControl({ value: {} });
   selectedDurationCategoriesControl = new FormControl({ value: {} });
   selectedDesignerCategoriesControl = new FormControl({ value: {} });

   get timeCategories(): StoreCategory[] {
      if (!this.categories) {
         return [];
      }

      return this.categories.filter(c => c.type === 0);
   }

   get statCategories(): StoreCategory[] {
      if (!this.categories) {
         return [];
      }

      return this.categories.filter(c => c.type === 1);
   }

   get slotCategories(): StoreCategory[] {
      if (!this.categories) {
         return [];
      }

      return this.categories.filter(c => c.type === 2);
   }

   get designerCategories(): StoreCategory[] {
      if (!this.categories) {
         return [];
      }

      return this.categories.filter(c => c.type === 3);
   }

   constructor(
      protected router: Router,
      protected route: ActivatedRoute,
      private formBuilder: FormBuilder
   ) {
      this.searchItemForm = this.formBuilder.group({
         search: this.searchControl,
         selectedStat: this.selectedStatCategoriesControl,
         selectedSlot: this.selectedSlotCategoriesControl,
         selectedDuration: this.selectedDurationCategoriesControl,
         selectedDesigner: this.selectedDesignerCategoriesControl,
      });

      this.searchControl.setValue('');
      this.selectedStatCategoriesControl.setValue([]);
      this.selectedSlotCategoriesControl.setValue([]);
      this.selectedDurationCategoriesControl.setValue([]);
      this.selectedDesignerCategoriesControl.setValue([]);
   }

   ngOnInit() {
      this.route.queryParamMap.pipe(
         map(queryParam => {
            let search = '';
            let stats: number[] = [];
            let slots: number[] = [];
            let duration: number[] = [];
            let designer: number[] = [];

            if (queryParam.has('search')) {
               search = queryParam.get('search');
            }
            if (queryParam.has('stats')) {
               stats = queryParam.getAll('stats').map(Number);
            }
            if (queryParam.has('slots')) {
               slots = queryParam.getAll('slots').map(Number);
            }
            if (queryParam.has('duration')) {
               duration = queryParam.getAll('duration').map(Number);
            }
            if (queryParam.has('designer')) {
               designer = queryParam.getAll('designer').map(Number);
            }

            return {
               search,
               stats,
               slots,
               duration,
               designer,
            };
         })).subscribe(values => {
            this.search = values.search;
            this.selectedStatCategoriesControl.setValue(values.stats);
            this.selectedSlotCategoriesControl.setValue(values.slots);
            this.selectedDurationCategoriesControl.setValue(values.duration);
            this.selectedDesignerCategoriesControl.setValue(values.designer);
         });
   }

   onFilter() {
      const params = new StoreItemSearch();
      if (this.searchControl.value.length > 0 || this.search.length > 0) {
         if (this.searchControl.value.length > 0) {
            params.search = this.searchControl.value;
         } else {
            params.search = this.search;
         }
      }

      if (this.selectedStatCategoriesControl.value.length > 0) {
         params.stats = this.selectedStatCategoriesControl.value;
      }
      if (this.selectedSlotCategoriesControl.value.length > 0) {
         params.slots = this.selectedSlotCategoriesControl.value;
      }
      if (this.selectedDurationCategoriesControl.value.length > 0) {
         params.duration = this.selectedDurationCategoriesControl.value;
      }
      if (this.selectedDesignerCategoriesControl.value.length > 0) {
         params.designer = this.selectedDesignerCategoriesControl.value;
      }
      
      // reset the search box
      this.searchControl.reset('');

      this.filter.emit(params);
   }
}
