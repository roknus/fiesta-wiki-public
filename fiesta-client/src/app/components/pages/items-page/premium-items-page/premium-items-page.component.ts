import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PageDescriptionService } from '@services/page-description.service';
import { LoadingStatus } from '@enums/loading-return';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { PageEvent } from '@angular/material/paginator';
import { switchMap, map } from 'rxjs/operators';
import { StoreItemService, StoreItemSearch } from '@fiesta-common/services/store-item.service';
import { combineLatest } from 'rxjs';
import { StoreItemView, StoreCategory } from '@fiesta-common/dto/store-items/store-item';
import { environment } from '@environments/environment';
import { Item } from '@fiesta-common/dto/items/item';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { SearchBottomSheetComponent } from './search-bottom-sheet/search-bottom-sheet.component';
import { isPlatformBrowser } from '@angular/common';

class StoreCategorySet extends Set<StoreCategory> {
   add(value: StoreCategory): this {
      let found = false;
      this.forEach(item => {
         if (value.id === item.id) {
            found = true;
         }
      });

      if (!found) {
         super.add(value);
      }

      return this;
   }
}

@Component({
   selector: 'app-premium-items-page',
   templateUrl: './premium-items-page.component.html',
   styleUrls: ['./premium-items-page.component.scss'],
})
export class PremiumItemsPageComponent implements OnInit {

   pageLoadingStatus: LoadingStatus = 'loading';

   items: StoreItemView[];

   STATIC_ASSETS = environment.static_assets;

   page: Page<StoreItemView>;

   categories: StoreCategory[];

   selectedCategories: StoreCategory[] = [];
   search = '';

   isMobileResolution = false;
   @HostListener('window:resize', ['$event'])
   onResize(event?) {
      if (isPlatformBrowser(this.platformId)) {
         if (window.innerWidth < 960) {
            this.isMobileResolution = true;
         } else {
            this.isMobileResolution = false;
         }
      }
   }

   constructor(
      @Inject(PLATFORM_ID) private platformId: any,
      protected router: Router,
      protected route: ActivatedRoute,
      protected pageDescriptionService: PageDescriptionService,
      private storeItemService: StoreItemService,
      private bottomSheetSearch: MatBottomSheet
   ) {
      this.onResize();
   }

   ngOnInit() {
      this.pageDescriptionService.setPageDescription({
         title: $localize`Premium items`,
         description: 'List of Premium items available in Fiesta Online.'
      });

      const queryParam$ = this.route.queryParamMap.pipe(
         map(queries => {

            const search = new StoreItemSearch();
            search.search = '';

            if (queries.has('search')) {
               search.search = queries.get('search');
            }
            search.stats = queries.getAll('stats').map(Number);
            search.slots = queries.getAll('slots').map(Number);
            search.duration = queries.getAll('duration').map(Number);
            search.designer = queries.getAll('designer').map(Number);

            return search;
         }));

      combineLatest(
         [this.route.paramMap,
            queryParam$]
      ).pipe(switchMap(results => {

         this.pageLoadingStatus = 'loading';

         let page = 0;

         if (results[0].has('page')) {
            page = +results[0].get('page');
         }

         return this.storeItemService.getStoreItemViews(page, results[1]);
      })).subscribe(
         page => {
            this.page = page;
            this.pageLoadingStatus = 'ok';
            this.items = page.content;
         },
         err => {
            this.pageLoadingStatus = 'error';
         });

      const categories$ = this.storeItemService.getStoreCategories().pipe(
         map(categories => categories.sort((c1, c2) => c1.name.localeCompare(c2.name)))
      );

      combineLatest(
         [categories$, queryParam$]
      ).subscribe(([categories, queries]) => {
         this.categories = categories;

         this.selectedCategories = [];
         this.selectedCategories.push(...queries.stats.map(i => categories.find(c => c.id === i)));
         this.selectedCategories.push(...queries.slots.map(i => categories.find(c => c.id === i)));
         this.selectedCategories.push(...queries.duration.map(i => categories.find(c => c.id === i)));
         this.selectedCategories.push(...queries.designer.map(i => categories.find(c => c.id === i)));
         this.search = queries.search;
      });
   }

   selectCategory(category: StoreCategory) {
      if (category != null) {
         this.selectedCategories.push(category);
      }
   }

   unselectCategory(category: StoreCategory): void {
      const index = this.selectedCategories.indexOf(category);

      if (index >= 0) {
         this.selectedCategories.splice(index, 1);
      }
   }

   getItemInfo(storeItemView: StoreItemView): Item[] {
      if (storeItemView.store_items && storeItemView.store_items.length > 0) {

         if (storeItemView.store_items[0].set) {
            const setID = storeItemView.store_items[0].set.id;

            return storeItemView.store_items.filter(i => i.set && i.set.id === setID).map(i => i.item);

         } else {
            return [storeItemView.store_items[0].item];
         }
      }
   }

   getCategories(storeItemView: StoreItemView): StoreCategory[] {
      const set = new StoreCategorySet();

      storeItemView.store_items.forEach(item => {
         item.categories.forEach(category => set.add(category));
      });

      return Array.from(set.values()).sort((c1, c2) => c1.type - c2.type);
   }

   onPageChange(pageEvent: PageEvent) {
      this.router.navigate(['/premium-items', pageEvent.pageIndex], { queryParamsHandling: 'preserve' });
   }

   clearSearch() {
      this.search = '';

      this.refreshFilter();
   }

   removeFilter(c: StoreCategory) {

      const index = this.selectedCategories.indexOf(c);
      if (index >= 0) {
         this.selectedCategories.splice(index, 1);
      }

      this.refreshFilter();
   }

   refreshFilter() {

      const params = new StoreItemSearch();
      if (this.search.length > 0) {
         params.search = this.search;
      }
      const duration = this.selectedCategories.filter(c => c.type === 0);
      if (duration.length > 0) {
         params.duration = duration.map(c => c.id);
      }
      const stats = this.selectedCategories.filter(c => c.type === 1);
      if (stats.length > 0) {
         params.stats = stats.map(c => c.id);
      }
      const slots = this.selectedCategories.filter(c => c.type === 2);
      if (slots.length > 0) {
         params.slots = slots.map(c => c.id);
      }
      const designer = this.selectedCategories.filter(c => c.type === 3);
      if (designer.length > 0) {
         params.designer = designer.map(c => c.id);
      }

      this.router.navigate(['../0'], { queryParams: params, relativeTo: this.route });
   }

   openBottomSheet() {
      this.bottomSheetSearch.open(SearchBottomSheetComponent,
         {
            data: { categories: this.categories }
         })
         .afterDismissed()
         .subscribe(search => {
            this.onFilter(search);
         });
   }

   onFilter(search: StoreItemSearch) {
      // ../0 to reset the page
      this.router.navigate(['../0'], { queryParams: search, relativeTo: this.route });
   }
}
