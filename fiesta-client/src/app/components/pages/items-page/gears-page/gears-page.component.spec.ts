import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GearsPageComponent } from './gears-page.component';

describe('GearsPageComponent', () => {
  let component: GearsPageComponent;
  let fixture: ComponentFixture<GearsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GearsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GearsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
