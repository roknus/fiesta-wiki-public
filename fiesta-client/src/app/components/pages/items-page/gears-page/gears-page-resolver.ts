import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { ROUTES_URL } from '@app/routing/routes';
import { PageDescription } from '@services/page-description.service';
import { ClassPipe } from '@fiesta-common/pipes/class-name';
import { PAGES_NAME } from '@app/global-strings';

@Injectable({
    providedIn: 'root'
})
export class GearsPageResolver implements Resolve<any> {

    constructor(private router: Router) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

        let type = 0;

        if (route.paramMap.has('class')) {
            type = +route.paramMap.get('class');
        }

        const description = new PageDescription();
        description.category = 'Gears';

        const utils = new ClassPipe();

        // classes are betwen 1 and 27
        if (type > 0 && type < 28) {
            description.title = `${utils.transform(type)} ${PAGES_NAME.gears}`;
        }
        else {
            this.router.navigate([ROUTES_URL.notFound]);
        }

        description.description =
            `List of ${description.title} available in Fiesta Online. Accessible gears are hat, top, pants and boots.`;

        return description;
    }
}
