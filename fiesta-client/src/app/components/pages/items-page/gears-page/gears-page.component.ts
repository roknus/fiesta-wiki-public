import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, map, share } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { PageEvent } from '@angular/material/paginator';
import { PageDescriptionService } from '@services/page-description.service';
import { ItemsPageTemplateComponent } from '@fiesta-common/components/items-page-template/items-page-template.component';
import { ItemService } from '@fiesta-common/services/item.service';
import { FiestaSettingsService } from '@fiesta-common/services/fiesta-settings.service';
import { LevelRangeFilterComponent } from '@fiesta-common/components/level-range-filter/level-range-filter.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-gears-page',
    templateUrl: './gears-page.component.html',
    styleUrls: ['./gears-page.component.scss']
})
export class GearsPageComponent extends ItemsPageTemplateComponent implements OnInit {

    @ViewChild(LevelRangeFilterComponent, { static: true }) levelRangeForm: LevelRangeFilterComponent;

    classType = undefined;

    PAGE_TITLE = PAGES_NAME.gears;

    filterFormGroup: FormGroup;

    displayedColumns: string[] =
        [
            'icon',
            'name',
            'level',
            'defense',
            'mdefense',
            'evasion',
            'sellprice',
        ];

    showRandomStats = true;

    readonly maxEnhancement = 10;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        private formBuilder: FormBuilder,
        protected pageDescriptionService: PageDescriptionService,
        private itemService: ItemService,
        private fiestaSettings: FiestaSettingsService,
    ) {
        super(router, route);

        this.route.data.subscribe(data => {
            this.pageDescriptionService.setPageDescription(data.pageDescription);
            this.PAGE_TITLE = data.pageDescription.title;
        });

        this.enhancement = this.maxEnhancement;
    }

    ngOnInit() {

        this.fiestaSettings.showRandomStats.subscribe(value => this.showRandomStats = value);

        this.createForm();

        this.page$ = combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(
            switchMap(([param, queryParam]) => {

                let page = 0;
                let order;
                let classType = 0;
                let rarities = [];
                let search = '';
                let min;
                let max;

                if (param.has('page')) {
                    page = +param.get('page');
                }
                if (param.has('class')) {
                    classType = +param.get('class');
                }

                rarities = queryParam.getAll('rarity').map(Number);

                if (queryParam.has('order')) {
                    order = +queryParam.get('order');
                }
                if (queryParam.has('search')) {
                    search = queryParam.get('search');
                }
                if (queryParam.has('min')) {
                    min = +queryParam.get('min');
                }
                if (queryParam.has('max')) {
                    max = +queryParam.get('max');
                }

                if (this.itemTable) {
                    this.itemTable.clear();
                }

                return this.itemService.getGears(classType, rarities, page, order, search, min, max);
            }),
            share()
        );

        combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(map(([param, queryParam]) => {
            let classType = 0;
            let rarities = [];
            let minLevel = 0;
            let maxLevel = fiestaMaxLevel;

            if (param.has('class')) {
                classType = +param.get('class');
            }
            rarities = queryParam.getAll('rarity').map(Number);

            if (queryParam.has('min')) {
                minLevel = +queryParam.get('min');
            }
            if (queryParam.has('max')) {
                maxLevel = +queryParam.get('max');
            }

            return [classType, rarities, minLevel, maxLevel];
        })).subscribe(([classType, rarities, minLevel, maxLevel]: [number, number[], number, number]) => {
            this.classType = classType;

            this.filterFormGroup.setValue({ rarity: rarities, search: '', levelRange: { min: minLevel, max: maxLevel } });
        });
    }

    createForm() {
        this.filterFormGroup = this.formBuilder.group({
            rarity: [],
            search: null,
            levelRange: this.levelRangeForm.createForm(),
        });
    }

    onSubmit(data: any) {
        this.filter({
            rarity: data.rarity,
            search: data.search ? data.search : null,
            min: data.levelRange.min ? data.levelRange.min : null,
            max: data.levelRange.max ? data.levelRange.max : null
        });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(['/gears', this.classType, pageEvent.pageIndex], { queryParamsHandling: 'merge' });
    }

    onRandomStatsChange(event: MatCheckboxChange) {
        this.fiestaSettings.setShowRandomStats(event.checked);
    }
}
