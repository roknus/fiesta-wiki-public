import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { JewelsPageComponent } from './jewels-page.component';

describe('JewelsPageComponent', () => {
  let component: JewelsPageComponent;
  let fixture: ComponentFixture<JewelsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ JewelsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JewelsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
