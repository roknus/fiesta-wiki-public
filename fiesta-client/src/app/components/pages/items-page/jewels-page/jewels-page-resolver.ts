import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { PAGES_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';
import { PageDescription } from '@services/page-description.service';

@Injectable({
   providedIn: 'root'
})
export class JewelsPageResolver implements Resolve<any> {

   constructor(private router: Router) { }

   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

      let type = 0;

      if (route.paramMap.has('jewelType')) {
         type = +route.paramMap.get('jewelType');
      }

      let meta = new PageDescription;
      meta.category = PAGES_NAME.jewels;

      switch (type) {
         case 26:
            meta.title = PAGES_NAME.rings;
            break;
         case 25:
            meta.title = PAGES_NAME.earrings;
            break;
         case 24:
            meta.title = PAGES_NAME.necklace;
            break;
         default:
            this.router.navigate([ROUTES_URL.notFound]);
      }

      meta.description = `List of ${meta.title} available in Fiesta Online.`;

      return meta;
   }
}

@Injectable({
   providedIn: 'root'
})
export class JewelsPageColumnResolver implements Resolve<any> {

   constructor(private router: Router) { }

   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

      let id = 0;

      if (route.paramMap.has('jewelType')) {
         id = +route.paramMap.get('jewelType');
      }

      let displayedColumns: string[] =
      [
         'icon',
         'name',
         'level',
         'mdefense'
      ];
         
      switch (id) {
         case 26: // Rings
            displayedColumns.splice(4, 0, 'mdamage');
            break;
         case 25: // Earrings
            displayedColumns.splice(4, 0, 'crit');
            displayedColumns.splice(5, 0, 'aimRate');
            displayedColumns.splice(6, 0, 'evasionRate');
            break;
         case 24: // Necklace
            displayedColumns.splice(4, 0, 'poisonresist');
            displayedColumns.splice(5, 0, 'diseaseresist');
            displayedColumns.splice(6, 0, 'curseresist');
            break;
      }

      return displayedColumns;
   }
}