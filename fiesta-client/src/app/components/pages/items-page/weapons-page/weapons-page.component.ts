import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, map, share } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { PageEvent } from '@angular/material/paginator';
import { PageDescriptionService } from '@services/page-description.service';
import { ItemsPageTemplateComponent } from '@fiesta-common/components/items-page-template/items-page-template.component';
import { ItemService } from '@fiesta-common/services/item.service';
import { FiestaSettingsService } from '@fiesta-common/services/fiesta-settings.service';
import { LevelRangeFilterComponent } from '@fiesta-common/components/level-range-filter/level-range-filter.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-weapons-page',
    templateUrl: './weapons-page.component.html',
    styleUrls: ['./weapons-page.component.scss']
})
export class WeaponsPageComponent extends ItemsPageTemplateComponent implements OnInit {

    @ViewChild(LevelRangeFilterComponent, { static: true }) levelRangeForm: LevelRangeFilterComponent;

    weaponType = undefined;

    PAGE_TITLE = PAGES_NAME.weapons;

    filterFormGroup: FormGroup;

    displayedColumns: string[] =
        [
            'icon',
            'name',
            'level',
            'aim',
            'crit'
        ];

    showRandomStats = true;

    readonly maxEnhancement = 12;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        protected pageDescriptionService: PageDescriptionService,
        private formBuilder: FormBuilder,
        private itemService: ItemService,
        private fiestaSettings: FiestaSettingsService,
    ) {
        super(router, route);

        this.route.data.subscribe(data => {
            this.PAGE_TITLE = data.pageDescription.title;
            this.pageDescriptionService.setPageDescription(data.pageDescription);
            this.displayedColumns = data.pageColumns;
        });

        this.enhancement = this.maxEnhancement;
    }

    ngOnInit() {

        this.createForm();

        this.fiestaSettings.showRandomStats.subscribe(value => this.showRandomStats = value);

        this.page$ = combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(
            switchMap(([param, queryParam]) => {
                let weaponType = 0;
                let page = 0;
                const order = +queryParam.get('order');
                const min = +queryParam.get('min');
                const max = +queryParam.get('max');
                const search = queryParam.get('search');
                const rarities = queryParam.getAll('rarity').map(Number);

                if (param.has('weaponType')) {
                    weaponType = +param.get('weaponType');
                }
                if (param.has('page')) {
                    page = +param.get('page');
                }

                if (this.itemTable) {
                    this.itemTable.clear();
                }

                return this.itemService.getWeapons({
                    page,
                    weaponType,
                    rarities,
                    order,
                    search,
                    min,
                    max
                });
            }),
            share()
        );

        combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(map(([param, queryParam]) => {
            let weaponType = 0;
            let rarities = [];
            let minLevel = 0;
            let maxLevel = fiestaMaxLevel;

            if (param.has('weaponType')) {
                weaponType = +param.get('weaponType');
            }

            rarities = queryParam.getAll('rarity').map(Number);

            if (queryParam.has('min')) {
                minLevel = +queryParam.get('min');
            }
            if (queryParam.has('max')) {
                maxLevel = +queryParam.get('max');
            }

            return [weaponType, rarities, minLevel, maxLevel];
        })).subscribe(([weaponType, rarities, minLevel, maxLevel]: [number, number[], number, number]) => {
            this.weaponType = weaponType;

            this.filterFormGroup.setValue({ rarity: rarities, search: '', levelRange: { min: minLevel, max: maxLevel } });
        });
    }

    createForm() {
        this.filterFormGroup = this.formBuilder.group({
            rarity: [],
            search: null,
            levelRange: this.levelRangeForm.createForm(),
        });
    }

    onSubmit(data: any) {
        this.filter({
            rarity: data.rarity,
            search: data.search ? data.search : null,
            min: data.levelRange.min ? data.levelRange.min : null,
            max: data.levelRange.max ? data.levelRange.max : null
        });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(['/weapons', this.weaponType, pageEvent.pageIndex], { queryParamsHandling: 'merge' });
    }

    onRandomStatsChange(event: MatCheckboxChange) {
        this.fiestaSettings.setShowRandomStats(event.checked);
    }
}
