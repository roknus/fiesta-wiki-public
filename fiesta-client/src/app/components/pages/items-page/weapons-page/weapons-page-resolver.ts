import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { PAGES_NAME, WEAPONS_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';
import { PageDescription } from '@services/page-description.service';

@Injectable({
   providedIn: 'root'
})
export class WeaponsPageResolver implements Resolve<any> {

   constructor(private router: Router) { }

   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

      let id = 0;

      if (route.paramMap.has('weaponType')) {
         id = +route.paramMap.get('weaponType');
      }

      let meta = new PageDescription;
      meta.category = PAGES_NAME.weapons;

      switch (id) {
         case 8:
            meta.title = WEAPONS_NAME.oneHandSword;
            break;
         case 9:
            meta.title = WEAPONS_NAME.twoHandSword;
            break;
         case 10:
            meta.title = WEAPONS_NAME.axe;
            break;
         case 11:
            meta.title = WEAPONS_NAME.mace;
            break;
         case 12:
            meta.title = WEAPONS_NAME.hammer;
            break;
         case 13:
            meta.title = WEAPONS_NAME.bow;
            break;
         case 14:
            meta.title = WEAPONS_NAME.crossbow;
            break;
         case 15:
            meta.title = WEAPONS_NAME.staff;
            break;
         case 16:
            meta.title = WEAPONS_NAME.wand;
            break;
         case 17:
            meta.title = WEAPONS_NAME.claws;
            break;
         case 18:
            meta.title = WEAPONS_NAME.dualSword;
            break;
         case 40:
            meta.title = WEAPONS_NAME.blade;
            break;
         default:
            this.router.navigate([ROUTES_URL.notFound]);
      }
      
      meta.description = `List of ${meta.title} available in Fiesta Online.`;

      return meta;
   }
}

@Injectable({
   providedIn: 'root'
})
export class WeaponsPageColumnResolver implements Resolve<any> {

   constructor(private router: Router) { }

   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

      let id = 0;

      if (route.paramMap.has('weaponType')) {
         id = +route.paramMap.get('weaponType');
      }
      
      let displayedColumns: string[] =
      [
         'icon',
         'name',
         'level',
         'aim',
         'crit'
      ];

      switch (id) {
         case 8:
         case 9:
         case 10:
         case 11:
         case 12:
         case 13:
         case 14:
         case 17:
         case 18:
            displayedColumns.splice(3, 0, 'damage');
            break;
         case 15:
         case 16:
            displayedColumns.splice(3, 0, 'mdamage');
            break;
         case 40:
            displayedColumns.splice(3, 0, 'damage');
            displayedColumns.splice(3, 0, 'mdamage');
            break;
      }

      return displayedColumns;
   }
}