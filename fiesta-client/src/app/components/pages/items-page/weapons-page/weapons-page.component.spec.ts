import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WeaponsPageComponent } from './weapons-page.component';

describe('WeaponsPageComponent', () => {
  let component: WeaponsPageComponent;
  let fixture: ComponentFixture<WeaponsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WeaponsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeaponsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
