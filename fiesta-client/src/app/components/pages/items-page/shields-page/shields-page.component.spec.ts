import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ShieldsPageComponent } from './shields-page.component';

describe('ShieldsPageComponent', () => {
  let component: ShieldsPageComponent;
  let fixture: ComponentFixture<ShieldsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ShieldsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShieldsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
