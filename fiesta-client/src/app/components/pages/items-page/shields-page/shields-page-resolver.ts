import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { PageDescription } from '@services/page-description.service';
import { ROUTES_URL } from '@app/routing/routes'

@Injectable({
   providedIn: 'root'
})
export class ShieldsPageResolver implements Resolve<any> {

   constructor(private router: Router) { }

   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

      let type = 0;

      if (route.paramMap.has('shieldType')) {
        type = +route.paramMap.get('shieldType');
      }

      let meta = new PageDescription;
      meta.category = 'Shields';

      switch (type) {
         case 19:
            meta.title = 'Fighter Shields';
            break;
         case 20:
            meta.title = 'Cleric Shields';
            break;
         default:
            this.router.navigate([ROUTES_URL.notFound]);
      }
      
      meta.description = `List of ${meta.title} available in Fiesta Online.`;

      return meta;
   }
}