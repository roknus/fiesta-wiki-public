import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BraceletsPageComponent } from './bracelets-page.component';

describe('BraceletsPageComponent', () => {
  let component: BraceletsPageComponent;
  let fixture: ComponentFixture<BraceletsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BraceletsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BraceletsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
