import { Component, OnInit, ViewChild } from '@angular/core';
import { PageDescriptionService } from '@services/page-description.service';
import { MobService } from '@fiesta-common/services/mob.service';
import { Router, ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { MobSpecies } from '@fiesta-common/dto/mobs/mob-info';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { MatTableDataSource } from '@angular/material/table';
import { PageEvent } from '@angular/material/paginator';
import { environment } from '@environments/environment';
import { ROUTES_URL } from '@app/routing/routes';
import { LoadingStatus } from '@enums/loading-return';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LevelRangeFilterComponent } from '@fiesta-common/components/level-range-filter/level-range-filter.component';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-mob-list-page',
    templateUrl: './mob-list-page.component.html',
    styleUrls: ['./mob-list-page.component.scss']
})
export class MobListPageComponent implements OnInit {

    @ViewChild(LevelRangeFilterComponent, { static: true }) levelRangeForm: LevelRangeFilterComponent;

    readonly STATIC_ASSETS = environment.static_assets;

    readonly ROUTES = ROUTES_URL;

    page = new Page<MobSpecies>();

    dataSource: MatTableDataSource<MobSpecies>;

    loadingStatus: LoadingStatus = 'loading';

    displayedColumns: string[] =
        [
            'icon'
            , 'name'
            // ,'level'
            // ,'exp'
        ];

    pageSizeOptions: number[] = [10, 20, 50, 100];

    mobsForm: FormGroup;

    readonly defaultPageSize = 20;
    readonly defaultMin = 0;
    readonly defaultMax = 150;

    readonly PAGE_TITLE = PAGES_NAME.mobs;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        protected pageDescriptionService: PageDescriptionService,
        private formBuilder: FormBuilder,
        private mobService: MobService
    ) { }

    ngOnInit(): void {
        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            description: 'List of mobs that you can encounter in the world of Isya of Fiesta Online'
        });

        this.mobsForm = this.formBuilder.group({
            levelRange: this.levelRangeForm.createForm(),
            search: ''
        });

        this.route.queryParamMap.pipe(
            switchMap(queryParam => {

                this.clear();

                let page = 0;
                let size = this.defaultPageSize;
                let min = this.defaultMin;
                let max = this.defaultMax;
                let search: string;

                if (queryParam.has('page')) {
                    page = +queryParam.get('page');
                }

                if (queryParam.has('size')) {
                    size = +queryParam.get('size');
                }

                if (queryParam.has('search')) {
                    search = queryParam.get('search');
                }
                if (queryParam.has('min')) {
                    min = +queryParam.get('min');
                }
                if (queryParam.has('max')) {
                    max = +queryParam.get('max');
                }

                return this.mobService.getSpeciesPage({
                    page,
                    size,
                    search,
                    min,
                    max,
                    sort: ['level', 'name'],
                    active: true,
                    location: 1
                }).pipe(
                    map(resultPage => {
                        return {
                            resultPage,
                            min,
                            max
                        };
                    })
                );
            })
        ).subscribe(
            res => {
                this.mobsForm.setValue({ search: '', levelRange: { min: res.min, max: res.max } });
                this.page = res.resultPage;
                this.dataSource = new MatTableDataSource(res.resultPage.content);
                this.loadingStatus = 'ok';
            },
            err => {
                this.loadingStatus = 'error';
            });
    }

    clear() {
        this.dataSource = new MatTableDataSource();
        this.loadingStatus = 'loading';
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(
            ['/', ROUTES_URL.mobs],
            {
                queryParams:
                {
                    ...this.route.snapshot.queryParams,
                    ...{
                        page: pageEvent.pageIndex !== 0 ? pageEvent.pageIndex : undefined,
                        size: pageEvent.pageSize !== this.defaultPageSize ? pageEvent.pageSize : undefined
                    }
                },
                queryParamsHandling: 'merge'
            });
    }

    onSubmit(value: any) {

        this.router.navigate(
            ['/', ROUTES_URL.mobs],
            {
                queryParams:
                {
                    ...this.route.snapshot.queryParams,
                    ...{
                        min: value.levelRange.min !== this.defaultMin ? value.levelRange.min : undefined,
                        max: value.levelRange.max !== this.defaultMax ? value.levelRange.max : undefined,
                        search: value.search.length ? value.search : undefined,
                        page: undefined
                    }
                },
                queryParamsHandling: 'merge'
            });
    }
}
