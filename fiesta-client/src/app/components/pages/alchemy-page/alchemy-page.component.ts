import { Component, OnInit, ViewChild } from '@angular/core';
import { Production } from '@fiesta-common/dto/production/production';
import { ProductionService } from '@fiesta-common/services/production.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PageDescriptionService } from '@services/page-description.service';
import { LoadingStatus } from '@enums/loading-return';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-alchemy-page',
    templateUrl: './alchemy-page.component.html',
    styleUrls: ['./alchemy-page.component.scss']
})
export class AlchemyPageComponent implements OnInit {

    readonly PAGE_TITLE = PAGES_NAME.alchemy;

    productions: Production[];

    sortedProductions: Production[];

    selectedProduction: Production = null;

    alchemyName = 'Undefined';

    displayedColumns = [
        'icon',
        'name',
        'neededMastery',
        'masteryGain'
    ];

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    dataSource: MatTableDataSource<Production>;

    loadingErr: LoadingStatus = 'loading';

    constructor(
        protected pageDescriptionService: PageDescriptionService,
        private route: ActivatedRoute,
        private productionService: ProductionService,
    ) {
        this.dataSource = new MatTableDataSource<Production>(null);
    }

    ngOnInit() {
        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            description: 'Alchemy can be learned to produce materials, enhancement stones or consumables.'
        });

        this.route.paramMap.pipe(
            switchMap(params => {
                this.loadingErr = 'loading';

                let productionType = 0;

                if (params.has('type')) {
                    productionType = +params.get('type');
                }

                this.alchemyName = this.getProductionName(productionType);

                return this.productionService.getProduction(productionType);
            })
        ).subscribe(productions => {

            this.loadingErr = 'ok';

            this.pageDescriptionService.setTitle(this.alchemyName, this.PAGE_TITLE);

            this.selectedProduction = null;

            this.productions = productions;
            this.sortedProductions = productions;

            this.dataSource.data = this.sortedProductions;
            this.dataSource.sort = this.sort;
        },
            () => {
                this.loadingErr = 'error';
            }
        );
    }

    getProductionName(id: number): string {
        switch (id) {
            case 0:
                return $localize`Stone Production`;
            case 1:
                return $localize`Potion Production`;
            case 2:
                return $localize`Scroll Production`;
            case 4:
                return $localize`Material Composition`;
            case 5:
                return $localize`Material Decomposition`;
        }
    }

    onSort(sort: Sort) {
        if (!sort.active || sort.direction === '') {
            this.sortedProductions = this.productions;
            return;
        }

        this.sortedProductions = this.productions.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'neededMastery': return compare(a.needed_mastery, b.needed_mastery, isAsc);
                case 'masteryGain': return compare(a.mastery_gain, b.mastery_gain, isAsc);
                default: return 0;
            }
        });

        this.dataSource.data = this.sortedProductions;
    }

    onRowClick(row: Production) {
        this.selectedProduction = row;
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
