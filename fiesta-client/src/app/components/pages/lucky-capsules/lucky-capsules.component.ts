import { Component, OnInit } from '@angular/core';
import { PageDescriptionService } from '@services/page-description.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LuckyCapsulesService } from '@services/lucky-capsules.service';
import { LuckyCapsule, LuckyCapsuleGroupRate } from '@dto/lucky-capsules/lucky-capsules';
import { LoadingStatus } from '@enums/loading-return';
import { ROUTES_URL } from '@app/routing/routes';
import { switchMap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-lucky-capsules',
    templateUrl: './lucky-capsules.component.html',
    styleUrls: ['./lucky-capsules.component.scss']
})
export class LuckyCapsulesComponent implements OnInit {

    readonly staticAssetsUrl = environment.static_assets;

    pageLoadingStatus: LoadingStatus = 'loading';

    luckyCapsuleGroupRates: LuckyCapsuleGroupRate[];

    readonly ROUTES = ROUTES_URL;

    readonly PAGE_TITLE = PAGES_NAME.luckyCapsule;

    displayedColumns: ['icon', 'name', 'rate'];

    capsules: LuckyCapsule[] = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private luckyCapsuleService: LuckyCapsulesService,
        protected pageDescriptionService: PageDescriptionService
    ) { }

    ngOnInit() {
        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            category: $localize`Lucky House`,
            description: 'Lucky Capsules can be obtained in the Lucky House in exchange for Lucky House Coins and opened to obtain one random item.'
        });

        this.route.data.subscribe(data => {
            this.capsules = data.pageData;
        });

        this.route.queryParamMap.pipe(
            switchMap(params => {

                this.pageLoadingStatus = 'loading';

                let capsuleId = 0;

                if (params.has('id')) {
                    capsuleId = +params.get('id');
                }
                else {
                    this.router.navigate([], {
                        queryParams: { id: this.capsules[0].server_id }
                    });
                }

                return this.luckyCapsuleService.getCapsuleGroupRate(capsuleId);
            })
        ).subscribe(
            value => {
                this.pageLoadingStatus = 'ok';
                this.luckyCapsuleGroupRates = value;
            },
            err => {
                this.pageLoadingStatus = 'error';
            });
    }
}
