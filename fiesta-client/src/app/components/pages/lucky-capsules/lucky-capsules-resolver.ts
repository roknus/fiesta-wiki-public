import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { LuckyCapsulesService } from '@services/lucky-capsules.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LuckyCapsulesResolver implements Resolve<any> {

    constructor(
        private luckyCapsulesService: LuckyCapsulesService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

        const type = route.data.type;

        return this.luckyCapsulesService.getLuckyCapsules(type);
    }
}
