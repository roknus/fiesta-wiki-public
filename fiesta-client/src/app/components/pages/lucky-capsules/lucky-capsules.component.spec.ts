import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LuckyCapsulesComponent } from './lucky-capsules.component';

describe('LuckyCapsulesComponent', () => {
  let component: LuckyCapsulesComponent;
  let fixture: ComponentFixture<LuckyCapsulesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LuckyCapsulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LuckyCapsulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
