import { Component, OnInit, ViewChild } from '@angular/core';
import { PageDescriptionService } from '@services/page-description.service';
import { ROUTES_URL } from '@app/routing/routes';
import { environment } from '@environments/environment';
import { PAGES_NAME } from '@app/global-strings';
/*import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditorComponent } from '@ckeditor/ckeditor5-angular';*/

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

    readonly ROUTES = ROUTES_URL;

    readonly PAGES =  PAGES_NAME;

    readonly environment = environment;

    /*public model = {
       editorData: 'hello ${ITEM:1} its dope'
    };

    public editor = ClassicEditor;

    @ViewChild('ckeditor', { static: false }) public ckeditor: CKEditorComponent;*/

    constructor(
        protected pageDescriptionService: PageDescriptionService
    ) { }

    ngOnInit(): void {
        this.pageDescriptionService.setPageDescription({
            description: 'Fiesta-Wiki is a fan-made database and wiki for the MMORPG Fiesta Online currently developed by Gamigo.'
        });
    }

    onAddToto(): void {

        /*const selection = this.ckeditor.editorInstance.model.document.selection;
        const range = selection.getFirstRange();


        this.ckeditor.editorInstance.model.change(writer => {
           writer.insert('appendData', range.start);
        });*/
    }
}
