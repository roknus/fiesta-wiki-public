import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { environment } from '@environments/environment';

export class PageDescription {
    title?: string;
    category?: string;
    description?: string;
}

@Injectable({
    providedIn: 'root'
})
export class PageDescriptionService {

    constructor(
        private titleService: Title,
        private meta: Meta
    ) { }

    setPageDescription(page: PageDescription) {

        if (page.title) {
            this.setTitle(page.title, page.category);
        } else {
            this.titleService.setTitle('Fiesta Wiki - Fiesta Online Wiki and Database');
        }

        if (page.description) {
            this.meta.updateTag({ name: 'description', content: page.description });
        } else {
            this.meta.updateTag(
                {
                    name: 'description',
                    content: 'Fiesta-Wiki is a fan-made database and wiki for the MMORPG Fiesta Online currently developed by Gamigo.'
                });
        }
    }

    setTitle(t: string, category?: string) {

        let title = t;
        if (category !== undefined) {
            title = title + ' - ' + category;
        }
        this.titleService.setTitle(`${title} | ${environment.locale.title}`);
    }
}
