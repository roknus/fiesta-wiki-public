import { Injectable } from '@angular/core';
import { Title } from '@dto/titles/title';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

   constructor(private http: HttpClient) { }

   getTitles(): Observable<Title[]> {
      return this.http.get<Title[]>(environment.api + 'titles');
   }
}
