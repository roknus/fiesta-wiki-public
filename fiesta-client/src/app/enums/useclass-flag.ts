import { UseClass } from './useclass';

export enum UseClassFlag {
   None            = 0,
   Fighter         = 1 << 0,
   CleverFighter   = 1 << 1,
   Warrior         = 1 << 2,
   Knight          = 1 << 3,
   Gladiator       = 1 << 4,
   Cleric          = 1 << 5,
   HighCleric      = 1 << 6,
   Paladin         = 1 << 7,
   Guardian        = 1 << 8,
   HolyKnight      = 1 << 9,
   Archer          = 1 << 10,
   HawkArcher      = 1 << 11,
   Scout           = 1 << 12,
   Ranger          = 1 << 13,
   SharpShooter    = 1 << 14,
   Mage            = 1 << 15,
   WizMage         = 1 << 16,
   Enchanter       = 1 << 17,
   Wizard          = 1 << 18,
   Warlock         = 1 << 19,
   Trickster       = 1 << 20,
   Gambit          = 1 << 21,
   Renegade        = 1 << 22,
   Reaper          = 1 << 23,
   Spectre         = 1 << 24,
   Crusader        = 1 << 25,
   Templar         = 1 << 26,
}

export class UseClassFlagUtils {

   public static fromUseClass(useClass: number): number {
      switch (useClass) {
         case UseClass.Fighter: return UseClassFlag.Fighter;
         case UseClass.CleverFighter: return UseClassFlag.CleverFighter;
         case UseClass.Warrior: return UseClassFlag.Warrior;
         case UseClass.KnightGladiator: return UseClassFlag.Knight | UseClassFlag.Gladiator;
         case UseClass.Gladiator: return UseClassFlag.Gladiator;
         case UseClass.Knight: return UseClassFlag.Knight;
         case UseClass.Cleric: return UseClassFlag.Cleric;
         case UseClass.HighCleric: return UseClassFlag.HighCleric;
         case UseClass.Paladin: return UseClassFlag.Paladin;
         case UseClass.Guardian: return UseClassFlag.Guardian;
         case UseClass.HolyKnight: return UseClassFlag.HolyKnight;
         case UseClass.GuardianHolyKnight: return UseClassFlag.Guardian | UseClassFlag.HolyKnight;
         case UseClass.Archer: return UseClassFlag.Archer;
         case UseClass.HawkArcher: return UseClassFlag.HawkArcher;
         case UseClass.Scout: return UseClassFlag.Scout;
         case UseClass.Ranger: return UseClassFlag.Ranger;
         case UseClass.SharpShooter: return UseClassFlag.SharpShooter;
         case UseClass.RangerSharpShooter: return UseClassFlag.Ranger | UseClassFlag.SharpShooter;
         case UseClass.Mage: return UseClassFlag.Mage;
         case UseClass.WizMage: return UseClassFlag.WizMage;
         case UseClass.Enchanter: return UseClassFlag.Enchanter;
         case UseClass.Wizard: return UseClassFlag.Wizard;
         case UseClass.Warlock: return UseClassFlag.Warlock;
         case UseClass.WizardWarlock: return UseClassFlag.Wizard | UseClassFlag.Warlock;
         case UseClass.Trickster: return UseClassFlag.Trickster;
         case UseClass.Gambit: return UseClassFlag.Gambit;
         case UseClass.Renegade: return UseClassFlag.Renegade;
         case UseClass.Reaper: return UseClassFlag.Reaper;
         case UseClass.Spectre: return UseClassFlag.Spectre;
         case UseClass.ReaperSpectre: return UseClassFlag.Reaper | UseClassFlag.Spectre;
         case UseClass.Crusader: return UseClassFlag.Crusader;
         case UseClass.Templar: return UseClassFlag.Templar;
         case UseClass.WarriorScout: return UseClassFlag.Warrior | UseClassFlag.Scout;
         case UseClass.PaladinWarrior: return UseClassFlag.Paladin | UseClassFlag.Warrior;
      }
      return 0;
   }

   public static completeWithSubclass(useClass: number): number {

      let completeSubClasses = useClass;

      switch (useClass) {
      case UseClassFlag.Fighter:
         break;
      case UseClassFlag.CleverFighter:
         completeSubClasses |= UseClassFlag.Fighter;
         break;
      case UseClassFlag.Warrior:
         completeSubClasses |= UseClassFlag.CleverFighter | UseClassFlag.Fighter;
         break;
      case UseClassFlag.Knight:
      case UseClassFlag.Gladiator:
         completeSubClasses |= UseClassFlag.Warrior | UseClassFlag.CleverFighter | UseClassFlag.Fighter;
         break;
      case UseClassFlag.Cleric:
         break;
      case UseClassFlag.HighCleric:
         completeSubClasses |= UseClassFlag.Cleric;
         break;
      case UseClassFlag.Paladin:
         completeSubClasses |= UseClassFlag.HighCleric | UseClassFlag.Cleric;
         break;
      case UseClassFlag.HolyKnight:
      case UseClassFlag.Guardian:
         completeSubClasses |= UseClassFlag.Paladin | UseClassFlag.HighCleric | UseClassFlag.Cleric;
         break;
      case UseClassFlag.Archer:
         break;
      case UseClassFlag.HawkArcher:
         completeSubClasses |= UseClassFlag.Archer;
         break;
      case UseClassFlag.Scout:
         completeSubClasses |= UseClassFlag.HawkArcher | UseClassFlag.Archer;
         break;
      case UseClassFlag.SharpShooter:
      case UseClassFlag.Ranger:
         completeSubClasses |= UseClassFlag.Scout | UseClassFlag.HawkArcher | UseClassFlag.Archer;
         break;
      case UseClassFlag.Mage:
         break;
      case UseClassFlag.WizMage:
         completeSubClasses |= UseClassFlag.Mage;
         break;
      case UseClassFlag.Enchanter:
         completeSubClasses |= UseClassFlag.WizMage | UseClassFlag.Mage;
         break;
      case UseClassFlag.Wizard:
      case UseClassFlag.Warlock:
         completeSubClasses |= UseClassFlag.Enchanter | UseClassFlag.WizMage | UseClassFlag.Mage;
         break;
      case UseClassFlag.Trickster:
         break;
      case UseClassFlag.Gambit:
         completeSubClasses |= UseClassFlag.Trickster;
         break;
      case UseClassFlag.Renegade:
         completeSubClasses |= UseClassFlag.Gambit | UseClassFlag.Trickster;
         break;
      case UseClassFlag.Reaper:
      case UseClassFlag.Spectre:
         completeSubClasses |= UseClassFlag.Renegade | UseClassFlag.Gambit | UseClassFlag.Trickster;
         break;
      case UseClassFlag.Crusader:
         break;
      case UseClassFlag.Templar:
         completeSubClasses |= UseClassFlag.Crusader;
         break;
      }

      return completeSubClasses;
   }
}
