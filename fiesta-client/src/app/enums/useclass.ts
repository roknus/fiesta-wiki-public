export enum UseClass {
   All = 1,
   Fighter = 2,
   CleverFighter = 3,
   Warrior = 4,
   KnightGladiator = 5,
   Gladiator = 6,
   Knight = 7,
   Cleric = 8,
   HighCleric = 9,
   Paladin = 10,
   Guardian = 11,
   HolyKnight = 12,
   GuardianHolyKnight = 13,
   Archer = 14,
   HawkArcher = 15,
   Scout = 16,
   Ranger = 17,
   SharpShooter = 18,
   RangerSharpShooter = 19,
   Mage = 20,
   WizMage = 21,
   Enchanter = 22,
   Wizard = 23,
   Warlock = 24,
   WizardWarlock = 25,
   Trickster = 27,
   Gambit = 28,
   Renegade = 29,
   Reaper = 30,
   Spectre = 31,
   ReaperSpectre = 32,
   Crusader = 33,
   Templar = 34,
   WarriorScout = 35,
   PaladinWarrior = 36,
   KnightGuardianHolyKnight = 38
}

export class UseClassUtils {

   public static classTier(useClass: number): number {
      switch (useClass) {
         case UseClass.Fighter:
         case UseClass.Cleric:
         case UseClass.Archer:
         case UseClass.Mage:
         case UseClass.Trickster:
            return 0;
         case UseClass.CleverFighter:
         case UseClass.HighCleric:
         case UseClass.HawkArcher:
         case UseClass.WizMage:
         case UseClass.Gambit:
            return 1;
         case UseClass.Warrior:
         case UseClass.Paladin:
         case UseClass.Scout:
         case UseClass.Renegade:
         case UseClass.Enchanter:
         case UseClass.Crusader:
            return 2;
         case UseClass.Gladiator:
         case UseClass.Knight:
         case UseClass.Guardian:
         case UseClass.HolyKnight:
         case UseClass.Ranger:
         case UseClass.SharpShooter:
         case UseClass.Wizard:
         case UseClass.Warlock:
         case UseClass.Reaper:
         case UseClass.Spectre:
         case UseClass.Templar:
            return 3;
      }
      return -1;
   }

   public static isSubClass(useClass: number, subClass: number): boolean {

      if (useClass === subClass) {
         return true;
      }

      switch (subClass) {
         case UseClass.Fighter: {
            switch (useClass) {
               case UseClass.CleverFighter:
               case UseClass.Warrior:
               case UseClass.Knight:
               case UseClass.Gladiator:
               case UseClass.WarriorScout:
               case UseClass.PaladinWarrior:
               case UseClass.KnightGladiator:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.CleverFighter: {
            switch (useClass) {
               case UseClass.Warrior:
               case UseClass.Knight:
               case UseClass.Gladiator:
               case UseClass.WarriorScout:
               case UseClass.PaladinWarrior:
               case UseClass.KnightGladiator:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Warrior: {
            switch (useClass) {
               case UseClass.Knight:
               case UseClass.Gladiator:
               case UseClass.WarriorScout:
               case UseClass.PaladinWarrior:
               case UseClass.KnightGladiator:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.KnightGladiator: {
            switch (useClass) {
               case UseClass.Gladiator:
               case UseClass.Knight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Gladiator: {
            switch (useClass) {
               case UseClass.KnightGladiator:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Knight: {
            switch (useClass) {
               case UseClass.KnightGladiator:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Cleric: {
            switch (useClass) {
               case UseClass.HighCleric:
               case UseClass.Paladin:
               case UseClass.Guardian:
               case UseClass.HolyKnight:
               case UseClass.PaladinWarrior:
               case UseClass.GuardianHolyKnight:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.HighCleric: {
            switch (useClass) {
               case UseClass.Paladin:
               case UseClass.Guardian:
               case UseClass.HolyKnight:
               case UseClass.PaladinWarrior:
               case UseClass.GuardianHolyKnight:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Paladin: {
            switch (useClass) {
               case UseClass.Guardian:
               case UseClass.HolyKnight:
               case UseClass.PaladinWarrior:
               case UseClass.GuardianHolyKnight:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Guardian: {
            switch (useClass) {
               case UseClass.GuardianHolyKnight:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.HolyKnight: {
            switch (useClass) {
               case UseClass.GuardianHolyKnight:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.GuardianHolyKnight: {
            switch (useClass) {
               case UseClass.KnightGuardianHolyKnight:
               case UseClass.Knight:
               case UseClass.Guardian:
               case UseClass.HolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Archer: {
            switch (useClass) {
               case UseClass.HawkArcher:
               case UseClass.Scout:
               case UseClass.SharpShooter:
               case UseClass.Ranger:
               case UseClass.WarriorScout:
               case UseClass.RangerSharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.HawkArcher: {
            switch (useClass) {
               case UseClass.Scout:
               case UseClass.SharpShooter:
               case UseClass.Ranger:
               case UseClass.WarriorScout:
               case UseClass.RangerSharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Scout: {
            switch (useClass) {
               case UseClass.SharpShooter:
               case UseClass.Ranger:
               case UseClass.WarriorScout:
               case UseClass.RangerSharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Ranger: {
            switch (useClass) {
               case UseClass.RangerSharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.SharpShooter: {
            switch (useClass) {
               case UseClass.RangerSharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.RangerSharpShooter: {
            switch (useClass) {
               case UseClass.Ranger:
               case UseClass.SharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Mage: {
            switch (useClass) {
               case UseClass.WizMage:
               case UseClass.Enchanter:
               case UseClass.Wizard:
               case UseClass.Warlock:
               case UseClass.WizardWarlock:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.WizMage: {
            switch (useClass) {
               case UseClass.Enchanter:
               case UseClass.Wizard:
               case UseClass.Warlock:
               case UseClass.WizardWarlock:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Enchanter: {
            switch (useClass) {
               case UseClass.Wizard:
               case UseClass.Warlock:
               case UseClass.WizardWarlock:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Wizard: {
            switch (useClass) {
               case UseClass.WizardWarlock:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Warlock: {
            switch (useClass) {
               case UseClass.WizardWarlock:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.WizardWarlock: {
            switch (useClass) {
               case UseClass.Wizard:
               case UseClass.Warlock:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Trickster: {
            switch (useClass) {
               case UseClass.Gambit:
               case UseClass.Renegade:
               case UseClass.Reaper:
               case UseClass.Spectre:
               case UseClass.ReaperSpectre:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Gambit: {
            switch (useClass) {
               case UseClass.Renegade:
               case UseClass.Reaper:
               case UseClass.Spectre:
               case UseClass.ReaperSpectre:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Renegade: {
            switch (useClass) {
               case UseClass.Reaper:
               case UseClass.Spectre:
               case UseClass.ReaperSpectre:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Reaper: {
            switch (useClass) {
               case UseClass.ReaperSpectre:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Spectre: {
            switch (useClass) {
               case UseClass.ReaperSpectre:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.ReaperSpectre: {
            switch (useClass) {
               case UseClass.Spectre:
               case UseClass.Reaper:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Crusader: {
            switch (useClass) {
               case UseClass.Templar:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.Templar: {
            return false;
         }
         case UseClass.WarriorScout: {
            switch (useClass) {
               case UseClass.Warrior:
               case UseClass.Knight:
               case UseClass.Gladiator:
               case UseClass.KnightGladiator:
               case UseClass.KnightGuardianHolyKnight:
               case UseClass.Scout:
               case UseClass.Ranger:
               case UseClass.SharpShooter:
               case UseClass.RangerSharpShooter:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.PaladinWarrior: {
            switch (useClass) {
               case UseClass.Paladin:
               case UseClass.Guardian:
               case UseClass.HolyKnight:
               case UseClass.GuardianHolyKnight:
               case UseClass.Warrior:
               case UseClass.Knight:
               case UseClass.Gladiator:
               case UseClass.KnightGladiator:
               case UseClass.KnightGuardianHolyKnight:
                  return true;
               default:
                  return false;
            }
         }
         case UseClass.KnightGuardianHolyKnight: {
            switch (useClass) {
               case UseClass.GuardianHolyKnight:
               case UseClass.Guardian:
               case UseClass.HolyKnight:
               case UseClass.Knight:
                  return true;
               default:
                  return false;
            }
         }
      }
      return false;
   }
}
