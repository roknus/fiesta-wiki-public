import { Item, ItemDTO } from '@fiesta-common/dto/items/item';

export class LuckyCapsuleDTO {
   capsule_id: number;
   server_id: number;
}

export class LuckyCapsule {
   capsule: Item;
   server_id: number;
}

export class LuckyCapsuleReward {
   item: Item;
}

export class LuckyCapsuleGroupRate {
   id: number;
   rewards: LuckyCapsuleReward[];
   rate: number;
}

export interface LuckyCapsuleRewardDTO {
   item: ItemDTO;
}

export interface LuckyCapsuleGroupRateDTO {
   id: number;
   rewards: LuckyCapsuleRewardDTO[];
   rate: number;
}
