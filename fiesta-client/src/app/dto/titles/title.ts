import { CardMinimal } from '@fiesta-common/dto/cards/card';

export interface TitleStat {
   titlelv: number;
   descript: string;
}

export interface TitleView {
   unlock_type: number;
   obtainable: number;
   description: string;
}

export interface Title {
   permit: number;
   refresh: number;
   title: string;
   value: number;
   fame: number;
   stats: TitleStat[];
   view: TitleView;
   card: CardMinimal;
}
