import { Directive, HostListener } from '@angular/core';
import { KeyboardService } from '@fiesta-common/services/keyboard.service';

@Directive({
   selector: '[appKeypressCatcher]'
})
export class KeypressCatcherDirective {

   @HostListener('document:keydown', ['$event'])
   onKeyDown(event: KeyboardEvent) {
      if (!event.repeat && event.ctrlKey) {
         this.keyboardService.onCtrl(true);
      }
   }

   @HostListener('document:keyup', ['$event'])
   onKeyUp(event: KeyboardEvent) {
      if (!event.ctrlKey) {
         this.keyboardService.onCtrl(false);
      }
   }

   constructor(private keyboardService: KeyboardService) { }
}
