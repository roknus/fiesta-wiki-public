import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './components/app.component';
import { QuestsComponent } from './components/pages/quests-page/quests.component';
import { RewardIconComponent } from './components/quest/reward-icon/reward-icon.component';
import { QuestLevelComponent } from './components/pages/quests-page/quest-level/quest-level.component';
import { QuestPreviewComponent } from '@components/quest/quest-preview/quest-preview.component';
import { NumberSpacerPipe } from './pipes/numbers';
import { NavigationBarComponent } from '@components/navigation-bar/navigation-bar.component';
import { WeaponsPageComponent } from '@components/pages/items-page/weapons-page/weapons-page.component';
import { ShieldsPageComponent } from '@components/pages/items-page/shields-page/shields-page.component';
import { GearsPageComponent } from '@components/pages/items-page/gears-page/gears-page.component';
import { QuestIconComponent } from './components/quest/quest-icon/quest-icon.component';
import { QuestNameComponent } from './components/quest/quest-name/quest-name.component';
import { AngularMaterialModule } from '../angular-material/angular-material-module.module';
import { JewelsPageComponent } from './components/pages/items-page/jewels-page/jewels-page.component';
import { EnhancementComboboxComponent } from './components/enhancement-combobox/enhancement-combobox.component';
import { BraceletsPageComponent } from './components/pages/items-page/bracelets-page/bracelets-page.component';
import { AlchemyPageComponent } from './components/pages/alchemy-page/alchemy-page.component';
import { Title } from '@angular/platform-browser';
import { FooterComponent } from './components/footer/footer.component';
import { MainPageComponent } from './components/pages/main-page/main-page.component';
import { TitlesPageComponent } from './components/pages/titles-page/titles-page.component';
import { PrintfPipe } from './pipes/printf';
import { FiestaMoneyPipe } from './pipes/money-pipe';
import { PremiumItemsPageComponent } from './components/pages/items-page/premium-items-page/premium-items-page.component';
import { LoginService } from 'admin/services/login.service';
import { FiestaCommonModule } from '@fiesta-common/fiesta-common.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTreeModule } from '@angular/material/tree';
import { MatMenuModule } from '@angular/material/menu';
import { KeypressCatcherDirective } from './directives/keypress-catcher.directive';
import {
    PremiumItemsSearchComponent
} from './components/pages/items-page/premium-items-page/premium-items-search/premium-items-search.component';
import {
    SearchBottomSheetComponent
} from './components/pages/items-page/premium-items-page/search-bottom-sheet/search-bottom-sheet.component';
import { LuckyCapsulesComponent } from '@components/pages/lucky-capsules/lucky-capsules.component';
import { LuckyCapsulesResolver } from '@components/pages/lucky-capsules/lucky-capsules-resolver';
import { LuckyCapsulesService } from '@services/lucky-capsules.service';
import { QuestDescriptionComponent } from './components/quest/quest-description/quest-description.component';
import { QuestLineComponent } from './components/quest/quest-line/quest-line.component';
import { QuestingToolComponent } from '@components/pages/questing-tool/questing-tool.component';
import { PageDescriptionService } from '@services/page-description.service';
// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { WeaponsMenuComponent } from './components/menus/weapons-menu/weapons-menu.component';
import { JewelsMenuComponent } from './components/menus/jewels-menu/jewels-menu.component';
import { TalismansPageComponent } from './components/pages/items-page/talismans-page/talismans-page.component';
import { ShieldsMenuComponent } from './components/menus/shields-menu/shields-menu.component';
import { GearsMenuComponent } from './components/menus/gears-menu/gears-menu.component';
import { MobListPageComponent } from './components/pages/mob-list-page/mob-list-page.component';
import { MobPageComponent } from './components/pages/mob-page/mob-page.component';
import { MobPageResolver } from './components/pages/mob-page/mob-page-resolver';
import { WeaponsPageResolver, WeaponsPageColumnResolver } from './components/pages/items-page/weapons-page/weapons-page-resolver';
import { ShieldsPageResolver } from './components/pages/items-page/shields-page/shields-page-resolver';
import { JewelsPageResolver, JewelsPageColumnResolver } from './components/pages/items-page/jewels-page/jewels-page-resolver';
import { GearsPageResolver } from '@components/pages/items-page/gears-page/gears-page-resolver';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { QuestPageResolver } from '@components/pages/quests-page/quest-page-resolver';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SkillsMenuComponent } from './components/menus/skills-menu/skills-menu.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { ROUTES_URL } from './routing/routes';

export const appRoutes: Routes = [
    { path: ROUTES_URL.quests, redirectTo: ROUTES_URL.quests + '/0' },
    {
        path: ROUTES_URL.quests + '/:page',
        component: QuestsComponent,
        resolve: { pageData: QuestPageResolver },
        runGuardsAndResolvers: 'paramsOrQueryParamsChange'
    },
    { path: ROUTES_URL.questingTool, redirectTo: ROUTES_URL.questingTool + '/1' },
    { path: ROUTES_URL.questingTool + '/:level', component: QuestingToolComponent },
    { path: ROUTES_URL.weapons + '/:weaponType', redirectTo: ROUTES_URL.weapons + '/:weaponType/0' },
    {
        path: ROUTES_URL.weapons + '/:weaponType/:page',
        component: WeaponsPageComponent,
        resolve:
        {
            pageDescription: WeaponsPageResolver,
            pageColumns: WeaponsPageColumnResolver
        }
    },
    { path: ROUTES_URL.shields + '/:shieldType', redirectTo: ROUTES_URL.shields + '/:shieldType/0' },
    { path: ROUTES_URL.shields + '/:shieldType/:page', component: ShieldsPageComponent, resolve: { pageDescription: ShieldsPageResolver } },
    { path: ROUTES_URL.gears + '/:class', redirectTo: ROUTES_URL.gears + '/:class/0' },
    {
        path: ROUTES_URL.gears + '/:class/:page',
        component: GearsPageComponent,
        resolve:
        {
            pageDescription: GearsPageResolver
        }
    },
    { path: ROUTES_URL.jewels + '/:jewelType', redirectTo: ROUTES_URL.jewels + '/:jewelType/0' },
    {
        path: ROUTES_URL.jewels + '/:jewelType/:page',
        component: JewelsPageComponent,
        resolve:
        {
            pageDescription: JewelsPageResolver,
            pageColumns: JewelsPageColumnResolver
        }
    },
    { path: ROUTES_URL.bracelets, redirectTo: ROUTES_URL.bracelets + '/0' },
    { path: ROUTES_URL.bracelets + '/:page', component: BraceletsPageComponent },
    { path: ROUTES_URL.talismans, redirectTo: ROUTES_URL.talismans + '/0' },
    { path: ROUTES_URL.talismans + '/:page', component: TalismansPageComponent },
    { path: ROUTES_URL.premium, redirectTo: ROUTES_URL.premium + '/0' },
    { path: ROUTES_URL.premium + '/:page', component: PremiumItemsPageComponent },
    { path: ROUTES_URL.alchemy, redirectTo: ROUTES_URL.alchemy + '/0' },
    { path: ROUTES_URL.alchemy + '/:type', component: AlchemyPageComponent },
    { path: ROUTES_URL.titles, component: TitlesPageComponent },
    { path: ROUTES_URL.luckyCapsule, component: LuckyCapsulesComponent, resolve: { pageData: LuckyCapsulesResolver }, data: { type: 2 } },
    { path: ROUTES_URL.mobs, component: MobListPageComponent },
    { path: ROUTES_URL.mobs + '/:id', component: MobPageComponent, resolve: { pageData: MobPageResolver } },
    {
        path: '',
        component: MainPageComponent,
        pathMatch: 'full'
    },
    { path: ROUTES_URL.skills, loadChildren: () => import('../skills/skills.module').then(m => m.SkillsModule) },
    { path: ROUTES_URL.cards, loadChildren: () => import('../cards/cards.module').then(m => m.CardsModule) },
    { path: ROUTES_URL.licenses, loadChildren: () => import('../license/license.module').then(m => m.LicenseModule) },
    { path: ROUTES_URL.notFound, component: PageNotFoundComponent },
    { path: '**', redirectTo: ROUTES_URL.notFound }
];

@NgModule({
    declarations: [
        // Pipes
        NumberSpacerPipe,
        PrintfPipe,
        FiestaMoneyPipe,
        // Components
        AppComponent,
        QuestsComponent,
        RewardIconComponent,
        QuestLevelComponent,
        QuestPreviewComponent,
        NavigationBarComponent,
        WeaponsPageComponent,
        ShieldsPageComponent,
        GearsPageComponent,
        QuestIconComponent,
        QuestNameComponent,
        JewelsPageComponent,
        EnhancementComboboxComponent,
        BraceletsPageComponent,
        AlchemyPageComponent,
        FooterComponent,
        MainPageComponent,
        TitlesPageComponent,
        PremiumItemsPageComponent,
        KeypressCatcherDirective,
        PremiumItemsSearchComponent,
        SearchBottomSheetComponent,
        LuckyCapsulesComponent,
        QuestDescriptionComponent,
        QuestLineComponent,
        QuestingToolComponent,
        WeaponsMenuComponent,
        JewelsMenuComponent,
        TalismansPageComponent,
        ShieldsMenuComponent,
        GearsMenuComponent,
        MobListPageComponent,
        MobPageComponent,
        PageNotFoundComponent,
        SkillsMenuComponent,
    ],
    imports: [
        RouterModule.forRoot(appRoutes, {
            enableTracing: false,
            onSameUrlNavigation: 'reload',
            anchorScrolling: 'enabled',
            scrollOffset: [0, 40],
            initialNavigation: 'enabled',
            relativeLinkResolution: 'legacy'
        }),
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        AngularMaterialModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        FiestaCommonModule,
        MatChipsModule,
        MatCheckboxModule,
        MatListModule,
        MatSidenavModule,
        MatTreeModule,
        MatMenuModule,
        MatIconModule,
        MatTooltipModule,
        MatExpansionModule,
        MatBottomSheetModule,
        // CKEditorModule,
    ],
    providers: [
        LoginService,
        LuckyCapsulesService,
        Title,
        FiestaMoneyPipe,
        PageDescriptionService,
    ],
    entryComponents: [
        SearchBottomSheetComponent,
    ]
})
export class AppModule { }
