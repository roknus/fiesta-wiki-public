import {
   Component,
   OnInit,
   HostBinding,
   ViewChild,
   ViewContainerRef,
   HostListener,
   ElementRef,
   ComponentFactoryResolver,
   AfterViewChecked,
   Type
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';

class Rect {
   x: number;
   y: number;
   width: number;
   height: number;

   get left(): number { return this.x; }
   set left(l: number) { this.x = l; }
   get right(): number { return this.x + this.width; }
   set right(r: number) { this.x = r - this.width; }
   get top(): number { return this.y; }
   set top(t: number) { this.y = t; }
   get bottom(): number { return this.y + this.height; }
   set bottom(b: number) { this.y = b - this.height; }

   constructor(x: number, y: number, width: number, height: number) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
   }

   toString(): string {
      return this.left + ' ' + this.top + ' ' + this.right + ' ' + this.bottom;
   }
}

@Component({
   selector: 'app-tooltip',
   templateUrl: './tooltip.component.html',
   styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent<T> implements OnInit, AfterViewChecked {

   @ViewChild('container', { read: ViewContainerRef, static: true }) vcr: ViewContainerRef;

   private fadeInTimer: any;
   private fadeOutTimer: any;

   show = false;

   target: Element;

   componentType: Type<T>;

   view: BehaviorSubject<T> = new BehaviorSubject(null);

   @HostBinding('style.left.px') left = -1000;
   @HostBinding('style.top.px') top = -1000;
   @HostBinding('style.display') display = 'block';

   @HostListener('mouseenter')
   mouseEnter(): void {

      clearTimeout(this.fadeOutTimer);
   }

   @HostListener('mouseleave')
   mouseLeave(event: MouseEvent): void {

      this.fadeOut(event);
   }

   get size(): Rect { return this.elementRef.nativeElement.getBoundingClientRect(); }

   constructor(
      private componentFactoryResolver: ComponentFactoryResolver,
      private elementRef: ElementRef) {
   }

   ngOnInit() {
      this.create();
   }

   ngAfterViewChecked() {
      if (this.size.width === 0) {
         return;
      }

      const rect2 = this.calculateBestTooltipPosition(this.target, this.size);

      setTimeout(() => {
         this.setPosition(rect2.left + document.documentElement.scrollLeft, rect2.top + document.documentElement.scrollTop);
      });
   }

   create() {

      const componentFactory = this.componentFactoryResolver.resolveComponentFactory<T>(this.componentType);

      const compRef = this.vcr.createComponent<T>(componentFactory);

      this.view.next(compRef.instance);
   }

   fadeIn() {
      clearTimeout(this.fadeOutTimer);

      this.fadeInTimer = setTimeout(() => {

         this.show = true;
      }, 200);
   }

   fadeOut(event: MouseEvent) {

      clearTimeout(this.fadeInTimer);

      this.fadeOutTimer = setTimeout(() => {
         this.show = false;
      }, 100);
   }

   setPosition(left: number, top: number): void {
      this.left = left;
      this.top = top;
   }

   private rectIntersect(r1: Rect, r2: Rect): boolean {
      return !(r2.left > r1.right ||
         r2.right < r1.left ||
         r2.top > r1.bottom ||
         r2.bottom < r1.top);
   }

   calculateBestTooltipPosition(target: Element, tooltipRect: Rect): Rect {
      const elRect = target.getBoundingClientRect();

      // viewport size
      const viewportWidth = document.documentElement.clientWidth;
      const viewportHeight = document.documentElement.clientHeight;

      // tooltip rect
      const ttrect = new Rect(elRect.right, elRect.top - tooltipRect.height, tooltipRect.width, tooltipRect.height);

      // element rect
      const elementRect = new Rect(elRect.left, elRect.bottom, elRect.width, elRect.height);

      if (ttrect.bottom > viewportHeight) {
         ttrect.bottom = viewportHeight;
         if (this.rectIntersect(ttrect, elementRect)) {
            ttrect.left = elementRect.right;
         }
      }
      if (ttrect.top < 0) {
         ttrect.top = 0;
         if (this.rectIntersect(ttrect, elementRect)) {
            ttrect.left = elementRect.right;
         }
      }
      if (ttrect.left < 0) {
         ttrect.left = 0;
         if (this.rectIntersect(ttrect, elementRect)) {
            ttrect.left = elementRect.right;
         }
      }
      if (ttrect.right > viewportWidth) {
         ttrect.right = viewportWidth;
         if (this.rectIntersect(ttrect, elementRect)) {
            ttrect.right = elementRect.left;
         }
      }

      return ttrect;
   }
}
