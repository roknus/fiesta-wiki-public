import { Injectable, ApplicationRef, Injector, EmbeddedViewRef, ComponentFactoryResolver, ComponentRef, Type } from '@angular/core';
import { TooltipComponent } from './tooltip/tooltip.component';

@Injectable({
   providedIn: 'root'
})
export class TooltipService {

   constructor(
      private componentFactoryResolver: ComponentFactoryResolver,
      private appRef: ApplicationRef,
      private injector: Injector) {
   }

   appendComponentToBody<T>(component: Type<T>, target: Element): ComponentRef<TooltipComponent<T>> {
      // 1. Create a component reference from the component
      const tooltipComponentRef = this.componentFactoryResolver
         .resolveComponentFactory<TooltipComponent<T>>(TooltipComponent)
         .create(this.injector);

      // 2. Attach component to the appRef so that it's inside the ng component tree
      this.appRef.attachView(tooltipComponentRef.hostView);

      // 3. Get DOM element from component
      const domElem = (tooltipComponentRef.hostView as EmbeddedViewRef<any>)
         .rootNodes[0] as HTMLElement;

      // 4. Append DOM element to the body
      document.body.appendChild(domElem);

      tooltipComponentRef.instance.componentType = component;
      tooltipComponentRef.instance.target = target;

      return tooltipComponentRef;
   }
}
