import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TooltipService } from './tooltip.service';

@NgModule({
  declarations: [
    TooltipComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
     TooltipService
  ],
  exports: [
    TooltipComponent,
  ],
  entryComponents: [
    TooltipComponent
  ]
})
export class TooltipModule { }
