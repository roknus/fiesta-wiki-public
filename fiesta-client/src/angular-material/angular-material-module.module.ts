import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
   declarations: [],
   imports: [
      CommonModule,
      MatButtonModule,
      MatSelectModule,
      MatInputModule,
      MatPaginatorModule,
      MatTableModule,
      MatSortModule,
      MatCardModule,
      MatToolbarModule,
      MatProgressSpinnerModule,
      MatDividerModule,
      MatSliderModule,
   ],
   exports: [
      MatButtonModule,
      MatSelectModule,
      MatInputModule,
      MatPaginatorModule,
      MatTableModule,
      MatSortModule,
      MatCardModule,
      MatToolbarModule,
      MatProgressSpinnerModule,
      MatDividerModule,
      MatSliderModule
   ]
})
export class AngularMaterialModule { }
