package com.fiestawiki.production;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dto.ItemDTO;

public class ProductionDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("recipe")
    private ItemDTO recipe;

    @JsonProperty("name")
    private String name;

    @JsonProperty("product")
    private ItemDTO product;

    @JsonProperty("quantity")
    private Long quantity;

    @JsonProperty("ingredient1")
    private ItemDTO ingredient1;

    @JsonProperty("quantity1")
    private Long quantity1;

    @JsonProperty("ingredient2")
    private ItemDTO ingredient2;

    @JsonProperty("quantity2")
    private Long quantity2;

    @JsonProperty("ingredient3")
    private ItemDTO ingredient3;

    @JsonProperty("quantity3")
    private Long quantity3;

    @JsonProperty("ingredient4")
    private ItemDTO ingredient4;

    @JsonProperty("quantity4")
    private Long quantity4;

    @JsonProperty("mastery_type")
    private Long masteryType;

    @JsonProperty("mastery_gain")
    private Long masteryGain;

    @JsonProperty("needed_mastery")
    private Long neededMastery;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ItemDTO getRecipe() { return recipe; }

    public void setRecipe(ItemDTO recipe) { this.recipe = recipe; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ItemDTO getProduct() { return product; }

    public void setProduct(ItemDTO product) { this.product = product; }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public ItemDTO getIngredient1() { return ingredient1; }

    public void setIngredient1(ItemDTO ingredient1) { this.ingredient1 = ingredient1; }

    public Long getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(Long quantity1) {
        this.quantity1 = quantity1;
    }

    public ItemDTO getIngredient2() { return ingredient2; }

    public void setIngredient2(ItemDTO ingredient2) { this.ingredient2 = ingredient2; }

    public Long getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(Long quantity2) {
        this.quantity2 = quantity2;
    }

    public ItemDTO getIngredient3() { return ingredient3; }

    public void setIngredient3(ItemDTO ingredient3) { this.ingredient3 = ingredient3; }

    public Long getQuantity3() {
        return quantity3;
    }

    public void setQuantity3(Long quantity3) {
        this.quantity3 = quantity3;
    }

    public ItemDTO getIngredient4() { return ingredient4; }

    public void setIngredient4(ItemDTO ingredient4) { this.ingredient4 = ingredient4; }

    public Long getQuantity4() {
        return quantity4;
    }

    public void setQuantity4(Long quantity4) {
        this.quantity4 = quantity4;
    }

    public Long getMasteryType() {
        return masteryType;
    }

    public void setMasteryType(Long masteryType) {
        this.masteryType = masteryType;
    }

    public Long getMasteryGain() {
        return masteryGain;
    }

    public void setMasteryGain(Long masteryGain) {
        this.masteryGain = masteryGain;
    }

    public Long getNeededMastery() {
        return neededMastery;
    }

    public void setNeededMastery(Long neededMastery) {
        this.neededMastery = neededMastery;
    }
}
