package com.fiestawiki.production;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductionRepository extends JpaRepository<Production, Long> {

    @Query("SELECT p " +
            "FROM Production p " +
            "WHERE p.masteryType = :type " +
            "ORDER BY p.neededMastery ASC"
    )
    List<Production> findByType(Long type);

    @Query("SELECT p " +
            "FROM Production p " +
            "WHERE p.recipe = :inxname"
    )
    Optional<Production> findByRecipe(String inxname);
}
