package com.fiestawiki.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductionController {

    @Autowired
    private ProductionService productionService;

    @GetMapping("production/{type}")
    public ResponseEntity<List<ProductionDTO>> getProductions(@PathVariable(value = "type") Long type)
            throws Exception
    {
        List<ProductionDTO> productions = productionService.getProductions(type);

        return ResponseEntity.ok(productions);
    }
}
