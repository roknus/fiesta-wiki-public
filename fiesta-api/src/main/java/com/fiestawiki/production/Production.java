package com.fiestawiki.production;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "production")
@EntityListeners(AuditingEntityListener.class)
public class Production {

    @Id
    @Column(name = "productid", nullable = false)
    private Long id;

    @Column(name = "produceindex")
    private String recipe;

    @Column(name = "name")
    private String name;

    @Column(name = "product")
    private String product;

    @Column(name = "lot")
    private Long quantity;

    @Column(name = "raw0")
    private String ingredient1;

    @Column(name = "quantity0")
    private Long quantity1;

    @Column(name = "raw1")
    private String ingredient2;

    @Column(name = "quantity1")
    private Long quantity2;

    @Column(name = "raw2")
    private String ingredient3;

    @Column(name = "quantity2")
    private Long quantity3;

    @Column(name = "raw3")
    private String ingredient4;

    @Column(name = "quantity3")
    private Long quantity4;

    @Column(name = "masterytype")
    private Long masteryType;

    @Column(name = "masterygain")
    private Long masteryGain;

    @Column(name = "neededmasterygain")
    private Long neededMastery;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getIngredient1() {
        return ingredient1;
    }

    public void setIngredient1(String ingredient1) {
        this.ingredient1 = ingredient1;
    }

    public Long getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(Long quantity1) {
        this.quantity1 = quantity1;
    }

    public String getIngredient2() {
        return ingredient2;
    }

    public void setIngredient2(String ingredient2) {
        this.ingredient2 = ingredient2;
    }

    public Long getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(Long quantity2) {
        this.quantity2 = quantity2;
    }

    public String getIngredient3() {
        return ingredient3;
    }

    public void setIngredient3(String ingredient3) {
        this.ingredient3 = ingredient3;
    }

    public Long getQuantity3() {
        return quantity3;
    }

    public void setQuantity3(Long quantity3) {
        this.quantity3 = quantity3;
    }

    public String getIngredient4() {
        return ingredient4;
    }

    public void setIngredient4(String ingredient4) {
        this.ingredient4 = ingredient4;
    }

    public Long getQuantity4() {
        return quantity4;
    }

    public void setQuantity4(Long quantity4) {
        this.quantity4 = quantity4;
    }

    public Long getMasteryType() {
        return masteryType;
    }

    public void setMasteryType(Long masteryType) {
        this.masteryType = masteryType;
    }

    public Long getMasteryGain() {
        return masteryGain;
    }

    public void setMasteryGain(Long masteryGain) {
        this.masteryGain = masteryGain;
    }

    public Long getNeededMastery() {
        return neededMastery;
    }

    public void setNeededMastery(Long neededMastery) {
        this.neededMastery = neededMastery;
    }
}
