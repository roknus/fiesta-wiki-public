package com.fiestawiki.map.mapinfo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MapInfoRepository extends JpaRepository<MapInfo, Long> {
}
