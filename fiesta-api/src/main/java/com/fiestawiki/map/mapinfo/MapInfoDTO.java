package com.fiestawiki.map.mapinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MapInfoDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("map_name")
    private String mapName;

    @JsonProperty("name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
