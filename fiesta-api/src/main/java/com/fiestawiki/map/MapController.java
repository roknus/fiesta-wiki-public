package com.fiestawiki.map;

import com.fiestawiki.map.mapinfo.MapInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MapController
{
    @Autowired
    private MapService mapService;

    @GetMapping("map/{id}")
    public ResponseEntity<MapInfoDTO> getMapInfo(@PathVariable(value = "id") Long id)
            throws Exception
    {
        MapInfoDTO mapInfo = mapService.getMapInfo(id);

        return ResponseEntity.ok(mapInfo);
    }

    @GetMapping("maps")
    public ResponseEntity<List<Long>> getMaps()
            throws Exception
    {
        List<Long> maps = mapService.getMaps();

        return ResponseEntity.ok(maps);
    }
}
