package com.fiestawiki.map;

import com.fiestawiki.map.mapinfo.MapInfo;
import com.fiestawiki.map.mapinfo.MapInfoDTO;
import com.fiestawiki.map.mapinfo.MapInfoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MapService
{
    @Autowired
    private MapInfoRepository mapInfoRepository;

    @Autowired
    private ModelMapper modelMapper;

    private MapInfoDTO convertMapInfo(MapInfo mapInfo)
    {
        return modelMapper.map(mapInfo, MapInfoDTO.class);
    }

    public MapInfoDTO getMapInfo(Long id) throws Exception
    {
        MapInfo mobInfo = mapInfoRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Map not found"));

        return convertMapInfo(mobInfo);
    }

    public List<Long> getMaps()
    {
        return mapInfoRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(MapInfo::getMapName))
                .map(map -> map.getId())
                .collect(Collectors.toList());
    }
}
