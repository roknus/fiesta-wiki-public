package com.fiestawiki.items;

import com.fiestawiki.items.dao.randomoption.RandomOption;
import com.fiestawiki.items.dao.randomoption.RandomOptionRepository;
import com.fiestawiki.items.dto.RandomOptionDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.lang.Math.toIntExact;

@Service
public class RandomOptionService {

    @Autowired
    private RandomOptionRepository randomOptionRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

    }

    public static RandomOptionDTO convertRandomOptions(List<RandomOption> random_options)
    {
        RandomOptionDTO randomOption = new RandomOptionDTO();

        if(random_options != null) {
            for (RandomOption o : random_options) {
                switch (toIntExact(o.getRandomOptionType())) {
                    case 0: // STR
                        randomOption.setStr(o.getMax());
                        break;
                    case 1: // DEX
                        randomOption.setDex(o.getMax());
                        break;
                    case 2: // END
                        randomOption.setEnd(o.getMax());
                        break;
                    case 3: // INT
                        randomOption.setIntel(o.getMax());
                        break;
                    case 4: // SPR
                        randomOption.setSpr(o.getMax());
                        break;
                    case 5: // Aim
                        randomOption.setAim(o.getMax() + 1);
                        break;
                    case 7: // Dmg
                        randomOption.setDmg(o.getMax() + 1);
                        break;
                    case 8: // Def
                        randomOption.setDef(o.getMax() + 1);
                        break;
                    case 9: // MDmg
                        randomOption.setMdmg(o.getMax() + 1);
                        break;
                    case 10: // MDef
                        randomOption.setMdef(o.getMax() + 1);
                        break;
                    case 11: // Eva
                        randomOption.setEva(o.getMax() + 1);
                        break;
                    case 12: // Crit
                        randomOption.setCrit(o.getMax());
                        break;
                    case 13: // Level
                        randomOption.setMinusLevel(o.getMax());
                        break;
                    case 14: // HP
                        randomOption.setHp(o.getMax() + 1);
                        break;
                }
            }
        }

        return randomOption;
    }
}
