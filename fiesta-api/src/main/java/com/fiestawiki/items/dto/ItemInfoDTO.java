package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dao.stats.ItemStatsDTO;
import com.fiestawiki.items.dao.upgrade.UpgradeInfoDTO;

public class ItemInfoDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("item_category")
    private int itemCategory;

    @JsonProperty("equip_slot")
    private int equipSlot;

    @JsonProperty("two_hand")
    private int twoHand;

    @JsonProperty("attack_speed")
    private Long attackSpeed;

    @JsonProperty("required_level")
    private Long requiredLevel;

    @JsonProperty("grade")
    private Long grade;

    @JsonProperty("min_phy_atk")
    private Long minPhyAtk;

    @JsonProperty("max_phy_atk")
    private Long maxPhyAtk;

    @JsonProperty("defense")
    private Long defense;

    @JsonProperty("min_mag_atk")
    private Long minMagAtk;

    @JsonProperty("max_mag_atk")
    private Long maxMagAtk;

    @JsonProperty("magical_defense")
    private Long magicalDefense;

    @JsonProperty("aim")
    private Long aim;

    @JsonProperty("evasion")
    private Long evasion;

    @JsonProperty("phy_atk_rate")
    private Long phyAtkRate;

    @JsonProperty("mag_atk_rate")
    private Long magAtkRate;

    @JsonProperty("defense_rate")
    private Long defenseRate;

    @JsonProperty("magical_defense_rate")
    private Long magicalDefenseRate;

    @JsonProperty("critical_rate")
    private Long criticalRate;

    @JsonProperty("buy_price")
    private Long buyPrice;

    @JsonProperty("sell_price")
    private Long sellPrice;

    @JsonProperty("buy_lh_coin")
    private Long buyGBCoin;

    @JsonProperty("weapon_type")
    private Long weaponType;

    @JsonProperty("use_class")
    private Long useClass;

    @JsonProperty("upgrade_max")
    private int upgradeMax;

    @JsonProperty("upgrade_info")
    private UpgradeInfoDTO upgradeInfo;

    @JsonProperty("block_rate")
    private int blockRate;

    @JsonProperty("stats")
    private ItemStatsDTO stats;

    @JsonProperty("random_stats")
    private RandomOptionDTO randomStats;

    @JsonProperty("item_set_id")
    private Long itemSetId;

    @JsonProperty("production_skill_required")
    private Long productionSkillRequired;

    @JsonProperty("description")
    private String description;

    @JsonProperty("stat_values_unsure")
    private Boolean statValuesUnsure;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getItemCategory() { return itemCategory; }

    public void setItemCategory(int itemCategory) { this.itemCategory = itemCategory; }

    public int getEquipSlot() { return equipSlot; }

    public void setEquipSlot(int equipSlot) { this.equipSlot = equipSlot; }

    public int getTwoHand() { return twoHand; }

    public void setTwoHand(int twoHand) { this.twoHand = twoHand; }

    public Long getAttackSpeed() { return attackSpeed; }

    public void setAttackSpeed(Long attackSpeed) { this.attackSpeed = attackSpeed; }

    public Long getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Long requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Long getGrade() {
        return grade;
    }

    public void setGrade(Long grade) {
        this.grade = grade;
    }

    public Long getMinPhyAtk() { return minPhyAtk; }

    public void setMinPhyAtk(Long minPhyAtk) { this.minPhyAtk = minPhyAtk; }

    public Long getMaxPhyAtk() { return maxPhyAtk; }

    public void setMaxPhyAtk(Long maxPhyAtk) { this.maxPhyAtk = maxPhyAtk; }

    public Long getDefense() { return defense; }

    public void setDefense(Long defense) { this.defense = defense; }

    public Long getMinMagAtk() { return minMagAtk; }

    public void setMinMagAtk(Long minMagAtk) { this.minMagAtk = minMagAtk; }

    public Long getMaxMagAtk() { return maxMagAtk; }

    public void setMaxMagAtk(Long maxMagAtk) { this.maxMagAtk = maxMagAtk; }

    public Long getMagicalDefense() { return magicalDefense; }

    public void setMagicalDefense(Long magicalDefense) { this.magicalDefense = magicalDefense; }

    public Long getAim() { return aim; }

    public void setAim(Long aim) { this.aim = aim; }

    public Long getEvasion() { return evasion; }

    public void setEvasion(Long evasion) { this.evasion = evasion; }

    public Long getPhyAtkRate() { return phyAtkRate; }

    public void setPhyAtkRate(Long phyAtkRate) { this.phyAtkRate = phyAtkRate; }

    public Long getMagAtkRate() { return magAtkRate; }

    public void setMagAtkRate(Long magAtkRate) { this.magAtkRate = magAtkRate; }

    public Long getDefenseRate() { return defenseRate; }

    public void setDefenseRate(Long defenseRate) { this.defenseRate = defenseRate; }

    public Long getMagicalDefenseRate() { return magicalDefenseRate; }

    public void setMagicalDefenseRate(Long magicalDefenseRate) { this.magicalDefenseRate = magicalDefenseRate; }

    public Long getCriticalRate() { return criticalRate; }

    public void setCriticalRate(Long criticalRate) { this.criticalRate = criticalRate; }

    public Long getBuyPrice() { return buyPrice; }

    public void setBuyPrice(Long buyPrice) { this.buyPrice = buyPrice; }

    public Long getSellPrice() { return sellPrice; }

    public void setSellPrice(Long sellPrice) { this.sellPrice = sellPrice; }

    public Long getBuyGBCoin() { return buyGBCoin; }

    public void setBuyGBCoin(Long buyGBCoin) { this.buyGBCoin = buyGBCoin; }

    public Long getWeaponType() { return weaponType; }

    public void setWeaponType(Long weaponType) { this.weaponType = weaponType; }

    public Long getUseClass() { return useClass; }

    public void setUseClass(Long useClass) { this.useClass = useClass; }

    public int getUpgradeMax() { return upgradeMax; }

    public void setUpgradeMax(int upgradeMax) { this.upgradeMax = upgradeMax; }

    public UpgradeInfoDTO getUpgradeInfo() { return upgradeInfo; }

    public void setUpgradeInfo(UpgradeInfoDTO upgradeInfo) { this.upgradeInfo = upgradeInfo; }

    public int getBlockRate() { return blockRate; }

    public void setBlockRate(int blockRate) { this.blockRate = blockRate; }

    public ItemStatsDTO getStats() { return stats; }

    public void setStats(ItemStatsDTO stats) { this.stats = stats; }

    public RandomOptionDTO getRandomStats() { return randomStats; }

    public void setRandomStats(RandomOptionDTO randomStats) { this.randomStats = randomStats; }

    public Long getItemSetId() { return itemSetId; }

    public void setItemSetId(Long itemSetId) { this.itemSetId = itemSetId; }

    public Long getProductionSkillRequired() { return productionSkillRequired; }

    public void setProductionSkillRequired(Long productionSkillRequired) { this.productionSkillRequired = productionSkillRequired; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Boolean getStatValuesUnsure() { return statValuesUnsure; }

    public void setStatValuesUnsure(Boolean statValuesUnsure) { this.statValuesUnsure = statValuesUnsure; }
}
