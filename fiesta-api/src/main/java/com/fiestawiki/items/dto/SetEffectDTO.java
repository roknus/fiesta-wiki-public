package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dto.ItemActionDTO;
import com.fiestawiki.items.dao.itemactioneffectdesc.ItemActionEffectDescDTO;

import java.util.ArrayList;
import java.util.List;

public class SetEffectDTO
{
    @JsonProperty("count")
    private int count;

    @JsonProperty("item_set")
    private Long itemSet;

    @JsonProperty("item_actions")
    private List<Long> itemActionsId;

    @JsonProperty("item_action_effect_desc")
    private List<ItemActionEffectDescDTO> itemActionEffectDescs = new ArrayList<>();

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Long getItemSet() { return itemSet; }

    public void setItemSet(Long itemSet) { this.itemSet = itemSet; }

    public List<Long> getItemActionsId() {
        return itemActionsId;
    }

    public void setItemActionsId(List<Long> itemActions) {
        this.itemActionsId = itemActions;
    }

    public List<ItemActionEffectDescDTO> getItemActionEffectDescs() {
        return itemActionEffectDescs;
    }

    public void setItemActionEffectDescs(List<ItemActionEffectDescDTO> itemActionEffectDescs) {
        this.itemActionEffectDescs = itemActionEffectDescs;
    }
}
