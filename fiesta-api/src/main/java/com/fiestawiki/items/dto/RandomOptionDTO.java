package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RandomOptionDTO
{
    @JsonProperty("STR")
    private Long str = 0L;

    @JsonProperty("DEX")
    private Long dex = 0L;

    @JsonProperty("END")
    private Long end = 0L;

    @JsonProperty("INT")
    private Long intel = 0L;

    @JsonProperty("SPR")
    private Long spr = 0L;

    @JsonProperty("aim")
    private Long aim = 0L;

    @JsonProperty("dmg")
    private Long dmg = 0L;

    @JsonProperty("def")
    private Long def = 0L;

    @JsonProperty("mdmg")
    private Long mdmg = 0L;

    @JsonProperty("mdef")
    private Long mdef = 0L;

    @JsonProperty("eva")
    private Long eva = 0L;

    @JsonProperty("crit")
    private Long crit = 0L;

    @JsonProperty("minus_level")
    private Long minusLevel = 0L;

    @JsonProperty("HP")
    private Long hp = 0L;

    public Long getStr() {
        return str;
    }

    public void setStr(Long str) {
        this.str = str;
    }

    public Long getDex() {
        return dex;
    }

    public void setDex(Long dex) {
        this.dex = dex;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getIntel() {
        return intel;
    }

    public void setIntel(Long intel) {
        this.intel = intel;
    }

    public Long getSpr() {
        return spr;
    }

    public void setSpr(Long spr) {
        this.spr = spr;
    }

    public Long getAim() {
        return aim;
    }

    public void setAim(Long aim) {
        this.aim = aim;
    }

    public Long getDmg() {
        return dmg;
    }

    public void setDmg(Long dmg) {
        this.dmg = dmg;
    }

    public Long getDef() {
        return def;
    }

    public void setDef(Long def) {
        this.def = def;
    }

    public Long getMdmg() {
        return mdmg;
    }

    public void setMdmg(Long mdmg) {
        this.mdmg = mdmg;
    }

    public Long getMdef() {
        return mdef;
    }

    public void setMdef(Long mdef) {
        this.mdef = mdef;
    }

    public Long getEva() {
        return eva;
    }

    public void setEva(Long eva) {
        this.eva = eva;
    }

    public Long getCrit() { return crit; }

    public void setCrit(Long crit) { this.crit = crit; }

    public Long getMinusLevel() { return minusLevel; }

    public void setMinusLevel(Long minusLevel) { this.minusLevel = minusLevel; }

    public Long getHp() {
        return hp;
    }

    public void setHp(Long hp) {
        this.hp = hp;
    }
}
