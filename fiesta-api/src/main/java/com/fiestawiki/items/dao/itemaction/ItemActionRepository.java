package com.fiestawiki.items.dao.itemaction;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemActionRepository extends JpaRepository<ItemAction, Long> {
}
