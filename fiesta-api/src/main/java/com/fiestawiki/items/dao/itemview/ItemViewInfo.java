package com.fiestawiki.items.dao.itemview;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "item_view_info")
@EntityListeners(AuditingEntityListener.class)
public class ItemViewInfo
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "iconindex")
    private int iconIndex;

    @Column(name = "iconfile")
    private String iconFile;

    @Column(name = "subiconindex")
    private int subIconIndex;

    @Column(name = "subiconfile")
    private String subIconFile;

    @Column(name = "periodiconindex")
    private int periodIconIndex;

    @Column(name = "periodiconfile")
    private String periodIconFile;

    @Column(name = "r")
    private int red;

    @Column(name = "g")
    private int green;

    @Column(name = "b")
    private int blue;

    @Column(name = "sub_r")
    private int backgroundRed;

    @Column(name = "sub_g")
    private int backgroundGreen;

    @Column(name = "sub_b")
    private int backgroundBlue;

    @Column(name = "descript")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public int getIconIndex() {
        return iconIndex;
    }

    public void setIconIndex(int iconIndex) {
        this.iconIndex = iconIndex;
    }

    public String getIconFile() {
        return iconFile;
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public int getSubIconIndex() { return subIconIndex; }

    public void setSubIconIndex(int subIconIndex) { this.subIconIndex = subIconIndex; }

    public String getSubIconFile() { return subIconFile; }

    public void setSubIconFile(String subIconFile) { this.subIconFile = subIconFile; }

    public int getRed() { return red; }

    public void setRed(int red) { this.red = red; }

    public int getGreen() { return green; }

    public void setGreen(int green) { this.green = green; }

    public int getBlue() { return blue; }

    public void setBlue(int blue) { this.blue = blue; }

    public int getBackgroundRed() { return backgroundRed; }

    public void setBackgroundRed(int backgroundRed) { this.backgroundRed = backgroundRed; }

    public int getBackgroundGreen() { return backgroundGreen; }

    public void setBackgroundGreen(int backgroundGreen) { this.backgroundGreen = backgroundGreen; }

    public int getBackgroundBlue() { return backgroundBlue; }

    public void setBackgroundBlue(int backgroundBlue) { this.backgroundBlue = backgroundBlue; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
