package com.fiestawiki.items.dao.upgrade;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemUpgradeInfoRepository extends JpaRepository<ItemUpgradeInfo, Long> {
}
