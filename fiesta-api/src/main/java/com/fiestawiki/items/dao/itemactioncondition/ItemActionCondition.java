package com.fiestawiki.items.dao.itemactioncondition;

import com.fiestawiki.skills.dao.activeskillgroup.ActiveSkillGroup;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "item_action_condition")
@EntityListeners(AuditingEntityListener.class)
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class ItemActionCondition {

    @Id
    @Column(name = "conditionid")
    private Long conditionId;

    @Type(type = "list-array")
    @Column(
            name = "subjecttarget",
            columnDefinition = "integer[]"
    )
    private List<Integer> subjectTarget;

    @Type(type = "list-array")
    @Column(
            name = "objecttarget",
            columnDefinition = "integer[]"
    )
    private List<Integer> objectTarget;

    @Type(type = "list-array")
    @Column(
            name = "conditionactivity",
            columnDefinition = "integer[]"
    )
    private List<Integer> conditionActivity;

    @Column(name = "activityrate")
    private int activityRate;

    @Column(name = "range")
    private int range;

    @ManyToOne
    @JoinColumn(name = "skill_group_fk")
    private ActiveSkillGroup skillGroup;

    public Long getConditionId() {
        return conditionId;
    }

    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    public List<Integer> getSubjectTarget() {
        return subjectTarget;
    }

    public void setSubjectTarget(List<Integer> subjectTarget) {
        this.subjectTarget = subjectTarget;
    }

    public List<Integer> getObjectTarget() {
        return objectTarget;
    }

    public void setObjectTarget(List<Integer> objectTarget) {
        this.objectTarget = objectTarget;
    }

    public List<Integer> getConditionActivity() {
        return conditionActivity;
    }

    public void setConditionActivity(List<Integer> conditionActivity) {
        this.conditionActivity = conditionActivity;
    }

    public int getActivityRate() {
        return activityRate;
    }

    public void setActivityRate(int activityRate) {
        this.activityRate = activityRate;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public ActiveSkillGroup getSkillGroup() { return skillGroup; }

    public void setSkillGroup(ActiveSkillGroup skillGroup) { this.skillGroup = skillGroup; }
}
