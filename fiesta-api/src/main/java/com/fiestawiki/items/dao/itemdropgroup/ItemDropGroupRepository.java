package com.fiestawiki.items.dao.itemdropgroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemDropGroupRepository extends JpaRepository<ItemDropGroup, Long> {
}
