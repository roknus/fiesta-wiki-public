package com.fiestawiki.items.dao.upgrade;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "upgrade_info")
@EntityListeners(AuditingEntityListener.class)
public class ItemUpgradeInfo {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "upfactor")
    private Long statUpgrade;

    @Column(name = "updata")
    private Long upgrade1;

    @Column(name = "unkcol0")
    private Long upgrade2;

    @Column(name = "unkcol1")
    private Long upgrade3;

    @Column(name = "unkcol2")
    private Long upgrade4;

    @Column(name = "unkcol3")
    private Long upgrade5;

    @Column(name = "unkcol4")
    private Long upgrade6;

    @Column(name = "unkcol5")
    private Long upgrade7;

    @Column(name = "unkcol6")
    private Long upgrade8;

    @Column(name = "unkcol7")
    private Long upgrade9;

    @Column(name = "unkcol8")
    private Long upgrade10;

    @Column(name = "unkcol9")
    private Long upgrade11;

    @Column(name = "unkcol10")
    private Long upgrade12;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getStatUpgrade() {
        return statUpgrade;
    }

    public void setStatUpgrade(Long statUpgrade) {
        this.statUpgrade = statUpgrade;
    }

    public Long getUpgrade1() {
        return upgrade1;
    }

    public void setUpgrade1(Long upgrade1) {
        this.upgrade1 = upgrade1;
    }

    public Long getUpgrade2() {
        return upgrade2;
    }

    public void setUpgrade2(Long upgrade2) {
        this.upgrade2 = upgrade2;
    }

    public Long getUpgrade3() {
        return upgrade3;
    }

    public void setUpgrade3(Long upgrade3) { this.upgrade3 = upgrade3; }

    public Long getUpgrade4() {
        return upgrade4;
    }

    public void setUpgrade4(Long upgrade4) {
        this.upgrade4 = upgrade4;
    }

    public Long getUpgrade5() {
        return upgrade5;
    }

    public void setUpgrade5(Long upgrade5) {
        this.upgrade5 = upgrade5;
    }

    public Long getUpgrade6() {
        return upgrade6;
    }

    public void setUpgrade6(Long upgrade6) {
        this.upgrade6 = upgrade6;
    }

    public Long getUpgrade7() {
        return upgrade7;
    }

    public void setUpgrade7(Long upgrade7) {
        this.upgrade7 = upgrade7;
    }

    public Long getUpgrade8() {
        return upgrade8;
    }

    public void setUpgrade8(Long upgrade8) {
        this.upgrade8 = upgrade8;
    }

    public Long getUpgrade9() {
        return upgrade9;
    }

    public void setUpgrade9(Long upgrade9) {
        this.upgrade9 = upgrade9;
    }

    public Long getUpgrade10() {
        return upgrade10;
    }

    public void setUpgrade10(Long upgrade10) {
        this.upgrade10 = upgrade10;
    }

    public Long getUpgrade11() {
        return upgrade11;
    }

    public void setUpgrade11(Long upgrade11) {
        this.upgrade11 = upgrade11;
    }

    public Long getUpgrade12() {
        return upgrade12;
    }

    public void setUpgrade12(Long upgrade12) {
        this.upgrade12 = upgrade12;
    }
}
