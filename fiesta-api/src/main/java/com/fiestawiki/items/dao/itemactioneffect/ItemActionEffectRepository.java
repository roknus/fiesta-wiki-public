package com.fiestawiki.items.dao.itemactioneffect;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemActionEffectRepository extends JpaRepository<ItemActionEffect, Long> {
}
