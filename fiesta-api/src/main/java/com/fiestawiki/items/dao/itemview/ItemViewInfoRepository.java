package com.fiestawiki.items.dao.itemview;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ItemViewInfoRepository extends JpaRepository<ItemViewInfo, Long>
{
    @Query("SELECT i FROM ItemViewInfo i WHERE i.id = :id")
    Optional<ItemViewInfo> findByItemId(Long id);
}
