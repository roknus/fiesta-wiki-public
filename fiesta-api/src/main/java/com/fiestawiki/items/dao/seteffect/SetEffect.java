package com.fiestawiki.items.dao.seteffect;

import com.fiestawiki.items.dao.itemaction.ItemAction;
import com.fiestawiki.items.dao.itemactioneffectdesc.ItemActionEffectDesc;
import com.fiestawiki.items.dao.itemset.ItemSet;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "set_effect")
@EntityListeners(AuditingEntityListener.class)
public class SetEffect implements Serializable {

    @Id
    @Column(name = "unique_id")
    private Long uniqueId;

    @Column(name = "count")
    private int count;

    @Column(name = "itemactionid")
    private int itemActionId;

    @ManyToMany
    @JoinTable(
            name = "set_effect_item_action",
            joinColumns = @JoinColumn(name = "pk1"),
            inverseJoinColumns = @JoinColumn(name = "pk2"))
    private List<ItemAction> itemActions = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "set_effect_item_action_effect_desc",
            joinColumns = @JoinColumn(name = "pk1"),
            inverseJoinColumns = @JoinColumn(name = "pk2"))
    private List<ItemActionEffectDesc> itemActionEffectDescs = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "item_set_id")
    private ItemSet itemSet;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) { this.uniqueId = uniqueId; }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getItemActionId() {
        return itemActionId;
    }

    public void setItemActionId(int itemActionId) {
        this.itemActionId = itemActionId;
    }

    public List<ItemAction> getItemActions() { return itemActions; }

    public void setItemActions(List<ItemAction> itemActions) { this.itemActions = itemActions; }

    public List<Long> getItemActionsId()
    {
        return itemActions.stream().map(a -> a.getUnique_id()).collect(Collectors.toList());
    }

    public List<ItemActionEffectDesc> getItemActionEffectDescs() {
        return itemActionEffectDescs;
    }

    public void setItemActionEffectDescs(List<ItemActionEffectDesc> itemActionEffectDescs) {
        this.itemActionEffectDescs = itemActionEffectDescs;
    }

    public ItemSet getItemSet() { return itemSet; }

    public void setItemSet(ItemSet itemSet) { this.itemSet = itemSet; }
}
