package com.fiestawiki.items.dao.info.repository;

import com.fiestawiki.FiestaUtils;
import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dao.info.ItemInfo_;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.query.QueryUtils;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

@Repository
@Transactional(readOnly = true)
public class ItemInfoRepositoryImpl implements ItemInfoRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    CriteriaBuilder criteriaBuilder;

    @PostConstruct
    private void init() {
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<ItemInfo> findWeapons(
            Long weaponType,
            Optional<List<Long>> rarities,
            Optional<String> search,
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable,
            Optional<Long> min_level,
            Optional<Long> max_level)
    {
        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 5));
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), weaponType));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), weaponType));
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 5));

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public Page<ItemInfo> findShields(
            Long shieldType,
            Optional<List<Long>> rarities,
            Optional<String> search,
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable,
            Optional<Long> min_level,
            Optional<Long> max_level
    )
    {
        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 7));
            criteria.add(criteriaBuilder.or(
                criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), shieldType),
                criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), 390)) // Both Fighter & Cleric Shield
            );

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 7));
            criteria.add(criteriaBuilder.or(
                    criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), shieldType),
                    criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), 390)) // Both Fighter & Cleric Shield
            );

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public Page<ItemInfo> findGears(
            Long theClass,
            Optional<List<Long>> rarities,
            Optional<String> search,
            Pageable pageable,
            Optional<Long> min_level,
            Optional<Long> max_level
    )
    {
        List<Integer> gearEquipSlots = Arrays.asList(1, 7, 19, 21);
        List<Integer> gearItemCategory = Arrays.asList(6, 8);

        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.useClass).get(FiestaUtils.convertClassCanUseClass(theClass)), 1));
            criteria.add(item.get(ItemInfo_.equipSlot).in(gearEquipSlots));
            criteria.add(item.get(ItemInfo_.itemCategory).in(gearItemCategory));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.useClass).get(FiestaUtils.convertClassCanUseClass(theClass)), 1));
            criteria.add(item.get(ItemInfo_.equipSlot).in(gearEquipSlots));
            criteria.add(item.get(ItemInfo_.itemCategory).in(gearItemCategory));

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public Page<ItemInfo> findJewels(
            Long jewelType,
            Optional<List<Long>> rarities,
            Optional<String> search,
            Pageable pageable,
            Optional<Long> min_level,
            Optional<Long> max_level
    )
    {
        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);

            List itemCategories = new ArrayList<Integer>();
            itemCategories.add(4); // jewels
            itemCategories.add(6); // GT jewels
            criteria.add(item.get(ItemInfo_.itemCategory).in(itemCategories));
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), jewelType));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteriaForItemInfo(item, rarities, new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 4));
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), jewelType));

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public Page<ItemInfo> findBracelets(
            Optional<String> search,
            Pageable pageable,
            Optional<Long> min_level,
            Optional<Long> max_level
    )
    {
        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteriaForItemInfo(item, Optional.empty(), new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 38));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteriaForItemInfo(item, Optional.empty(), new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 38));

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public Page<ItemInfo> findTalismans(
            Optional<String> search,
            Pageable pageable,
            Optional<Long> min_level,
            Optional<Long> max_level
    )
    {
        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteriaForItemInfo(item, Optional.empty(), new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 4));
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), 7));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteriaForItemInfo(item, Optional.empty(), new ArrayList<>(), search, min_level, max_level);
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemCategory), 4));
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), 7));

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public Page<ItemInfo> findPremium(
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable
    )
    {
        Function<Root<ItemInfo>, List<Predicate>> getCriteria = (item) ->
        {
            List<Predicate> criteria = new ArrayList<>();
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), 36)); // premium
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.type), 0)); // equipable
            criteria.add(criteriaBuilder.notEqual(item.get(ItemInfo_.equipSlot), 25)); // not mini pet

            return criteria;
        };

        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteria.apply(item);

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<ItemInfo> item = countQuery.from(ItemInfo.class);
            countQuery.select(criteriaBuilder.count(item));

            List<Predicate> criteria = getCriteria.apply(item);

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    @Override
    public List<ItemInfo> findPremiumByName(String search)
    {
        BiFunction<Root<ItemInfo>, String, List<Predicate>> getCriteria = (item, s) ->
        {
            List<Predicate> criteria = new ArrayList<>();
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.itemAuctionGroup), 36)); // premium
            criteria.add(criteriaBuilder.equal(item.get(ItemInfo_.type), 0)); // equipable
            criteria.add(criteriaBuilder.notEqual(item.get(ItemInfo_.equipSlot), 25)); // not mini pet

            criteria.add(criteriaBuilder.like(criteriaBuilder.lower(item.<String>get(ItemInfo_.name)), "%" + s.toLowerCase() + "%"));

            return criteria;
        };

        CriteriaQuery<ItemInfo> query = criteriaBuilder.createQuery(ItemInfo.class);
        {
            Root<ItemInfo> item = query.from(ItemInfo.class);
            query.select(item);

            List<Predicate> criteria = getCriteria.apply(item, search);

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        TypedQuery<ItemInfo> q = entityManager.createQuery(query);

        return  q.getResultList();
    }

    private Page<ItemInfo> getPageItemInfo(CriteriaQuery<ItemInfo> itemQuery, CriteriaQuery<Long> countQuery, Pageable pageable)
    {
        TypedQuery<ItemInfo> typedQuery = entityManager.createQuery(itemQuery);
        typedQuery.setFirstResult((int)pageable.getOffset());
        typedQuery.setMaxResults(pageable.getPageSize());

        TypedQuery<Long> count = entityManager.createQuery(countQuery);

        return new PageImpl<>(typedQuery.getResultList(), pageable, count.getSingleResult());
    }

    private List<Predicate> getCriteriaForItemInfo(Root<ItemInfo> item, Optional<List<Long>> rarities, List<Long> useClasses, Optional<String> search, Optional<Long> min_level, Optional<Long> max_level) {

        List<Predicate> criteria = new ArrayList<>();

        if(!useClasses.isEmpty())
        {
            criteria.add(item.get(ItemInfo_.useClass).in(useClasses));
        }
        if(rarities.isPresent())
        {
            List<Long> r = rarities.get();
            if(!r.isEmpty()) {
                criteria.add(item.get(ItemInfo_.itemGradeType).in(r));
            }
        }
        if(search.isPresent())
        {
            criteria.add(criteriaBuilder.like(criteriaBuilder.lower(item.<String>get(ItemInfo_.name)), "%" + search.get().toLowerCase() + "%"));
        }
        if(min_level.isPresent())
        {
            criteria.add(criteriaBuilder.greaterThanOrEqualTo(item.get(ItemInfo_.requiredLevel), min_level.get()));
        }
        if(max_level.isPresent())
        {
            criteria.add(criteriaBuilder.lessThanOrEqualTo(item.get(ItemInfo_.requiredLevel), max_level.get()));
        }

        return criteria;
    }
}
