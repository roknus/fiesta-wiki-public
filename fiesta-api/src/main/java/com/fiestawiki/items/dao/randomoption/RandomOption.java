package com.fiestawiki.items.dao.randomoption;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "random_option")
@EntityListeners(AuditingEntityListener.class)
public class RandomOption implements Serializable
{
    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @Column(name = "dropitemindex")
    private String dropItemIndex;

    @Column(name = "randomoptiontype")
    private Long randomOptionType;

    @Column(name = "min")
    private Long min;

    @Column(name = "max")
    private Long max;

    @Column(name = "typedroprate")
    private String typeDropRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDropItemIndex() {
        return dropItemIndex;
    }

    public void setDropItemIndex(String dropItemIndex) {
        this.dropItemIndex = dropItemIndex;
    }

    public Long getRandomOptionType() {
        return randomOptionType;
    }

    public void setRandomOptionType(Long randomOptionType) {
        this.randomOptionType = randomOptionType;
    }

    public Long getMin() {
        return min;
    }

    public void setMin(Long min) {
        this.min = min;
    }

    public Long getMax() {
        return max;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    public String getTypeDropRate() {
        return typeDropRate;
    }

    public void setTypeDropRate(String typeDropRate) {
        this.typeDropRate = typeDropRate;
    }
}
