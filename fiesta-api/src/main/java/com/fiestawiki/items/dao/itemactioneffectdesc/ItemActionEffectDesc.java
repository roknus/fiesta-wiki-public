package com.fiestawiki.items.dao.itemactioneffectdesc;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "item_action_effect_desc")
@EntityListeners(AuditingEntityListener.class)
public class ItemActionEffectDesc {

    @Id
    @Column(name = "unique_id")
    private Long unique_id;

    @Column(name = "itemactionid")
    private int item_action_id;

    @Column(name = "useitem")
    private int use_item;

    @Column(name = "itemactiondesc")
    private String item_action_description;

    public Long getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(Long unique_id) {
        this.unique_id = unique_id;
    }

    public int getItem_action_id() {
        return item_action_id;
    }

    public void setItem_action_id(int item_action_id) {
        this.item_action_id = item_action_id;
    }

    public int getUse_item() {
        return use_item;
    }

    public void setUse_item(int use_item) {
        this.use_item = use_item;
    }

    public String getItem_action_description() {
        return item_action_description;
    }

    public void setItem_action_description(String item_action_description) {
        this.item_action_description = item_action_description;
    }
}
