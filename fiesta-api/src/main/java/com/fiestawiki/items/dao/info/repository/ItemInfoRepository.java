package com.fiestawiki.items.dao.info.repository;

import com.fiestawiki.items.dao.info.ItemInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ItemInfoRepository extends JpaRepository<ItemInfo, Long>, ItemInfoRepositoryCustom {

    @Query("SELECT i FROM ItemInfo i WHERE i.id = :id")
    Optional<ItemInfo> findByItemId(Long id);

    @Query("SELECT i FROM ItemInfo i WHERE i.inxname = :inxname")
    List<ItemInfo> findByItemInxname(String inxname);
}
