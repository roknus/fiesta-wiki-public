package com.fiestawiki.items.dao.iteminfoserver;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemInfoServerRepository extends JpaRepository<ItemInfoServer, Long> {
}
