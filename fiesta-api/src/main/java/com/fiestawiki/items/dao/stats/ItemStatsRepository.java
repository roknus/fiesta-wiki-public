package com.fiestawiki.items.dao.stats;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ItemStatsRepository extends JpaRepository<ItemStats, Long> {

    @Query("SELECT i FROM ItemStats i WHERE i.itemindex = :inxname")
    Optional<ItemStats> findByItemIndex(@Param("inxname") String item_index);
}
