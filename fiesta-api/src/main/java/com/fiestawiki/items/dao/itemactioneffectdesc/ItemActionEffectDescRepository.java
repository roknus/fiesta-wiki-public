package com.fiestawiki.items.dao.itemactioneffectdesc;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemActionEffectDescRepository extends JpaRepository<ItemActionEffectDesc, Long> {
}
