package com.fiestawiki.items.dao.itemdropgroup;

import com.fiestawiki.items.dao.iteminfoserver.ItemInfoServer;
import com.fiestawiki.mobs.mobdroptable.MobDropGroupInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "item_drop_group")
@EntityListeners(AuditingEntityListener.class)
public class ItemDropGroup
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "itemid")
    private String itemGroupId;

    @ManyToMany(mappedBy = "dropGroups")
    private List<ItemInfoServer> items = new ArrayList<>();

    @OneToMany(mappedBy = "itemDropGroup")
    private List<MobDropGroupInfo> dropGroupInfo = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemGroupId() {
        return itemGroupId;
    }

    public void setItemGroupId(String itemGroupId) {
        this.itemGroupId = itemGroupId;
    }

    public List<ItemInfoServer> getItems() {
        return items;
    }

    public void setItems(List<ItemInfoServer> items) {
        this.items = items;
    }

    public List<MobDropGroupInfo> getDropGroupInfo() {
        return dropGroupInfo;
    }

    public void setDropGroupInfo(List<MobDropGroupInfo> dropGroupInfo) {
        this.dropGroupInfo = dropGroupInfo;
    }
}
