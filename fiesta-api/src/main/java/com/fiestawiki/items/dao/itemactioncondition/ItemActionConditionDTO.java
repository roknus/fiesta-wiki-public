package com.fiestawiki.items.dao.itemactioncondition;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.skills.dto.SkillGroupDTO;

import java.util.List;

public class ItemActionConditionDTO {

    @JsonProperty("condition_id")
    private Long conditionId;

    @JsonProperty("subject_target")
    private List<Integer> subjectTarget;

    @JsonProperty("object_target")
    private List<Integer> objectTarget;

    @JsonProperty("activity_rate")
    private int activityRate;

    @JsonProperty("range")
    private int range;

    @JsonProperty("skill_group")
    private SkillGroupDTO skillGroup;

    public Long getConditionId() {
        return conditionId;
    }

    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    public List<Integer> getSubjectTarget() {
        return subjectTarget;
    }

    public void setSubjectTarget(List<Integer> subjectTarget) {
        this.subjectTarget = subjectTarget;
    }

    public List<Integer> getObjectTarget() {
        return objectTarget;
    }

    public void setObjectTarget(List<Integer> objectTarget) {
        this.objectTarget = objectTarget;
    }

    public int getActivityRate() {
        return activityRate;
    }

    public void setActivityRate(int activityRate) {
        this.activityRate = activityRate;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public SkillGroupDTO getSkillGroup() { return skillGroup; }

    public void setSkillGroup(SkillGroupDTO skillGroup) { this.skillGroup = skillGroup; }
}
