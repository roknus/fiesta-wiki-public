package com.fiestawiki.items.dao.stats;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemStatsDTO {

    @JsonProperty("strength")
    private int strength;

    @JsonProperty("endurance")
    private int endurance;

    @JsonProperty("dexterity")
    private int dexterity;

    @JsonProperty("intelligence")
    private int intelligence;

    @JsonProperty("mentality")
    private int mentality;

    @JsonProperty("poison_resist")
    private int poisonResist;

    @JsonProperty("disease_resist")
    private int diseaseResist;

    @JsonProperty("curse_resist")
    private int curseResist;

    @JsonProperty("move_speed_reduction_resist")
    private int moveSpeedReductionResist;

    @JsonProperty("aim_rate")
    private int aimRate;

    @JsonProperty("eva_rate")
    private int evaRate;

    @JsonProperty("max_hp")
    private int maxHp;

    @JsonProperty("max_sp")
    private int maxSp;

    @JsonProperty("dmg_plus")
    private int wcPlus;

    @JsonProperty("mdmg_plus")
    private int maPlus;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getMentality() {
        return mentality;
    }

    public void setMentality(int mentality) {
        this.mentality = mentality;
    }

    public int getPoisonResist() {
        return poisonResist;
    }

    public void setPoisonResist(int poisonResist) {
        this.poisonResist = poisonResist;
    }

    public int getDiseaseResist() {
        return diseaseResist;
    }

    public void setDiseaseResist(int diseaseResist) {
        this.diseaseResist = diseaseResist;
    }

    public int getCurseResist() {
        return curseResist;
    }

    public void setCurseResist(int curseResist) {
        this.curseResist = curseResist;
    }

    public int getMoveSpeedReductionResist() {
        return moveSpeedReductionResist;
    }

    public void setMoveSpeedReductionResist(int moveSpeedReductionResist) {
        this.moveSpeedReductionResist = moveSpeedReductionResist;
    }

    public int getAimRate() { return aimRate; }

    public void setAimRate(int aimRate) { this.aimRate = aimRate; }

    public int getEvaRate() { return evaRate; }

    public void setEvaRate(int evaRate) { this.evaRate = evaRate; }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public int getMaxSp() {
        return maxSp;
    }

    public void setMaxSp(int maxSp) {
        this.maxSp = maxSp;
    }

    public int getWcPlus() { return wcPlus; }

    public void setWcPlus(int wcPlus) { this.wcPlus = wcPlus; }

    public int getMaPlus() { return maPlus; }

    public void setMaPlus(int maPlus) { this.maPlus = maPlus; }
}
