package com.fiestawiki.items.dao.itemactioneffectdesc;

public class ItemActionEffectDescDTO {

    private int item_action_id;

    private int use_item;

    private String item_action_description;

    public int getItem_action_id() {
        return item_action_id;
    }

    public void setItem_action_id(int item_action_id) {
        this.item_action_id = item_action_id;
    }

    public int getUse_item() {
        return use_item;
    }

    public void setUse_item(int use_item) {
        this.use_item = use_item;
    }

    public String getItem_action_description() {
        return item_action_description;
    }

    public void setItem_action_description(String item_action_description) {
        this.item_action_description = item_action_description;
    }
}
