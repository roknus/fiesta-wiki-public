package com.fiestawiki.items.dao.iteminfoserver;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dto.RandomOptionDTO;

import java.util.ArrayList;
import java.util.List;

public class ItemInfoServerDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("market_index")
    private String marketIndex;

    @JsonProperty("city")
    private short city;

    @JsonProperty("random_stats")
    private List<RandomOptionDTO> randomStats = new ArrayList<>();

    @JsonProperty("vanish")
    private Long vanish;

    @JsonProperty("looting")
    private Long looting;

    @JsonProperty("drop_rate_killed_by_player")
    private int dropRateKilledByPlayer;

    @JsonProperty("iset_index")
    private Long isetIndex;

    @JsonProperty("item_sort_index")
    private String itemSortIndex;

    @JsonProperty("kq_item")
    private short KQItem;

    @JsonProperty("pk_kq_use")
    private short pkKqUse;

    @JsonProperty("kq_item_drop")
    private short kqItemDrop;

    @JsonProperty("prevent_attack")
    private short preventAttack;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getMarketIndex() {
        return marketIndex;
    }

    public void setMarketIndex(String marketIndex) {
        this.marketIndex = marketIndex;
    }

    public short getCity() {
        return city;
    }

    public void setCity(short city) {
        this.city = city;
    }

    public List<RandomOptionDTO> getRandomStats() {
        return randomStats;
    }

    public void setRandomStats(List<RandomOptionDTO> randomStats) {
        this.randomStats = randomStats;
    }

    public Long getVanish() {
        return vanish;
    }

    public void setVanish(Long vanish) {
        this.vanish = vanish;
    }

    public Long getLooting() {
        return looting;
    }

    public void setLooting(Long looting) {
        this.looting = looting;
    }

    public int getDropRateKilledByPlayer() {
        return dropRateKilledByPlayer;
    }

    public void setDropRateKilledByPlayer(int dropRateKilledByPlayer) {
        this.dropRateKilledByPlayer = dropRateKilledByPlayer;
    }

    public Long getIsetIndex() {
        return isetIndex;
    }

    public void setIsetIndex(Long isetIndex) {
        this.isetIndex = isetIndex;
    }

    public String getItemSortIndex() {
        return itemSortIndex;
    }

    public void setItemSortIndex(String itemSortIndex) {
        this.itemSortIndex = itemSortIndex;
    }

    public short getKQItem() {
        return KQItem;
    }

    public void setKQItem(short KQItem) {
        this.KQItem = KQItem;
    }

    public short getPkKqUse() {
        return pkKqUse;
    }

    public void setPkKqUse(short pkKqUse) {
        this.pkKqUse = pkKqUse;
    }

    public short getKqItemDrop() {
        return kqItemDrop;
    }

    public void setKqItemDrop(short kqItemDrop) {
        this.kqItemDrop = kqItemDrop;
    }

    public short getPreventAttack() {
        return preventAttack;
    }

    public void setPreventAttack(short preventAttack) {
        this.preventAttack = preventAttack;
    }
}
