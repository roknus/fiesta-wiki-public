package com.fiestawiki.items.dao.upgrade;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UpgradeInfoDTO
{
    @JsonProperty("stat_upgrade")
    private Long statUpgrade;

    @JsonProperty("upgrades")
    private List<Long> upgrades;

    public Long getStatUpgrade() { return statUpgrade; }

    public void setStatUpgrade(Long statUpgrade) { this.statUpgrade = statUpgrade; }

    public List<Long> getUpgrades() { return upgrades; }

    public void setUpgrades(List<Long> upgrades) { this.upgrades = upgrades; }
}
