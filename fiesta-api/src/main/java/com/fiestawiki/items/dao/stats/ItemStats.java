package com.fiestawiki.items.dao.stats;

import com.fiestawiki.items.dao.info.ItemInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "grade_item_option")
@EntityListeners(AuditingEntityListener.class)
public class ItemStats implements Serializable {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long unique_id;

    @Column(name = "itemindex")
    private String itemindex;

    @OneToOne
    @JoinColumn(name = "item_fk")
    private ItemInfo item;

    @OneToMany(mappedBy = "stats")
    private List<ItemInfo> infos = new ArrayList<>();

    @Column(name = "str")
    private int strength;

    @Column(name = "con")
    private int endurance;

    @Column(name = "dex")
    private int dexterity;

    @Column(name = "int")
    private int intelligence;

    @Column(name = "men")
    private int mentality;

    @Column(name = "resistpoison")
    private int poisonResist;

    @Column(name = "resistdeaseas")
    private int diseaseResist;

    @Column(name = "resistcurse")
    private int curseResist;

    @Column(name = "resistmovespddown")
    private int moveSpeedReductionResist;

    @Column(name = "tohitrate")
    private int aimRate;

    @Column(name = "toblockrate")
    private int evaRate;

    @Column(name = "maxhp")
    private int maxHp;

    @Column(name = "maxsp")
    private int maxSp;

    @Column(name = "wcplus")
    private int wcPlus;

    @Column(name = "maplus")
    private int maPlus;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getMentality() {
        return mentality;
    }

    public void setMentality(int mentality) {
        this.mentality = mentality;
    }

    public int getPoisonResist() {
        return poisonResist;
    }

    public void setPoisonResist(int poisonResist) {
        this.poisonResist = poisonResist;
    }

    public int getDiseaseResist() {
        return diseaseResist;
    }

    public void setDiseaseResist(int diseaseResist) {
        this.diseaseResist = diseaseResist;
    }

    public int getCurseResist() {
        return curseResist;
    }

    public void setCurseResist(int curseResist) {
        this.curseResist = curseResist;
    }

    public int getMoveSpeedReductionResist() {
        return moveSpeedReductionResist;
    }

    public void setMoveSpeedReductionResist(int moveSpeedReductionResist) {
        this.moveSpeedReductionResist = moveSpeedReductionResist;
    }

    public int getAimRate() { return aimRate - 1000; }

    public void setAimRate(int aimRate) { this.aimRate = aimRate; }

    public int getEvaRate() { return evaRate - 1000; }

    public void setEvaRate(int evaRate) { this.evaRate = evaRate; }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public int getMaxSp() {
        return maxSp;
    }

    public void setMaxSp(int maxSp) {
        this.maxSp = maxSp;
    }

    public int getWcPlus() {
        return wcPlus;
    }

    public void setWcPlus(int wcPlus) {
        this.wcPlus = wcPlus;
    }

    public int getMaPlus() {
        return maPlus;
    }

    public void setMaPlus(int maPlus) {
        this.maPlus = maPlus;
    }

    public List<ItemInfo> getInfos() {
        return infos;
    }

    public void setInfos(List<ItemInfo> infos) {
        this.infos = infos;
    }
}
