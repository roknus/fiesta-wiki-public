package com.fiestawiki.quest.dao.questitemdropinfo;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import com.fiestawiki.quest.dao.endItem.QuestEndItem;
import com.fiestawiki.quest.dao.quest.QuestData;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "quest_action")
@EntityListeners(AuditingEntityListener.class)
public class QuestItemDropInfo
{
    @Id
    @Column(name = "unique_id", nullable = false)
    private Long uniqueId;

    @Column(
            name = "id",
            nullable = false,
            insertable = false,
            updatable = false
    )
    private Long questId;

    @ManyToOne
    @JoinColumn(name = "id")
    private QuestData quest;

    @Column(name = "condition")
    private Long condition;

    @Column(
            name = "conditiontarget",
            insertable = false,
            updatable = false
    )
    private Long mobId;

    @ManyToOne
    @JoinColumn(name = "conditiontarget")
    private MobInfo mob;

    @Column(name = "group_id")
    private Long groupId;

    /* always 1 */
    @Column(name = "result")
    private Long result;

    @Column(name = "resulttarget")
    private Long dropId;

    @Column(name = "percent")
    private Long percent;

    @Column(name = "countmin")
    private Long countMin;

    @Column(name = "countmax")
    private Long countMax;

    @ManyToOne
    @JoinColumn(name = "quest_item_fk")
    private QuestEndItem questEndItem;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getQuestId() {
        return questId;
    }

    public void setQuestId(Long questId) {
        this.questId = questId;
    }

    public QuestData getQuest() { return quest; }

    public void setQuest(QuestData quest) { this.quest = quest; }

    public Long getCondition() {
        return condition;
    }

    public void setCondition(Long condition) {
        this.condition = condition;
    }

    public Long getMobId() { return mobId; }

    public void setMobId(Long mobId) { this.mobId = mobId; }

    public MobInfo getMob() { return mob; }

    public void setMob(MobInfo mob) { this.mob = mob; }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }

    public Long getDropId() {
        return dropId;
    }

    public void setDropId(Long dropId) {
        this.dropId = dropId;
    }

    public Long getPercent() {
        return percent;
    }

    public void setPercent(Long percent) {
        this.percent = percent;
    }

    public Long getCountMin() {
        return countMin;
    }

    public void setCountMin(Long countMin) {
        this.countMin = countMin;
    }

    public Long getCountMax() {
        return countMax;
    }

    public void setCountMax(Long countMax) {
        this.countMax = countMax;
    }

    public QuestEndItem getQuestEndItem() { return questEndItem; }

    public void setQuestEndItem(QuestEndItem questEndItem) { this.questEndItem = questEndItem; }
}
