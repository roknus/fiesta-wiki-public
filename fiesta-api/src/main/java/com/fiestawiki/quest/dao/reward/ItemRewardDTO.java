package com.fiestawiki.quest.dao.reward;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemRewardDTO
{
    @JsonProperty("selectable")
    private int selectable;

    @JsonProperty("quantity")
    private Long quantity;

    @JsonProperty("item_id")
    private Long itemId;

    public int getSelectable() { return selectable; }

    public void setSelectable(int selectable) { this.selectable = selectable; }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getItemId() { return itemId; }

    public void setItemId(Long itemId) { this.itemId = itemId; }
}
