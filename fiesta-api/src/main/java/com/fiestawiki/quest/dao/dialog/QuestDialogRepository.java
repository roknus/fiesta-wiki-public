package com.fiestawiki.quest.dao.dialog;

import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestDialogRepository extends JpaRepository<QuestDialog, Long> {
}
