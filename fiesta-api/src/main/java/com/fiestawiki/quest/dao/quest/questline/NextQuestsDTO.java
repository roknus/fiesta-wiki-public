package com.fiestawiki.quest.dao.quest.questline;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class NextQuestsDTO extends BaseQuestLineDTO
{
    @JsonProperty("next_quests")
    private List<NextQuestsDTO> nextQuests = new ArrayList<>();

    public NextQuestsDTO(Long id, String name)
    {
        super(id, name);
    }

    public List<NextQuestsDTO> getNextQuests() {
        return nextQuests;
    }

    public void setNextQuests(List<NextQuestsDTO> nextQuests) {
        this.nextQuests = nextQuests;
    }
}
