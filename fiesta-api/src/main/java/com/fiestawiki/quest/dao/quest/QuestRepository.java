package com.fiestawiki.quest.dao.quest;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface QuestRepository extends PagingAndSortingRepository<QuestData, Long>, QuestRepositoryCustom {

    @Query("SELECT q FROM QuestData q WHERE " +
            "q.minLevel BETWEEN :min AND :max " +
            "ORDER BY q.minLevel ASC, q.id ASC")
    List<QuestData> findByLevelBetween(int min, int max);
}
