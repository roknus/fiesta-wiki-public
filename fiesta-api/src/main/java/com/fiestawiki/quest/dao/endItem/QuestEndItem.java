package com.fiestawiki.quest.dao.endItem;

import com.fiestawiki.quest.dao.questitemdropinfo.QuestItemDropInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "quest_end_item")
@EntityListeners(AuditingEntityListener.class)
public class QuestEndItem {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long unique_id;

    @Column (name = "id", nullable = false)
    private Long questId;

    @Column (name = "type", nullable = false)
    private Long type;

    @Column(name = "itemid")
    private Long itemId;

    @Column (name = "count")
    private int count;

    @OneToMany(mappedBy = "questEndItem")
    private List<QuestItemDropInfo> questItemDropInfos = new ArrayList<>();

    public Long getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(Long unique_id) {
        this.unique_id = unique_id;
    }

    public Long getQuestId() { return questId; }

    public void setQuestId(Long questId) { this.questId = questId; }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getItemId() { return itemId; }

    public void setItemId(Long itemId) { this.itemId = itemId; }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<QuestItemDropInfo> getQuestItemDropInfos() { return questItemDropInfos; }

    public void setQuestItemDropInfos(List<QuestItemDropInfo> questItemDropInfos) { this.questItemDropInfos = questItemDropInfos; }
}