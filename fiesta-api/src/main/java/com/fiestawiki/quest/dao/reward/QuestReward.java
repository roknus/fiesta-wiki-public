package com.fiestawiki.quest.dao.reward;

import com.fiestawiki.quest.dao.quest.QuestData;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "quest_reward")
@EntityListeners(AuditingEntityListener.class)
public class QuestReward
{
    @Id
    @Column(name = "unique_id", nullable = false)
    private Long unique_id;

    @ManyToOne
    @JoinColumn(name = "id")
    private QuestData quest;

    @Column(name = "selectable")
    private int selectable;

    @Column(name = "rewardtype")
    private int rewardType;

    @Column(name = "flag")
    private Long flag;

    @Column(name = "unkcol0")
    private Long unkcol0;

    @Column(name = "unkcol1")
    private int unkcol1;

    public Long getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(Long unique_id) {
        this.unique_id = unique_id;
    }

    public QuestData getQuest() { return quest; }

    public void setQuest(QuestData quest) { this.quest = quest; }

    public int getSelectable() {
        return selectable;
    }

    public void setSelectable(int selectable) {
        this.selectable = selectable;
    }

    public int getRewardType() {
        return rewardType;
    }

    public void setRewardType(int rewardType) {
        this.rewardType = rewardType;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public Long getUnkcol0() {
        return unkcol0;
    }

    public void setUnkcol0(Long unkcol0) {
        this.unkcol0 = unkcol0;
    }

    public int getUnkcol1() {
        return unkcol1;
    }

    public void setUnkcol1(int unkcol1) {
        this.unkcol1 = unkcol1;
    }
}
