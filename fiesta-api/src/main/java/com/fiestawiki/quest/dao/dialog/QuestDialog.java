package com.fiestawiki.quest.dao.dialog;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "quest_dialog")
@EntityListeners(AuditingEntityListener.class)
public class QuestDialog implements Serializable {

    @Id
    @Column (name = "id", nullable = false)
    private Long id;

    @Column (name = "dialog")
    private String dialog;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDialog() {
        return dialog;
    }

    public void setDialog(String dialog) {
        this.dialog = dialog;
    }
}
