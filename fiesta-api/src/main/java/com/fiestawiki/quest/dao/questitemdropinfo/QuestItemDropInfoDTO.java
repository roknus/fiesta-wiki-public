package com.fiestawiki.quest.dao.questitemdropinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestItemDropInfoDTO
{
    @JsonProperty("mob_id")
    private Long mobId;

    @JsonProperty("percent")
    private Long percent;

    @JsonProperty("count_min")
    private Long countMin;

    @JsonProperty("count_max")
    private Long countMax;

    public Long getMobId() { return mobId; }

    public void setMobId(Long mobId) { this.mobId = mobId; }

    public Long getPercent() {
        return percent;
    }

    public void setPercent(Long percent) {
        this.percent = percent;
    }

    public Long getCountMin() {
        return countMin;
    }

    public void setCountMin(Long countMin) {
        this.countMin = countMin;
    }

    public Long getCountMax() {
        return countMax;
    }

    public void setCountMax(Long countMax) {
        this.countMax = countMax;
    }
}
