package com.fiestawiki.quest.dao.questlevelinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class QuestLevelInfoDTO
{
    @JsonProperty("level")
    private Long level;

    @JsonProperty("xp_required")
    private Long xpRequired;

    @JsonProperty("total_xp")
    private Long totalXp;

    @JsonProperty("quests")
    private List<Long> quests = new ArrayList<>();

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getXpRequired() {
        return xpRequired;
    }

    public void setXpRequired(Long xpRequired) {
        this.xpRequired = xpRequired;
    }

    public Long getTotalXp() { return totalXp; }

    public void setTotalXp(Long totalXp) { this.totalXp = totalXp; }

    public List<Long> getQuests() {
        return quests;
    }

    public void setQuests(List<Long> quests) {
        this.quests = quests;
    }
}
