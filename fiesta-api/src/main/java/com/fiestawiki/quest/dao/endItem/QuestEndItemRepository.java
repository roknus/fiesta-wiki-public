package com.fiestawiki.quest.dao.endItem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestEndItemRepository extends JpaRepository<QuestEndItem, Long> {
}
