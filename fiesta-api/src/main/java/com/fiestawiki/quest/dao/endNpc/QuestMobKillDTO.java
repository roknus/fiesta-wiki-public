package com.fiestawiki.quest.dao.endNpc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestMobKillDTO
{
    @JsonProperty("mob_id")
    private Long mobId;

    @JsonProperty("kill_count")
    private int killCount;

    public Long getMobId() {
        return mobId;
    }

    public void setMobId(Long mobId) {
        this.mobId = mobId;
    }

    public int getKillCount() {
        return killCount;
    }

    public void setKillCount(int killCount) {
        this.killCount = killCount;
    }
}
