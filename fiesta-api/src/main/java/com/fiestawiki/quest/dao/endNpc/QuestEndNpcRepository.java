package com.fiestawiki.quest.dao.endNpc;


import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestEndNpcRepository extends JpaRepository<QuestEndNpc, Long> {
}
