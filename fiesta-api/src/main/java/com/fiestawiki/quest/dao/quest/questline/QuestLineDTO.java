package com.fiestawiki.quest.dao.quest.questline;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class QuestLineDTO extends NextQuestsDTO
{
    @JsonProperty("previous_quests")
    private List<BaseQuestLineDTO> previousQuests = new ArrayList<>();

    public QuestLineDTO(Long id, String name) {
        super(id, name);
    }

    public List<BaseQuestLineDTO> getPreviousQuests() {
        return previousQuests;
    }

    public void setPreviousQuests(List<BaseQuestLineDTO> previousQuests) {
        this.previousQuests = previousQuests;
    }
}