package com.fiestawiki.quest.dao.questitemdropinfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestItemDropInfoRepository extends JpaRepository<QuestItemDropInfo, Long>
{
    @Query("SELECT q FROM QuestItemDropInfo q WHERE " +
            "q.questId = :questId AND " +
            "q.dropId = :dropId")
    List<QuestItemDropInfo> findByQuestAndDrop(Long questId, Long dropId);
}
