package com.fiestawiki.quest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.quest.dao.reward.ItemRewardDTO;

import java.util.ArrayList;
import java.util.List;

public class QuestDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("quest_type")
    private int questType;

    @JsonProperty("repeat")
    private boolean repeat;

    @JsonProperty("remote_turn_in")
    private boolean remoteTurnIn;

    @JsonProperty("remote_pick_up")
    private boolean remotePickUp;

    @JsonProperty("start_class")
    private int startClass;

    @JsonProperty("min_level")
    private int minLevel;

    @JsonProperty("max_level")
    private int maxLevel;

    @JsonProperty("exp")
    private Long exp;

    @JsonProperty("money")
    private Long money;

    @JsonProperty("item_rewards")
    private List<ItemRewardDTO> itemRewards = new ArrayList<>();

    @JsonProperty("selectable_item_rewards")
    private List<ItemRewardDTO> selectableItemRewards = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuestType() {
        return questType;
    }

    public void setQuestType(int questType) {
        this.questType = questType;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isRemoteTurnIn() { return remoteTurnIn; }

    public void setRemoteTurnIn(boolean remoteTurnIn) { this.remoteTurnIn = remoteTurnIn; }

    public boolean isRemotePickUp() { return remotePickUp; }

    public void setRemotePickUp(boolean remotePickUp) { this.remotePickUp = remotePickUp; }

    public int getStartClass() {
        return startClass;
    }

    public void setStartClass(int startClass) {
        this.startClass = startClass;
    }

    public int getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(int minLevel) {
        this.minLevel = minLevel;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public Long getExp() {
        return exp;
    }

    public void setExp(Long exp) {
        this.exp = exp;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public List<ItemRewardDTO> getItemRewards() {
        return itemRewards;
    }

    public void setItemRewards(List<ItemRewardDTO> itemRewards) {
        this.itemRewards = itemRewards;
    }

    public List<ItemRewardDTO> getSelectableItemRewards() {
        return selectableItemRewards;
    }

    public void setSelectableItemRewards(List<ItemRewardDTO> selectableItemRewards) {
        this.selectableItemRewards = selectableItemRewards;
    }
}
