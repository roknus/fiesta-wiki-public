package com.fiestawiki.title.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.cards.dto.CardDTOMinimal;
import com.fiestawiki.title.stats.TitleStateViewDTO;
import com.fiestawiki.title.view.TitleViewDTO;

import java.util.ArrayList;
import java.util.List;

public class TitleDataDTO {

    @JsonProperty("permit")
    private Long permit;

    @JsonProperty("refresh")
    private Long refresh;

    @JsonProperty("title")
    private String title;

    @JsonProperty("value")
    private Long value;

    @JsonProperty("fame")
    private Long fame;

    @JsonProperty("stats")
    List<TitleStateViewDTO> stats = new ArrayList<>();

    @JsonProperty("view")
    TitleViewDTO view;

    @JsonProperty("card")
    CardDTOMinimal card;

    public Long getPermit() {
        return permit;
    }

    public void setPermit(Long permit) {
        this.permit = permit;
    }

    public Long getRefresh() {
        return refresh;
    }

    public void setRefresh(Long refresh) {
        this.refresh = refresh;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getFame() {
        return fame;
    }

    public void setFame(Long fame) {
        this.fame = fame;
    }

    public List<TitleStateViewDTO> getStats() {
        return stats;
    }

    public void setStats(List<TitleStateViewDTO> stats) {
        this.stats = stats;
    }

    public TitleViewDTO getView() { return view; }

    public void setView(TitleViewDTO view) { this.view = view; }

    public CardDTOMinimal getCard() { return card; }

    public void setCard(CardDTOMinimal card) { this.card = card; }
}
