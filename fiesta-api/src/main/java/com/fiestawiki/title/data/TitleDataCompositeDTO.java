package com.fiestawiki.title.data;

import com.fiestawiki.title.stats.TitleStateViewDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TitleDataCompositeDTO {

    List<TitleDataDTO> titles = new ArrayList<>();

    List<TitleStateViewDTO> stats = new ArrayList<>();

    public void addTitle(TitleDataDTO titleData) {
        titles.add(titleData);
    }

    public List<TitleDataDTO> getTitles() {
        return titles;
    }

    public TitleDataDTO getTitle(int index) { return titles.get(index); }

    public void setTitles(List<TitleDataDTO> titles) {
        this.titles = titles;
    }

    public List<TitleStateViewDTO> getStats() {
        return stats;
    }

    public List<TitleStateViewDTO> getStat(int level) {
        return stats.stream()
                .filter(s -> s.getTitlelv() == level)
                .collect(Collectors.toList());
    }

    public void setStats(List<TitleStateViewDTO> stats) {
        this.stats = stats;
    }
}
