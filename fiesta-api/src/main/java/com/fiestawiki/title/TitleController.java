package com.fiestawiki.title;

import com.fiestawiki.title.data.TitleDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TitleController {

    @Autowired
    private TitleService titleService;

    @GetMapping("titles")
    public ResponseEntity<List<TitleDataDTO>> getTitles()
            throws Exception
    {
        List<TitleDataDTO> titles = titleService.getTitles();

        return ResponseEntity.ok(titles);
    }
}
