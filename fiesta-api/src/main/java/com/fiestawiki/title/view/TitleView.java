package com.fiestawiki.title.view;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "title_view")
@EntityListeners(AuditingEntityListener.class)
public class TitleView {

    @Id
    @Column(name = "type", nullable = false)
    private Long type;

    @Column(name = "unlock_type")
    private int unlockType;

    @Column(name = "obtainable")
    private int obtainable;

    @Column(name = "description")
    private String description;

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public int getUnlockType() {
        return unlockType;
    }

    public void setUnlockType(int unlockType) {
        this.unlockType = unlockType;
    }

    public int getObtainable() {
        return obtainable;
    }

    public void setObtainable(int obtainable) {
        this.obtainable = obtainable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
