package com.fiestawiki.title.view;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TitleViewDTO {

    @JsonProperty("unlock_type")
    private int unlockType;

    @JsonProperty("obtainable")
    private int obtainable;

    @JsonProperty("description")
    private String description;

    public int getUnlockType() {
        return unlockType;
    }

    public void setUnlockType(int unlockType) {
        this.unlockType = unlockType;
    }

    public int getObtainable() {
        return obtainable;
    }

    public void setObtainable(int obtainable) {
        this.obtainable = obtainable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
