package com.fiestawiki.title.stats;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "title_state_view")
@EntityListeners(AuditingEntityListener.class)
public class TitleStateView {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long uniqueId;

    @Column(name = "type")
    private Long type;

    @Column(name = "titlelv")
    private int titlelv;

    @Column(name = "descript")
    private String descript;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public int getTitlelv() {
        return titlelv;
    }

    public void setTitlelv(int titlelv) {
        this.titlelv = titlelv;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }
}
