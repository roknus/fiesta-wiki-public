package com.fiestawiki.title.stats;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TitleStateViewDTO
{
    @JsonProperty("titlelv")
    private int titlelv;

    @JsonProperty("descript")
    private String descript;

    public int getTitlelv() {
        return titlelv;
    }

    public void setTitlelv(int titlelv) {
        this.titlelv = titlelv;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }
}
