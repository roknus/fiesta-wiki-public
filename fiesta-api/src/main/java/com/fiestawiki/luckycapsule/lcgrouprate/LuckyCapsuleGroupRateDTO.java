package com.fiestawiki.luckycapsule.lcgrouprate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.luckycapsule.lcreward.LuckyCapsuleRewardDTO;

import java.util.ArrayList;
import java.util.List;

public class LuckyCapsuleGroupRateDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("rewards")
    private List<LuckyCapsuleRewardDTO> rewards = new ArrayList<>();

    @JsonProperty("rate")
    private Long rate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<LuckyCapsuleRewardDTO> getRewards() {
        return rewards;
    }

    public void setRewards(List<LuckyCapsuleRewardDTO> rewards) {
        this.rewards = rewards;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }
}
