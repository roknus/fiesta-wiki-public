package com.fiestawiki.luckycapsule.lcgrouprate;

import com.fiestawiki.luckycapsule.lcreward.LuckyCapsuleReward;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
        name = "lucky_capsule_group_rate",
        schema = "lucky_capsule"
)
public class LuckyCapsuleGroupRate implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unique_id")
    private Long id;

    @Column(name = "item_id")
    private Long itemId;

    @ManyToMany
    @JoinTable(
            schema = "lucky_capsule",
            name = "lucky_capsule_group_reward_join",
            joinColumns = @JoinColumn(name = "pk2"),
            inverseJoinColumns = @JoinColumn(name = "pk1"))
    @OrderBy("id ASC")
    private List<LuckyCapsuleReward> rewards = new ArrayList<>();

    @Column(name = "lcr_rate")
    private Long rate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public List<LuckyCapsuleReward> getRewards() {
        return rewards;
    }

    public void setRewards(List<LuckyCapsuleReward> rewards) {
        this.rewards = rewards;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }
}
