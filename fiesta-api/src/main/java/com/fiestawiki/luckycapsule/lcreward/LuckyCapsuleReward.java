package com.fiestawiki.luckycapsule.lcreward;

import com.fiestawiki.items.dao.info.ItemInfo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "lucky_capsule_reward",
        schema = "lucky_capsule"
)
public class LuckyCapsuleReward implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unique_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "item_fk")
    private ItemInfo item;

    @Column(name = "lcr_lot")
    private Long lot;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ItemInfo getItem() {
        return item;
    }

    public void setItem(ItemInfo item) {
        this.item = item;
    }

    public Long getLot() {
        return lot;
    }

    public void setLot(Long lot) {
        this.lot = lot;
    }
}
