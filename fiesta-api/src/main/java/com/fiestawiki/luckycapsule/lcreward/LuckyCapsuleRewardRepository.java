package com.fiestawiki.luckycapsule.lcreward;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LuckyCapsuleRewardRepository extends JpaRepository<LuckyCapsuleReward, Long> {
}
