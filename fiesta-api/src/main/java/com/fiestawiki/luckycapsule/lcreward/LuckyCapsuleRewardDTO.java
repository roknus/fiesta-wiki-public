package com.fiestawiki.luckycapsule.lcreward;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dto.ItemDTO;

public class LuckyCapsuleRewardDTO
{
    @JsonProperty("item")
    private ItemDTO item;

    public ItemDTO getItem() { return item; }

    public void setItem(ItemDTO item) { this.item = item; }
}
