package com.fiestawiki.luckycapsule.luckycapsule;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LuckyCapsuleDTO
{
    @JsonProperty("capsule_id")
    private Long capsuleId;

    @JsonProperty("server_id")
    private Long serverId;

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }
}
