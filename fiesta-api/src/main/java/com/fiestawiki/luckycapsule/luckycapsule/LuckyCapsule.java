package com.fiestawiki.luckycapsule.luckycapsule;

import com.fiestawiki.items.dao.info.ItemInfo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "lucky_capsule",
        schema = "lucky_capsule"
)
public class LuckyCapsule implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "serverid")
    private Long serverId;

    @Column(name = "type")
    private Byte type;

    @OneToOne
    @JoinColumn(name = "capsule_item_fk")
    private ItemInfo capsule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public ItemInfo getCapsule() {
        return capsule;
    }

    public void setCapsule(ItemInfo capsule) {
        this.capsule = capsule;
    }
}
