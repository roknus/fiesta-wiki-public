package com.fiestawiki.skills.dao.activeskill;

import com.fiestawiki.skills.dao.activeskillgroupjoin.ActiveSkillGroupJoin;
import com.fiestawiki.skills.dao.activeskillview.ActiveSkillView;
import com.fiestawiki.skills.dao.multihitskill.MultiHitSkill;
import com.fiestawiki.states.abstate.AbState;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "active_skill")
@EntityListeners(AuditingEntityListener.class)
public class ActiveSkill implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(name = "id")
    private ActiveSkillView view;

    @OneToOne
    @JoinColumn(name = "previous_skill_fk")
    private ActiveSkill previousSkill;

    @OneToOne(mappedBy = "previousSkill")
    private ActiveSkill nextSkill;

    @Column(name = "step")
    private Long step;

    @Column(name = "maxstep")
    private Long maxStep;

    @Column(name = "demandtype")
    private Long requiredWeapon;

    @Column(name = "sp")
    private Long sp;

    @Column(name = "sprate")
    private Long spRate;

    @Column(name = "hp")
    private Long hp;

    @Column(name = "hprate")
    private Long hpRate;

    @Column(name = "lp")
    private Long lp;

    @Column(name = "range")
    private Long range;

    @Column(name = "first")
    private Long castedOn;

    @Column(name = "last")
    private Long affectedTarget;

    @Column(name = "casttime")
    private Long castTime;

    @Column(name = "dlytime")
    private Long cooldown;

    @Column(name = "minwc")
    private Long minDmg;

    @Column(name = "minwcrate")
    private Long minDmgRate;

    @Column(name = "maxwc")
    private Long maxDmg;

    @Column(name = "maxwcrate")
    private Long maxDmgRate;

    @Column(name = "minma")
    private Long minMdmg;

    @Column(name = "minmarate")
    private Long minMdmgRate;

    @Column(name = "maxma")
    private Long maxMdmg;

    @Column(name = "maxmarate")
    private Long maxMdmgRate;

    @Column(name = "useclass")
    private Long useClass;

    @OneToOne
    @JoinColumn(name = "stanamea_fk")
    private AbState abStateA;

    @Column(name = "stastrengtha")
    private Long abStateAStrength;

    @Column(name = "stasucratea")
    private Long abStateASuccessRate;

    @OneToOne
    @JoinColumn(name = "stanameb_fk")
    private AbState abStateB;

    @Column(name = "stastrengthb")
    private Long abStateBStrength;

    @Column(name = "stasucrateb")
    private Long abStateBSuccessRate;

    @OneToOne
    @JoinColumn(name = "stanamec_fk")
    private AbState abStateC;

    @Column(name = "stastrengthc")
    private Long abStateCStrength;

    @Column(name = "stasucratec")
    private Long abStateCSuccessRate;

    @Column(name = "nimpt")
    private int empowerPowerTotal;

    @Column(name = "unkcol0")
    private int empowerSpTotal;

    @Column(name = "unkcol1")
    private int empowerDurationTotal;

    @Column(name = "unkcol2")
    private int empowerCooldownTotal;

    @Column(name = "nt0")
    private int empowerPower1;

    @Column(name = "unkcol3")
    private int empowerPower2;

    @Column(name = "unkcol4")
    private int empowerPower3;

    @Column(name = "unkcol5")
    private int empowerPower4;

    @Column(name = "unkcol6")
    private int empowerPower5;

    @Column(name = "nt1")
    private int empowerSp1;

    @Column(name = "unkcol7")
    private int empowerSp2;

    @Column(name = "unkcol8")
    private int empowerSp3;

    @Column(name = "unkcol9")
    private int empowerSp4;

    @Column(name = "unkcol10")
    private int empowerSp5;

    @Column(name = "nt2")
    private int empowerDuration1;

    @Column(name = "unkcol11")
    private int empowerDuration2;

    @Column(name = "unkcol12")
    private int empowerDuration3;

    @Column(name = "unkcol13")
    private int empowerDuration4;

    @Column(name = "unkcol14")
    private int empowerDuration5;

    @Column(name = "nt3")
    private int empowerCooldown1;

    @Column(name = "unkcol15")
    private int empowerCooldown2;

    @Column(name = "unkcol16")
    private int empowerCooldown3;

    @Column(name = "unkcol17")
    private int empowerCooldown4;

    @Column(name = "unkcol18")
    private int empowerCooldown5;

    @Column(name = "effecttype")
    private Long skillType;

    @Column(name = "specialindexa")
    private Long specialIndexA;

    @Column(name = "specialvaluea")
    private Long specialValueA;

    @Column(name = "specialindexb")
    private Long specialIndexB;

    @Column(name = "specialvalueb")
    private Long specialValueB;

    @Column(name = "specialindexc")
    private Long specialIndexC;

    @Column(name = "specialvaluec")
    private Long specialValueC;

    @Column(name = "specialindexd")
    private Long specialIndexD;

    @Column(name = "specialvalued")
    private Long specialValueD;

    @Column(name = "specialindexe")
    private Long specialIndexE;

    @Column(name = "specialvaluee")
    private Long specialValueE;

    @Column(name = "demandsoul")
    private Long soulRequired;

    @OneToMany(mappedBy = "skill")
    private List<ActiveSkillGroupJoin> groups;

    @ManyToMany(mappedBy = "multiHit")
    private List<MultiHitSkill> multiHit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActiveSkillView getView() {
        return view;
    }

    public void setView(ActiveSkillView view) {
        this.view = view;
    }

    public ActiveSkill getPreviousSkill() {
        return previousSkill;
    }

    public void setPreviousSkill(ActiveSkill previousSkill) {
        this.previousSkill = previousSkill;
    }

    public ActiveSkill getNextSkill() { return nextSkill; }

    public void setNextSkill(ActiveSkill nextSkill) { this.nextSkill = nextSkill; }

    public Long getStep() {
        return step;
    }

    public void setStep(Long step) {
        this.step = step;
    }

    public Long getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(Long maxStep) {
        this.maxStep = maxStep;
    }

    public Long getRequiredWeapon() {
        return requiredWeapon;
    }

    public void setRequiredWeapon(Long requiredWeapon) {
        this.requiredWeapon = requiredWeapon;
    }

    public Long getSp() {
        return sp;
    }

    public void setSp(Long sp) {
        this.sp = sp;
    }

    public Long getSpRate() {
        return spRate;
    }

    public void setSpRate(Long spRate) {
        this.spRate = spRate;
    }

    public Long getHp() {
        return hp;
    }

    public void setHp(Long hp) {
        this.hp = hp;
    }

    public Long getHpRate() {
        return hpRate;
    }

    public void setHpRate(Long hpRate) {
        this.hpRate = hpRate;
    }

    public Long getLp() {
        return lp;
    }

    public void setLp(Long lp) {
        this.lp = lp;
    }

    public Long getRange() {
        return range;
    }

    public void setRange(Long range) {
        this.range = range;
    }

    public Long getCastedOn() {
        return castedOn;
    }

    public void setCastedOn(Long castedOn) {
        this.castedOn = castedOn;
    }

    public Long getAffectedTarget() {
        return affectedTarget;
    }

    public void setAffectedTarget(Long affectedTarget) {
        this.affectedTarget = affectedTarget;
    }

    public Long getCastTime() {
        return castTime;
    }

    public void setCastTime(Long castTime) {
        this.castTime = castTime;
    }

    public Long getCooldown() {
        return cooldown;
    }

    public void setCooldown(Long cooldown) {
        this.cooldown = cooldown;
    }

    public Long getMinDmg() {
        return minDmg;
    }

    public void setMinDmg(Long minDmg) {
        this.minDmg = minDmg;
    }

    public Long getMinDmgRate() { return minDmgRate; }

    public void setMinDmgRate(Long minDmgRate) { this.minDmgRate = minDmgRate; }

    public Long getMaxDmg() {
        return maxDmg;
    }

    public void setMaxDmg(Long maxDmg) {
        this.maxDmg = maxDmg;
    }

    public Long getMaxDmgRate() {
        return maxDmgRate;
    }

    public void setMaxDmgRate(Long maxDmgRate) {
        this.maxDmgRate = maxDmgRate;
    }

    public Long getMinMdmg() {
        return minMdmg;
    }

    public void setMinMdmg(Long minMdmg) {
        this.minMdmg = minMdmg;
    }

    public Long getMinMdmgRate() { return minMdmgRate; }

    public void setMinMdmgRate(Long minMdmgRate) { this.minMdmgRate = minMdmgRate; }

    public Long getMaxMdmg() {
        return maxMdmg;
    }

    public void setMaxMdmg(Long maxMdmg) {
        this.maxMdmg = maxMdmg;
    }

    public Long getMaxMdmgRate() {
        return maxMdmgRate;
    }

    public void setMaxMdmgRate(Long maxMdmgRate) {
        this.maxMdmgRate = maxMdmgRate;
    }

    public Long getUseClass() { return useClass; }

    public void setUseClass(Long useClass) {
        this.useClass = useClass;
    }

    public AbState getAbStateA() {
        return abStateA;
    }

    public void setAbStateA(AbState abStateA) {
        this.abStateA = abStateA;
    }

    public Long getAbStateAStrength() {
        return abStateAStrength;
    }

    public void setAbStateAStrength(Long abStateAStrength) {
        this.abStateAStrength = abStateAStrength;
    }

    public Long getAbStateASuccessRate() { return abStateASuccessRate; }

    public void setAbStateASuccessRate(Long abStateASuccessRate) { this.abStateASuccessRate = abStateASuccessRate; }

    public AbState getAbStateB() { return abStateB; }

    public void setAbStateB(AbState abStateB) { this.abStateB = abStateB; }

    public Long getAbStateBStrength() { return abStateBStrength; }

    public void setAbStateBStrength(Long abStateBStrength) { this.abStateBStrength = abStateBStrength; }

    public Long getAbStateBSuccessRate() { return abStateBSuccessRate; }

    public void setAbStateBSuccessRate(Long abStateBSuccessRate) { this.abStateBSuccessRate = abStateBSuccessRate; }

    public AbState getAbStateC() { return abStateC; }

    public void setAbStateC(AbState abStateC) { this.abStateC = abStateC; }

    public Long getAbStateCStrength() { return abStateCStrength; }

    public void setAbStateCStrength(Long abStateCStrength) { this.abStateCStrength = abStateCStrength; }

    public Long getAbStateCSuccessRate() { return abStateCSuccessRate; }

    public void setAbStateCSuccessRate(Long abStateCSuccessRate) { this.abStateCSuccessRate = abStateCSuccessRate; }

    public int getEmpowerPowerTotal() {
        return empowerPowerTotal;
    }

    public void setEmpowerPowerTotal(int empowerPowerTotal) {
        this.empowerPowerTotal = empowerPowerTotal;
    }

    public int getEmpowerSpTotal() {
        return empowerSpTotal;
    }

    public void setEmpowerSpTotal(int empowerSpTotal) {
        this.empowerSpTotal = empowerSpTotal;
    }

    public int getEmpowerDurationTotal() {
        return empowerDurationTotal;
    }

    public void setEmpowerDurationTotal(int empowerDurationTotal) {
        this.empowerDurationTotal = empowerDurationTotal;
    }

    public int getEmpowerCooldownTotal() {
        return empowerCooldownTotal;
    }

    public void setEmpowerCooldownTotal(int empowerCooldownTotal) {
        this.empowerCooldownTotal = empowerCooldownTotal;
    }

    public int getEmpowerPower1() {
        return empowerPower1;
    }

    public void setEmpowerPower1(int empowerPower1) {
        this.empowerPower1 = empowerPower1;
    }

    public int getEmpowerPower2() {
        return empowerPower2;
    }

    public void setEmpowerPower2(int empowerPower2) {
        this.empowerPower2 = empowerPower2;
    }

    public int getEmpowerPower3() {
        return empowerPower3;
    }

    public void setEmpowerPower3(int empowerPower3) {
        this.empowerPower3 = empowerPower3;
    }

    public int getEmpowerPower4() {
        return empowerPower4;
    }

    public void setEmpowerPower4(int empowerPower4) {
        this.empowerPower4 = empowerPower4;
    }

    public int getEmpowerPower5() {
        return empowerPower5;
    }

    public void setEmpowerPower5(int empowerPower5) {
        this.empowerPower5 = empowerPower5;
    }

    public int getEmpowerSp1() {
        return empowerSp1;
    }

    public void setEmpowerSp1(int empowerSp1) {
        this.empowerSp1 = empowerSp1;
    }

    public int getEmpowerSp2() {
        return empowerSp2;
    }

    public void setEmpowerSp2(int empowerSp2) {
        this.empowerSp2 = empowerSp2;
    }

    public int getEmpowerSp3() {
        return empowerSp3;
    }

    public void setEmpowerSp3(int empowerSp3) {
        this.empowerSp3 = empowerSp3;
    }

    public int getEmpowerSp4() {
        return empowerSp4;
    }

    public void setEmpowerSp4(int empowerSp4) {
        this.empowerSp4 = empowerSp4;
    }

    public int getEmpowerSp5() {
        return empowerSp5;
    }

    public void setEmpowerSp5(int empowerSp5) {
        this.empowerSp5 = empowerSp5;
    }

    public int getEmpowerDuration1() {
        return empowerDuration1;
    }

    public void setEmpowerDuration1(int empowerDuration1) {
        this.empowerDuration1 = empowerDuration1;
    }

    public int getEmpowerDuration2() {
        return empowerDuration2;
    }

    public void setEmpowerDuration2(int empowerDuration2) {
        this.empowerDuration2 = empowerDuration2;
    }

    public int getEmpowerDuration3() {
        return empowerDuration3;
    }

    public void setEmpowerDuration3(int empowerDuration3) {
        this.empowerDuration3 = empowerDuration3;
    }

    public int getEmpowerDuration4() {
        return empowerDuration4;
    }

    public void setEmpowerDuration4(int empowerDuration4) {
        this.empowerDuration4 = empowerDuration4;
    }

    public int getEmpowerDuration5() {
        return empowerDuration5;
    }

    public void setEmpowerDuration5(int empowerDuration5) {
        this.empowerDuration5 = empowerDuration5;
    }

    public int getEmpowerCooldown1() {
        return empowerCooldown1;
    }

    public void setEmpowerCooldown1(int empowerCooldown1) {
        this.empowerCooldown1 = empowerCooldown1;
    }

    public int getEmpowerCooldown2() {
        return empowerCooldown2;
    }

    public void setEmpowerCooldown2(int empowerCooldown2) {
        this.empowerCooldown2 = empowerCooldown2;
    }

    public int getEmpowerCooldown3() {
        return empowerCooldown3;
    }

    public void setEmpowerCooldown3(int empowerCooldown3) {
        this.empowerCooldown3 = empowerCooldown3;
    }

    public int getEmpowerCooldown4() {
        return empowerCooldown4;
    }

    public void setEmpowerCooldown4(int empowerCooldown4) {
        this.empowerCooldown4 = empowerCooldown4;
    }

    public int getEmpowerCooldown5() {
        return empowerCooldown5;
    }

    public void setEmpowerCooldown5(int empowerCooldown5) {
        this.empowerCooldown5 = empowerCooldown5;
    }

    public Long getSkillType() {
        return skillType;
    }

    public void setSkillType(Long skillType) {
        this.skillType = skillType;
    }

    public Long getSpecialIndexA() {
        return specialIndexA;
    }

    public void setSpecialIndexA(Long specialIndexA) {
        this.specialIndexA = specialIndexA;
    }

    public Long getSpecialValueA() {
        return specialValueA;
    }

    public void setSpecialValueA(Long specialValueA) {
        this.specialValueA = specialValueA;
    }

    public Long getSpecialIndexB() { return specialIndexB; }

    public void setSpecialIndexB(Long specialIndexB) { this.specialIndexB = specialIndexB; }

    public Long getSpecialValueB() { return specialValueB; }

    public void setSpecialValueB(Long specialValueB) { this.specialValueB = specialValueB; }

    public Long getSpecialIndexC() { return specialIndexC; }

    public void setSpecialIndexC(Long specialIndexC) { this.specialIndexC = specialIndexC; }

    public Long getSpecialValueC() { return specialValueC; }

    public void setSpecialValueC(Long specialValueC) { this.specialValueC = specialValueC; }

    public Long getSpecialIndexD() { return specialIndexD; }

    public void setSpecialIndexD(Long specialIndexD) { this.specialIndexD = specialIndexD; }

    public Long getSpecialValueD() { return specialValueD; }

    public void setSpecialValueD(Long specialValueD) { this.specialValueD = specialValueD; }

    public Long getSpecialIndexE() { return specialIndexE; }

    public void setSpecialIndexE(Long specialIndexE) { this.specialIndexE = specialIndexE; }

    public Long getSpecialValueE() { return specialValueE; }

    public void setSpecialValueE(Long specialValueE) { this.specialValueE = specialValueE; }

    public Long getSoulRequired() { return soulRequired; }

    public void setSoulRequired(Long soulRequired) { this.soulRequired = soulRequired; }

    public String getGroupName() {
        return inxname.replaceAll("(\\d+)$", "");
    }

    public List<ActiveSkill> getGroupSkills()
    {
        List<ActiveSkill> new_list = new ArrayList<>();
        new_list.add(this);

        ActiveSkill next = getNextSkill();
        while(next != null)
        {
            new_list.add(next);

            next = next.getNextSkill();
        }

        ActiveSkill previous = getPreviousSkill();
        while(previous != null)
        {
            new_list.add(previous);

            previous = previous.getNextSkill();
        }

        return new_list;
    }

    public List<ActiveSkillGroupJoin> getGroups() { return groups; }

    public void setGroups(List<ActiveSkillGroupJoin> groups) { this.groups = groups; }

    public List<MultiHitSkill> getMultiHit() { return multiHit; }

    public void setMultiHit(List<MultiHitSkill> multiHit) { this.multiHit = multiHit; }
}
