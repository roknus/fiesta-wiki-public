package com.fiestawiki.skills.dao.activeskillview;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActiveSkillViewDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("icon_index")
    private int iconIndex;

    @JsonProperty("icon_file")
    private String iconFile;

    @JsonProperty("red")
    private int red;

    @JsonProperty("green")
    private int green;

    @JsonProperty("blue")
    private int blue;

    @JsonProperty("description")
    private String description;

    @JsonProperty("required_level")
    private int requiredLevel;

    @JsonProperty("function")
    private String function;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getIconIndex() { return iconIndex; }

    public void setIconIndex(int iconIndex) { this.iconIndex = iconIndex; }

    public String getIconFile() { return iconFile.toLowerCase(); }

    public void setIconFile(String iconFile) { this.iconFile = iconFile; }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() { return blue; }

    public void setBlue(int blue) { this.blue = blue; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRequiredLevel() { return requiredLevel; }

    public void setRequiredLevel(int requiredLevel) { this.requiredLevel = requiredLevel; }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }
}
