package com.fiestawiki.skills.dao.passiveskillview;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "passive_skill_view")
@EntityListeners(AuditingEntityListener.class)
public class PassiveSkillView {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "icon")
    private Long icon;

    @Column(name = "iconfile")
    private String iconFile;

    @Column(name = "r")
    private Long red;

    @Column(name = "g")
    private Long green;

    @Column(name = "b")
    private Long blue;

    @Column(name = "descript")
    private String description;

    @Column(name = "demandlv")
    private Long requiredLevel;

    @Column(name = "useclass")
    private Long requiredClass;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getIcon() {
        return icon;
    }

    public void setIcon(Long icon) {
        this.icon = icon;
    }

    public String getIconFile() {
        return iconFile;
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public Long getRed() {
        return red;
    }

    public void setRed(Long red) {
        this.red = red;
    }

    public Long getGreen() {
        return green;
    }

    public void setGreen(Long green) {
        this.green = green;
    }

    public Long getBlue() {
        return blue;
    }

    public void setBlue(Long blue) {
        this.blue = blue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Long requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Long getRequiredClass() {
        return requiredClass;
    }

    public void setRequiredClass(Long requiredClass) {
        this.requiredClass = requiredClass;
    }
}
