package com.fiestawiki.skills.dao.activeskillgroupjoin;

import com.fiestawiki.skills.dao.activeskill.ActiveSkill;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "active_skill_group_join")
@EntityListeners(AuditingEntityListener.class)
public class ActiveSkillGroupJoin {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long uniqueId;

    @ManyToOne
    @JoinColumn(name = "active_skill_fk")
    private ActiveSkill skill;

    @Column(name = "activeskillgroupindex")
    private Long groupIndex;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public ActiveSkill getSkill() {
        return skill;
    }

    public void setSkill(ActiveSkill skill) {
        this.skill = skill;
    }

    public Long getGroupIndex() {
        return groupIndex;
    }

    public void setGroupIndex(Long groupIndex) {
        this.groupIndex = groupIndex;
    }
}
