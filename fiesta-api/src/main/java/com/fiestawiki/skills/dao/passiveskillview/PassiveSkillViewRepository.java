package com.fiestawiki.skills.dao.passiveskillview;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PassiveSkillViewRepository extends JpaRepository<PassiveSkillView, Long> {
}
