package com.fiestawiki.skills.dao.activeskill;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ActiveSkillRepository extends JpaRepository<ActiveSkill, Long> {

    @Query("SELECT s " +
            "FROM ActiveSkill s " +
            "WHERE s.useClass IN(:useClasses) " +
            "AND s.step = 1 " +
            "ORDER BY s.view.requiredLevel")
    List<ActiveSkill> findBaseSkillsByClass(List<Long> useClasses);

    @Query("SELECT s FROM ActiveSkill s WHERE s.inxname LIKE :inxname% ORDER BY s.view.requiredLevel DESC")
    List<ActiveSkill> findByInxname(String inxname);
}
