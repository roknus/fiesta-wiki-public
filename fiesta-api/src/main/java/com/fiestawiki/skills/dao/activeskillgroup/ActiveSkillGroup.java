package com.fiestawiki.skills.dao.activeskillgroup;

import com.fiestawiki.skills.dao.activeskill.ActiveSkill;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Entity
@Table(name = "active_skill_group")
@EntityListeners(AuditingEntityListener.class)
public class ActiveSkillGroup {

    @Id
    @Column(name = "id")
    private Long id;

    @ManyToMany
    @JoinTable(
            name = "active_skill_group_join",
            joinColumns = @JoinColumn(name = "activeskillgroupindex"),
            inverseJoinColumns = @JoinColumn(name = "active_skill_fk"))
    private List<ActiveSkill> skill = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ActiveSkill> getSkill() {
        return skill;
    }

    public void setSkill(List<ActiveSkill> skill) {
        this.skill = skill;
    }

    public List<ActiveSkill> getUniqueSkills() {
        if(skill.isEmpty())
            return new ArrayList<>();

        return skill
                .stream()
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(ActiveSkill::getGroupName))), ArrayList::new));
    }
}
