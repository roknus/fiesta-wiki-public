package com.fiestawiki.skills.dao.activeskillview;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ActiveSkillViewRepository extends JpaRepository<ActiveSkillView, Long> {
}
