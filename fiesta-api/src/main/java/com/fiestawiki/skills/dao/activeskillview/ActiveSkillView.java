package com.fiestawiki.skills.dao.activeskillview;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "active_skill_view")
@EntityListeners(AuditingEntityListener.class)
public class ActiveSkillView {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "iconindex")
    private int iconIndex;

    @Column(name = "iconfile")
    private String iconFile;

    @Column(name = "r")
    private int red;

    @Column(name = "g")
    private int green;

    @Column(name = "b")
    private int blue;

    @Column(name = "descript")
    private String description;

    @Column(name = "uidemandlv")
    private int requiredLevel;

    @Column(name = "function")
    private String function;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public int getIconIndex() {
        return iconIndex;
    }

    public void setIconIndex(int iconIndex) {
        this.iconIndex = iconIndex;
    }

    public String getIconFile() {
        return iconFile;
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() { return blue; }

    public void setBlue(int blue) { this.blue = blue; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRequiredLevel() { return requiredLevel; }

    public void setRequiredLevel(int requiredLevel) { this.requiredLevel = requiredLevel; }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }
}
