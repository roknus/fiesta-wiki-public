package com.fiestawiki.skills.dao.passiveskill;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PassiveSkillRepository extends JpaRepository<PassiveSkill, Long> {

    @Query("SELECT ps " +
            "FROM PassiveSkill ps " +
            "WHERE ps.view.requiredClass IN(:useClasses) " +
            "AND ps.previousSkill IS NULL " +
            "ORDER BY ps.view.requiredLevel")
    List<PassiveSkill> findBaseSkillsByClass(List<Long> useClasses);

    @Query("SELECT s FROM PassiveSkill s WHERE s.inxname LIKE :inxname% ORDER BY s.view.requiredLevel DESC")
    List<PassiveSkill> findByInxname(String inxname);
}
