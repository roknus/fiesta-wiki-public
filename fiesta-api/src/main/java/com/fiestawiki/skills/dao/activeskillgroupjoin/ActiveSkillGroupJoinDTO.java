package com.fiestawiki.skills.dao.activeskillgroupjoin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.skills.dto.ActiveSkillDTO;

public class ActiveSkillGroupJoinDTO {

    @JsonProperty("unique_id")
    private Long uniqueId;

    @JsonProperty("skill")
    private ActiveSkillDTO skill;

    @JsonProperty("group_index")
    private Long groupIndex;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public ActiveSkillDTO getSkill() { return skill; }

    public void setSkill(ActiveSkillDTO skill) { this.skill = skill; }

    public Long getGroupIndex() {
        return groupIndex;
    }

    public void setGroupIndex(Long groupIndex) {
        this.groupIndex = groupIndex;
    }
}
