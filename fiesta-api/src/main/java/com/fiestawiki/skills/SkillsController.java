package com.fiestawiki.skills;

import com.fiestawiki.skills.dto.PassiveSkillDTO;
import com.fiestawiki.skills.dto.ActiveSkillDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class SkillsController
{
    @Autowired
    private SkillsService skillsService;

    @GetMapping("active-skill/{id}")
    public ResponseEntity<ActiveSkillDTO> getActiveSkill(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ActiveSkillDTO skill = skillsService.getActiveSkill(id);

        return ResponseEntity.ok(skill);
    }

    @GetMapping("passive-skill/{id}")
    public ResponseEntity<PassiveSkillDTO> getPassiveSkill(@PathVariable(value = "id") Long id)
            throws Exception
    {
        PassiveSkillDTO skill = skillsService.getPassiveSkill(id);

        return ResponseEntity.ok(skill);
    }

    @GetMapping("active-skill-list/{class}")
    public ResponseEntity<List<Long>> getActiveSkillList(
            @PathVariable(name = "class") Long theClass,
            @RequestParam(name = "level") Optional<Integer> level

    )
            throws Exception
    {
        List<Long> skills = skillsService.getActiveSkillList(theClass, level);

        return ResponseEntity.ok(skills);
    }

    @GetMapping("passive-skill-list/{class}")
    public ResponseEntity<List<Long>> getPassiveSkillList(
            @PathVariable(name = "class") Long theClass,
            @RequestParam(name = "level") Optional<Integer> level
    )
            throws Exception
    {
        List<Long> skills = skillsService.getPassiveSkillList(theClass, level);

        return ResponseEntity.ok(skills);
    }

    @GetMapping("active-skill-group/{class}/{inxname}")
    public ResponseEntity<List<Long>> getActiveSkillGroups(
            @PathVariable(name = "class") Long theClass,
            @PathVariable(value = "inxname") String inxname,
            @RequestParam(name = "level") Optional<Integer> level
    )
            throws Exception
    {
        List<Long> skills = skillsService.getActiveSkillGroups(theClass, inxname, level);

        return ResponseEntity.ok(skills);
    }

    @GetMapping("passive-skill-group/{class}/{inxname}")
    public ResponseEntity<List<Long>> getPassiveSkillGroups(
            @PathVariable(name = "class") Long theClass,
            @PathVariable(value = "inxname") String inxname,
            @RequestParam(name = "level") Optional<Integer> level
    )
            throws Exception
    {
        List<Long> skills = skillsService.getPassiveSkillGroups(theClass, inxname, level);

        return ResponseEntity.ok(skills);
    }
}
