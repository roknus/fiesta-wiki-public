package com.fiestawiki.skills.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActiveSkillEffectDTO
{
    @JsonProperty("effect_id")
    private Long effectId;

    @JsonProperty("effect_value")
    private Long effectValue;

    public ActiveSkillEffectDTO(Long effectId, Long effectValue) {
        this.effectId = effectId;
        this.effectValue = effectValue;
    }

    public Long getEffectId() {
        return effectId;
    }

    public void setEffectId(Long effectId) {
        this.effectId = effectId;
    }

    public Long getEffectValue() {
        return effectValue;
    }

    public void setEffectValue(Long effectValue) {
        this.effectValue = effectValue;
    }
}
