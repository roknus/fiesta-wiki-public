package com.fiestawiki.skills.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.skills.dao.passiveskillview.PassiveSkillViewDTO;
import com.fiestawiki.states.passiveskillabstate.PassiveSkillAbStateDTO;

public class PassiveSkillDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("name")
    private String name;

    @JsonProperty("view")
    private PassiveSkillViewDTO view;

    @JsonProperty("passive_ab_state")
    private PassiveSkillAbStateDTO abState;

    @JsonProperty("mastery_sword_dmg_rate")
    private Long masterySwordDmgRate;

    @JsonProperty("mastery_hammer_dmg_rate")
    private Long masteryHammerDmgRate;

    @JsonProperty("mastery_two_hand_dmg_rate")
    private Long masteryTwoHandDmgRate;

    @JsonProperty("mastery_axe_dmg_rate")
    private Long masteryAxeDmgRate;

    @JsonProperty("mastery_mace_dmg_rate")
    private Long masteryMaceDmgRate;

    @JsonProperty("mastery_bow_dmg_rate")
    private Long masteryBowDmgRate;

    @JsonProperty("mastery_crossbow_dmg_rate")
    private Long masteryCrossbowDmgRate;

    @JsonProperty("mastery_wand_dmg_rate")
    private Long masteryWandDmgRate;

    @JsonProperty("mastery_staff_dmg_rate")
    private Long masteryStaffDmgRate;

    @JsonProperty("mastery_claw_dmg_rate")
    private Long masteryClawDmgRate;

    @JsonProperty("mastery_dsword_dmg_rate")
    private Long masteryDSwordDmgRate;

    @JsonProperty("mastery_sword_dmg")
    private Long masterySwordDmg;

    @JsonProperty("mastery_hammer_dmg")
    private Long masteryHammerDmg;

    @JsonProperty("mastery_two_hand_dmg")
    private Long masteryTwoHandDmg;

    @JsonProperty("mastery_axe_dmg")
    private Long masteryAxeDmg;

    @JsonProperty("mastery_mace_dmg")
    private Long masteryMaceDmg;

    @JsonProperty("mastery_bow_dmg")
    private Long masteryBowDmg;

    @JsonProperty("mastery_crossbow_dmg")
    private Long masteryCrossbowDmg;

    @JsonProperty("mastery_wand_dmg")
    private Long masteryWandDmg;

    @JsonProperty("mastery_staff_dmg")
    private Long masteryStaffDmg;

    @JsonProperty("mastery_claw_dmg")
    private Long masteryClawDmg;

    @JsonProperty("mastery_dsword_dmg")
    private Long masteryDSwordDmg;

    @JsonProperty("evasion")
    private Long evasion;

    @JsonProperty("sp_increase")
    private Long SPIncrease;

    @JsonProperty("lp_increase")
    private Long LPIncrease;

    @JsonProperty("phy_atk_up_rate")
    private Long phyAtkUpRate;

    @JsonProperty("mag_atk_up_rate")
    private Long magAtkUpRate;

    @JsonProperty("dmg_up_per_hp_down")
    private int dmgUpPerHPDown;

    @JsonProperty("dmg_up_per_hp_down_ratio")
    private int dmgUpPerHPDownRatio;

    @JsonProperty("def_up_per_hp_down")
    private int defUpPerHPDown;

    @JsonProperty("def_up_per_hp_down_ratio")
    private int defUpPerHPDownRatio;

    @JsonProperty("movement_increase_evasion_up")
    private int movementIncreaseEvasionUp;

    @JsonProperty("heal_up_rate")
    private int healUpRate;

    @JsonProperty("buff_duration_up_rate")
    private int buffDurationUpRate;

    @JsonProperty("crit_dmg_up_rate")
    private int critDmgUpRate;

    @JsonProperty("active_skill_group_index")
    private Long activeSkillGroupIndex;

    @JsonProperty("dmg_receive_minus_rate")
    private int dmgReceiveMinusRate;

    @JsonProperty("group_name")
    private String groupName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PassiveSkillViewDTO getView() {
        return view;
    }

    public void setView(PassiveSkillViewDTO view) {
        this.view = view;
    }

    public PassiveSkillAbStateDTO getAbState() { return abState; }

    public void setAbState(PassiveSkillAbStateDTO abState) { this.abState = abState; }

    public Long getMasterySwordDmgRate() {
        return masterySwordDmgRate;
    }

    public void setMasterySwordDmgRate(Long masterySwordDmgRate) {
        this.masterySwordDmgRate = masterySwordDmgRate;
    }

    public Long getMasteryHammerDmgRate() {
        return masteryHammerDmgRate;
    }

    public void setMasteryHammerDmgRate(Long masteryHammerDmgRate) {
        this.masteryHammerDmgRate = masteryHammerDmgRate;
    }

    public Long getMasteryTwoHandDmgRate() {
        return masteryTwoHandDmgRate;
    }

    public void setMasteryTwoHandDmgRate(Long masteryTwoHandDmgRate) {
        this.masteryTwoHandDmgRate = masteryTwoHandDmgRate;
    }

    public Long getMasteryAxeDmgRate() {
        return masteryAxeDmgRate;
    }

    public void setMasteryAxeDmgRate(Long masteryAxeDmgRate) {
        this.masteryAxeDmgRate = masteryAxeDmgRate;
    }

    public Long getMasteryMaceDmgRate() {
        return masteryMaceDmgRate;
    }

    public void setMasteryMaceDmgRate(Long masteryMaceDmgRate) {
        this.masteryMaceDmgRate = masteryMaceDmgRate;
    }

    public Long getMasteryBowDmgRate() {
        return masteryBowDmgRate;
    }

    public void setMasteryBowDmgRate(Long masteryBowDmgRate) {
        this.masteryBowDmgRate = masteryBowDmgRate;
    }

    public Long getMasteryCrossbowDmgRate() {
        return masteryCrossbowDmgRate;
    }

    public void setMasteryCrossbowDmgRate(Long masteryCrossbowDmgRate) {
        this.masteryCrossbowDmgRate = masteryCrossbowDmgRate;
    }

    public Long getMasteryWandDmgRate() {
        return masteryWandDmgRate;
    }

    public void setMasteryWandDmgRate(Long masteryWandDmgRate) {
        this.masteryWandDmgRate = masteryWandDmgRate;
    }

    public Long getMasteryStaffDmgRate() {
        return masteryStaffDmgRate;
    }

    public void setMasteryStaffDmgRate(Long masteryStaffDmgRate) {
        this.masteryStaffDmgRate = masteryStaffDmgRate;
    }

    public Long getMasteryClawDmgRate() {
        return masteryClawDmgRate;
    }

    public void setMasteryClawDmgRate(Long masteryClawDmgRate) {
        this.masteryClawDmgRate = masteryClawDmgRate;
    }

    public Long getMasteryDSwordDmgRate() {
        return masteryDSwordDmgRate;
    }

    public void setMasteryDSwordDmgRate(Long masteryDSwordDmgRate) {
        this.masteryDSwordDmgRate = masteryDSwordDmgRate;
    }

    public Long getMasterySwordDmg() {
        return masterySwordDmg;
    }

    public void setMasterySwordDmg(Long masterySwordDmg) {
        this.masterySwordDmg = masterySwordDmg;
    }

    public Long getMasteryHammerDmg() {
        return masteryHammerDmg;
    }

    public void setMasteryHammerDmg(Long masteryHammerDmg) {
        this.masteryHammerDmg = masteryHammerDmg;
    }

    public Long getMasteryTwoHandDmg() {
        return masteryTwoHandDmg;
    }

    public void setMasteryTwoHandDmg(Long masteryTwoHandDmg) {
        this.masteryTwoHandDmg = masteryTwoHandDmg;
    }

    public Long getMasteryAxeDmg() {
        return masteryAxeDmg;
    }

    public void setMasteryAxeDmg(Long masteryAxeDmg) {
        this.masteryAxeDmg = masteryAxeDmg;
    }

    public Long getMasteryMaceDmg() {
        return masteryMaceDmg;
    }

    public void setMasteryMaceDmg(Long masteryMaceDmg) {
        this.masteryMaceDmg = masteryMaceDmg;
    }

    public Long getMasteryBowDmg() {
        return masteryBowDmg;
    }

    public void setMasteryBowDmg(Long masteryBowDmg) {
        this.masteryBowDmg = masteryBowDmg;
    }

    public Long getMasteryCrossbowDmg() {
        return masteryCrossbowDmg;
    }

    public void setMasteryCrossbowDmg(Long masteryCrossbowDmg) {
        this.masteryCrossbowDmg = masteryCrossbowDmg;
    }

    public Long getMasteryWandDmg() {
        return masteryWandDmg;
    }

    public void setMasteryWandDmg(Long masteryWandDmg) {
        this.masteryWandDmg = masteryWandDmg;
    }

    public Long getMasteryStaffDmg() {
        return masteryStaffDmg;
    }

    public void setMasteryStaffDmg(Long masteryStaffDmg) {
        this.masteryStaffDmg = masteryStaffDmg;
    }

    public Long getMasteryClawDmg() {
        return masteryClawDmg;
    }

    public void setMasteryClawDmg(Long masteryClawDmg) {
        this.masteryClawDmg = masteryClawDmg;
    }

    public Long getMasteryDSwordDmg() {
        return masteryDSwordDmg;
    }

    public void setMasteryDSwordDmg(Long masteryDSwordDmg) {
        this.masteryDSwordDmg = masteryDSwordDmg;
    }

    public Long getEvasion() {
        return evasion;
    }

    public void setEvasion(Long evasion) {
        this.evasion = evasion;
    }

    public Long getSPIncrease() {
        return SPIncrease;
    }

    public void setSPIncrease(Long SPIncrease) {
        this.SPIncrease = SPIncrease;
    }

    public Long getLPIncrease() {
        return LPIncrease;
    }

    public void setLPIncrease(Long LPIncrease) {
        this.LPIncrease = LPIncrease;
    }

    public Long getPhyAtkUpRate() {
        return phyAtkUpRate;
    }

    public void setPhyAtkUpRate(Long phyAtkUpRate) {
        this.phyAtkUpRate = phyAtkUpRate;
    }

    public Long getMagAtkUpRate() {
        return magAtkUpRate;
    }

    public void setMagAtkUpRate(Long magAtkUpRate) {
        this.magAtkUpRate = magAtkUpRate;
    }

    public int getDmgUpPerHPDown() {
        return dmgUpPerHPDown;
    }

    public void setDmgUpPerHPDown(int dmgUpPerHPDown) {
        this.dmgUpPerHPDown = dmgUpPerHPDown;
    }

    public int getDmgUpPerHPDownRatio() {
        return dmgUpPerHPDownRatio;
    }

    public void setDmgUpPerHPDownRatio(int dmgUpPerHPDownRatio) {
        this.dmgUpPerHPDownRatio = dmgUpPerHPDownRatio;
    }

    public int getDefUpPerHPDown() {
        return defUpPerHPDown;
    }

    public void setDefUpPerHPDown(int defUpPerHPDown) {
        this.defUpPerHPDown = defUpPerHPDown;
    }

    public int getDefUpPerHPDownRatio() {
        return defUpPerHPDownRatio;
    }

    public void setDefUpPerHPDownRatio(int defUpPerHPDownRatio) {
        this.defUpPerHPDownRatio = defUpPerHPDownRatio;
    }

    public int getMovementIncreaseEvasionUp() {
        return movementIncreaseEvasionUp;
    }

    public void setMovementIncreaseEvasionUp(int movementIncreaseEvasionUp) {
        this.movementIncreaseEvasionUp = movementIncreaseEvasionUp;
    }

    public int getHealUpRate() {
        return healUpRate;
    }

    public void setHealUpRate(int healUpRate) {
        this.healUpRate = healUpRate;
    }

    public int getBuffDurationUpRate() {
        return buffDurationUpRate;
    }

    public void setBuffDurationUpRate(int buffDurationUpRate) {
        this.buffDurationUpRate = buffDurationUpRate;
    }

    public int getCritDmgUpRate() {
        return critDmgUpRate;
    }

    public void setCritDmgUpRate(int critDmgUpRate) {
        this.critDmgUpRate = critDmgUpRate;
    }

    public Long getActiveSkillGroupIndex() {
        return activeSkillGroupIndex;
    }

    public void setActiveSkillGroupIndex(Long activeSkillGroupIndex) {
        this.activeSkillGroupIndex = activeSkillGroupIndex;
    }

    public int getDmgReceiveMinusRate() {
        return dmgReceiveMinusRate;
    }

    public void setDmgReceiveMinusRate(int dmgReceiveMinusRate) {
        this.dmgReceiveMinusRate = dmgReceiveMinusRate;
    }

    public String getGroupName() { return groupName; }

    public void setGroupName(String groupName) { this.groupName = groupName; }
}
