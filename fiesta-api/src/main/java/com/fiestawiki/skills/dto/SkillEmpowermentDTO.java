package com.fiestawiki.skills.dto;

import java.util.ArrayList;
import java.util.List;

public class SkillEmpowermentDTO {

    private List<Integer> power = new ArrayList<>();
    private List<Integer> sp = new ArrayList<>();
    private List<Integer> duration = new ArrayList<>();
    private List<Integer> cooldown = new ArrayList<>();

    public List<Integer> getPower() {
        return power;
    }

    public void setPower(List<Integer> power) {
        this.power = power;
    }

    public List<Integer> getSp() {
        return sp;
    }

    public void setSp(List<Integer> sp) {
        this.sp = sp;
    }

    public List<Integer> getDuration() {
        return duration;
    }

    public void setDuration(List<Integer> duration) {
        this.duration = duration;
    }

    public List<Integer> getCooldown() {
        return cooldown;
    }

    public void setCooldown(List<Integer> cooldown) {
        this.cooldown = cooldown;
    }
}
