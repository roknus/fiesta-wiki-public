package com.fiestawiki.skills;

import com.fiestawiki.ClassEnum;
import com.fiestawiki.items.dao.itemactioncondition.ItemActionCondition;
import com.fiestawiki.items.dao.seteffect.SetEffect;
import com.fiestawiki.items.dto.SetEffectDTO;
import com.fiestawiki.items.dao.seteffect.SetEffectRepository;
import com.fiestawiki.skills.dao.activeskill.*;
import com.fiestawiki.skills.dao.activeskillgroup.ActiveSkillGroup;
import com.fiestawiki.skills.dao.activeskillgroupjoin.ActiveSkillGroupJoinRepository;
import com.fiestawiki.skills.dao.activeskillview.ActiveSkillViewDTO;
import com.fiestawiki.skills.dao.activeskillview.ActiveSkillViewRepository;
import com.fiestawiki.skills.dao.passiveskill.PassiveSkill;
import com.fiestawiki.skills.dao.passiveskillview.PassiveSkillViewDTO;
import com.fiestawiki.skills.dto.*;
import com.fiestawiki.skills.dao.passiveskill.PassiveSkillRepository;
import com.fiestawiki.states.StatesService;
import com.fiestawiki.states.abstate.AbState;
import com.fiestawiki.states.abstate.AbStateDTO;
import com.fiestawiki.states.passiveskillabstate.PassiveSkillAbState;
import com.fiestawiki.states.passiveskillabstate.PassiveSkillAbStateDTO;
import com.fiestawiki.states.subabstate.SubAbStateRepository;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Service
public class SkillsService {

    @Autowired
    private ActiveSkillRepository activeSkillRepository;

    @Autowired
    private ActiveSkillViewRepository activeSkillViewRepository;

    @Autowired
    private ActiveSkillGroupJoinRepository activeSkillGroupRepository;

    @Autowired
    private PassiveSkillRepository passiveSkillRepository;

    @Autowired
    private SubAbStateRepository subAbStateRepository;

    @Autowired
    private StatesService statesService;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct()
    {
        Converter<List<ActiveSkill>, List<Long>> convertListSkillToId = new AbstractConverter<List<ActiveSkill>, List<Long>>() {
            public List<Long> convert(List<ActiveSkill> src) {
                return src.stream().map(s -> s.getId()).collect(Collectors.toList());
            }
        };

        modelMapper.emptyTypeMap(ActiveSkillGroup.class, SkillGroupDTO.class).addMappings(mapper -> {
            mapper.map(ActiveSkillGroup::getId, SkillGroupDTO::setId);
            mapper.using(convertListSkillToId).map(ActiveSkillGroup::getUniqueSkills, SkillGroupDTO::setSkills);
        });

        Converter<PassiveSkill, PassiveSkillDTO> passiveSkillToPassiveSkillDTO = new AbstractConverter<PassiveSkill, PassiveSkillDTO>() {
            public PassiveSkillDTO convert(PassiveSkill src) {

                PassiveSkillDTO dto = new PassiveSkillDTO();

                dto.setId(src.getId());
                dto.setInxname(src.getInxname());
                dto.setName(src.getName());
                dto.setView(modelMapper.map(src.getView(), PassiveSkillViewDTO.class));

                if(src.getAbState() != null)
                {
                    PassiveSkillAbState passive_ab_state = src.getAbState();

                    PassiveSkillAbStateDTO passive_ab_state_dto = modelMapper.map(passive_ab_state, PassiveSkillAbStateDTO.class);

                    passive_ab_state_dto.setAbState(statesService.convertAbState(passive_ab_state.getAbState(), passive_ab_state.getStrength()));
                    dto.setAbState(passive_ab_state_dto);
                }

                dto.setMasterySwordDmgRate(src.getMasterySwordDmgRate());
                dto.setMasteryHammerDmgRate(src.getMasteryHammerDmgRate());
                dto.setMasteryTwoHandDmgRate(src.getMasteryTwoHandDmgRate());
                dto.setMasteryAxeDmgRate(src.getMasteryAxeDmgRate());
                dto.setMasteryMaceDmgRate(src.getMasteryMaceDmgRate());
                dto.setMasteryBowDmgRate(src.getMasteryBowDmgRate());
                dto.setMasteryCrossbowDmgRate(src.getMasteryCrossbowDmgRate());
                dto.setMasteryWandDmgRate(src.getMasteryWandDmgRate());
                dto.setMasteryStaffDmgRate(src.getMasteryStaffDmgRate());
                dto.setMasteryClawDmgRate(src.getMasteryClawDmgRate());
                dto.setMasteryDSwordDmgRate(src.getMasteryDSwordDmgRate());

                dto.setMasterySwordDmg(src.getMasterySwordDmg());
                dto.setMasteryHammerDmg(src.getMasteryHammerDmg());
                dto.setMasteryTwoHandDmg(src.getMasteryTwoHandDmg());
                dto.setMasteryAxeDmg(src.getMasteryAxeDmg());
                dto.setMasteryMaceDmg(src.getMasteryMaceDmg());
                dto.setMasteryBowDmg(src.getMasteryBowDmg());
                dto.setMasteryCrossbowDmg(src.getMasteryCrossbowDmg());
                dto.setMasteryWandDmg(src.getMasteryWandDmg());
                dto.setMasteryStaffDmg(src.getMasteryStaffDmg());
                dto.setMasteryClawDmg(src.getMasteryClawDmg());
                dto.setMasteryDSwordDmg(src.getMasteryDSwordDmg());

                dto.setEvasion(src.getEvasion());
                dto.setSPIncrease(src.getSPIncrease());
                dto.setLPIncrease(src.getLPIncrease());
                dto.setPhyAtkUpRate(src.getPhyAtkUpRate());
                dto.setMagAtkUpRate(src.getMagAtkUpRate());
                dto.setDmgUpPerHPDown(src.getDmgUpPerHPDown());
                dto.setDmgUpPerHPDownRatio(src.getDmgUpPerHPDownRatio());
                dto.setDefUpPerHPDown(src.getDefUpPerHPDown());
                dto.setDefUpPerHPDownRatio(src.getDefUpPerHPDownRatio());
                dto.setMovementIncreaseEvasionUp(src.getMovementIncreaseEvasionUp());
                dto.setHealUpRate(src.getHealUpRate());
                dto.setBuffDurationUpRate(src.getBuffDurationUpRate());
                dto.setCritDmgUpRate(src.getCritDmgUpRate());
                dto.setActiveSkillGroupIndex(src.getActiveSkillGroupIndex());
                dto.setDmgReceiveMinusRate(src.getDmgReceiveMinusRate());
                dto.setGroupName(src.getGroupName());

                return dto;
            }
        };

        Converter<ActiveSkill, ActiveSkillDTO> activeSkillToActiveSkillDTO = new AbstractConverter<ActiveSkill, ActiveSkillDTO>() {
            @Override
            protected ActiveSkillDTO convert(ActiveSkill activeSkill) {

                ActiveSkillDTO dto = new ActiveSkillDTO();

                dto.setId(activeSkill.getId());
                dto.setInxname(activeSkill.getInxname());
                dto.setName(activeSkill.getName());
                dto.setView(modelMapper.map(activeSkill.getView(), ActiveSkillViewDTO.class));

                dto.setStep(activeSkill.getStep());
                dto.setMaxStep(activeSkill.getMaxStep());
                dto.setRequiredWeapon(activeSkill.getRequiredWeapon());
                dto.setSp(activeSkill.getSp());
                dto.setSpRate(activeSkill.getSpRate());
                dto.setHp(activeSkill.getHp());
                dto.setHpRate(activeSkill.getHpRate());
                dto.setLp(activeSkill.getLp());
                dto.setRange(activeSkill.getRange());
                dto.setCastedOn(activeSkill.getCastedOn());
                dto.setAffectedTarget(activeSkill.getAffectedTarget());
                dto.setCastTime(activeSkill.getCastTime());
                dto.setCooldown(activeSkill.getCooldown());
                dto.setCooldown(activeSkill.getCooldown());
                dto.setMinDmg(activeSkill.getMinDmg());
                dto.setMaxDmg(activeSkill.getMaxDmg());
                dto.setMinDmgRate(activeSkill.getMinDmgRate());
                dto.setMaxDmgRate(activeSkill.getMaxDmgRate());
                dto.setMinMdmg(activeSkill.getMinMdmg());
                dto.setMaxMdmg(activeSkill.getMaxMdmg());
                dto.setMinMdmgRate(activeSkill.getMinMdmgRate());
                dto.setMaxMdmgRate(activeSkill.getMaxMdmgRate());
                dto.setUseClass(activeSkill.getUseClass());
                dto.setGroupName(activeSkill.getGroupName());
                dto.setGroups(activeSkill.getGroups().stream().map(g -> g.getGroupIndex()).collect(Collectors.toList()));

                List<AbStateDTO> ab_states = new ArrayList<>();
                List<ActiveSkillEffectDTO> effects = new ArrayList<>();

                ab_states.addAll(statesService.convertAbState(activeSkill.getAbStateA(), activeSkill.getAbStateAStrength()));
                ab_states.addAll(statesService.convertAbState(activeSkill.getAbStateB(), activeSkill.getAbStateBStrength()));
                ab_states.addAll(statesService.convertAbState(activeSkill.getAbStateC(), activeSkill.getAbStateCStrength()));

                // Adding the abStates from multiHitType file
               for(List<AbStateDTO> abStates : activeSkill.getMultiHit().stream().map(ab -> statesService.convertAbState(ab.getAbState(), ab.getAbStr())).collect(Collectors.toList()))
               {
                   ab_states.addAll(abStates);
               }

                BiFunction<Long, Long, Void> AdditionalEffectFunc = (Long index, Long value) -> {
                    if(index > 0)
                    {
                        if(index == 51)
                        {
                            AbState ab_state = statesService.getAbState(value);

                            if(ab_state != null)
                            {
                                ab_states.addAll(statesService.convertAbState(ab_state, 1L));
                            }

                            return null;
                        }

                        effects.add(new ActiveSkillEffectDTO(index, value));
                    }
                    return null;
                };

                AdditionalEffectFunc.apply(activeSkill.getSpecialIndexA(), activeSkill.getSpecialValueA());
                AdditionalEffectFunc.apply(activeSkill.getSpecialIndexB(), activeSkill.getSpecialValueB());
                AdditionalEffectFunc.apply(activeSkill.getSpecialIndexC(), activeSkill.getSpecialValueC());
                AdditionalEffectFunc.apply(activeSkill.getSpecialIndexD(), activeSkill.getSpecialValueD());
                AdditionalEffectFunc.apply(activeSkill.getSpecialIndexE(), activeSkill.getSpecialValueE());

                dto.setEffects(effects);
                dto.setAbStates(ab_states);

                {
                    SkillEmpowermentDTO empowerment = new SkillEmpowermentDTO();

                    if(activeSkill.getEmpowerPowerTotal() > 0)
                    {
                        List<Integer> power = new ArrayList<>();
                        power.add(activeSkill.getEmpowerPower1());
                        power.add(activeSkill.getEmpowerPower2());
                        power.add(activeSkill.getEmpowerPower3());
                        power.add(activeSkill.getEmpowerPower4());
                        power.add(activeSkill.getEmpowerPower5());

                        empowerment.setPower(power);
                    }
                    if(activeSkill.getEmpowerSpTotal() > 0)
                    {
                        List<Integer> sp = new ArrayList<>();
                        sp.add(activeSkill.getEmpowerSp1());
                        sp.add(activeSkill.getEmpowerSp2());
                        sp.add(activeSkill.getEmpowerSp3());
                        sp.add(activeSkill.getEmpowerSp4());
                        sp.add(activeSkill.getEmpowerSp5());

                        empowerment.setSp(sp);
                    }
                    if(activeSkill.getEmpowerDurationTotal() > 0)
                    {
                        List<Integer> duration = new ArrayList<>();
                        duration.add(activeSkill.getEmpowerDuration1());
                        duration.add(activeSkill.getEmpowerDuration2());
                        duration.add(activeSkill.getEmpowerDuration3());
                        duration.add(activeSkill.getEmpowerDuration4());
                        duration.add(activeSkill.getEmpowerDuration5());

                        empowerment.setDuration(duration);
                    }
                    if(activeSkill.getEmpowerCooldownTotal() > 0)
                    {
                        List<Integer> cooldown = new ArrayList<>();
                        cooldown.add(activeSkill.getEmpowerCooldown1());
                        cooldown.add(activeSkill.getEmpowerCooldown2());
                        cooldown.add(activeSkill.getEmpowerCooldown3());
                        cooldown.add(activeSkill.getEmpowerCooldown4());
                        cooldown.add(activeSkill.getEmpowerCooldown5());

                        empowerment.setCooldown(cooldown);
                    }

                    dto.setEmpowerment(empowerment);
                }

                dto.setSkillType(activeSkill.getSkillType());
                dto.setSoulRequired(activeSkill.getSoulRequired());

                return dto;
            }
        };

        modelMapper.addConverter(activeSkillToActiveSkillDTO);

        ///
        modelMapper.addConverter(passiveSkillToPassiveSkillDTO);
    }

    public List<Long> getActiveSkillList(Long theClass, Optional<Integer> level) throws Exception
    {
        List<Long> listClass = ClassEnum.fromFlagToUseClass(theClass);

        if(listClass.isEmpty())
            throw new Exception("No classes selected");

        List<ActiveSkill> base_skills = activeSkillRepository.findBaseSkillsByClass(listClass);

        if(level.isPresent())
        {
            base_skills = base_skills.stream()
                    .map(s -> {
                        ActiveSkill highestLevelSkillForLevel = null;

                        while(s != null && s.getView().getRequiredLevel() <= level.get() &&
                                listClass.contains(s.getUseClass()))
                        {
                            highestLevelSkillForLevel = s;
                            s = s.getNextSkill();
                        }
                        return highestLevelSkillForLevel;
                    })
                    .filter(s -> s != null) // Remove skills that don't match the minimum required level
                    .collect(Collectors.toList());
        }

        return base_skills.stream()
                .map(s -> s.getId())
                .collect(Collectors.toList());
    }

    public List<Long> getPassiveSkillList(Long theClass, Optional<Integer> level) throws Exception
    {
        List<Long> listClass = ClassEnum.fromFlagToUseClass(theClass);

        if(listClass.isEmpty())
            throw new Exception("No classes selected");

        List<PassiveSkill> base_skills = passiveSkillRepository.findBaseSkillsByClass(listClass);

        if(level.isPresent())
        {
            base_skills = base_skills.stream()
                    .map(s -> {
                        PassiveSkill highestLevelSkillForLevel = null;

                        while(s != null && s.getView().getRequiredLevel() <= level.get() &&
                                listClass.contains(s.getView().getRequiredClass()))
                        {
                            highestLevelSkillForLevel = s;
                            s = s.getNextSkill();
                        }
                        return highestLevelSkillForLevel;
                    })
                    .filter(s -> s != null) // Remove skills that don't match the minimum required level
                    .collect(Collectors.toList());
        }

        return base_skills.stream()
                .map(s -> s.getId())
                .collect(Collectors.toList());
    }

    public ActiveSkillDTO getActiveSkill(Long id) throws Exception
    {
        ActiveSkill skill = activeSkillRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Couldn't find the skill"));

        return modelMapper.map(skill, ActiveSkillDTO.class);
    }

    public PassiveSkillDTO getPassiveSkill(Long id) throws Exception
    {
        PassiveSkill skill = passiveSkillRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Couldn't find the skill"));

        return modelMapper.map(skill, PassiveSkillDTO.class);
    }

    public List<Long> getActiveSkillGroups(Long theClass, String inxname, Optional<Integer> level)
    {
        List<Long> listClass = ClassEnum.fromFlagToUseClass(theClass);

        List<ActiveSkill> skills = activeSkillRepository
                .findByInxname(inxname);

        if(level.isPresent())
        {
            skills = skills.stream()
                    .filter(s -> s.getView().getRequiredLevel() <= level.get() &&
                            listClass.contains(s.getUseClass()))
                    .collect(Collectors.toList());
        }

        return skills.stream()
                .map(s -> s.getId())
                .collect(Collectors.toList());
    }

    public List<Long> getPassiveSkillGroups(Long theClass, String inxname, Optional<Integer> level)
    {
        List<Long> listClass = ClassEnum.fromFlagToUseClass(theClass);

        List<PassiveSkill> skills = passiveSkillRepository
                .findByInxname(inxname);

        if(level.isPresent())
        {
            skills = skills.stream()
                    .filter(s -> s.getView().getRequiredLevel() <= level.get() &&
                            listClass.contains(s.getView().getRequiredClass()))
                    .collect(Collectors.toList());
        }

        return skills.stream()
                .map(s -> s.getId())
                .collect(Collectors.toList());
    }
}
