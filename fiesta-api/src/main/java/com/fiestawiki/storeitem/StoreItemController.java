package com.fiestawiki.storeitem;

import com.fiestawiki.storeitem.storecategory.StoreCategoryDTO;
import com.fiestawiki.storeitem.storeitem.StoreItemDTO;
import com.fiestawiki.storeitem.storeitemset.StoreItemSetDTO;
import com.fiestawiki.storeitem.storeitemview.StoreItemViewDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class StoreItemController
{
    @Autowired
    StoreItemService storeItemService;

    @GetMapping("store-item-views/{page}")
    public ResponseEntity<Page<StoreItemViewDTO>> getStoreItemViews(
            @PathVariable(value = "page") int page_index,
            @RequestParam(name = "search", required = false) Optional<String> search,
            @RequestParam(name = "stats", required = false) Optional<List<Long>> filterStats,
            @RequestParam(name = "slots", required = false) Optional<List<Long>> filterSlots,
            @RequestParam(name = "duration", required = false) Optional<List<Long>> filterDuration,
            @RequestParam(name = "designer", required = false) Optional<List<Long>> filterDesigner
    ) throws Exception
    {
        Page<StoreItemViewDTO> items = storeItemService.getStoreItemViewPage(
                page_index,
                search,
                filterStats,
                filterSlots,
                filterDuration,
                filterDesigner);

        return ResponseEntity.ok(items);
    }

    @GetMapping("store-item-view/{id}")
    public ResponseEntity<StoreItemViewDTO> getStoreItemView(@PathVariable(value = "id") Long id) throws Exception
    {
        StoreItemViewDTO item = storeItemService.getStoreItemView(id);

        return ResponseEntity.ok(item);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("store-item-view")
    public ResponseEntity<Long> createStoreItemList(@RequestBody StoreItemViewDTO itemList) throws Exception
    {
        Long id = storeItemService.addStoreItemView(itemList);

        return ResponseEntity.ok(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("store-item-view")
    public ResponseEntity<StoreItemViewDTO> updateStoreitemList(@RequestBody StoreItemViewDTO itemList) throws Exception
    {
        StoreItemViewDTO updated = storeItemService.updateStoreItemView(itemList);

        return ResponseEntity.ok(updated);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("store-item-view/{id}")
    public ResponseEntity<Boolean> deleteStoreItemList(@PathVariable(value = "id") Long id) throws Exception
    {
        Boolean deleted = storeItemService.deleteStoreItemView(id);

        return ResponseEntity.ok(deleted);
    }

    @GetMapping("store-item/{id}")
    public ResponseEntity<StoreItemDTO> getStoreItem(@PathVariable(value = "id") Long id) throws Exception
    {
        StoreItemDTO item = storeItemService.getStoreItem(id);

        return ResponseEntity.ok(item);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("store-item/{view}")
    public ResponseEntity<Long> createStoreItem(@RequestBody StoreItemDTO item, @PathVariable(value = "view") Long view) throws Exception
    {
        Long id = storeItemService.addStoreItem(item, view);

        return ResponseEntity.ok(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("store-item")
    public ResponseEntity<StoreItemDTO> updateStoreitemList(@RequestBody StoreItemDTO item) throws Exception
    {
        StoreItemDTO updated = storeItemService.updateStoreItem(item);

        return ResponseEntity.ok(updated);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("store-item/{id}")
    public ResponseEntity<Boolean> deleteStoreItem(@PathVariable(value = "id") Long id) throws Exception
    {
        Boolean deleted = storeItemService.deleteStoreItem(id);

        return ResponseEntity.ok(deleted);
    }

    @GetMapping("store-item-set/{id}")
    public ResponseEntity<StoreItemSetDTO> getStoreItemSet(@PathVariable(value = "id") Long id) throws Exception
    {
        StoreItemSetDTO item = storeItemService.getStoreItemSet(id);

        return ResponseEntity.ok(item);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("store-item-set")
    public ResponseEntity<Long> createStoreItemSet(@RequestBody StoreItemSetDTO itemSet) throws Exception
    {
        Long id = storeItemService.addStoreItemSet(itemSet);

        return ResponseEntity.ok(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("store-item-set/{set}")
    public ResponseEntity<Long> addStoreItemToSet(@RequestBody StoreItemDTO item, @PathVariable(value = "set") Long set) throws Exception
    {
        Long id = storeItemService.addStoreItemToSet(item, set);

        return ResponseEntity.ok(id);
    }

    @GetMapping("store-categories")
    public ResponseEntity<List<StoreCategoryDTO>> getStoreCategories() throws Exception
    {
        List<StoreCategoryDTO> categories = storeItemService.getStoreCategories();

        return ResponseEntity.ok(categories);
    }

    @GetMapping("store-category/{id}")
    public ResponseEntity<StoreCategoryDTO> getStoreCategory(@PathVariable(value = "id") Long id) throws Exception
    {
        StoreCategoryDTO category = storeItemService.getStoreCategory(id);

        return ResponseEntity.ok(category);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("store-category")
    public ResponseEntity<Long> createStoreItem(@RequestBody StoreCategoryDTO category) throws Exception
    {
        Long id = storeItemService.addStoreCategory(category);

        return ResponseEntity.ok(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("store-category")
    public ResponseEntity<StoreCategoryDTO> updateStoreitemList(@RequestBody StoreCategoryDTO category) throws Exception
    {
        StoreCategoryDTO updated = storeItemService.updateStoreCategory(category);

        return ResponseEntity.ok(updated);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("store-category/{id}")
    public ResponseEntity<Boolean> deleteStoreCategory(@PathVariable(value = "id") Long id) throws Exception
    {
        Boolean deleted = storeItemService.deleteStoreCategory(id);

        return ResponseEntity.ok(deleted);
    }
}
