package com.fiestawiki.storeitem.storecategory;

import com.fiestawiki.storeitem.storeitem.StoreItem;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(
        name = "store_category",
        schema = "store_item"
)
@EntityListeners(AuditingEntityListener.class)
public class StoreCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private int type;

    @ManyToMany(mappedBy = "categories")
    Set<StoreItem> storeItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() { return type; }

    public void setType(int type) { this.type = type; }

    public Set<StoreItem> getStoreItems() { return storeItems; }

    public void setStoreItems(Set<StoreItem> storeItems) { this.storeItems = storeItems; }
}
