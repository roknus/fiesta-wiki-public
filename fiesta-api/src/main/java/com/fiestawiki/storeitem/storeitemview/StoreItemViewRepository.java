package com.fiestawiki.storeitem.storeitemview;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreItemViewRepository extends JpaRepository<StoreItemView, Long>, StoreItemViewRepositoryCustom {
}
