package com.fiestawiki.storeitem.storeitemview;

import com.fiestawiki.storeitem.storecategory.StoreCategory;
import com.fiestawiki.storeitem.storecategory.StoreCategory_;
import com.fiestawiki.storeitem.storeitem.StoreItem;
import com.fiestawiki.storeitem.storeitem.StoreItem_;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.data.web.PageableDefault;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StoreItemViewRepositoryImpl implements StoreItemViewRepositoryCustom
{
    @PersistenceContext
    EntityManager entityManager;

    CriteriaBuilder criteriaBuilder;

    @PostConstruct
    private void init() {
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<StoreItemView> filteredItemView(
            Optional<String> search,
            Optional<List<Long>> filteredStats,
            Optional<List<Long>> filteredSlots,
            Optional<List<Long>> filteredDuration,
            Optional<List<Long>> filteredDesigner,
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable)
    {
        CriteriaQuery<StoreItemView> query = criteriaBuilder.createQuery(StoreItemView.class);
        {
            Root<StoreItemView> item = query.from(StoreItemView.class);
            query.select(item).distinct(true);

            List<Predicate> criteria = getCriteriaForStoreItemView(
                    item,
                    search,
                    filteredStats,
                    filteredSlots,
                    filteredDuration,
                    filteredDesigner);

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
            query.orderBy(QueryUtils.toOrders(pageable.getSort(), item, criteriaBuilder));
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<StoreItemView> item = countQuery.from(StoreItemView.class);
            countQuery.select(criteriaBuilder.countDistinct(item));

            List<Predicate> criteria = getCriteriaForStoreItemView(
                    item,
                    search,
                    filteredStats,
                    filteredSlots,
                    filteredDuration,
                    filteredDesigner);

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageItemInfo(query, countQuery, pageable);
    }

    private List<Predicate> getCriteriaForStoreItemView(
            Root<StoreItemView> item,
            Optional<String> search,
            Optional<List<Long>> stats,
            Optional<List<Long>> slots,
            Optional<List<Long>> duration,
            Optional<List<Long>> designer
    ) {

        List<Predicate> criteria = new ArrayList<>();

        ListJoin<StoreItemView, StoreItem> store_items = item.join(StoreItemView_.storeItems, JoinType.LEFT);

        if(stats.isPresent())
        {
            SetJoin<StoreItem, StoreCategory> store_items_categories = store_items.join(StoreItem_.categories);

            criteria.add(store_items_categories.get(StoreCategory_.id).in(stats.get()));
        }
        if(slots.isPresent())
        {
            SetJoin<StoreItem, StoreCategory> store_items_categories = store_items.join(StoreItem_.categories);

            criteria.add(store_items_categories.get(StoreCategory_.id).in(slots.get()));
        }
        if(duration.isPresent())
        {
            SetJoin<StoreItem, StoreCategory> store_items_categories = store_items.join(StoreItem_.categories);

            criteria.add(store_items_categories.get(StoreCategory_.id).in(duration.get()));
        }
        if(designer.isPresent())
        {
            SetJoin<StoreItem, StoreCategory> store_items_categories = store_items.join(StoreItem_.categories);

            criteria.add(store_items_categories.get(StoreCategory_.id).in(designer.get()));
        }
        if(search.isPresent())
        {
            criteria.add(criteriaBuilder.like(criteriaBuilder.lower(item.get(StoreItemView_.name)), "%" + search.get().toLowerCase() + "%"));
        }

        return criteria;
    }

    private Page<StoreItemView> getPageItemInfo(CriteriaQuery<StoreItemView> itemQuery, CriteriaQuery<Long> countQuery, Pageable pageable)
    {
        TypedQuery<StoreItemView> typedQuery = entityManager.createQuery(itemQuery);
        typedQuery.setFirstResult((int)pageable.getOffset());
        typedQuery.setMaxResults(pageable.getPageSize());

        TypedQuery<Long> count = entityManager.createQuery(countQuery);

        return new PageImpl<>(typedQuery.getResultList(), pageable, count.getSingleResult());
    }
}
