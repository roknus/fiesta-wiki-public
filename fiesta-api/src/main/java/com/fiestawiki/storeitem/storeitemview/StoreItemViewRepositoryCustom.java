package com.fiestawiki.storeitem.storeitemview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import java.util.List;
import java.util.Optional;

public interface StoreItemViewRepositoryCustom {

    Page<StoreItemView> filteredItemView(
            Optional<String> search,
            Optional<List<Long>> filteredStats,
            Optional<List<Long>> filteredSlots,
            Optional<List<Long>> duration,
            Optional<List<Long>> designer,
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable);
}
