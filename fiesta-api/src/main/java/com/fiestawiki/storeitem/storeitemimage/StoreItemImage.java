package com.fiestawiki.storeitem.storeitemimage;

import com.fiestawiki.storeitem.storeitemview.StoreItemView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(
        name = "store_item_image",
        schema = "store_item"
)
@EntityListeners(AuditingEntityListener.class)
public class StoreItemImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "filename")
    private String filename;

    @ManyToOne(optional = false)
    @JoinColumn(name = "store_item_view")
    private StoreItemView storeItemView;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public StoreItemView getStoreItemView() {
        return storeItemView;
    }

    public void setStoreItemView(StoreItemView storeItemView) {
        this.storeItemView = storeItemView;
    }
}
