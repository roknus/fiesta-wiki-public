package com.fiestawiki.storeitem.storeitemimage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreItemImageRepository extends JpaRepository<StoreItemImage, Long> {
}
