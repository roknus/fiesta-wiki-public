package com.fiestawiki.storeitem.storeitem;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dto.ItemDTO;
import com.fiestawiki.storeitem.storecategory.StoreCategoryDTO;
import com.fiestawiki.storeitem.storeitemset.StoreItemSetDTO;

import java.util.ArrayList;
import java.util.List;

public class StoreItemDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("item")
    private ItemDTO item;

    @JsonProperty("sc")
    private Long sc;

    @JsonProperty("duration")
    private short duration;

    @JsonProperty("link")
    private String link;

    @JsonProperty("categories")
    List<StoreCategoryDTO> categories = new ArrayList<>();

    @JsonProperty("set")
    StoreItemSetDTO set;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public ItemDTO getItem() { return item; }

    public void setItem(ItemDTO item) { this.item = item; }

    public Long getSc() {
        return sc;
    }

    public void setSc(Long sc) {
        this.sc = sc;
    }

    public short getDuration() { return duration; }

    public void setDuration(short duration) { this.duration = duration; }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<StoreCategoryDTO> getCategories() { return categories; }

    public void setCategories(List<StoreCategoryDTO> categories) { this.categories = categories; }

    public StoreItemSetDTO getSet() { return set; }

    public void setSet(StoreItemSetDTO set) { this.set = set; }
}
