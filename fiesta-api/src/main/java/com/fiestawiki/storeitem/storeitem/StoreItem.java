package com.fiestawiki.storeitem.storeitem;

import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.storeitem.storecategory.StoreCategory;
import com.fiestawiki.storeitem.storeitemset.StoreItemSet;
import com.fiestawiki.storeitem.storeitemview.StoreItemView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(
        name = "store_item",
        schema = "store_item"
)
@EntityListeners(AuditingEntityListener.class)
public class StoreItem
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "item_info")
    private ItemInfo item;

    @Column(
            nullable = false,
            name = "sc"
    )
    private Long sc = 0L;

    @Column(
            nullable = false,
            name = "duration"
    )
    private short duration = 0;

    @Column(
            columnDefinition = "TEXT",
            name = "link"
    )
    private String link;

    @ManyToOne
    @JoinColumn(name = "store_item_view")
    private StoreItemView storeItemView;

    @ManyToMany
    @JoinTable(
            schema = "store_item",
            name = "store_item_category",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    Set<StoreCategory> categories = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "store_item_set")
    private StoreItemSet storeItemSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ItemInfo getItem() { return item; }

    public void setItem(ItemInfo item) { this.item = item; }

    public Long getSc() {
        return sc;
    }

    public void setSc(Long sc) {
        this.sc = sc;
    }

    public short getDuration() { return duration; }

    public void setDuration(short duration) { this.duration = duration; }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public StoreItemView getStoreItemView() { return storeItemView; }

    public void setStoreItemView(StoreItemView storeItemList) { this.storeItemView = storeItemList; }

    public Set<StoreCategory> getCategories() { return categories; }

    public void setCategories(Set<StoreCategory> categories) { this.categories = categories; }

    public StoreItemSet getStoreItemSet() { return storeItemSet; }

    public void setStoreItemSet(StoreItemSet storeItemSet) { this.storeItemSet = storeItemSet; }
}
