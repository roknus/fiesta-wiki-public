package com.fiestawiki.mobs.mobinfoserver;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MobInfoServerRepository extends JpaRepository<MobInfoServer, Long> {
}
