package com.fiestawiki.mobs.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class MobSpeciesDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("mob_portrait")
    private String mobPortrait;

    @JsonProperty("mobs")
    private List<Long> mobs = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobPortrait() {
        return mobPortrait;
    }

    public void setMobPortrait(String mobPortrait) {
        this.mobPortrait = mobPortrait;
    }

    public List<Long> getMobs() {
        return mobs;
    }

    public void setMobs(List<Long> mobs) {
        this.mobs = mobs;
    }
}
