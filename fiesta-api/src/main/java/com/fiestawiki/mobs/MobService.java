package com.fiestawiki.mobs;

import com.fiestawiki.FiestaUtils;
import com.fiestawiki.mobs.dto.MobSpeciesDTO;
import com.fiestawiki.mobs.mobdroptable.MobDropGroupInfo;
import com.fiestawiki.mobs.mobdroptable.MobDropGroupInfoDTO;
import com.fiestawiki.mobs.mobdroptable.MobDropTable;
import com.fiestawiki.mobs.mobinfo.MobInfo;
import com.fiestawiki.mobs.mobinfo.MobInfoDTO;
import com.fiestawiki.mobs.mobinfo.repository.MobInfoRepository;
import com.fiestawiki.mobs.mobspecies.MobSpecies;
import com.fiestawiki.mobs.mobspecies.repository.MobSpeciesRepository;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MobService {

    @Autowired
    private FiestaUtils fiestaUtils;

    @Autowired
    private MobInfoRepository mobInfoRepository;

    @Autowired
    private MobSpeciesRepository mobSpeciesRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct()
    {
        ///
        Converter<MobInfo, List<Long>> getSpecies = new AbstractConverter<MobInfo, List<Long>>()
        {
            public List<Long> convert(MobInfo src)
            {
                return src.getSpecies().getMobs()
                        .stream()
                        .filter(m -> m.getId() != src.getId() && m.getLocation() != 0)
                        .map(m -> m.getId())
                        .collect(Collectors.toList());
            }
        };

        ///
        modelMapper.emptyTypeMap(MobInfo.class, MobInfoDTO.class).addMappings( mapper -> {
            mapper.map(src -> src.getId(), MobInfoDTO::setId);
            mapper.map(src -> src.getName(), MobInfoDTO::setName);
            mapper.map(src -> src.getLevel(), MobInfoDTO::setLevel);
            mapper.map(src -> src.getMaxHP(), MobInfoDTO::setMaxHP);
            mapper.map(src -> src.getWalkSpeed(), MobInfoDTO::setWalkSpeed);
            mapper.map(src -> src.getRunSpeed(), MobInfoDTO::setRunSpeed);
            mapper.map(src -> src.getIsNPC(), MobInfoDTO::setNPC);
            mapper.map(src -> src.getWeaponType(), MobInfoDTO::setWeaponType);
            mapper.map(src -> src.getArmorType(), MobInfoDTO::setArmorType);
            mapper.map(src -> src.getGradeType(), MobInfoDTO::setGradeType);
            mapper.map(src -> src.getType(), MobInfoDTO::setType);
            mapper.map(src -> src.getLocation(), MobInfoDTO::setLocation);
            mapper.map(src -> src.getActive(), MobInfoDTO::setActive);
            mapper.map(src -> src.getView(), MobInfoDTO::setView);
            mapper.map(src -> src.getCoordinates(), MobInfoDTO::setCoordinates);
            mapper.map(src -> src.getRelatedQuests(), MobInfoDTO::setRelatedQuests);
            mapper.map(src -> src.getSpecies().getId(), MobInfoDTO::setSpecies);

            mapper.map(src -> src.getServerInfo().getDefense(), MobInfoDTO::setDefense);
            mapper.map(src -> src.getServerInfo().getEvasion(), MobInfoDTO::setEvasion);
            mapper.map(src -> src.getServerInfo().getMagicalDefense(), MobInfoDTO::setMagicalDefense);
            mapper.map(src -> src.getServerInfo().getAggroType(), MobInfoDTO::setAggroType);
            mapper.map(src -> src.getServerInfo().getAggroRadius(), MobInfoDTO::setAggroRadius);
            mapper.map(src -> src.getServerInfo().getAggroRange(), MobInfoDTO::setAggroRange);
            mapper.map(src -> src.getServerInfo().getExp(), MobInfoDTO::setExp);
            mapper.map(src -> src.getServerInfo().getStrength(), MobInfoDTO::setStrength);
            mapper.map(src -> src.getServerInfo().getDexterity(), MobInfoDTO::setDexterity);
            mapper.map(src -> src.getServerInfo().getEndurance(), MobInfoDTO::setEndurance);
            mapper.map(src -> src.getServerInfo().getIntelligence(), MobInfoDTO::setIntelligence);
            mapper.map(src -> src.getServerInfo().getSpirit(), MobInfoDTO::setSpirit);
            mapper.map(src -> src.getServerInfo().getBloodingResistance(), MobInfoDTO::setBloodingResistance);
            mapper.map(src -> src.getServerInfo().getStunResistance(), MobInfoDTO::setStunResistance);
            mapper.map(src -> src.getServerInfo().getMoveSpeedResistance(), MobInfoDTO::setMoveSpeedResistance);
            mapper.map(src -> src.getServerInfo().getFearResistance(), MobInfoDTO::setFearResistance);
        });


        modelMapper.emptyTypeMap(MobSpecies.class, MobSpeciesDTO.class).addMappings(mapper -> {
            mapper.map(MobSpecies::getId, MobSpeciesDTO::setId);
            mapper.map(MobSpecies::getMobName, MobSpeciesDTO::setName);
            mapper.map(MobSpecies::getMobPortrait, MobSpeciesDTO::setMobPortrait);
            mapper.map(MobSpecies::getMobsIDs, MobSpeciesDTO::setMobs);
        });

        ///
        Converter<MobDropGroupInfo, List<Long>> getItemsDropsID = new AbstractConverter<MobDropGroupInfo, List<Long>>()
        {
            public List<Long> convert(MobDropGroupInfo src)
            {
                return src.getItemDropGroup()
                        .getItems()
                        .stream()
                        .map(i -> i.getItemInfo().getId())
                        .collect(Collectors.toList());
            }
        };

        ///
        modelMapper.emptyTypeMap(MobDropGroupInfo.class, MobDropGroupInfoDTO.class).addMappings(mapper -> {
            mapper.map(src -> src.getDropRate(), MobDropGroupInfoDTO::setDropRate);
            mapper.using(getItemsDropsID).map(
                    src -> src,
                    MobDropGroupInfoDTO::setItems);
        });
    }

    private MobInfoDTO convertMobInfo(MobInfo mobInfo)
    {
        return modelMapper.map(mobInfo, MobInfoDTO.class);
    }

    private MobSpeciesDTO convertMobSpecies(MobSpecies species) { return modelMapper.map(species, MobSpeciesDTO.class); }

    public MobDropGroupInfoDTO convertMobGroupInfo(MobDropGroupInfo mobGroupInfo)
    {
        return modelMapper.map(mobGroupInfo, MobDropGroupInfoDTO.class);
    }

    public MobInfoDTO getMobInfo(Long id) throws Exception {

        MobInfo mobInfo = mobInfoRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Mob not found"));

        return convertMobInfo(mobInfo);
    }

    public MobSpeciesDTO getSpecies(Long id) throws Exception
    {
        MobSpecies species = mobSpeciesRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Species not found"));

        return convertMobSpecies(species);
    }

    public Page<Long> getMobSpeciesPage(
            Pageable pageable,
            Long min,
            Long max,
            Optional<Boolean> npc,
            Optional<Boolean> active,
            Optional<String> search,
            Optional<Long> location)
    {
        Page<MobSpecies> species_page = mobSpeciesRepository
                .findSpecies(
                        pageable,
                        min,
                        max,
                        npc,
                        active,
                        search,
                        location.orElse(null));

        return species_page.map(s -> s.getId());
    }

    public List<MobDropGroupInfoDTO> getMobDrops(Long id) throws Exception
    {
        MobInfo mob = mobInfoRepository.findById(id).orElseThrow(() -> new Exception("Mob not found"));

        MobDropTable drop_table = mob.getDropTable();
        if(drop_table != null)
        {
            return drop_table.getDropGroupsInfo()
                            .stream()
                            .filter(x -> !x.getItemDropGroup().getItems().isEmpty())
                            .map(this::convertMobGroupInfo)
                            .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    public MobInfoDTO updateMobInfo(MobInfoDTO mobDTO)
    {
        Optional<MobInfo> fetchMob = mobInfoRepository.findById(mobDTO.getId());
        if(fetchMob.isPresent())
        {
            MobInfo mob = fetchMob.get();

            mob.setLocation(mobDTO.getLocation());
            mob.setActive(mobDTO.getActive());

            Long id = mobInfoRepository.save(mob).getId();

            Optional<MobInfo> newMob = mobInfoRepository.findById(id);

            if(newMob.isPresent()) {
                return modelMapper.map(newMob.get(), MobInfoDTO.class);
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
}
