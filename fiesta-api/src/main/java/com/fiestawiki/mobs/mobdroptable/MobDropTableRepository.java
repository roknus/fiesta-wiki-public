package com.fiestawiki.mobs.mobdroptable;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MobDropTableRepository extends JpaRepository<MobDropTable, Long> {
}
