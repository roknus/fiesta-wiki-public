package com.fiestawiki.mobs.mobdroptable;

import com.fiestawiki.items.dao.itemdropgroup.ItemDropGroup;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "item_drop_group_join")
@EntityListeners(AuditingEntityListener.class)
public class MobDropGroupInfo
{
    @Id
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pk1")
    private MobDropTable mobDropTable;

    @ManyToOne
    @JoinColumn(name = "pk2")
    private ItemDropGroup itemDropGroup;

    @Column(name = "droprate")
    private Long dropRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MobDropTable getMobDropTable() {
        return mobDropTable;
    }

    public void setMobDropTable(MobDropTable mobDropTable) {
        this.mobDropTable = mobDropTable;
    }

    public ItemDropGroup getItemDropGroup() {
        return itemDropGroup;
    }

    public void setItemDropGroup(ItemDropGroup itemDropGroup) {
        this.itemDropGroup = itemDropGroup;
    }

    public Long getDropRate() {
        return dropRate;
    }

    public void setDropRate(Long dropRate) {
        this.dropRate = dropRate;
    }
}
