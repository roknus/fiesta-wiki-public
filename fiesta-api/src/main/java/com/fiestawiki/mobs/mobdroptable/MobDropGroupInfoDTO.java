package com.fiestawiki.mobs.mobdroptable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class MobDropGroupInfoDTO {

    @JsonProperty("drop_rate")
    private Long dropRate;

    @JsonProperty("item_drops")
    private List<Long> items = new ArrayList<>();

    public Long getDropRate() {
        return dropRate;
    }

    public void setDropRate(Long dropRate) {
        this.dropRate = dropRate;
    }

    public List<Long> getItems() {
        return items;
    }

    public void setItems(List<Long> items) {
        this.items = items;
    }
}
