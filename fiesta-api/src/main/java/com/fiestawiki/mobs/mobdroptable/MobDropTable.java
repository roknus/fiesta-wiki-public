package com.fiestawiki.mobs.mobdroptable;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "item_drop_table")
@EntityListeners(AuditingEntityListener.class)
public class MobDropTable
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "mobid")
    private String mobId;

    @ManyToOne
    @JoinColumn(name = "mob_fk")
    private MobInfo mobInfo;

    @OneToMany(mappedBy = "mobDropTable")
    @OrderBy("dropRate DESC")
    private List<MobDropGroupInfo> dropGroupsInfo = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobId() {
        return mobId;
    }

    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    public MobInfo getMobInfo() {
        return mobInfo;
    }

    public void setMobInfo(MobInfo mobInfo) {
        this.mobInfo = mobInfo;
    }

    public List<MobDropGroupInfo> getDropGroupsInfo() {
        return dropGroupsInfo;
    }

    public void setDropGroupsInfo(List<MobDropGroupInfo> dropGroupsInfo) {
        this.dropGroupsInfo = dropGroupsInfo;
    }
}
