package com.fiestawiki.mobs.mobcoordinate;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MobCoordinateDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("map_id")
    private Long mapId;

    @JsonProperty("center_x")
    private Long centerX;

    @JsonProperty("center_y")
    private Long centerY;

    @JsonProperty("width")
    private Long width;

    @JsonProperty("height")
    private Long height;

    @JsonProperty("range_degree")
    private Long rangeDegree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMapId() { return mapId; }

    public void setMapId(Long mapId) { this.mapId = mapId; }

    public Long getCenterX() {
        return centerX;
    }

    public void setCenterX(Long centerX) {
        this.centerX = centerX;
    }

    public Long getCenterY() {
        return centerY;
    }

    public void setCenterY(Long centerY) {
        this.centerY = centerY;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getRangeDegree() {
        return rangeDegree;
    }

    public void setRangeDegree(Long rangeDegree) {
        this.rangeDegree = rangeDegree;
    }
}
