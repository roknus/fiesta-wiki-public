package com.fiestawiki.mobs.mobcoordinate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MobCoordinateRepository extends JpaRepository<MobCoordinate, Long> {
}
