package com.fiestawiki.mobs.mobcoordinate;

import com.fiestawiki.map.mapinfo.MapInfo;
import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mob_coordinate")
@EntityListeners(AuditingEntityListener.class)
public class MobCoordinate implements Serializable
{
    @Id
    @Column(name = "mc_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "mob_id")
    private MobInfo mob;

    @OneToOne
    @JoinColumn(name = "map_id")
    private MapInfo map;

    @Column(name = "centerx")
    private Long centerX;

    @Column(name = "centery")
    private Long centerY;

    @Column(name = "width")
    private Long width;

    @Column(name = "height")
    private Long height;

    @Column(name = "rangedegree")
    private Long rangeDegree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MobInfo getMob() { return mob; }

    public void setMob(MobInfo mob) { this.mob = mob; }

    public MapInfo getMap() {
        return map;
    }

    public void setMap(MapInfo map) {
        this.map = map;
    }

    public Long getCenterX() {
        return centerX;
    }

    public void setCenterX(Long centerX) {
        this.centerX = centerX;
    }

    public Long getCenterY() {
        return centerY;
    }

    public void setCenterY(Long centerY) {
        this.centerY = centerY;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getRangeDegree() {
        return rangeDegree;
    }

    public void setRangeDegree(Long rangeDegree) {
        this.rangeDegree = rangeDegree;
    }
}