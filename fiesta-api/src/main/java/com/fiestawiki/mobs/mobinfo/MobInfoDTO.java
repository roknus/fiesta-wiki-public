package com.fiestawiki.mobs.mobinfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.mobs.mobcoordinate.MobCoordinateDTO;
import com.fiestawiki.mobs.mobviewinfo.MobViewInfoDTO;

import java.util.ArrayList;
import java.util.List;

public class MobInfoDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("level")
    private Long level;

    @JsonProperty("max_hp")
    private Long maxHP;

    @JsonProperty("walk_speed")
    private Long walkSpeed;

    @JsonProperty("run_speed")
    private Long runSpeed;

    @JsonProperty("npc")
    private Boolean isNPC;

    @JsonProperty("weapon_type")
    private Long weaponType;

    @JsonProperty("armor_type")
    private Long armorType;

    @JsonProperty("grade_type")
    private Long gradeType;

    @JsonProperty("type")
    private Long type;

    @JsonProperty("location")
    private Long location;

    @JsonProperty("active")
    private Boolean active;

    @JsonProperty("view")
    private MobViewInfoDTO view;

    @JsonProperty("coordinates")
    private List<MobCoordinateDTO> coordinates = new ArrayList<>();

    @JsonProperty("related_quests")
    private List<Long> relatedQuests = new ArrayList<>();

    @JsonProperty("species")
    private Long species;

    ////////////////////////////////////////////////
    /// MobInfoServer
    ////////////////////////////////////////////////
    @JsonProperty("defense")
    private Integer defense;

    @JsonProperty("evasion")
    private Integer evasion;

    @JsonProperty("magical_defense")
    private Integer magicalDefense;

    @JsonProperty("aggro_type")
    private Long aggroType;

    @JsonProperty("aggro_radius")
    private Integer aggroRadius;

    @JsonProperty("aggro_range")
    private Integer aggroRange;

    @JsonProperty("exp")
    private Long exp;

    @JsonProperty("str")
    private Integer strength;

    @JsonProperty("dex")
    private Integer dexterity;

    @JsonProperty("end")
    private Integer endurance;

    @JsonProperty("int")
    private Integer intelligence;

    @JsonProperty("spr")
    private Integer spirit;

    @JsonProperty("blooding_resistance")
    private Integer bloodingResistance;

    @JsonProperty("stun_resistance")
    private Integer stunResistance;

    @JsonProperty("move_speed_resistance")
    private Integer moveSpeedResistance;

    @JsonProperty("fear_resistance")
    private Integer fearResistance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() { return level; }

    public void setLevel(Long level) { this.level = level; }

    public Long getMaxHP() { return maxHP; }

    public void setMaxHP(Long maxHP) { this.maxHP = maxHP; }

    public Long getWalkSpeed() { return walkSpeed; }

    public void setWalkSpeed(Long walkSpeed) { this.walkSpeed = walkSpeed; }

    public Long getRunSpeed() { return runSpeed; }

    public void setRunSpeed(Long runSpeed) { this.runSpeed = runSpeed; }

    public Boolean getNPC() { return isNPC; }

    public void setNPC(Boolean NPC) { isNPC = NPC; }

    public Long getWeaponType() { return weaponType; }

    public void setWeaponType(Long weaponType) { this.weaponType = weaponType; }

    public Long getArmorType() { return armorType; }

    public void setArmorType(Long armorType) { this.armorType = armorType; }

    public Long getGradeType() { return gradeType; }

    public void setGradeType(Long gradeType) { this.gradeType = gradeType; }

    public Long getType() { return type; }

    public void setType(Long type) { this.type = type; }

    public Long getLocation() { return location; }

    public void setLocation(Long location) { this.location = location; }

    public Boolean getActive() { return active; }

    public void setActive(Boolean active) { this.active = active; }

    public MobViewInfoDTO getView() { return view; }

    public void setView(MobViewInfoDTO view) { this.view = view; }

    public List<MobCoordinateDTO> getCoordinates() { return coordinates; }

    public void setCoordinates(List<MobCoordinateDTO> coordinates) { this.coordinates = coordinates; }

    public List<Long> getRelatedQuests() { return relatedQuests; }

    public void setRelatedQuests(List<Long> relatedQuests) { this.relatedQuests = relatedQuests; }

    public Long getSpecies() { return species; }

    public void setSpecies(Long species) { this.species = species; }

    public Integer getDefense() { return defense; }

    public void setDefense(Integer defense) { this.defense = defense; }

    public Integer getEvasion() { return evasion; }

    public void setEvasion(Integer evasion) { this.evasion = evasion; }

    public Integer getMagicalDefense() { return magicalDefense; }

    public void setMagicalDefense(Integer magicalDefense) { this.magicalDefense = magicalDefense; }

    public Long getAggroType() { return aggroType; }

    public void setAggroType(Long aggroType) { this.aggroType = aggroType; }

    public Integer getAggroRadius() { return aggroRadius; }

    public void setAggroRadius(Integer aggroRadius) { this.aggroRadius = aggroRadius; }

    public Integer getAggroRange() { return aggroRange; }

    public void setAggroRange(Integer aggroRange) { this.aggroRange = aggroRange; }

    public Long getExp() { return exp; }

    public void setExp(Long exp) { this.exp = exp; }

    public Integer getStrength() { return strength; }

    public void setStrength(Integer strength) { this.strength = strength; }

    public Integer getDexterity() { return dexterity; }

    public void setDexterity(Integer dexterity) { this.dexterity = dexterity; }

    public Integer getEndurance() { return endurance; }

    public void setEndurance(Integer endurance) { this.endurance = endurance; }

    public Integer getIntelligence() { return intelligence; }

    public void setIntelligence(Integer intelligence) { this.intelligence = intelligence; }

    public Integer getSpirit() { return spirit; }

    public void setSpirit(Integer spirit) { this.spirit = spirit; }

    public Integer getBloodingResistance() { return bloodingResistance; }

    public void setBloodingResistance(Integer bloodingResistance) { this.bloodingResistance = bloodingResistance; }

    public Integer getStunResistance() { return stunResistance; }

    public void setStunResistance(Integer stunResistance) { this.stunResistance = stunResistance; }

    public Integer getMoveSpeedResistance() { return moveSpeedResistance; }

    public void setMoveSpeedResistance(Integer moveSpeedResistance) { this.moveSpeedResistance = moveSpeedResistance; }

    public Integer getFearResistance() { return fearResistance; }

    public void setFearResistance(Integer fearResistance) { this.fearResistance = fearResistance; }
}
