package com.fiestawiki.mobs.mobinfo.repository;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import java.util.Optional;

public interface MobInfoRepositoryCustom {

    Page<MobInfo> findMobs(
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable,
            Long min,
            Long max,
            Optional<String> search,
            Optional<Long> location,
            Optional<Long> original
    );
}
