package com.fiestawiki.mobs.mobspecies.repository;

import com.fiestawiki.mobs.mobspecies.MobSpecies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MobSpeciesRepository extends JpaRepository<MobSpecies, Long>, MobSpeciesRepositoryCustom {
}
