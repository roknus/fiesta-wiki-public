package com.fiestawiki.mobs.mobspecies.repository;

import com.fiestawiki.mobs.mobspecies.MobSpecies;
import org.hibernate.SessionFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public class MobSpeciesRepositoryImpl implements MobSpeciesRepositoryCustom
{
    @PersistenceContext
    EntityManager entityManager;

    CriteriaBuilder criteriaBuilder;

    SessionFactory sessionFactory;

    @PostConstruct
    private void init()
    {
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<MobSpecies> findSpecies(
            Pageable pageable,
            Long min,
            Long max,
            Optional<Boolean> npc,
            Optional<Boolean> active,
            Optional<String> search,
            Long location)
    {
        List<String> subQueryWhereClauses = new ArrayList<>();

        subQueryWhereClauses.add("level >= :min");
        subQueryWhereClauses.add("level <= :max");

        if(search.isPresent()) {
            subQueryWhereClauses.add("LOWER(mi.name) LIKE :search");
        }
        if(active.isPresent()) {
            subQueryWhereClauses.add("active = :active");
        }
        if(npc.isPresent()) {
            subQueryWhereClauses.add("isnpc = :npc");
        }

        String orderByClause = "";
        if(!pageable.getSort().isEmpty()) {
            orderByClause = "ORDER BY ";
            List<String> orders = new ArrayList<>();

            for(Sort.Order o : pageable.getSort().toList())
            {
                switch(o.getProperty())
                {
                    case "name":
                        orders.add("name " + o.getDirection().toString());
                    case "level":
                        orders.add("level " + o.getDirection().toString());
                }
            }

            orderByClause += String.join(", ", orders);
        }

        // Select distinct on allow to avoid GROUP BY and SELECT other columns that are not part of the GROUP BY
        // ORDER BY ms.name is mandatory first for DISTINCT ON and then order by level to always get lowest level mob
        String subQuery = "(SELECT DISTINCT ON(ms.species_name) ms.id, ms.species_name, mi.name, mi.level " +
                "FROM mob_species AS ms " +
                "INNER JOIN " +
                "(SELECT * FROM mob_info) AS mi " +
                "ON ms.id = mi.mob_species_fk " +
                (!subQueryWhereClauses.isEmpty() ? ("WHERE " + String.join(" AND ", subQueryWhereClauses) + " ") : "") + // WHERE clause should be on sub query because the WHERE in main query only apply to the first of this subset
                "ORDER BY ms.species_name, mi.level)";

        Query nativeQuery = entityManager.createNativeQuery("SELECT * " +
                "FROM " + subQuery + " AS r " +
                orderByClause,
                MobSpecies.class)
                .setFirstResult((int) pageable.getOffset())
                .setMaxResults(pageable.getPageSize());

        Query countQuery = entityManager.createNativeQuery("SELECT COUNT(*) " +
                "FROM " + subQuery + " AS r ");

        {
            nativeQuery.setParameter("min", min);
            countQuery.setParameter("min", min);
            nativeQuery.setParameter("max", max);
            countQuery.setParameter("max", max);
        }
        if(search.isPresent())
        {
            String s = search.get().toLowerCase();
            nativeQuery.setParameter("search", "%" + s + "%");
            countQuery.setParameter("search", "%" + s + "%");
        }
        if(active.isPresent())
        {
            nativeQuery.setParameter("active", active.get());
            countQuery.setParameter("active", active.get());
        }
        if(npc.isPresent()) {
            int value = npc.get() ? 1 : 0;
            nativeQuery.setParameter("npc", value);
            countQuery.setParameter("npc", value);
        }

        @SuppressWarnings("unchecked")
        List<MobSpecies> result = nativeQuery.getResultList();

        return new PageImpl<>(
                result,
                pageable,
                ((Number) countQuery.getSingleResult()).longValue());
    }
}
