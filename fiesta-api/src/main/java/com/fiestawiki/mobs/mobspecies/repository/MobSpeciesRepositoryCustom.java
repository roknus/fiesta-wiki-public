package com.fiestawiki.mobs.mobspecies.repository;

import com.fiestawiki.mobs.mobspecies.MobSpecies;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import java.util.Optional;

public interface MobSpeciesRepositoryCustom {

    Page<MobSpecies> findSpecies(
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable,
            Long min,
            Long max,
            Optional<Boolean> npc,
            Optional<Boolean> active,
            Optional<String> search,
            Long location);
}
