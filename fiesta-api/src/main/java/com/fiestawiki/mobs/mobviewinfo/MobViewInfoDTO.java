package com.fiestawiki.mobs.mobviewinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MobViewInfoDTO
{
    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("filename")
    private String filename;

    @JsonProperty("attack_type")
    private Long attackType;

    @JsonProperty("mob_portrait")
    private String mobPortrait;

    @JsonProperty("minimap_icon")
    private Long miniMapIcon;

    @JsonProperty("npc_view_id")
    private Long npcViewIndex;

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getAttackType() {
        return attackType;
    }

    public void setAttackType(Long attackType) {
        this.attackType = attackType;
    }

    public String getMobPortrait() {
        return mobPortrait;
    }

    public void setMobPortrait(String mobPortrait) {
        this.mobPortrait = mobPortrait;
    }

    public Long getMiniMapIcon() {
        return miniMapIcon;
    }

    public void setMiniMapIcon(Long miniMapIcon) {
        this.miniMapIcon = miniMapIcon;
    }

    public Long getNpcViewIndex() {
        return npcViewIndex;
    }

    public void setNpcViewIndex(Long npcViewIndex) {
        this.npcViewIndex = npcViewIndex;
    }
}
