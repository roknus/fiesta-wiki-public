package com.fiestawiki.mobs.mobviewinfo;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mob_view_info")
@EntityListeners(AuditingEntityListener.class)
public class MobViewInfo implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "filename")
    private String filename;

    @Column(name = "texture")
    private String texture;

    @Column(name = "attacktype")
    private Long attackType;

    @Column(name = "mobportrait")
    private String mobPortrait;

    @Column(name = "minimapicon")
    private Long miniMapIcon;

    @Column(name = "npcviewindex")
    private Long npcViewIndex;

    @OneToOne
    @JoinColumn(name = "mob_info_fk")
    private MobInfo mob;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public Long getAttackType() {
        return attackType;
    }

    public void setAttackType(Long attackType) {
        this.attackType = attackType;
    }

    public String getMobPortrait()
    {
        // npcViewIndex != 0 is a generated portrait from model in NPCViewInfo.shn
        if(npcViewIndex != 0 || mobPortrait == null || mobPortrait.isEmpty())
            return "mp_none";

        return mobPortrait.toLowerCase();
    }

    public void setMobPortrait(String mobPortrait) {
        this.mobPortrait = mobPortrait;
    }

    public Long getMiniMapIcon() {
        return miniMapIcon;
    }

    public void setMiniMapIcon(Long miniMapIcon) {
        this.miniMapIcon = miniMapIcon;
    }

    public Long getNpcViewIndex() {
        return npcViewIndex;
    }

    public void setNpcViewIndex(Long npcViewIndex) {
        this.npcViewIndex = npcViewIndex;
    }

    public MobInfo getMob() { return mob; }

    public void setMob(MobInfo mob) { this.mob = mob; }
}
