package com.fiestawiki.mobs.mobviewinfo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MobViewInfoRepository extends JpaRepository<MobViewInfo, Long> {
}
