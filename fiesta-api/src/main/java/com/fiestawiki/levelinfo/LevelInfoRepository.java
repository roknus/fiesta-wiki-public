package com.fiestawiki.levelinfo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LevelInfoRepository extends JpaRepository<LevelInfo, Long> {
}
