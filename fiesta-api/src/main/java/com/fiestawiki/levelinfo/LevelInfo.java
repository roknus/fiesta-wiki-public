package com.fiestawiki.levelinfo;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "level_info")
@EntityListeners(AuditingEntityListener.class)
public class LevelInfo
{
    @Id
    @Column(name = "level", nullable = false)
    private Long level;

    @Column(name = "xp_required")
    private Long xpRequired;

    @Column(name = "xp_total")
    private Long xpTotal;

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getXpRequired() {
        return xpRequired;
    }

    public void setXpRequired(Long xpRequired) {
        this.xpRequired = xpRequired;
    }

    public Long getXpTotal() {
        return xpTotal;
    }

    public void setXpTotal(Long xpTotal) {
        this.xpTotal = xpTotal;
    }
}
