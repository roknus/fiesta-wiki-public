package com.fiestawiki.common.classname;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassNameRepository extends JpaRepository<ClassName, Long> {
}
