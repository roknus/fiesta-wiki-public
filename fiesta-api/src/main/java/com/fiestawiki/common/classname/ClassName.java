package com.fiestawiki.common.classname;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "class_name")
@EntityListeners(AuditingEntityListener.class)
public class ClassName
{
    @Id
    @Column(name = "classid", nullable = false)
    private Long id;

    @Column(name = "acprefix")
    private String shortName;

    @Column(name = "acengname")
    private String gameName;

    @Column(name = "aclocalname")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
