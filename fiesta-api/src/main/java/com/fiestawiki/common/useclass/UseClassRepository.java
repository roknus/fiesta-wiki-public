package com.fiestawiki.common.useclass;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UseClassRepository extends JpaRepository<UseClass, Long> {
}
