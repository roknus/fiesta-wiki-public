package com.fiestawiki.common.useclass;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "use_class")
@EntityListeners(AuditingEntityListener.class)
public class UseClass
{
    @Id
    @Column(name = "useclass", nullable = false)
    private Long id;

    @Column(name = "fig")
    private short fighter;

    @Column(name = "cfig")
    private short cleverFighter;

    @Column(name = "war")
    private short warrior;

    @Column(name = "gla")
    private short gladiator;

    @Column(name = "kni")
    private short knight;

    @Column(name = "cle")
    private short cleric;

    @Column(name = "hcle")
    private short highCleric;

    @Column(name = "pal")
    private short paladin;

    @Column(name = "hol")
    private short holyknight;

    @Column(name = "gua")
    private short guardian;

    @Column(name = "arc")
    private short archer;

    @Column(name = "harc")
    private short hawkArcher;

    @Column(name = "sco")
    private short scout;

    @Column(name = "sha")
    private short sharpshooter;

    @Column(name = "ran")
    private short ranger;

    @Column(name = "mag")
    private short mage;

    @Column(name = "wmag")
    private short wizMage;

    @Column(name = "enc")
    private short enchanter;

    @Column(name = "warl")
    private short warlock;

    @Column(name = "wiz")
    private short wizard;

    @Column(name = "jok")
    private short trickster;

    @Column(name = "chs")
    private short gambit;

    @Column(name = "cru")
    private short renegade;

    @Column(name = "cls")
    private short spectre;

    @Column(name = "ass")
    private short reaper;

    @Column(name = "sen")
    private short crusader;

    @Column(name = "sav")
    private short templar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean canFighter() {
        return fighter == 1;
    }

    public boolean canCleverFighter() {
        return cleverFighter == 1;
    }

    public boolean canWarrior() {
        return warrior == 1;
    }

    public boolean canGladiator() {
        return gladiator == 1;
    }

    public boolean canKnight() {
        return knight == 1;
    }

    public boolean canCleric() {
        return cleric == 1;
    }

    public boolean canHighCleric() {
        return highCleric == 1;
    }

    public boolean canPaladin() {
        return paladin == 1;
    }

    public boolean canHolyknight() {
        return holyknight == 1;
    }

    public boolean canGuardian() {
        return guardian == 1;
    }

    public boolean canArcher() {
        return archer == 1;
    }

    public boolean canHawkArcher() {
        return hawkArcher == 1;
    }

    public boolean canScout() {
        return scout == 1;
    }

    public boolean canSharpshooter() {
        return sharpshooter == 1;
    }

    public boolean canRanger() {
        return ranger == 1;
    }

    public boolean canMage() {
        return mage == 1;
    }

    public boolean canWizMage() {
        return wizMage == 1;
    }

    public boolean canEnchanter() {
        return enchanter == 1;
    }

    public boolean canWarlock() {
        return warlock == 1;
    }

    public boolean canWizard() {
        return wizard == 1;
    }

    public boolean canTrickster() {
        return trickster == 1;
    }

    public boolean canGambit() {
        return gambit == 1;
    }

    public boolean canRenegade() {
        return renegade == 1;
    }

    public boolean canSpectre() {
        return spectre == 1;
    }

    public boolean canReaper() {
        return reaper == 1;
    }

    public boolean canCrusader() {
        return crusader == 1;
    }

    public boolean canTemplar() {
        return templar == 1;
    }
}
