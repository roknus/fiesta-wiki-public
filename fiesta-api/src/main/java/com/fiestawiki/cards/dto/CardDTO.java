package com.fiestawiki.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.title.data.TitleDataDTO;

import java.util.List;

public class CardDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("itemInx")
    private String itemInx;

    @JsonProperty("grade_type")
    private Long gradeType;

    @JsonProperty("mob_species")
    private List<Long> mobSpecies;

    @JsonProperty("view")
    private CardViewDTO view;

    @JsonProperty("get_rate")
    private Long getRate;

    @JsonProperty("title")
    private TitleDataDTO title;

    @JsonProperty("card_group")
    private Long cardGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemInx() {
        return itemInx;
    }

    public void setItemInx(String itemInx) {
        this.itemInx = itemInx;
    }

    public Long getGradeType() {
        return gradeType;
    }

    public void setGradeType(Long gradeType) {
        this.gradeType = gradeType;
    }

    public List<Long> getMobSpecies() { return mobSpecies; }

    public void setMobSpecies(List<Long> mobSpecies) { this.mobSpecies = mobSpecies; }

    public CardViewDTO getView() {
        return view;
    }

    public void setView(CardViewDTO view) {
        this.view = view;
    }

    public Long getGetRate() { return getRate; }

    public void setGetRate(Long getRate) { this.getRate = getRate; }

    public TitleDataDTO getTitle() {
        return title;
    }

    public void setTitle(TitleDataDTO title) {
        this.title = title;
    }

    public Long getCardGroup() { return cardGroup; }

    public void setCardGroup(Long cardGroup) { this.cardGroup = cardGroup; }
}
