package com.fiestawiki.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CardViewDTO {

    @JsonProperty("filename")
    private String cardFilename;

    @JsonProperty("name")
    private String cardName;

    @JsonProperty("description")
    private String cardDesc;

    @JsonProperty("mob_inx")
    private String mobInx;

    @JsonProperty("season")
    private int cardSeason;

    public String getCardFilename() {
        return cardFilename.toLowerCase();
    }

    public void setCardFilename(String cardFilename) {
        this.cardFilename = cardFilename;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardDesc() {
        return cardDesc;
    }

    public void setCardDesc(String cardDesc) {
        this.cardDesc = cardDesc;
    }

    public String getMobInx() {
        return mobInx;
    }

    public void setMobInx(String mobInx) {
        this.mobInx = mobInx;
    }

    public int getCardSeason() {
        return cardSeason;
    }

    public void setCardSeason(int cardSeason) {
        this.cardSeason = cardSeason;
    }
}
