package com.fiestawiki.cards.dao.reward;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "collect_card_reward")
@EntityListeners(AuditingEntityListener.class)
public class CardReward {

    @Id
    @Column(name = "cc_rewardid", nullable = false)
    private Long id;

    @Column(name = "cc_cardrewardtype")
    private Long rewardType;

    @Column(name = "cc_cardlot")
    private int cardLot;

    @Column(name = "cc_rewarditeminx")
    private String itemInx;

    @Column(name = "cc_rewardlot")
    private int lot;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRewardType() {
        return rewardType;
    }

    public void setRewardType(Long rewardType) {
        this.rewardType = rewardType;
    }

    public int getCardLot() {
        return cardLot;
    }

    public void setCardLot(int cardLot) {
        this.cardLot = cardLot;
    }

    public String getItemInx() {
        return itemInx;
    }

    public void setItemInx(String itemInx) {
        this.itemInx = itemInx;
    }

    public int getLot() {
        return lot;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }
}
