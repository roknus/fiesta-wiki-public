package com.fiestawiki.cards.dao.carddroprate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardDropRateRepository extends JpaRepository<CardDropRate, Long> {
}
