package com.fiestawiki.cards.dao.view;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardViewRepository extends JpaRepository<CardView, Long> {
}
