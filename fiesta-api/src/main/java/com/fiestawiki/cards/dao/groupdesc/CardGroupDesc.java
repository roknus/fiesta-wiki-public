package com.fiestawiki.cards.dao.groupdesc;

import com.fiestawiki.cards.dao.card.Card;
import com.fiestawiki.cards.dao.collectcardmobgroup.CollectCardMobGroup;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "collect_card_group_desc")
@EntityListeners(AuditingEntityListener.class)
public class CardGroupDesc implements Serializable
{
    @Id
    @Column(name = "cc_cardmobgroup", nullable = false)
    private Long mobGroupId;

    @Column(name = "cc_mobgroupname")
    private String mobGroupName;

    @OneToMany(mappedBy = "cardGroup")
    private List<Card> cards = new ArrayList<>();

    @OneToMany(mappedBy = "cardGroupDesc")
    private List<CollectCardMobGroup> mobGroup = new ArrayList<>();

    public Long getMobGroupId() { return mobGroupId; }

    public void setMobGroupId(Long mobGroupId) { this.mobGroupId = mobGroupId; }

    public String getMobGroupName() {
        return mobGroupName;
    }

    public void setMobGroupName(String mobGroupName) {
        this.mobGroupName = mobGroupName;
    }

    public List<Card> getCards() { return cards; }

    public void setCards(List<Card> cards) { this.cards = cards; }

    public List<CollectCardMobGroup> getMobGroup() { return mobGroup; }

    public void setMobGroup(List<CollectCardMobGroup> mobGroup) { this.mobGroup = mobGroup; }
}
