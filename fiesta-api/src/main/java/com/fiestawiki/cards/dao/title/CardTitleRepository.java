package com.fiestawiki.cards.dao.title;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardTitleRepository extends JpaRepository<CardTitle, Long> {
}
