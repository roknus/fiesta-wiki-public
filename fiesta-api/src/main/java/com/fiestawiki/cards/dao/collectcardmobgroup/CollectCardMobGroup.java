package com.fiestawiki.cards.dao.collectcardmobgroup;

import com.fiestawiki.cards.dao.groupdesc.CardGroupDesc;
import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "collect_card_mob_group")
@EntityListeners(AuditingEntityListener.class)
public class CollectCardMobGroup
{
    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @Column(name = "cc_mobinx")
    private String mobInx;

    @OneToOne
    @JoinColumn(name = "mob_info_fk")
    private MobInfo mobInfo;

    @ManyToOne
    @JoinColumn(name = "cc_cardmobgroup")
    private CardGroupDesc cardGroupDesc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobInx() {
        return mobInx;
    }

    public void setMobInx(String mobInx) {
        this.mobInx = mobInx;
    }

    public MobInfo getMobInfo() {
        return mobInfo;
    }

    public void setMobInfo(MobInfo mobInfo) {
        this.mobInfo = mobInfo;
    }

    public CardGroupDesc getCardGroupDesc() {
        return cardGroupDesc;
    }

    public void setCardGroupDesc(CardGroupDesc cardGroupDesc) {
        this.cardGroupDesc = cardGroupDesc;
    }
}
