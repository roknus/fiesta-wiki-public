package com.fiestawiki.cards.dao.view;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "collect_card_view")
@EntityListeners(AuditingEntityListener.class)
public class CardView {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @Column(name = "cc_cardid")
    private int cardId;

    @Column(name = "cc_cardfilename")
    private String cardFilename;

    @Column(name = "cc_cardname")
    private String cardName;

    @Column(name = "cc_carddesc")
    private String cardDesc;

    @Column(name = "cc_mobinx")
    private String mobInx;

    @Column(name = "cc_cardseason")
    private int cardSeason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCardId() {
        return cardId;
    }

    public String getCardFilename() {
        return cardFilename;
    }

    public void setCardFilename(String cardFilename) {
        this.cardFilename = cardFilename;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardDesc() {
        return cardDesc;
    }

    public void setCardDesc(String cardDesc) {
        this.cardDesc = cardDesc;
    }

    public String getMobInx() {
        return mobInx;
    }

    public void setMobInx(String mobInx) {
        this.mobInx = mobInx;
    }

    public int getCardSeason() {
        return cardSeason;
    }

    public void setCardSeason(int cardSeason) {
        this.cardSeason = cardSeason;
    }
}
