package com.fiestawiki.cards.dao.title;

import com.fiestawiki.cards.dao.card.Card;
import com.fiestawiki.title.data.TitleData;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "collect_card_title")
@EntityListeners(AuditingEntityListener.class)
public class CardTitle implements Serializable {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @Column(name = "cc_iteminx")
    private String itemInx;

    @Column(name = "type")
    private Long type;

    @OneToOne
    @JoinColumn(
            name = "type",
            referencedColumnName = "type",
            insertable = false,
            updatable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT)
    )
    private TitleData title;

    @OneToMany(mappedBy = "cardTitle")
    private List<Card> card;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getItemInx() {
        return itemInx;
    }

    public void setItemInx(String itemInx) {
        this.itemInx = itemInx;
    }

    public TitleData getTitle() {
        return title;
    }

    public void setTitle(TitleData title) {
        this.title = title;
    }

    public Card getCard() {
        return card.get(0);
    }

    public void setCard(List<Card> card) {
        this.card = card;
    }
}
