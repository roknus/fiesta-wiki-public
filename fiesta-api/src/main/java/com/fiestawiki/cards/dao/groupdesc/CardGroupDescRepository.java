package com.fiestawiki.cards.dao.groupdesc;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardGroupDescRepository extends JpaRepository<CardGroupDesc, Long> {
}
