package com.fiestawiki.cards.dao.carddroprate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "collect_card_drop_rate")
@EntityListeners(AuditingEntityListener.class)
public class CardDropRate
{
    @Id
    @Column(name = "cc_cardid", nullable = false)
    private Long id;

    @Column(name = "cc_cardgetrate")
    private Long getRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGetRate() {
        return getRate;
    }

    public void setGetRate(Long getRate) {
        this.getRate = getRate;
    }
}
