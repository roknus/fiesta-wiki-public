package com.fiestawiki;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClassEnum {
    public static final int None                = 0;
    public static final int Fighter             = 1 << 0;
    public static final int CleverFighter       = 1 << 1;
    public static final int Warrior             = 1 << 2;
    public static final int Knight              = 1 << 3;
    public static final int Gladiator           = 1 << 4;
    public static final int KnightAndGladiator = Knight + Gladiator;
    public static final int Cleric              = 1 << 5;
    public static final int HighCleric          = 1 << 6;
    public static final int Paladin             = 1 << 7;
    public static final int Guardian            = 1 << 8;
    public static final int HolyKnight          = 1 << 9;
    public static final int GuardianAndHolyKnight = Guardian + HolyKnight;
    public static final int Archer              = 1 << 10;
    public static final int HawkArcher          = 1 << 11;
    public static final int Scout               = 1 << 12;
    public static final int Ranger              = 1 << 13;
    public static final int SharpShooter        = 1 << 14;
    public static final int Mage                = 1 << 15;
    public static final int WizMage             = 1 << 16;
    public static final int Enchanter           = 1 << 17;
    public static final int Wizard              = 1 << 18;
    public static final int Warlock             = 1 << 19;
    public static final int WizardAndWarlock    = Wizard + Warlock;
    public static final int Trickster           = 1 << 20;
    public static final int Gambit              = 1 << 21;
    public static final int Renegade            = 1 << 22;
    public static final int Reaper              = 1 << 23;
    public static final int Spectre             = 1 << 24;
    public static final int ReaperAndSpectre    = Reaper + Spectre;
    public static final int Crusader            = 1 << 25;
    public static final int Templar             = 1 << 26;
    public static final int WarriorAndScout     = Warrior + Scout;
    public static final int PaladinAndWarrior   = Paladin + Warrior;
    public static final int KnightAndGuardianAndHolyKnight = Knight + Guardian + HolyKnight;

    /*
1 : All
2 : Fighter
3 : CleverFighter
4 : Warrior
5 : Knight & Gladiator
6 : Gladiator
7 : Knight
8 : Cleric
9 : High Cleric
10 : Paladin
11 : Guardian
12 : Holy Knight
13 : Guardian & Holy Knight
14 : Archer
15 : HawkArcher
16 : Scout
17 : Ranger
18 : SharpShooter
19 : Ranger & SharpShooter
20 : Mage
21 : WizMage
22 : Enchanter
23 : Wizard
24 : Warlock
25 : Wizard & Warlock
26 : All ?
27 : Trickster
28 : Gambit
29 : Renegade
30 : Reaper
31 : Spectre
32 : Reaper & Spectre
33 : Crusader
34 : Templar
35 : Warrior & Scout
36 : Paladin & Warrior
37 : All
38 : Knight, Guardian & HolyKnight
*/

    public static final List<Long> fromFlagToUseClass(Long flag, boolean includeAll)
    {
        List<Long> useClasses = fromFlagToUseClass(flag);

        if(includeAll)
        {
            // All
            useClasses.add(1L);
        }

        return useClasses;
    }

    public static final List<Long> fromFlagToUseClass(Long flag)
    {
        List<Long> useClasses = new ArrayList<>();

        if((flag & Fighter) != 0) useClasses.add(2L);
        if((flag & CleverFighter) != 0) useClasses.add(3L);
        if((flag & Warrior) != 0) useClasses.add(4L);
        if((flag & KnightAndGladiator) != 0) useClasses.add(5L);
        if((flag & Gladiator) != 0) useClasses.add(6L);
        if((flag & Knight) != 0) useClasses.add(7L);
        if((flag & Cleric) != 0) useClasses.add(8L);
        if((flag & HighCleric) != 0) useClasses.add(9L);
        if((flag & Paladin) != 0) useClasses.add(10L);
        if((flag & Guardian) != 0) useClasses.add(11L);
        if((flag & HolyKnight) != 0) useClasses.add(12L);
        if((flag & GuardianAndHolyKnight) != 0) useClasses.add(13L);
        if((flag & Archer) != 0) useClasses.add(14L);
        if((flag & HawkArcher) != 0) useClasses.add(15L);
        if((flag & Scout) != 0) useClasses.add(16L);
        if((flag & Ranger) != 0) useClasses.add(17L);
        if((flag & SharpShooter) != 0) useClasses.add(18L);
        // ?
        if((flag & Mage) != 0) useClasses.add(20L);
        if((flag & WizMage) != 0) useClasses.add(21L);
        if((flag & Enchanter) != 0) useClasses.add(22L);
        if((flag & Wizard) != 0) useClasses.add(23L);
        if((flag & Warlock) != 0) useClasses.add(24L);
        if((flag & WizardAndWarlock) != 0) useClasses.add(25L);
        // All 26
        if((flag & Trickster) != 0) useClasses.add(27L);
        if((flag & Gambit) != 0) useClasses.add(28L);
        if((flag & Renegade) != 0) useClasses.add(29L);
        if((flag & Reaper) != 0) useClasses.add(30L);
        if((flag & Spectre) != 0) useClasses.add(31L);
        if((flag & ReaperAndSpectre) != 0) useClasses.add(32L);
        if((flag & Crusader) != 0) useClasses.add(33L);
        if((flag & Templar) != 0) useClasses.add(34L);
        if((flag & WarriorAndScout) != 0) useClasses.add(35L);
        if((flag & PaladinAndWarrior) != 0) useClasses.add(36L);
        // ? 37
        if((flag & KnightAndGuardianAndHolyKnight) != 0) useClasses.add(38L);

        return useClasses;
    }
}
