package com.fiestawiki.states.abstate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AbStateRepository extends JpaRepository<AbState, Long> {
}
