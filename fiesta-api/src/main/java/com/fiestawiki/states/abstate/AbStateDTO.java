package com.fiestawiki.states.abstate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.states.abstateview.AbStateViewDTO;
import com.fiestawiki.states.subabstate.SubAbStateDTO;

import java.util.ArrayList;
import java.util.List;

public class AbStateDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("view")
    private AbStateViewDTO view;

    @JsonProperty("sub_ab_state")
    private SubAbStateDTO subAbState;

    @JsonProperty("party_ab_state")
    private List<AbStateDTO> partyAbState = new ArrayList<>();

    @JsonProperty("index")
    private Long index;

    @JsonProperty("keep_time_ratio")
    private Long keepTimeRatio;

    @JsonProperty("keep_time_power")
    private Long keepTimePower;

    @JsonProperty("grade")
    private int grade;

    @JsonProperty("dispel_index")
    private Long dispelIndex;

    @JsonProperty("sub_dispel_index")
    private Long subDispelIndex;

    @JsonProperty("save_type")
    private Long saveType;

    @JsonProperty("main_state_index")
    private String mainStateIndex;

    @JsonProperty("stackable")
    private int stackable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public AbStateViewDTO getView() {
        return view;
    }

    public void setView(AbStateViewDTO view) {
        this.view = view;
    }

    public SubAbStateDTO getSubAbState() {
        return subAbState;
    }

    public void setSubAbState(SubAbStateDTO subAbState) {
        this.subAbState = subAbState;
    }

    public List<AbStateDTO> getPartyAbState() {
        return partyAbState;
    }

    public void setPartyAbState(List<AbStateDTO> partyAbState) {
        this.partyAbState = partyAbState;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public Long getKeepTimeRatio() {
        return keepTimeRatio;
    }

    public void setKeepTimeRatio(Long keepTimeRatio) {
        this.keepTimeRatio = keepTimeRatio;
    }

    public Long getKeepTimePower() {
        return keepTimePower;
    }

    public void setKeepTimePower(Long keepTimePower) {
        this.keepTimePower = keepTimePower;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Long getDispelIndex() {
        return dispelIndex;
    }

    public void setDispelIndex(Long dispelIndex) {
        this.dispelIndex = dispelIndex;
    }

    public Long getSubDispelIndex() {
        return subDispelIndex;
    }

    public void setSubDispelIndex(Long subDispelIndex) {
        this.subDispelIndex = subDispelIndex;
    }

    public Long getSaveType() {
        return saveType;
    }

    public void setSaveType(Long saveType) {
        this.saveType = saveType;
    }

    public String getMainStateIndex() {
        return mainStateIndex;
    }

    public void setMainStateIndex(String mainStateIndex) {
        this.mainStateIndex = mainStateIndex;
    }

    public int getStackable() {
        return stackable;
    }

    public void setStackable(int stackable) {
        this.stackable = stackable;
    }
}
