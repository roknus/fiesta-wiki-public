package com.fiestawiki.states.abstateview;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AbStateViewRepository extends JpaRepository<AbStateView, Long> {
}
