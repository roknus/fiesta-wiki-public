package com.fiestawiki.states;

import com.fiestawiki.states.abstate.AbState;
import com.fiestawiki.states.abstate.AbStateDTO;
import com.fiestawiki.states.abstate.AbStateRepository;
import com.fiestawiki.states.abstateview.AbStateView;
import com.fiestawiki.states.abstateview.AbStateViewDTO;
import com.fiestawiki.states.abstateview.AbStateViewRepository;
import com.fiestawiki.states.subabstate.SubAbState;
import com.fiestawiki.states.subabstate.SubAbStateDTO;
import com.fiestawiki.states.subabstate.SubAbStateEffectDTO;
import com.fiestawiki.states.subabstate.SubAbStateRepository;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

@Service
public class StatesService {
    @Autowired
    private SubAbStateRepository subAbStateRepository;

    @Autowired
    private AbStateRepository abStateRepository;

    @Autowired
    private AbStateViewRepository abStateViewRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

        ///
        Converter<AbStateView, String> getBorderColor = new AbstractConverter<AbStateView, String>() {
            public String convert(AbStateView src) {

                if(src.getRed() == 0 && src.getGreen() == 0 && src.getBlue() == 0)
                    return "#FFFFFF";

                String out = "#";

                out += String.format("%02X", src.getRed());
                out += String.format("%02X", src.getGreen());
                out += String.format("%02X", src.getBlue());

                return out;
            }
        };

        ///
        TypeMap<AbStateView, AbStateViewDTO> typeMap =
                modelMapper.createTypeMap(AbStateView.class, AbStateViewDTO.class);

        typeMap.addMappings( mapper -> {
            mapper.using(getBorderColor).map(src -> src,
                    AbStateViewDTO::setBorderColor);
        });
    }

    public AbState getAbState(Long id)
    {
        Optional<AbState> ab_state = abStateRepository.findById(id);

        return ab_state.isPresent() ? ab_state.get() : null;
    }

    public SubAbState getSubAbState(String ab_state, Long strength)
    {
        if(ab_state == null)
            return null;

        List<SubAbState> sub_ab_state = subAbStateRepository.findSubAbState(ab_state);

        if(sub_ab_state.size() == 1)
        {
            return sub_ab_state.get(0);
        }
        else
        {
            SubAbState sub = sub_ab_state.stream()
                    .filter(s -> s.getStrength() == strength)
                    .findFirst()
                    .orElse(null);

            if(sub != null) {
                return sub;
            }
        }
        return null;
    }

    public List<AbStateDTO> convertAbState(AbState ab_state, Long strength)
    {
        List<AbStateDTO> ab_states = new ArrayList<>();

        if(ab_state == null)
            return ab_states;

        AbStateDTO ab_state_dto = modelMapper.map(ab_state, AbStateDTO.class);

        SubAbState sub_ab_state = getSubAbState(ab_state.getSubAbState(), strength);

        if(sub_ab_state != null)
        {
            SubAbStateDTO sub_ab_state_dto = modelMapper.map(sub_ab_state, SubAbStateDTO.class);
            List<SubAbStateEffectDTO> effects_dto = new ArrayList<>();

            BiFunction<Long, Long, Void> AdditionalEffectFunc = (Long index, Long value) -> {
                if (index > 0)
                {
                    if (index == 54) {
                        AbState additional_ab_state = getAbState(value);

                        if (additional_ab_state != null) {
                            ab_states.addAll(convertAbState(additional_ab_state, 1L));
                        }

                        return null;
                    }

                    effects_dto.add(new SubAbStateEffectDTO(index, value));
                }
                return null;
            };

            AdditionalEffectFunc.apply(sub_ab_state.getActionIndexA(), sub_ab_state.getActionArgA());
            AdditionalEffectFunc.apply(sub_ab_state.getActionIndexB(), sub_ab_state.getActionArgB());
            AdditionalEffectFunc.apply(sub_ab_state.getActionIndexC(), sub_ab_state.getActionArgC());
            AdditionalEffectFunc.apply(sub_ab_state.getActionIndexD(), sub_ab_state.getActionArgD());

            sub_ab_state_dto.setEffects(effects_dto);

            ab_state_dto.setSubAbState(sub_ab_state_dto);
        }

        List<AbStateDTO> party_sub_ab_state_list = new ArrayList<>();

        if(ab_state.getPartyState1() != null)
        {
            party_sub_ab_state_list.addAll(convertAbState(ab_state.getPartyState1(), strength));
        }
        if(ab_state.getPartyState2() != null)
        {
            party_sub_ab_state_list.addAll(convertAbState(ab_state.getPartyState2(), strength));
        }
        if(ab_state.getPartyState3() != null)
        {
            party_sub_ab_state_list.addAll(convertAbState(ab_state.getPartyState3(), strength));
        }
        if(ab_state.getPartyState4() != null)
        {
            party_sub_ab_state_list.addAll(convertAbState(ab_state.getPartyState4(), strength));
        }
        if(ab_state.getPartyState5() != null)
        {
            party_sub_ab_state_list.addAll(convertAbState(ab_state.getPartyState5(), strength));
        }

        ab_state_dto.setPartyAbState(party_sub_ab_state_list);

        ab_states.add(ab_state_dto);

        return ab_states;
    }
}
