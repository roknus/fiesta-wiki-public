package com.fiestawiki.states.subabstate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "sub_ab_state")
@EntityListeners(AuditingEntityListener.class)
public class SubAbState {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "strength")
    private Long strength;

    @Column(name = "type")
    private Long type;

    @Column(name = "subtype")
    private int subType;

    @Column(name = "keeptime")
    private Long duration;

    @Column(name = "actionindexa")
    private Long actionIndexA;

    @Column(name = "actionarga")
    private Long actionArgA;

    @Column(name = "actionindexb")
    private Long actionIndexB;

    @Column(name = "actionargb")
    private Long actionArgB;

    @Column(name = "actionindexc")
    private Long actionIndexC;

    @Column(name = "actionargc")
    private Long actionArgC;

    @Column(name = "actionindexd")
    private Long actionIndexD;

    @Column(name = "actionargd")
    private Long actionArgD;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getStrength() {
        return strength;
    }

    public void setStrength(Long strength) {
        this.strength = strength;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getActionIndexA() {
        return actionIndexA;
    }

    public void setActionIndexA(Long actionIndexA) {
        this.actionIndexA = actionIndexA;
    }

    public Long getActionArgA() {
        return actionArgA;
    }

    public void setActionArgA(Long actionArgA) {
        this.actionArgA = actionArgA;
    }

    public Long getActionIndexB() {
        return actionIndexB;
    }

    public void setActionIndexB(Long actionIndexB) {
        this.actionIndexB = actionIndexB;
    }

    public Long getActionArgB() {
        return actionArgB;
    }

    public void setActionArgB(Long actionArgB) {
        this.actionArgB = actionArgB;
    }

    public Long getActionIndexC() {
        return actionIndexC;
    }

    public void setActionIndexC(Long actionIndexC) {
        this.actionIndexC = actionIndexC;
    }

    public Long getActionArgC() {
        return actionArgC;
    }

    public void setActionArgC(Long actionArgC) {
        this.actionArgC = actionArgC;
    }

    public Long getActionIndexD() {
        return actionIndexD;
    }

    public void setActionIndexD(Long actionIndexD) {
        this.actionIndexD = actionIndexD;
    }

    public Long getActionArgD() {
        return actionArgD;
    }

    public void setActionArgD(Long actionArgD) {
        this.actionArgD = actionArgD;
    }
}
