package com.fiestawiki.states.subabstate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubAbStateRepository extends JpaRepository<SubAbState, Long> {

    @Query("SELECT sab FROM SubAbState sab WHERE sab.inxname = :inxname")
    List<SubAbState> findSubAbState(String inxname);
}
