package com.fiestawiki.states.passiveskillabstate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.states.abstate.AbStateDTO;

import java.util.ArrayList;
import java.util.List;

public class PassiveSkillAbStateDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("condition")
    private Long condition;

    @JsonProperty("condition_rate")
    private Long conditionRate;

    @JsonProperty("ab_states")
    private List<AbStateDTO> abState = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCondition() {
        return condition;
    }

    public void setCondition(Long condition) {
        this.condition = condition;
    }

    public Long getConditionRate() {
        return conditionRate;
    }

    public void setConditionRate(Long conditionRate) {
        this.conditionRate = conditionRate;
    }

    public List<AbStateDTO> getAbState() { return abState; }

    public void setAbState(List<AbStateDTO> abState) { this.abState = abState; }
}
