package com.fiestawiki.license;

import com.fiestawiki.NotFoundException;
import com.fiestawiki.license.dao.data.LicenseData;
import com.fiestawiki.license.dao.join.LicenseJoin;
import com.fiestawiki.license.dao.join.repository.LicenseJoinRepository;
import com.fiestawiki.license.dto.LicenseDTO;
import com.fiestawiki.license.dto.LicenseLevelDTO;
import com.fiestawiki.mobs.mobinfo.repository.MobInfoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class LicenseService
{
    @Autowired
    private MobInfoRepository mobInfoRepository;

    @Autowired
    private LicenseJoinRepository licenseJoinRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

        modelMapper.emptyTypeMap(LicenseData.class, LicenseLevelDTO.class).addMappings(mapper -> {
            mapper.map(LicenseData::getLevel, LicenseLevelDTO::setLevel);
            mapper.map(LicenseData::getSuffix, LicenseLevelDTO::setSuffix);
            mapper.map(LicenseData::getMobKillCount, LicenseLevelDTO::setMobKillCount);
            mapper.map(LicenseData::getMaxAdd, LicenseLevelDTO::setMaxAdd);
            mapper.map(LicenseData::getValue, LicenseLevelDTO::setValue);
        });

        modelMapper.emptyTypeMap(LicenseJoin.class, LicenseDTO.class).addMappings(mapper -> {
            mapper.map(LicenseJoin::getId, LicenseDTO::setId);
            mapper.map(src -> src.getItem().getId(), LicenseDTO::setItemId);
            mapper.map(src -> src.getSpecies().getId(), LicenseDTO::setSpeciesId);
            mapper.map(src -> src.getLicenseData(), LicenseDTO::setLevels);
        });
    }

    public LicenseDTO convertLicenseJoin(LicenseJoin license)
    {
        return modelMapper.map(license, LicenseDTO.class);
    }

    public Page<Long> getLicenses(Pageable pageable, Optional<Boolean> active)
    {
        Page<LicenseJoin> licenses = licenseJoinRepository.findActiveLicenses(pageable, active);

        return licenses.map(l -> l.getId());
    }

    public LicenseDTO getLicense(Long id) throws NotFoundException
    {
        LicenseJoin license = licenseJoinRepository.findById(id).orElseThrow(NotFoundException::new);

        return convertLicenseJoin(license);
    }
}
