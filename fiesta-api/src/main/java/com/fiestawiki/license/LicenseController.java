package com.fiestawiki.license;

import com.fiestawiki.NotFoundException;
import com.fiestawiki.license.dto.LicenseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
public class LicenseController
{
    @Autowired
    private LicenseService licenseService;

    @GetMapping("licenses")
    public ResponseEntity<Page<Long>> getLicenses(
        @PageableDefault(size = 20) Pageable pageable,
        @RequestParam() Optional<Boolean> active
    )
    {
        Page<Long> licenses = licenseService.getLicenses(pageable, active);

        return ResponseEntity.ok(licenses);
    }

    @GetMapping("license/{id}")
    public ResponseEntity<LicenseDTO> getLicense(@PathVariable(value = "id") Long id)
    {
        try
        {
            return ResponseEntity.ok(licenseService.getLicense(id));
        }
        catch (NotFoundException ex)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "License not found", ex);
        }
    }
}