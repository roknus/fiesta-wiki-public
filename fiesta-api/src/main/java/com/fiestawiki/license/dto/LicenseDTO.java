package com.fiestawiki.license.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class LicenseDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("species_id")
    private Long speciesId;

    @JsonProperty("item_id")
    private Long itemId;

    @JsonProperty("levels")
    private List<LicenseLevelDTO> levels = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpeciesId() {
        return speciesId;
    }

    public void setSpeciesId(Long speciesId) {
        this.speciesId = speciesId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public List<LicenseLevelDTO> getLevels() {
        return levels;
    }

    public void setLevels(List<LicenseLevelDTO> levels) {
        this.levels = levels;
    }
}
