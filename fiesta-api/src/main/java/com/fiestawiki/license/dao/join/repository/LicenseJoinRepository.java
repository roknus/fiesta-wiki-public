package com.fiestawiki.license.dao.join.repository;

import com.fiestawiki.license.dao.join.LicenseJoin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenseJoinRepository  extends JpaRepository<LicenseJoin, Long>, LicenseJoinRepositoryCustom {
}
