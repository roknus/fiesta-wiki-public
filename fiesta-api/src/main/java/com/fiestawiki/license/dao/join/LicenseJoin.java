package com.fiestawiki.license.dao.join;

import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.license.dao.data.LicenseData;
import com.fiestawiki.mobs.mobspecies.MobSpecies;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "licenses")
@EntityListeners(AuditingEntityListener.class)
public class LicenseJoin
{
    @Id
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pk1")
    private MobSpecies species;

    @ManyToOne
    @JoinColumn(name = "pk2")
    private ItemInfo item;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MobSpecies getSpecies() { return species; }

    public void setSpecies(MobSpecies species) { this.species = species; }

    public ItemInfo getItem() {
        return item;
    }

    public void setItem(ItemInfo item) {
        this.item = item;
    }

    public List<LicenseData> getLicenseData() {
        return species.getLicenseData()
                .stream()
                .sorted(Comparator.comparingInt(LicenseData::getLevel))
                .collect(Collectors.toList());
    }
}
