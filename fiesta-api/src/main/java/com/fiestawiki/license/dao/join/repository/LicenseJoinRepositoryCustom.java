package com.fiestawiki.license.dao.join.repository;

import com.fiestawiki.license.dao.join.LicenseJoin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import java.util.Optional;

public interface LicenseJoinRepositoryCustom {

    Page<LicenseJoin> findActiveLicenses(
            @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable,
            Optional<Boolean>active);
}
