package com.fiestawiki.license.dao.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenseRepository extends JpaRepository<LicenseData, Long> {
}
