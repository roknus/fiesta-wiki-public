package com.fiestawiki.admin;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class AdminController {

    @GetMapping ("user/login")
    public ResponseEntity<Principal> user(Principal user) {
        return ResponseEntity.ok(user);
    }

    @GetMapping("admin/hello")
    public ResponseEntity<String> getCards()
            throws Exception
    {
        return ResponseEntity.ok("Hello admin");
    }
}
