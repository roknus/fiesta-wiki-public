# DB

Docker
Docker-compose

# Backend

InteliJ
* JDK 8
* Lombok

# Frontend

npm
* npm install -g @angular/cli

# Static

npm

# webp to convert png to webp game icons
webp

sshpass
maven