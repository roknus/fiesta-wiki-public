
ALTER TABLE "public"."mob_info"
 ADD CONSTRAINT "mob_info_mob_species_id"
	 FOREIGN KEY (mob_species_fk)
		 REFERENCES "public"."mob_species"(id);

ALTER TABLE "public"."mob_view_info"
 ADD CONSTRAINT "mob_view_info_mob_info_mob_info_fk"
	 FOREIGN KEY (mob_info_fk)
		 REFERENCES "public"."mob_info"(ID);

ALTER TABLE "public"."mob_info_server"
 ADD CONSTRAINT "mob_info_server_mob_info_mob_info_fk"
	 FOREIGN KEY (mob_info_fk)
		 REFERENCES "public"."mob_info"(ID);

ALTER TABLE "public"."mob_coordinate"
 ADD CONSTRAINT "mob_coordinate_mob_info_mob_info_fk"
	 FOREIGN KEY (Mob_ID)
		 REFERENCES "public"."mob_info"(ID),
 ADD CONSTRAINT "mob_coordinate_map_info_map_id"
	 FOREIGN KEY (map_id)
		 REFERENCES "public"."map_info"(ID);

ALTER TABLE "public"."quest_data"
 ADD CONSTRAINT "quest_data_quest_dialog_quest_dialog_fk"
	 FOREIGN KEY (DescId)
		 REFERENCES "public"."quest_dialog"(ID),
 ADD CONSTRAINT "quest_data_quest_dialog_quest_title_fk"
	 FOREIGN KEY (NameId)
		 REFERENCES "public"."quest_dialog"(ID),
 ADD CONSTRAINT "quest_data_quest_data_previous_quest_fk"
	 FOREIGN KEY (PrevQuestID)
		 REFERENCES "public"."quest_data"(ID);

ALTER TABLE "public"."quest_reward"
 ADD CONSTRAINT "quest_reward_quest_data_quest_reward_quest_fk"
	 FOREIGN KEY (ID)
		 REFERENCES "public"."quest_data"(ID);

ALTER TABLE "public"."quest_end_npc"
 ADD CONSTRAINT "quest_end_npc_mob_info_mob_info_id_fk"
	 FOREIGN KEY (MobID)
		 REFERENCES "public"."mob_info"(ID),
 ADD CONSTRAINT "quest_end_npc_quest_data_quest_id"
	 FOREIGN KEY (quest_id)
		 REFERENCES "public"."quest_data"(ID);

ALTER TABLE "public"."quest_action"
 ADD CONSTRAINT "quest_action_quest_end_item_quest_item_fk"
	 FOREIGN KEY (quest_item_fk)
		 REFERENCES "public"."quest_end_item"(unique_id),
 ADD CONSTRAINT "quest_action_mob_info_mob_info_fk"
	 FOREIGN KEY (ConditionTarget)
		 REFERENCES "public"."mob_info"(ID),
 ADD CONSTRAINT "quest_action_quest_data_quest_data_fk"
	 FOREIGN KEY (ID)
		 REFERENCES "public"."quest_data"(ID);

ALTER TABLE "public"."quest_end_item"
 ADD CONSTRAINT "quest_end_item_quest_data_quest_fk"
	 FOREIGN KEY (ID)
		 REFERENCES "public"."quest_data"(ID);

ALTER TABLE "public"."license_data"
 ADD CONSTRAINT "license_data_mob_info_mob_info_fk"
	 FOREIGN KEY (MobID)
		 REFERENCES "public"."mob_info"(ID),
 ADD CONSTRAINT "license_data_mob_species_mob_species_fk"
	 FOREIGN KEY (species_id)
		 REFERENCES "public"."mob_species"(id);

ALTER TABLE "public"."item_drop_table"
 ADD CONSTRAINT "item_drop_table_mob_info_mob_fk"
	 FOREIGN KEY (mob_fk)
		 REFERENCES "public"."mob_info"(ID);

ALTER TABLE "public"."ab_state"
 ADD CONSTRAINT "ab_state_ab_state_party_state_1_fk"
	 FOREIGN KEY (party_state_1_fk)
		 REFERENCES "public"."ab_state"(unique_id),
 ADD CONSTRAINT "ab_state_ab_state_party_state_2_fk"
	 FOREIGN KEY (party_state_2_fk)
		 REFERENCES "public"."ab_state"(unique_id),
 ADD CONSTRAINT "ab_state_ab_state_party_state_3_fk"
	 FOREIGN KEY (party_state_3_fk)
		 REFERENCES "public"."ab_state"(unique_id),
 ADD CONSTRAINT "ab_state_ab_state_party_state_4_fk"
	 FOREIGN KEY (party_state_4_fk)
		 REFERENCES "public"."ab_state"(unique_id),
 ADD CONSTRAINT "ab_state_ab_state_party_state_5_fk"
	 FOREIGN KEY (party_state_5_fk)
		 REFERENCES "public"."ab_state"(unique_id);

ALTER TABLE "public"."active_skill"
 ADD CONSTRAINT "active_skill_active_skill_previous_skill_fk"
	 FOREIGN KEY (previous_skill_fk)
		 REFERENCES "public"."active_skill"(ID),
 ADD CONSTRAINT "active_skill_ab_state_stanamea_fk"
	 FOREIGN KEY (stanamea_fk)
		 REFERENCES "public"."ab_state"(unique_id),
 ADD CONSTRAINT "active_skill_ab_state_stanameb_fk"
	 FOREIGN KEY (stanameb_fk)
		 REFERENCES "public"."ab_state"(unique_id),
 ADD CONSTRAINT "active_skill_ab_state_stanamec_fk"
	 FOREIGN KEY (stanamec_fk)
		 REFERENCES "public"."ab_state"(unique_id);

ALTER TABLE "public"."active_skill_group_join"
 ADD CONSTRAINT "active_skill_group_join_active_skill_active_skill_fk"
	 FOREIGN KEY (active_skill_fk)
		 REFERENCES "public"."active_skill"(ID),
 ADD CONSTRAINT "active_skill_group_join_active_skill_group_skill_group_fk"
	 FOREIGN KEY (ActiveSkillGroupIndex)
		 REFERENCES "public"."active_skill_group"(id);

ALTER TABLE "public"."passive_skill"
 ADD CONSTRAINT "passive_skill_passive_skill_previous_skill_fk"
	 FOREIGN KEY (previous_skill_fk)
		 REFERENCES "public"."passive_skill"(ID),
 ADD CONSTRAINT "passive_skill_passive_skill_ab_state_ab_state_fk"
	 FOREIGN KEY (ab_state_fk)
		 REFERENCES "public"."passive_skill_ab_state"(unique_id);

ALTER TABLE "public"."passive_skill_ab_state"
 ADD CONSTRAINT "passive_skill_ab_state_ab_state_ab_state_fk"
	 FOREIGN KEY (ab_state_fk)
		 REFERENCES "public"."ab_state"(unique_id);

ALTER TABLE "public"."multi_hit_type"
 ADD CONSTRAINT "multi_hit_type_ab_state_abstate_fk"
	 FOREIGN KEY (abstate_fk)
		 REFERENCES "public"."ab_state"(unique_id);

ALTER TABLE "public"."item_info"
 ADD CONSTRAINT "item_info_item_info_server_item_info_server_fk"
	 FOREIGN KEY (item_info_server_fk)
		 REFERENCES "public"."item_info_server"(unique_id),
 ADD CONSTRAINT "item_info_item_set_item_set_id"
	 FOREIGN KEY (item_set_id)
		 REFERENCES "public"."item_set"(unique_id),
 ADD CONSTRAINT "item_info_grade_item_option_grade_item_option_id"
	 FOREIGN KEY (grade_item_option_id)
		 REFERENCES "public"."grade_item_option"(unique_id),
 ADD CONSTRAINT "item_info_use_class_use_class_fk"
	 FOREIGN KEY (UseClass)
		 REFERENCES "public"."use_class"(UseClass);

ALTER TABLE "public"."grade_item_option"
 ADD CONSTRAINT "grade_item_option_item_info_item_fk"
	 FOREIGN KEY (item_fk)
		 REFERENCES "public"."item_info"(ID);

ALTER TABLE "public"."set_effect"
 ADD CONSTRAINT "set_effect_item_set_item_set_id"
	 FOREIGN KEY (item_set_id)
		 REFERENCES "public"."item_set"(unique_id);

ALTER TABLE "public"."item_action"
 ADD CONSTRAINT "item_action_item_action_effect_item_action_effect_id"
	 FOREIGN KEY (item_action_effect_id)
		 REFERENCES "public"."item_action_effect"(unique_id),
 ADD CONSTRAINT "item_action_item_action_condition_action_condition_fk"
	 FOREIGN KEY (ConditionID)
		 REFERENCES "public"."item_action_condition"(ConditionID);

ALTER TABLE "public"."item_action_condition"
 ADD CONSTRAINT "item_action_condition_active_skill_group_skill_group_fk"
	 FOREIGN KEY (skill_group_fk)
		 REFERENCES "public"."active_skill_group"(id);

ALTER TABLE "public"."collect_card"
 ADD CONSTRAINT "collect_card_collect_card_group_desc_card_mob_group_id"
	 FOREIGN KEY (card_mob_group_id)
		 REFERENCES "public"."collect_card_group_desc"(CC_CardMobGroup),
 ADD CONSTRAINT "collect_card_collect_card_title_card_title_id"
	 FOREIGN KEY (card_title_id)
		 REFERENCES "public"."collect_card_title"(unique_id);

ALTER TABLE "public"."collect_card_mob_group"
 ADD CONSTRAINT "collect_card_mob_group_mob_info_mob_info_fk"
	 FOREIGN KEY (mob_info_fk)
		 REFERENCES "public"."mob_info"(ID),
 ADD CONSTRAINT "collect_card_mob_group_collect_card_group_desc_card_group_fk"
	 FOREIGN KEY (CC_CardMobGroup)
		 REFERENCES "public"."collect_card_group_desc"(CC_CardMobGroup);

ALTER TABLE "lucky_capsule"."lucky_capsule"
 ADD CONSTRAINT "lucky_capsule_item_info_capsule_item_fk"
	 FOREIGN KEY (capsule_item_fk)
		 REFERENCES "public"."item_info"(ID);

ALTER TABLE "lucky_capsule"."lucky_capsule_reward"
 ADD CONSTRAINT "lucky_capsule_reward_item_info_item_fk"
	 FOREIGN KEY (item_fk)
		 REFERENCES "public"."item_info"(ID);

ALTER TABLE "public"."item_info_server_random_option_join"
 ADD CONSTRAINT "item_info_server_random_option_join_item_info_server_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."item_info_server"(unique_id),
 ADD CONSTRAINT "item_info_server_random_option_join_random_option_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."random_option"(unique_id);

ALTER TABLE "public"."set_effect_item_action"
 ADD CONSTRAINT "set_effect_item_action_set_effect_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."set_effect"(unique_id),
 ADD CONSTRAINT "set_effect_item_action_item_action_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."item_action"(unique_id);

ALTER TABLE "public"."set_effect_item_action_effect_desc"
 ADD CONSTRAINT "set_effect_item_action_effect_desc_set_effect_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."set_effect"(unique_id),
 ADD CONSTRAINT "set_effect_item_action_effect_desc_item_action_effect_desc_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."item_action_effect_desc"(unique_id);

ALTER TABLE "public"."licenses"
 ADD CONSTRAINT "licenses_mob_species_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."mob_species"(id),
 ADD CONSTRAINT "licenses_item_info_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."item_info"(ID);

ALTER TABLE "public"."item_info_drop_group_join"
 ADD CONSTRAINT "item_info_drop_group_join_item_info_server_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."item_info_server"(unique_id),
 ADD CONSTRAINT "item_info_drop_group_join_item_drop_group_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."item_drop_group"(ID);

ALTER TABLE "public"."item_drop_group_join"
 ADD CONSTRAINT "item_drop_group_join_item_drop_table_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."item_drop_table"(ID),
 ADD CONSTRAINT "item_drop_group_join_item_drop_group_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."item_drop_group"(ID);

ALTER TABLE "lucky_capsule"."lucky_capsule_group_reward_join"
 ADD CONSTRAINT "lucky_capsule_group_reward_join_lucky_capsule_reward_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "lucky_capsule"."lucky_capsule_reward"(unique_id),
 ADD CONSTRAINT "lucky_capsule_group_reward_join_lucky_capsule_group_rate_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "lucky_capsule"."lucky_capsule_group_rate"(unique_id);

ALTER TABLE "public"."multi_hit_join"
 ADD CONSTRAINT "multi_hit_join_multi_hit_type_pk1_fk"
	 FOREIGN KEY (pk1)
		 REFERENCES "public"."multi_hit_type"(unique_id),
 ADD CONSTRAINT "multi_hit_join_active_skill_pk2_fk"
	 FOREIGN KEY (pk2)
		 REFERENCES "public"."active_skill"(ID);
