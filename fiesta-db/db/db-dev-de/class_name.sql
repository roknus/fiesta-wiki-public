CREATE TABLE "public"."class_name"(
 ClassID  smallint UNIQUE PRIMARY KEY,
 acPrefix  VARCHAR(2000),
 acEngName  VARCHAR(2000),
 acLocalName  VARCHAR(2000)
);
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (0, E'-', E'-', E'-');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (1, E'Fig', E'Fighter', E'Krieger');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (2, E'Cfi', E'CleverFighter', E'Highlander');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (3, E'War', E'Warrior', E'Feldherr');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (4, E'Gla', E'Gladiator', E'Gladiator');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (5, E'Kni', E'Knight', E'Ordensritter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (6, E'Cle', E'Cleric', E'Priester');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (7, E'Hcl', E'HighCleric', E'Kleriker');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (8, E'Pal', E'Paladin', E'Hohepriester');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (9, E'Hol', E'HolyKnight', E'Heiliger Ritter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (10, E'Gua', E'Guardian', E'Wächter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (11, E'Arc', E'Archer', E'Jäger');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (12, E'Har', E'HawkArcher', E'Fallensteller');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (13, E'Sco', E'Scout', E'Großwildjäger');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (14, E'Sha', E'SharpShooter', E'Scharfschütze');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (15, E'Ran', E'Ranger', E'Waldläufer');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (16, E'Mag', E'Mage', E'Magier');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (17, E'Wma', E'WizMage', E'Erzmagier');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (18, E'Enc', E'Enchanter', E'Beschwörer');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (19, E'War', E'Warlock', E'Hexenmeister');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (20, E'Wiz', E'Wizard', E'Zauberer');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (21, E'Jok', E'Joker', E'Joker');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (22, E'Chs', E'Chaser', E'Schurke');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (23, E'Cru', E'Cruel', E'Räuber');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (24, E'Cls', E'Closer', E'Lunatiker');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (25, E'Ass', E'Assassin', E'Assassine');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (26, E'Sen', E'Sentinel', E'Kreuzritter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (27, E'Sav', E'Savior', E'Tempelritter');
