--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4 (Debian 13.4-1.pgdg100+1)
-- Dumped by pg_dump version 13.4 (Debian 13.4-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: store_item; Type: SCHEMA; Schema: -; Owner: docker
--

CREATE SCHEMA store_item;


ALTER SCHEMA store_item OWNER TO docker;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: store_category; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_category (
    id bigint NOT NULL,
    name character varying(255),
    type integer
);


ALTER TABLE store_item.store_category OWNER TO docker;

--
-- Name: store_category_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_category_id_seq OWNER TO docker;

--
-- Name: store_category_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_category_id_seq OWNED BY store_item.store_category.id;


--
-- Name: store_item; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_item (
    id bigint NOT NULL,
    duration smallint NOT NULL,
    link text,
    sc bigint NOT NULL,
    item_info bigint,
    store_item_view bigint,
    store_item_set bigint
);


ALTER TABLE store_item.store_item OWNER TO docker;

--
-- Name: store_item_category; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_item_category (
    category_id bigint NOT NULL,
    item_id bigint NOT NULL
);


ALTER TABLE store_item.store_item_category OWNER TO docker;

--
-- Name: store_item_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_item_id_seq OWNER TO docker;

--
-- Name: store_item_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_item_id_seq OWNED BY store_item.store_item.id;


--
-- Name: store_item_image; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_item_image (
    id bigint NOT NULL,
    filename character varying(255),
    store_item_view bigint NOT NULL
);


ALTER TABLE store_item.store_item_image OWNER TO docker;

--
-- Name: store_item_image_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_item_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_item_image_id_seq OWNER TO docker;

--
-- Name: store_item_image_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_item_image_id_seq OWNED BY store_item.store_item_image.id;


--
-- Name: store_item_set; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_item_set (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE store_item.store_item_set OWNER TO docker;

--
-- Name: store_item_set_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_item_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_item_set_id_seq OWNER TO docker;

--
-- Name: store_item_set_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_item_set_id_seq OWNED BY store_item.store_item_set.id;


--
-- Name: store_item_view; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_item_view (
    id bigint NOT NULL,
    description text,
    icon character varying(255),
    name character varying(255)
);


ALTER TABLE store_item.store_item_view OWNER TO docker;

--
-- Name: store_item_view_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_item_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_item_view_id_seq OWNER TO docker;

--
-- Name: store_item_view_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_item_view_id_seq OWNED BY store_item.store_item_view.id;


--
-- Name: store_category id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_category ALTER COLUMN id SET DEFAULT nextval('store_item.store_category_id_seq'::regclass);


--
-- Name: store_item id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item ALTER COLUMN id SET DEFAULT nextval('store_item.store_item_id_seq'::regclass);


--
-- Name: store_item_image id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_image ALTER COLUMN id SET DEFAULT nextval('store_item.store_item_image_id_seq'::regclass);


--
-- Name: store_item_set id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_set ALTER COLUMN id SET DEFAULT nextval('store_item.store_item_set_id_seq'::regclass);


--
-- Name: store_item_view id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_view ALTER COLUMN id SET DEFAULT nextval('store_item.store_item_view_id_seq'::regclass);


--
-- Name: store_category store_category_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_category
    ADD CONSTRAINT store_category_pkey PRIMARY KEY (id);


--
-- Name: store_item_category store_item_category_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_category
    ADD CONSTRAINT store_item_category_pkey PRIMARY KEY (category_id, item_id);


--
-- Name: store_item_image store_item_image_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_image
    ADD CONSTRAINT store_item_image_pkey PRIMARY KEY (id);


--
-- Name: store_item store_item_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item
    ADD CONSTRAINT store_item_pkey PRIMARY KEY (id);


--
-- Name: store_item_set store_item_set_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_set
    ADD CONSTRAINT store_item_set_pkey PRIMARY KEY (id);


--
-- Name: store_item_view store_item_view_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_view
    ADD CONSTRAINT store_item_view_pkey PRIMARY KEY (id);


--
-- Name: store_item_image fk3rv32c70o4by98g0hssq27745; Type: FK CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_image
    ADD CONSTRAINT fk3rv32c70o4by98g0hssq27745 FOREIGN KEY (store_item_view) REFERENCES store_item.store_item_view(id);


--
-- Name: store_item_category fkf6sdyk69bfcq32bl5l6pbig1c; Type: FK CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_category
    ADD CONSTRAINT fkf6sdyk69bfcq32bl5l6pbig1c FOREIGN KEY (item_id) REFERENCES store_item.store_category(id);


--
-- Name: store_item fkiqjay0mumpjgwifjhmgjhn4bh; Type: FK CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item
    ADD CONSTRAINT fkiqjay0mumpjgwifjhmgjhn4bh FOREIGN KEY (store_item_view) REFERENCES store_item.store_item_view(id);


--
-- Name: store_item fklgggo0xhvjpex3qsw0kcmscko; Type: FK CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item
    ADD CONSTRAINT fklgggo0xhvjpex3qsw0kcmscko FOREIGN KEY (item_info) REFERENCES public.item_info(id);


--
-- Name: store_item fkpro0l8ky77xbw4vg0y1c4xqrq; Type: FK CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item
    ADD CONSTRAINT fkpro0l8ky77xbw4vg0y1c4xqrq FOREIGN KEY (store_item_set) REFERENCES store_item.store_item_set(id);


--
-- Name: store_item_category fkqowjiqhr9jay2emuk2ppvjr3v; Type: FK CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_category
    ADD CONSTRAINT fkqowjiqhr9jay2emuk2ppvjr3v FOREIGN KEY (category_id) REFERENCES store_item.store_item(id);


--
-- PostgreSQL database dump complete
--

