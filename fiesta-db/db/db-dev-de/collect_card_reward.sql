CREATE TABLE "public"."collect_card_reward"(
 CC_RewardID  int,
 CC_CardRewardType  bigint,
 CC_CardLot  int,
 CC_RewardItemInx  VARCHAR(2000),
 CC_RewardLot  int
);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (1, 1, 14, E'Neck_Collect01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (2, 2, 28, E'Ear_Collect01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (3, 3, 35, E'Ring_Collect01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (4, 4, 48, E'Ring_Collect02', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (5, 1, 28, E'Back_Collect01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (6, 2, 57, E'Cos_Collect01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (7, 3, 71, E'Hat_Collect01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (8, 4, 97, E'M_Collect_Coll', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (8, 4, 97, E'M_Collect_WhiteColl', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (8, 4, 97, E'M_Collect_Hobby', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (8, 4, 97, E'M_Collect_WhiteHobby', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (9, 0, 253, E'MiniHelga01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (9, 0, 253, E'MiniHumar01', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (10, 0, 334, E'MiniAstanica', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (11, 1, 39, E'MiniFreloan', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (12, 2, 81, E'Cos_CollectS02', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (13, 3, 91, E'Hat_CollectS02', 1);
INSERT INTO "public"."collect_card_reward"(CC_RewardID, CC_CardRewardType, CC_CardLot, CC_RewardItemInx, CC_RewardLot) VALUES (14, 4, 123, E'Back_CollectS02', 1);
