CREATE TABLE "public"."class_name"(
 ClassID  smallint UNIQUE PRIMARY KEY,
 acPrefix  VARCHAR(2000),
 acEngName  VARCHAR(2000),
 acLocalName  VARCHAR(2000)
);
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (0, E'-', E'-', E'-');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (1, E'Fig', E'Fighter', E'Fighter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (2, E'Cfi', E'CleverFighter', E'CleverFighter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (3, E'War', E'Warrior', E'Warrior');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (4, E'Gla', E'Gladiator', E'Gladiator');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (5, E'Kni', E'Knight', E'Knight');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (6, E'Cle', E'Cleric', E'Cleric');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (7, E'Hcl', E'HighCleric', E'HighCleric');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (8, E'Pal', E'Paladin', E'Paladin');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (9, E'Hol', E'HolyKnight', E'HolyKnight');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (10, E'Gua', E'Guardian', E'Guardian');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (11, E'Arc', E'Archer', E'Archer');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (12, E'Har', E'HawkArcher', E'HawkArcher');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (13, E'Sco', E'Scout', E'Scout');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (14, E'Sha', E'SharpShooter', E'SharpShooter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (15, E'Ran', E'Ranger', E'Ranger');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (16, E'Mag', E'Mage', E'Mage');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (17, E'Wma', E'WizMage', E'WizMage');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (18, E'Enc', E'Enchanter', E'Enchanter');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (19, E'War', E'Warlock', E'Warlock');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (20, E'Wiz', E'Wizard', E'Wizard');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (21, E'Jok', E'Joker', E'Trickster');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (22, E'Chs', E'Chaser', E'Gambit');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (23, E'Cru', E'Cruel', E'Renegade');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (24, E'Cls', E'Closer', E'Spectre');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (25, E'Ass', E'Assassin', E'Reaper');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (26, E'Sen', E'Sentinel', E'Crusader');
INSERT INTO "public"."class_name"(ClassID, acPrefix, acEngName, acLocalName) VALUES (27, E'Sav', E'Savior', E'Templar');
