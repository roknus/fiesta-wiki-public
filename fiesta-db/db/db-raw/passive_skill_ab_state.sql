CREATE TABLE "public"."passive_skill_ab_state"(
 unique_id  int UNIQUE PRIMARY KEY,
 PS_InxName  VARCHAR(2000) UNIQUE,
 PS_Condition  bigint,
 PS_ConditioRate  int,
 PS_AbStateInx  VARCHAR(2000),
 Strength  smallint,
 ab_state_fk  int
);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (1, E'MagicDance01', 1, 1000, E'StaAimUp', 1, 881);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (2, E'MagicDance02', 1, 1000, E'StaAimUp', 2, 881);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (3, E'MagicDance03', 1, 1000, E'StaAimUp', 3, 881);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (4, E'MagicDance04', 1, 1000, E'StaAimUp', 4, 881);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (5, E'DeepFear01', 2, 1000, E'StaDeepFearMenDownRate01', 1, 505);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (6, E'DeepFear02', 2, 1000, E'StaDeepFearMenDownRate01', 2, 505);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (7, E'DeepFear03', 2, 1000, E'StaDeepFearMenDownRate01', 3, 505);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (8, E'DeepFear04', 2, 1000, E'StaDeepFearMenDownRate01', 4, 505);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (9, E'Shame01', 3, 1000, E'StaShameCRIUp01', 1, 507);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (10, E'Shame02', 3, 1000, E'StaShameCRIUp02', 1, 525);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (11, E'Shame03', 3, 1000, E'StaShameCRIUp03', 1, 526);
INSERT INTO "public"."passive_skill_ab_state"(unique_id, PS_InxName, PS_Condition, PS_ConditioRate, PS_AbStateInx, Strength, ab_state_fk) VALUES (12, E'Shame04', 3, 1000, E'StaShameCRIUp04', 1, 527);
