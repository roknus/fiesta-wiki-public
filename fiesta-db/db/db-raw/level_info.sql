CREATE TABLE level_info
(
  level int NOT NULL,
  xp_required bigint,
  xp_total bigint,
  CONSTRAINT level_info_pkey PRIMARY KEY (level)
);

\copy level_info(level, xp_total, xp_required) FROM '/docker-entrypoint-initdb.d/csv/level_info.csv' DELIMITER ',' CSV HEADER;