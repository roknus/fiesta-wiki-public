CREATE TABLE "public"."multi_hit_type"(
 unique_id  int UNIQUE PRIMARY KEY,
 ID  int,
 HitTime  int,
 AbIndex  VARCHAR(2000),
 AS_Step  smallint,
 AbStr  smallint,
 AbRate  int,
 DmgRate  int,
 abstate_fk  int
);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (1, 1, 500, null, 0, 0, 0, 1000, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (2, 1, 767, null, 0, 0, 0, 1000, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (3, 2, 433, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (4, 2, 633, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (5, 2, 833, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (6, 2, 1000, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (7, 2, 1233, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (8, 3, 267, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (9, 3, 533, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (10, 3, 733, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (11, 3, 967, null, 0, 0, 0, 150, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (12, 3, 1067, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (13, 4, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (14, 4, 1233, E'StaEvasionDown', 0, 1, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (15, 4, 1367, E'StaFitBlood', 0, 11, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (16, 5, 233, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (17, 5, 466, null, 0, 0, 0, 600, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (18, 6, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (19, 6, 533, E'StaNortfBurn', 0, 1, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (20, 7, 133, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (21, 7, 533, E'StaPoisonDmgAdd', 0, 1, 1000, 500, 364);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (22, 8, 366, E'StaIceField', 0, 0, 0, 200, 99);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (23, 8, 933, null, 0, 0, 0, 100, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (24, 8, 2100, null, 0, 0, 0, 300, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (25, 8, 2733, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (26, 8, 2700, null, 0, 0, 0, 600, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (27, 9, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (28, 9, 1233, E'StaEvasionDown', 0, 2, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (29, 9, 1367, E'StaFitBlood', 0, 12, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (30, 10, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (31, 10, 1233, E'StaEvasionDown', 0, 3, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (32, 10, 1367, E'StaFitBlood', 0, 13, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (33, 11, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (34, 11, 1233, E'StaEvasionDown', 0, 4, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (35, 11, 1367, E'StaFitBlood', 0, 14, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (36, 12, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (37, 12, 533, E'StaNortfBurn', 0, 2, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (38, 13, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (39, 13, 533, E'StaNortfBurn', 0, 3, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (40, 14, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (41, 14, 533, E'StaNortfBurn', 0, 4, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (42, 15, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (43, 15, 533, E'StaNortfBurn', 0, 5, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (44, 16, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (45, 16, 533, E'StaNortfBurn', 0, 6, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (46, 17, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (47, 17, 533, E'StaNortfBurn', 0, 7, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (48, 18, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (49, 18, 533, E'StaNortfBurn', 0, 8, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (50, 19, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (51, 19, 533, E'StaNortfBurn', 0, 9, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (52, 20, 100, null, 0, 0, 0, 300, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (53, 20, 400, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (54, 20, 667, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (55, 21, 200, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (56, 21, 400, null, 0, 0, 0, 600, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (57, 22, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (58, 22, 533, E'StaNortfBurn', 0, 10, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (59, 23, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (60, 23, 533, E'StaNortfBurn', 0, 11, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (61, 24, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (62, 24, 533, E'StaNortfBurn', 0, 12, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (63, 25, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (64, 25, 533, E'StaNortfBurn', 0, 13, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (65, 26, 733, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (66, 26, 1433, E'StaGroundStrike', 0, 1, 1000, 500, 500);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (67, 27, 10, E'StaMeteor', 0, 1, 1000, 0, 503);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (68, 27, 1000, null, 0, 1, 1000, 0, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (69, 27, 2000, null, 0, 1, 1000, 0, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (70, 27, 3000, null, 0, 1, 1000, 0, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (71, 27, 4000, null, 0, 0, 0, 1000, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (72, 28, 100, null, 0, 0, 0, 0, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (73, 29, 4000, E'StaKnockBackRoll', 1, 2, 1000, 500, 356);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (74, 29, 5000, E'StaKnockBackRoll', 2, 2, 1000, 500, 356);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (75, 29, 5466, E'StaKnockBackRoll', 3, 2, 1000, 500, 356);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (76, 29, 5933, E'StaKnockBackRoll', 4, 2, 1000, 500, 356);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (77, 29, 6333, E'StaKnockBackRoll', 5, 2, 1000, 1000, 356);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (78, 30, 2000, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (79, 30, 3000, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (80, 30, 5000, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (81, 30, 6000, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (82, 30, 8000, null, 0, 0, 0, 1000, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (83, 31, 1533, null, 1, 0, 0, 1000, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (84, 31, 3300, E'StaCommonStun06', 2, 1, 1000, 0, 319);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (85, 32, 1533, null, 1, 0, 0, 1000, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (86, 32, 1966, E'StaKnockBack', 2, 1, 1000, 0, 354);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (87, 33, 766, E'StaKahalBLD', 1, 1, 1000, 333, 693);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (88, 33, 1600, null, 2, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (89, 33, 2166, null, 3, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (90, 34, 833, null, 1, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (91, 34, 1033, null, 2, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (92, 34, 1400, null, 3, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (93, 35, 333, null, 1, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (94, 35, 633, null, 2, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (95, 35, 1000, null, 3, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (96, 36, 2766, null, 1, 0, 0, 250, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (97, 36, 3166, null, 2, 0, 0, 250, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (98, 36, 3500, null, 3, 0, 0, 250, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (99, 36, 4366, null, 4, 0, 0, 250, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (100, 37, 1233, null, 1, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (101, 37, 1500, null, 2, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (102, 38, 1133, null, 0, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (103, 38, 1500, null, 0, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (104, 38, 2133, null, 0, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (105, 39, 1000, null, 1, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (106, 39, 1166, null, 2, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (107, 39, 1300, null, 3, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (108, 40, 1066, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (109, 40, 1300, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (110, 40, 1533, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (111, 40, 1733, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (112, 40, 1933, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (113, 40, 2166, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (114, 40, 2366, null, 0, 0, 0, 143, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (115, 41, 766, null, 1, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (116, 41, 933, null, 2, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (117, 41, 1100, null, 3, 0, 0, 333, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (118, 42, 1433, null, 1, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (119, 42, 1566, null, 2, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (120, 42, 1733, null, 3, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (121, 42, 1900, null, 4, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (122, 42, 2066, null, 5, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (123, 43, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (124, 43, 533, E'StaNortfBurn', 0, 14, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (125, 44, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (126, 44, 533, E'StaNortfBurn', 0, 15, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (127, 45, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (128, 45, 533, E'StaNortfBurn', 0, 16, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (129, 46, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (130, 46, 533, E'StaNortfBurn', 0, 17, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (131, 47, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (132, 47, 533, E'StaNortfBurn', 0, 18, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (133, 48, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (134, 48, 533, E'StaNortfBurn', 0, 19, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (135, 49, 100, null, 0, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (136, 49, 533, E'StaNortfBurn', 0, 20, 1000, 600, 362);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (137, 50, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (138, 50, 1233, E'StaEvasionDown', 0, 5, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (139, 50, 1367, E'StaFitBlood', 0, 15, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (140, 51, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (141, 51, 1233, E'StaEvasionDown', 0, 6, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (142, 51, 1367, E'StaFitBlood', 0, 16, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (143, 52, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (144, 52, 1233, E'StaEvasionDown', 0, 7, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (145, 52, 1367, E'StaFitBlood', 0, 17, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (146, 53, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (147, 53, 1233, E'StaEvasionDown', 0, 8, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (148, 53, 1367, E'StaFitBlood', 0, 18, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (149, 54, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (150, 54, 1233, E'StaEvasionDown', 0, 9, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (151, 54, 1367, E'StaFitBlood', 0, 19, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (152, 55, 1100, null, 0, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (153, 55, 1233, E'StaEvasionDown', 0, 10, 1000, 200, 369);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (154, 55, 1367, E'StaFitBlood', 0, 20, 1000, 200, 360);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (155, 56, 667, E'StaMobSTN01', 0, 1, 1000, 300, 493);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (156, 56, 1000, null, 0, 0, 0, 300, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (157, 56, 1533, null, 0, 0, 0, 300, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (158, 57, 1700, null, 1, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (159, 57, 2200, null, 2, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (160, 57, 2466, null, 3, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (161, 57, 2666, null, 4, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (162, 57, 2833, null, 5, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (163, 58, 500, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (164, 58, 900, null, 0, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (165, 59, 566, null, 0, 0, 0, 300, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (166, 59, 833, null, 1, 0, 0, 200, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (167, 59, 1533, null, 2, 0, 0, 500, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (168, 60, 1766, null, 0, 0, 0, 100, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (169, 60, 1900, null, 1, 0, 0, 100, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (170, 60, 2000, null, 2, 0, 0, 100, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (171, 60, 2450, null, 3, 0, 0, 300, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (172, 60, 2500, null, 4, 0, 0, 400, null);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (173, 62, 3233, E'StaSDVale01_BRN', 1, 1, 1000, 100, 762);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (174, 62, 3233, E'StaSDVale01_Knock', 1, 1, 1000, 0, 764);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (175, 62, 3366, E'StaSDVale01_BRN', 2, 1, 1000, 300, 762);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (176, 62, 3366, E'StaSDVale01_Knock', 2, 1, 1000, 0, 764);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (177, 62, 3500, E'StaSDVale01_BRN', 3, 1, 1000, 300, 762);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (178, 62, 3500, E'StaSDVale01_Knock', 3, 1, 1000, 0, 764);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (179, 62, 3633, E'StaSDVale01_BRN', 4, 1, 1000, 300, 762);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (180, 62, 3633, E'StaSDVale01_Knock', 4, 1, 1000, 0, 764);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (181, 62, 3766, E'StaSDVale01_BRN', 5, 1, 1000, 300, 762);
INSERT INTO "public"."multi_hit_type"(unique_id, ID, HitTime, AbIndex, AS_Step, AbStr, AbRate, DmgRate, abstate_fk) VALUES (182, 62, 3766, E'StaSDVale01_Knock', 5, 1, 1000, 0, 764);
