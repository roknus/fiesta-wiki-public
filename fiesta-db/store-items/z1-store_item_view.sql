--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12.3 (Debian 12.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: store_item_view; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_item_view (
    id bigint NOT NULL,
    description text,
    icon character varying(255),
    name character varying(255)
);


ALTER TABLE store_item.store_item_view OWNER TO docker;

--
-- Name: store_item_view_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_item_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_item_view_id_seq OWNER TO docker;

--
-- Name: store_item_view_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_item_view_id_seq OWNED BY store_item.store_item_view.id;


--
-- Name: store_item_view id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_view ALTER COLUMN id SET DEFAULT nextval('store_item.store_item_view_id_seq'::regclass);


--
-- Data for Name: store_item_view; Type: TABLE DATA; Schema: store_item; Owner: docker
--

INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (1, 'A special Valentine''s edition of the classic crown.', 'red-crown-thumbnail.jpg', 'Red Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (2, 'A special Valentine''s edition of the classic crown.', 'pink-crown-thumbnail.jpg', 'Pink Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (3, 'Cute and cuddly, this cow is sure to turn heads!', 'daisy-the-cow-thumbnail.jpg', 'Daisy the Cow');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (4, 'Look like the KING in this spiffy hair-do!', 'elvis-hair-thumbnail.jpg', 'Elvis hair');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (5, 'Wear your heart on your head with these festive antenna.', 'heart-antenna-thumbnail.jpg', 'Heart Antenna');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (6, 'Style your hat with some hearts!', 'heart-fedora-thumbnail.jpg', 'Heart''s Fedora');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (7, 'The crown jewel of the love season. Stand out from everyone else with this shiny crown.', 'heart-jewel-thumbnail.jpg', 'Heart''s Jewel Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (8, 'With a playful personality and a crazy sense of humor, jesters create mayhem wherever they go.', 'jester-hat-thumbnail.jpg', 'Jester Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (9, 'Ever wonder how things would sound if you had the ears of an Adealian? Now you don''t have to!', 'adealian-ears-thumbnail.jpg', 'Adealian Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (10, 'Do my ears make me look more beautiful?', 'red-panda-ears-thumbnail.jpg', 'Red Panda Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (11, 'The mask of a dragon.', 'green-dragon-mask-thumbnail.jpg', 'Green Dragon Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (12, 'A loyal and energetic puppy pal! Be sure to have some tasty treats on hand at all times.', 'rosco-the-dog-thumbnail.jpg', 'Rosco the Dog');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (13, 'You''re an adorable puppy!', 'puppy-ears-thumbnail.jpg', 'Puppy Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (14, 'Keeps your hair out of your face while swashbuckling.', 'pirate-bandana-thumbnail.jpg', 'Pirate Bandana');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (15, 'Traditional Dragon horns.', 'dragon-horns-thumbnail.jpg', 'Dragon Horns');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (16, 'Traditional Chinese Hair. A unique hair style for you to wear and show your friends.', 'traditional-chinese-hair-thumbnail.jpg', 'Traditional Chinese Hair');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (17, 'Traditional Chinese Hat. Wear this hat and remember the ancient times in China.', 'traditional-chinese-hat-thumbnail.jpg', 'Traditional Chinese Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (18, 'Traditional Korean Hat. Wear this hat and remember the ancient times in Korea.', 'traditional-korean-hat-thumbnail.jpg', 'Traditional Korean Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (19, 'Wiggle your ears!', 'panda-head-thumbnail.jpg', 'Panda Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (20, 'No voyage is complete without a Mara Hat', 'mara-hat-thumbnail.jpg', 'Mara Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (21, 'A ninja is not complete without a Ninja Hood', 'ninja-hood-thumbnail.jpg', 'Ninja Hood');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (22, 'Show everyone that the doctor is here.', 'doctors-hat-thumbnail.jpg', 'Doctor''s Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (23, 'Protect yourself from the sun and rain during your journey.', 'rice-hat-thumbnail.jpg', 'Rice Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (25, 'Complete your Meowlicious Outfit with this pink hood.', 'pink-meowlicious-hood-thumbnail.jpg', 'Pink Meowlicious Hood');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (26, 'Spread your love with this awesome accessory.', 'hearts-hat-thumbnail.jpg', 'Hearts Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (27, 'Listen to music while killing evil monsters with these fantastic Heart Headphones.', 'heart-headphones-thumbnail.png', 'Heart Headphones');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (28, 'Listen to music while killing evil monsters with these fantastic Panda Headphones.', 'panda-headphones-thumbnail.png', 'Panda Headphones');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (29, 'Feel like a queen / a king', 'silver-crown-thumbnail.jpg', 'Silver Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (30, 'The feather is there for a reason! Be a musketeer with this fashionable hat.', 'muketeer-hat-thumbnail.jpg', 'Musketeer Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (31, NULL, 'baphomet-helm-thumbnail.jpg', 'Baphomet Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (32, 'Check the power level of your target with these enhanced glasses.', 'isya-tech-glasses-thumbnail.jpg', 'Isya Tech Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (33, 'Can never be too careful out there.', 'explorer-goggles-thumbnail.jpg', 'Explorer''s Goggles');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (34, 'Look smart in these Black Glasses', 'nerd-glasses-thumbnail.jpg', 'Nerd Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (35, 'The perfect look to plot those dastardly deeds.', 'villainous-moustache-thumbnail.jpg', 'Villainous Moustache');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (36, 'Ooh la la! You''ll definitely have that look of love in your eyes when you''re rocking these awesome glasses!', 'heart-shades-thumbnail.jpg', 'Heart Shades');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (37, 'The newest in Neo Isyan eyewear.', 'iGlasses-3000-mk1-thumbnail.jpg', 'iGlasses 3000 mk1');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (38, 'Look smart in these Black Glasses', 'black-glasses-thumbnail.jpg', 'Black Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (39, 'Unlucky prey will fear your style just as much as your weapon.', 'hunter-glasses-thumbnail.jpg', 'Hunter Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (40, 'Look cool in these Neo Sunglasses', 'neo-sunglasses-thumbnail.jpg', 'Neo Sunglasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (41, 'This mask will sure keep you in the shadows and prevent your foes from finding you.', 'shadow-fox-mask-thumbnail.jpg', 'Shadow Fox Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (42, 'Look sharp in these Shiny Glasses', 'shiny-glasses-thumbnail.jpg', 'Shiny Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (43, 'By using these ice cream cones as glasses, I can see 10 times further.', 'freezie-glasses-thumbnail.jpg', 'Freezie Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (44, 'Look crazy in these Dizzy Glasses', 'dizzy-glasses-thumbnail.png', 'Dizzy Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (45, 'Are these hip or vintage? You decide!', 'vintage-glasses-thumbnail.jpg', 'Vintage Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (46, 'The disguise of evil.', 'demon-mask-thumbnail.jpg', 'Demon Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (47, 'Can you see the light?', 'holy-light-mask-thumbnail.jpg', 'Holy Light Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (48, 'Set your sights on your partner across the ballroom.', 'masquerade-mask-thumbnail.jpg', 'Masquerade Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (49, 'I feel really bug-eyed wearing these sunglasses.', 'buggy-sunglasses-thumbnail.jpg', 'Buggy''s Sunglasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (50, 'A traditional outfit that gives great defensive bonuses.', 'black-kimono-pack-thumbnail.jpg', 'Black Kimono Pack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (51, 'Show that special someone how much you care.', 'couples-shirt-thumbnail.jpg', 'Couples Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (52, 'An elegant tribute to the wonders and mysteries of the Eastern world.', 'crimson-silks-thumbnail.jpg', 'Crimson Silks');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (53, 'You can''t see me!', 'invisible-suit-thumbnail.jpg', 'Invisible Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (54, 'Bestow honor to Cupid, the god of desire, affection and love. As a son of Venus and Mars, he unifies the power of love and strength.', 'cupid-desire-thumbnail.jpg', 'Cupid''s Desire');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (55, 'Beautiful and serene.', 'sanguine-rose-thumbnail.jpg', 'Sanguine Rose');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (56, 'Let the sweet song of the alps and the fresh air inspire you to greatness.', 'alpine-melody-thumbnail.jpg', 'Alpine Melody');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (57, 'Go coo-coo for coconuts!', 'colada-costume-thumbnail.jpg', 'Colada Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (58, 'Fight for Justice!', 'dark-knight-thumbnail.jpg', 'Dark Knight');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (59, 'Do you have a hidden crush? This outfit might help you stand out.', 'hidden-crush-thumbnail.jpg', 'Hidden Crush');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (60, 'Ha Ha Ha!', 'joker-costume-thumbnail.jpg', 'Joker Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (247, NULL, NULL, 'Bandana of Wisdom');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (61, 'The garbs that were once worn by the disciples of Neptune.', 'neptune-abyss-thumbnail.jpg', 'Neptune''s Abyss');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (62, 'A formal red Tuxedo/Wine Dress', 'red-tuxedo-dress-thumbnail.jpg', 'Red Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (63, 'A renegade glides silently through the night, awaiting the perfect time to strike.', 'scarlet-assassin-thumbnail.jpg', 'Scarlet Assassin');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (64, 'Trained by the elusive Shadow Fox, show off your cunning skills while looking the part.', 'shadow-fox-costume-thumbnail.jpg', 'Shadow Fox Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (65, 'It''s tea time Govna!', 'silky-maid-suit-thumbnail.jpg', 'Silky Maid/Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (66, 'Yawn, I''m getting sleepy.... This costume was designed by Baktwerel, one of our talented Artist Corner members!', 'slumber-party-thumbnail.jpg', 'Slumber Party');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (67, 'Give your enemies the cold shoulder.', 'ice-wings-thumbnail.jpg', 'Ice Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (68, 'Music will give you wings!', 'music-wing-thumbnail.png', 'Music Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (69, 'I went to the ice cream store and the owner slapped an ice cream cone on my forehead.', 'freezie-horn-thumbnail.jpg', 'Freezie Horn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (70, 'Before starting any battle, make sure to have a hearty breakfast!', 'breakfast-of-vikings-thumbnail.jpg', 'Breakfast of Vikings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (71, 'Protect your eyes from the sun and stay full with some delicious pancakes!', 'pancake-hat-thumbnail.jpg', 'Pancake Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (72, 'Feel like a queen / a king', 'black-crown-thumbnail.jpg', 'Black Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (73, 'A royal costume fit for a King or Queen.', 'fiesta-royal-costume-thumbnail.jpg', 'Fiesta Royal Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (74, 'A majestic bow that shines as bright as a star.', 'majestic-bow-thumbnail.png', 'Majestic Bow');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (75, 'Whether you''re dressing up for a night on the town with friends or a romantic first date, this classic outfit is sure to add an elegant touch to your evening.', 'midnight-gala-thumbnail.jpg', 'Midnight Gala');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (76, 'A costume that puts you in a wonderland full of joy.', 'wonderland-costume-thumbnail.jpg', 'Wonderland Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (77, 'A formal Light Blue Tuxedo/Wine Dress.', 'light-blue-tuxedo-dress-thumbnail.jpg', 'Light Blue Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (78, 'These spider legs have got your back.', 'creepy-crawly-legs-thumbnail.png', 'Creepy Crawly Legs');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (79, 'When it comes to weather, it''s always good to be prepared for the worst.', NULL, 'Yellow Umbrella');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (80, 'Wings of the Yellow Dragon.', NULL, 'Yellow Dragon Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (81, NULL, NULL, 'Wintertide Sack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (82, 'Spread your wings fly into the welcoming darkness.', NULL, 'Wing''s of Darkness');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (83, NULL, NULL, 'Wind-Up Key');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (84, NULL, NULL, 'Whistle');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (85, 'Once wielded by heroes long forgotten, these weapons sheathe a dormant power, waiting to be tapped. The bearers only need wear them on their backs.', NULL, 'Twin Sealed Swords');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (86, 'Harness the patience and power of the tortoise.', NULL, 'Tortoise Shell');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (87, NULL, NULL, 'Tasty Burger');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (88, 'Be prepared for every meal by having your utensils on your back.', NULL, 'Tasteful Delight');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (89, NULL, NULL, 'Supporter Hand');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (90, 'The sky will no longer be the limit with these wings!', NULL, 'Starshine Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (91, 'Don''t dive under using this awesome Starfish swim support.', NULL, 'Honeying Tube');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (92, 'A symbol from the Heavens.', NULL, 'Star Emblem');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (93, 'Enlighten your day with these beautiful wings!', NULL, 'Sparkle Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (94, 'Even in death, the dragon inspires fear in its enemies.', NULL, 'Skeleton Dragon Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (95, NULL, NULL, 'Silver Tennis Racquet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (96, NULL, NULL, 'Golden Tennis Racquet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (97, NULL, NULL, 'Blue Tennis Racquet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (98, 'Legend says that these wings were made out of pure luck essence.', NULL, 'Shamrock Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (99, 'Ignite your inner passions and surrender to the darkness.', NULL, 'Shadowflame Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (100, NULL, NULL, 'Shadow Dagger');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (101, NULL, NULL, 'Seal of Neptune');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (102, NULL, NULL, 'Seal of Aquarius');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (103, NULL, NULL, 'Sealed Sword');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (104, NULL, NULL, 'Sealed Bow');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (105, 'Warm bunny, fluffy bunny, little Master Hare. Shy bunny, scaredy bunny, you can hide in here.', NULL, 'Scaredy Bunny');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (106, NULL, NULL, 'Santa''s Bag');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (107, NULL, NULL, 'Santa''s Magic Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (108, 'A special blade stolen from the grim reaper.', NULL, 'Reaper Scythe');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (109, NULL, NULL, 'Rainbow Snowboard');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (110, 'Boost your defense and stay alive in the Isyan wilds.', NULL, 'Protective Ornaments');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (111, 'Hey! Who did that?!', NULL, 'Playful Prank');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (112, 'Is your outfit lacking a magical touch? No problem! These pixie flames will spice up any of your fabulous ensembles. Let the mysterious mist surround you!', NULL, 'Pixie Flame');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (113, NULL, NULL, 'Piney Wreath');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (114, NULL, NULL, 'Panda Pack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (115, NULL, NULL, 'Ninja Star');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (116, NULL, NULL, 'Neko Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (117, NULL, NULL, 'Mystic Snowflake');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (118, NULL, NULL, 'Musketeer Noise Maker');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (120, NULL, NULL, 'Monarch Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (121, 'Mmm, freshly delivered. Now to find some chocolate!', NULL, 'Milk Bottle');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (122, NULL, NULL, 'Merry Kebing');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (123, 'Share your passion for cats with the world of Isya!', NULL, 'Meow Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (124, 'We will rule the skies with these mechanical wings!', NULL, 'Mechanical Flight');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (125, NULL, NULL, 'Meadow Fairy Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (126, NULL, NULL, 'Master Hare''s Brush');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (127, 'Bewitch your friend with this heart-shaped broom.', NULL, 'Love Witch Broom');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (128, NULL, NULL, 'Lovesick Potion');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (129, 'Send your love some wings to show your affection.', NULL, 'Lover''s Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (130, 'Show the world that you are in love with the help of these balloons.', NULL, 'Love Balloons');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (131, 'Bring honour on the battlefield with the legendary twin swords.', NULL, 'Legendary Twin Swords');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (132, NULL, NULL, 'Kittybuster2000');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (133, 'An adorable Karen to take on all your travels!', NULL, 'Karen Plushie Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (134, NULL, NULL, 'Isya Tech Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (135, NULL, NULL, 'Isya''s Fries');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (136, NULL, NULL, 'Idol Snowboard');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (137, 'Float like a heart, sting like a honeying.', NULL, 'Helium Hearts');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (138, NULL, NULL, 'Golden Key');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (139, NULL, NULL, 'Golden Fan');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (140, 'Unicorns are awesome! Spread your wings and chase the rainbow.', NULL, 'Glory Unicorn Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (141, 'Watch your back for Slimes with a sweet tooth!', NULL, 'Giant Candy Cane');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (142, 'One part wing, one part machine, two parts awesome.', NULL, 'Future Angel Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (143, 'Unicorns are awesome! Spread your wings and chase the rainbow.', NULL, 'Fluffy Unicorn Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (144, NULL, NULL, 'First Aid Kit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (248, NULL, NULL, 'Bat Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (145, 'Always be prepared with your snowboard strapped to your back.', NULL, 'Fiesta Snowboard');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (146, 'Be as light as a fairy with these wings. Now you''re harder to catch!', NULL, 'Fairy Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (147, 'A magical fairy potion as a backpack!', NULL, 'Fairy Potion Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (148, 'An explorer never leaves home without this backpack!', NULL, 'Explorer''s Sack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (149, 'Never forget your skills with this evil grimoire!', NULL, 'Evil Eye Grimoire');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (150, 'Some says that this bag can cary a lot of gifts.', NULL, 'Elven Magic Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (151, NULL, NULL, 'Devious Dagger');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (152, 'Rise from the ashes with this wings.', NULL, 'Devil Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (153, NULL, NULL, 'Devil Unicorn Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (154, NULL, NULL, 'Devil''s Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (155, 'Poke your enemies to death!', NULL, 'Devilish Pitchfork');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (156, 'Wear your favourite candy on your back!', NULL, 'Delicious Candy');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (157, 'Bury thine enemies.', NULL, 'Dark Horse''s Coffin');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (158, NULL, NULL, 'Dark Cross');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (159, NULL, NULL, 'Cupid''s Arrow');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (160, NULL, NULL, 'Crimson Sealed Swords');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (161, 'Be a corrupt angel!', NULL, 'Corrupt Angel Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (162, 'Carry the time around.', NULL, 'Clockwork Watch');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (163, 'A faded roll of parchment, full of secrets waiting to be uncovered.', NULL, 'Clan Scroll');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (164, NULL, NULL, 'Christmas Icy Star');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (165, 'Let the melodious sounds whisk you away to the Emerald Isle.', NULL, 'Celtic Harp');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (166, 'Always keep a steady hand on the wheel!', NULL, 'Captain''s Wheel');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (167, 'Always be prepared with your snowboard strapped to your back.', NULL, 'Buzzard Beak Snowboard');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (168, NULL, NULL, 'Butterfly Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (169, 'Keep your easter eggs safe in the bunny backpack.', NULL, 'Bunny Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (170, NULL, NULL, 'Bountiful Cornucopia');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (171, NULL, NULL, 'Blue Dragon Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (172, NULL, NULL, 'Bernie the Cursed Bear');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (173, 'A beautiful bouquet of flowers to help bring in the start of spring and fresh smells.', NULL, 'Beautiful Bouquet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (174, NULL, NULL, 'Bat Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (175, NULL, NULL, 'Archeologist''s Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (176, NULL, NULL, 'Antidote');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (177, 'Gives you the power you need for the afterlife.', NULL, 'Ankh of Power');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (178, 'You''re an angel!', NULL, 'Angel Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (179, NULL, NULL, 'Ama Spears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (180, 'Drifter or Adventurer?', NULL, 'Adventurer Backpack');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (181, NULL, NULL, 'Adealian Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (182, 'Pierce the heart of your enemy with Aby''s death stare.', NULL, 'Aby the Cursed Bunny');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (183, NULL, NULL, 'Adealian Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (184, 'A big fluffy tail, perfect for staying warm on those cold winter nights.', NULL, 'Arctic Fox Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (185, 'Meow! Be a cute Cat with this cat tail!', NULL, 'Black Cat Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (186, 'A Devil''s Tail.', NULL, 'Devil''s Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (187, NULL, NULL, 'Devil Unicorn Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (189, NULL, NULL, 'Electric Guitar');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (188, 'Oh, shake those eggs! Shake ''em fast! Shake ''em, shake ''em, shake ''em, shake ''em!', NULL, 'Eggstravagant Bustle');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (190, NULL, NULL, 'Fox Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (191, NULL, NULL, 'Glory Unicorn Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (192, 'The tail of a dragon.', NULL, 'Green Dragon Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (193, NULL, NULL, 'Heroic Guitar of Darkness');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (194, 'Make the battlegrounds ring with a devilish sound.', NULL, 'Hellway Guitar');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (195, '', NULL, 'Horse Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (196, 'Use music to attack your foes.', NULL, 'Kei Guitar');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (197, 'Strange events have brought this legend to life, and and you can be a part of the magic. Wear the fabled tails of the Nine-Tailed Fox, and let the mysteries unfold before you!', NULL, 'Legendary Fox Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (198, 'Does this tail make me look ugly?', NULL, 'Livin'' Faun Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (199, 'Who hopes around all day and night? It''s Master Hare hiding the surprise.', NULL, 'Master Hare''s Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (200, 'Uki? Uki kiiii!', NULL, 'Monkey Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (201, 'Don''t be shy and display your colors proudly!', NULL, 'Peacock Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (202, 'A symbol of rebirth.', NULL, 'Phoenix Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (203, NULL, NULL, 'Rabbit Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (204, 'A bushy tail to make you stand out from other foxes.', NULL, 'Shadow Fox Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (205, NULL, NULL, 'Sprite''s Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (206, 'So fluffy I could die.', NULL, 'Squirrel Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (207, 'Gobble gobble! Flaunt your feathers!', NULL, 'Turkey Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (208, 'Just the thing to always help you land on your feet.', NULL, 'White Cat''s Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (209, NULL, NULL, 'White Tiger Tail');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (210, 'For the bearded lady in all of us.', NULL, 'Black Beard');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (211, NULL, NULL, 'Fallen Angel Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (212, NULL, NULL, 'Flu Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (213, NULL, NULL, 'Halloween''s Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (214, NULL, NULL, 'Mask of the Foras Chief');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (215, NULL, NULL, 'Plague Doctor Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (216, 'My nose is so bright that I can light up the darkest sky.', NULL, 'Rudolph''s Red Nose');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (217, NULL, NULL, 'Santa Beard');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (218, NULL, NULL, 'ANBOO Cat Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (219, 'These glasses will add cuteness to your style while looking smart and unique.', NULL, 'Egghead''s Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (220, NULL, NULL, 'Evergreen Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (221, NULL, NULL, 'Eye Patch');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (222, NULL, NULL, 'Fiesta Glasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (223, 'This eye patch is anything but a simple accessory. Let its extravagant and elegant design speak for itself.', NULL, 'Frilly Black Bunny Eye Patch');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (224, 'It''s all fun and games until somebody loses an eye.', NULL, 'Gaze Gauze');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (225, NULL, NULL, 'Golden Stoic Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (226, NULL, NULL, 'Harlequin Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (227, NULL, NULL, 'iGlasses 3000 mk2');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (228, NULL, NULL, 'Red Swag Sunglasses');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (229, NULL, NULL, 'Ski Goggles');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (230, NULL, NULL, 'Snorkel Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (231, NULL, NULL, 'Soul Devourer Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (232, 'Let the music drive you to lunacy.', NULL, 'Visual Kei Monocle');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (233, 'This eye patch is a cute accessory that will make any outfit pop.', NULL, 'White Bunny Eye Patch');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (234, 'Rock on!', NULL, 'DJ Headphones');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (235, NULL, NULL, '4-Leaf Clover');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (236, NULL, NULL, 'Adelia Knight Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (238, NULL, NULL, 'Angel Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (239, NULL, NULL, 'Anniversary Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (240, 'Dream of the stories told in one thousand and one nights.', NULL, 'Arabian Dreams Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (241, 'The perfect accessory for your Arabian dreams costume.', NULL, 'Arabian Dreams Turban');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (242, NULL, NULL, 'Archeologist''s Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (243, 'Blend in with the snowy hills with these frisky ears!', NULL, 'Arctic Fox Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (244, NULL, NULL, 'Autumn Harvest Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (245, 'Fashionable and practical!', NULL, 'Bandana of Courage');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (246, NULL, NULL, 'Bandana of Power');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (249, 'Look cute with this giant ribbon.', NULL, 'Big Red Ribbon');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (250, 'This cute bunny mask is a great accessory that will give your style quite a different look and feel.', NULL, 'Black Bunny Masque');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (251, NULL, NULL, 'Black Cat Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (252, NULL, NULL, 'Black Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (253, 'A Black Santa Hat to keep you warm!', NULL, 'Black Santa Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (254, NULL, NULL, 'Blue Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (255, NULL, NULL, 'Blue Japanese Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (256, NULL, NULL, 'Blue Meowlicious Hood');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (257, 'This cowboy hat was worn by a famous cowboy, wear it proud.', NULL, 'Blue Rider Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (258, NULL, NULL, 'Bowler Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (259, NULL, NULL, 'Bronze Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (260, 'Comes equipped with a chocolate radar for finding hidden easter eggs!', NULL, 'Bunny Beanie');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (261, 'A playful hairstyle, inspired by the long and elegant ears of the rabbit.', NULL, 'Bunny Buns');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (262, NULL, NULL, 'Burning Halo');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (263, 'This cap is the perfect accessory to complete your stylish and elegant outfit.', NULL, 'Captain Hare''s Blue Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (264, 'This cap is the perfect accessory to complete your stylish and elegant outfit.', NULL, 'Captain Hare''s Red Peaked Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (296, NULL, NULL, 'Cat Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (297, NULL, NULL, 'Cat Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (298, NULL, NULL, 'Charcoal Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (299, 'Big, pink, and ready to cook, it''s a pig!', NULL, 'Chef Porky');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (300, 'Yummy! You scream, I scream, we all scream for ICE CREAM! Vanilla flavor is the best.', NULL, 'Chocolate Ice Cream Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (301, NULL, NULL, 'Christmas Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (302, NULL, NULL, 'Coral Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (303, NULL, NULL, 'Criterion Helmet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (304, NULL, NULL, 'Crusader Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (305, NULL, NULL, 'Curse of Jack-o Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (306, NULL, NULL, 'Dark Candle Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (307, NULL, NULL, 'Dark Lantern Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (308, NULL, NULL, 'Demon Horns');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (309, 'Reign your terror on Isya.', NULL, 'Demon Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (310, NULL, NULL, 'Devil''s Helmet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (311, NULL, NULL, 'Devil Unicorn Horn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (312, NULL, NULL, 'Dunce Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (313, NULL, NULL, 'Elven Santa Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (315, NULL, NULL, 'Explorer''s Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (316, 'Smile! Kaboom!', NULL, 'Explosive Smiles');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (317, 'Dress up like a real pirate and set sail toward Isya''s adventures!', NULL, 'Feathered Pirate Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (318, NULL, NULL, 'Festive Frosty');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (319, NULL, NULL, 'Fiesta Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (320, NULL, NULL, 'Fiesta Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (321, NULL, NULL, 'Fiesta Football Helmet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (322, NULL, NULL, 'Fiesta Party Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (323, NULL, NULL, 'Fighting Spirit Headband');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (324, 'Rock it out with this hot ribbon.', NULL, 'Flame Rock Ribbon');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (325, NULL, NULL, 'Fox Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (326, NULL, NULL, 'Frog Prince');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (327, 'Bring Frosty with you during your journey in the snowy areas of Isya.', NULL, 'Frosty the Penguin');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (328, NULL, NULL, 'Fungal Matters');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (329, NULL, NULL, 'Furry Lion Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (330, NULL, NULL, 'Ghost Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (331, NULL, NULL, 'Glory Unicorn Horn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (332, 'A great gift from a little luck.', NULL, 'Golden 4-Leaf Clover');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (333, NULL, NULL, 'Good Luck Spirit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (334, NULL, NULL, 'Graduation Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (335, NULL, NULL, 'Green Afro');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (336, NULL, NULL, 'Green Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (337, NULL, NULL, 'Grimjaw Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (338, NULL, NULL, 'Harmony Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (339, NULL, NULL, 'Hatatee');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (340, NULL, NULL, 'Healing Wings Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (343, NULL, NULL, 'Hero Slime Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (344, NULL, NULL, 'Honeying Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (345, 'Look at my horse, my horse is amazing!', NULL, 'Horse Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (346, NULL, NULL, 'Huge Chocolate Ice Cream');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (347, NULL, NULL, 'Huge Strawberry Ice Cream');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (348, 'Yummy! You scream, I scream, we all scream for ICE CREAM! Vanilla flavor is the best.', NULL, 'Huge Vanilla Ice Cream');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (349, NULL, NULL, 'iElbama');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (350, NULL, NULL, 'iKarl');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (351, NULL, NULL, 'iKenton');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (352, NULL, NULL, 'iKyle');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (353, NULL, NULL, 'iRaina');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (354, NULL, NULL, 'iRemi');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (355, NULL, NULL, 'iRobin');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (356, NULL, NULL, 'iRoumenus');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (357, NULL, NULL, 'iShutian');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (358, NULL, NULL, 'Isyan Tennis Star Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (359, 'Specially build to protect you from any harm.', NULL, 'Isya Tech Helmet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (360, NULL, NULL, 'iUriel');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (361, NULL, NULL, 'iVietree');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (362, NULL, NULL, 'iZach');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (363, 'Not only Anubis could wear this fashionable hat! Adds 4% to Physical and Magical Defense.', NULL, 'Jackal Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (364, NULL, NULL, 'Kebing Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (365, 'This unusual accessory will make your outfit bloom.', NULL, 'Lacey Long-Ear''s Coronal');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (366, NULL, NULL, 'Liberty Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (367, 'This bonnet is a great accessory that will give your style quite a different look and feel.', NULL, 'Lieutenant Hare''s Black Bonnet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (368, 'This bonnet is a great accessory that will give your style quite a different look and feel.', NULL, 'Lieutenant Hare''s Red Bonnet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (369, NULL, NULL, 'Lion Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (370, NULL, NULL, 'Little King''s Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (371, 'Do these horns make me look ugly?', NULL, 'Livin'' Faun Horn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (24, 'Complete your Love Witch costume with this magical hat.', 'love-witch-hat-thumbnail.jpg', 'Love Witch Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (373, 'These ears are a must to have a hopping good time!', NULL, 'Lucky Bunny Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (374, NULL, NULL, 'Mad Hopper''s Blue Opera Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (375, NULL, NULL, 'Mad Hopper''s Turquoise Opera Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (376, 'Be careful not to wear this head piece around Grandpa Robin!', NULL, 'Magical Mistletoe');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (377, NULL, NULL, 'Magician Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (378, 'Wear the Marine Hat with pride of your fleet.', NULL, 'Marine Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (379, NULL, NULL, 'Mighty Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (380, NULL, NULL, 'Miracle Headphones');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (381, 'A hat so evil, it''s come alive.', NULL, 'Monster Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (382, 'Moooooh!', NULL, 'Moo Cow Bonnet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (383, 'Squeek!', NULL, 'Mouse Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (384, 'A symbol of heritage and pride for the native people.', NULL, 'Native Headband');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (385, NULL, NULL, 'Nurse Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (386, NULL, NULL, 'Obsidian Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (387, NULL, NULL, 'Onyx Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (388, NULL, NULL, 'Orange Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (389, NULL, NULL, 'Oranguhangin Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (390, NULL, NULL, 'Orca Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (391, 'Watch out for polar bears while wearing this penguin head.', NULL, 'Penguin Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (392, NULL, NULL, 'Perfect Aim Headband');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (393, NULL, NULL, 'Phoenix Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (394, 'Land ho! A whole new world is waiting to be discovered.', NULL, 'Pilgrim Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (395, NULL, NULL, 'Pink Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (396, NULL, NULL, 'Pink Flower Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (397, NULL, NULL, 'Plaid Stocking');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (398, NULL, NULL, 'Platinum Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (399, NULL, NULL, 'Polar Bear Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (400, NULL, NULL, 'Police Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (401, 'This cute accessory helps to bring out your outfit and pull it all together.', NULL, 'Pom-Pompous Harebands');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (402, NULL, NULL, 'Pumpkin Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (403, NULL, NULL, 'Pumpkin Kitty Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (404, NULL, NULL, 'Rabbit Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (405, NULL, NULL, 'Rainbow Unicorn Mane');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (406, NULL, NULL, 'Rain Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (407, NULL, NULL, 'Ram Horns');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (408, NULL, NULL, 'Red Elven Santa Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (409, NULL, NULL, 'Red Japanese Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (410, NULL, NULL, 'Red Panda');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (411, 'This cowboy hat was worn by a famous cowboy, wear it proud.', NULL, 'Red Rider hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (412, NULL, NULL, 'Red Swag Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (413, 'Perfect for those casual walks in the far east.', NULL, 'Reed Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (414, '', NULL, 'Reindeer Antlers');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (415, NULL, NULL, 'Revolutionary Top Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (416, NULL, NULL, 'Riding Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (417, 'A sign of your circus mastery.', NULL, 'Ringmaster Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (418, NULL, NULL, 'Roumen Laurel');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (419, 'A sea-worthy and stylish hat.', NULL, 'Roumen Mariner Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (420, NULL, NULL, 'Royal Icy Diadem');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (421, NULL, NULL, 'Red Santa Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (422, NULL, NULL, 'Scarecrow Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (423, 'Use your keen ears to stay ahead of the game.', NULL, 'Shadow Fox Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (424, NULL, NULL, 'Shamrock''s Beret');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (425, 'Tip your hat to Lady Luck and perhaps she will smile upon you in return.', NULL, 'Shamrock Top Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (426, NULL, NULL, 'Shark Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (427, NULL, NULL, 'Skull Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (428, NULL, NULL, 'Skull Helmet');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (429, 'A festive Sky Blue Santa Hat with a critical bonus of 3%.', NULL, 'Sky Blue Santa Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (430, NULL, NULL, 'Slime Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (431, NULL, NULL, 'Snow Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (432, NULL, NULL, 'Snowman Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (433, 'I have my snow head but seem to be missing my snow body.', NULL, 'Snowman Mask');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (434, NULL, NULL, 'Soccer Ball Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (435, NULL, NULL, 'Soccer Top Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (436, NULL, NULL, 'Sorceror''s Soul Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (437, NULL, NULL, 'Star Headphones');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (438, NULL, NULL, 'Steam Professor Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (439, NULL, NULL, 'Strawberry Ice Cream Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (440, NULL, NULL, 'Straw Fedora');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (441, NULL, NULL, 'Stylish Sphinx Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (442, 'The gentle sunlight brings happiness to all of Isya.', NULL, 'Sunflower Smiles');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (443, NULL, NULL, 'Tidal Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (444, NULL, NULL, 'Tiger Hoodie');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (445, 'Never go hungry again with this edible cap.', NULL, 'Toadstool Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (446, 'As you peer into the deep blue waters, you sense a strong power brewing beneath the calming ocean waves.', NULL, 'Torrent Helm');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (447, NULL, NULL, 'Toy Soldier Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (448, 'With the trendy bunny hat you can be hip while you hop!', NULL, 'Trendy Bunny Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (449, NULL, NULL, 'Twin Angels');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (450, NULL, NULL, 'Unicorn Horn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (451, NULL, NULL, 'Vanilla Ice Cream Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (452, NULL, NULL, 'Veteran''s Day Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (453, 'The perfect accessory for your Victorian steampunk costume. Adds 3% to Physical and Magical Defense.', NULL, 'Victorian Steampunk Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (454, NULL, NULL, 'Viking Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (455, NULL, NULL, 'Vintage Headpiece');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (456, NULL, NULL, 'Wedding Veil');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (457, 'Cute and cuddly.', NULL, 'White Cat Ears');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (458, NULL, NULL, 'White Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (459, NULL, NULL, 'White Santa Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (460, NULL, NULL, 'Winter Knight Diadem');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (461, NULL, NULL, 'Witch Hat');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (462, NULL, NULL, 'Yellow Flat Cap');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (463, 'What does a koala eat?', NULL, 'Yuki Liptus');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (464, NULL, NULL, 'Academy Uniform');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (465, 'Be the protector of Adelia with this costume!', NULL, 'Adelia Knight');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (466, NULL, NULL, 'Aloha');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (467, 'With this streamlined diving suit, you can cut through the water and your enemies!', NULL, 'Ama Diving Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (468, 'Look your best at the anniversary celebrations this year.', NULL, 'Anniversary Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (469, 'Get dressed like one thousand and one night.', NULL, 'Arabian Dreams');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (470, NULL, NULL, 'Archeologist');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (471, 'Spooky, yet strangely adorable!', NULL, 'Archmage Tanktop');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (472, 'The weather is changing so its time for some fall fashions!', NULL, 'Autumn Harvest');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (473, 'Be the bat that Isya needs!', NULL, 'Bat Attire');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (474, 'Inspired by classic tavern styles.', NULL, 'Bavarian Festival');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (475, NULL, NULL, 'Beach Volleyball');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (476, NULL, NULL, 'Bee Witch');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (477, 'A festive Black Santa Suit', NULL, 'Black Santa Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (478, '

Teach those monsters a lesson!', NULL, 'Black Sensei Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (479, NULL, NULL, 'Black Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (480, 'Enjoy the Summer with this nice Swimsuit set!', NULL, 'Black Swimsuit Set');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (481, NULL, NULL, 'Cloud Nine');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (482, NULL, NULL, 'Black Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (483, NULL, NULL, 'Blue Angel Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (484, NULL, NULL, 'Blue Dragon Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (485, NULL, NULL, 'Blue Succubus Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (486, NULL, NULL, 'Blue Valentine''s Day Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (487, NULL, NULL, 'Blue Western Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (488, NULL, NULL, 'Buccaneer''s Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (489, NULL, NULL, 'Cerulean Eastern Silks');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (490, NULL, NULL, 'Cerulean Nightfall');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (492, NULL, NULL, 'Cold Snap Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (493, NULL, NULL, 'Cosmo Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (494, NULL, NULL, 'Cotton Candy');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (496, NULL, NULL, 'Crimson Jeans');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (497, NULL, NULL, 'Criterion Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (498, NULL, NULL, 'Curse of Jack''O Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (499, NULL, NULL, 'Dandy Jeans');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (500, NULL, NULL, 'Dark Goth');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (501, NULL, NULL, 'Dark Jeans');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (502, NULL, NULL, 'Dark Topaz');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (503, NULL, NULL, 'Demon Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (504, NULL, NULL, 'Devilish Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (505, NULL, NULL, 'Devil''s Armor');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (506, NULL, NULL, 'Devil Unicorn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (507, NULL, NULL, 'Doctors Robe');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (508, NULL, NULL, 'Domo Sweater');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (509, NULL, NULL, 'Easter Sunday Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (510, NULL, NULL, 'Egyptian God Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (511, NULL, NULL, 'Emerald Eastern Silks');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (512, NULL, NULL, 'Elven Santa Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (513, NULL, NULL, 'Engagement Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (514, NULL, NULL, 'Evil Servant of Halloween');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (515, NULL, NULL, 'English EL Fan');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (516, NULL, NULL, 'Fan Argentina 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (517, NULL, NULL, 'Fan Australia 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (518, NULL, NULL, 'Fan Belgium 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (519, NULL, NULL, 'Fan Brazil 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (520, NULL, NULL, 'Fan England 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (521, NULL, NULL, 'Fan France 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (522, NULL, NULL, 'Fan Germany 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (523, NULL, NULL, 'Fan Mexico 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (524, NULL, NULL, 'Fan Portugal 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (525, NULL, NULL, 'Fan Russia 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (526, NULL, NULL, 'Fan Spain 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (527, NULL, NULL, 'Fan Sweden 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (528, NULL, NULL, 'Fan Switzerland 2018');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (529, NULL, NULL, 'Fantastic Birthday Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (530, NULL, NULL, 'Fiesta Birthday Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (531, NULL, NULL, 'Fiesta Football');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (532, NULL, NULL, 'Fighting Spirit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (533, NULL, NULL, 'Fire Dancer');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (534, NULL, NULL, 'Flame Rock');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (535, NULL, NULL, 'French EL Fan');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (536, NULL, NULL, 'Fury of the Ronin');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (537, NULL, NULL, 'German EL Fan');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (538, NULL, NULL, 'Glory Unicorn');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (539, NULL, NULL, 'Good Morning Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (540, NULL, NULL, 'Green Heart Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (541, NULL, NULL, 'Green Leader''s Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (542, NULL, NULL, 'Green Lucky Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (543, NULL, NULL, 'Hawaiian Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (544, NULL, NULL, 'Headless Rider Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (545, NULL, NULL, 'Healing Wings');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (546, NULL, NULL, 'Heart of Aquarius');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (547, NULL, NULL, 'Holy Light Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (548, NULL, NULL, 'Honeying Sweater Set');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (549, NULL, NULL, 'Honeying Tank-Top');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (550, NULL, NULL, 'Hunter''s Union Gambeson');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (551, NULL, NULL, 'Ice Blue Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (552, NULL, NULL, 'Imperial Gala');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (553, NULL, NULL, 'Isyan Enforcer Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (554, NULL, NULL, 'Isyan Tennis Star');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (555, NULL, NULL, 'Isya Tech Mk I');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (556, NULL, NULL, 'Java Couple');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (557, NULL, NULL, 'Kaffoo Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (558, NULL, NULL, 'Kebing Tanktop');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (559, NULL, NULL, 'Lantern Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (560, NULL, NULL, 'Lover''s Masquerade Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (561, NULL, NULL, 'Love Witch');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (562, NULL, NULL, 'Lucky Clover');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (563, NULL, NULL, 'Lunar Radiance');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (564, NULL, NULL, 'Maleficent Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (565, NULL, NULL, 'Mango Hawaiian Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (566, NULL, NULL, 'Marine Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (567, NULL, NULL, 'Meadown Fairy');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (568, NULL, NULL, 'Meowlicious Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (569, NULL, NULL, 'Midori Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (570, NULL, NULL, 'Musketeer Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (572, NULL, NULL, 'National Soccer Uniform - England');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (571, NULL, NULL, 'National Soccer Uniform - Denmark');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (573, NULL, NULL, 'National Soccer Uniform - Germany');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (574, NULL, NULL, 'National Soccer Uniform - Greece');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (575, NULL, NULL, 'National Soccer Uniform - Holland');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (576, NULL, NULL, 'National Soccer Uniform - Italy');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (577, NULL, NULL, 'National Soccer Uniform - Portugal');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (578, NULL, NULL, 'National Soccer Uniform - Switzerland');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (579, NULL, NULL, 'National Soccer Uniform - USA');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (580, NULL, NULL, 'Neo Isyan Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (581, NULL, NULL, 'Nightmare Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (582, NULL, NULL, 'Ninja Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (583, NULL, NULL, 'Noble Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (584, NULL, NULL, 'Obsidian Nightfall');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (585, NULL, NULL, 'Orange Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (586, NULL, NULL, 'Orchid Blossom');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (587, NULL, NULL, 'Palm Hawaiian Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (588, NULL, NULL, 'Panda Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (589, NULL, NULL, 'Passionate Suit Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (590, NULL, NULL, 'Penguin Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (591, NULL, NULL, 'Perfect Aim');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (592, NULL, NULL, 'Phouch Tanktop');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (593, NULL, NULL, 'Poinsettia Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (594, NULL, NULL, 'Polar Bear Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (595, NULL, NULL, 'Poolside Party');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (596, NULL, NULL, 'Pre Historic Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (597, NULL, NULL, 'Pride of Poseidon Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (598, NULL, NULL, 'Pumpkin Kitty');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (599, NULL, NULL, 'Punk Attire');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (600, NULL, NULL, 'Pure White Robe');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (601, NULL, NULL, 'Purple Dreams');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (602, NULL, NULL, 'Purple Nightmare Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (603, NULL, NULL, 'Queen''s Memento');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (604, NULL, NULL, 'Rainy Days');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (605, NULL, NULL, 'Red Elderine Uniform');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (606, NULL, NULL, 'Red Elven Santa Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (607, NULL, NULL, 'Red Heart Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (608, NULL, NULL, 'Red Leader''s Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (609, NULL, NULL, 'Red Lucky Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (610, NULL, NULL, 'Red Santa Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (611, NULL, NULL, 'Red Swag Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (644, NULL, NULL, 'Red Valentine Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (645, NULL, NULL, 'Red Western Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (646, NULL, NULL, 'Regal Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (647, NULL, NULL, 'Revolutionary Steampunk');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (648, NULL, NULL, 'Ride or Die');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (649, NULL, NULL, 'Ringmaster Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (650, NULL, NULL, 'Roumen Mariner Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (651, NULL, NULL, 'Royal Winter Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (652, NULL, NULL, 'Ruby Gerbera');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (653, NULL, NULL, 'Sailor Uniform');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (654, NULL, NULL, 'Sakura Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (655, NULL, NULL, 'Sakura Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (656, NULL, NULL, 'School Uniform');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (657, NULL, NULL, 'Scout''s Honor');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (659, NULL, NULL, 'Servant of Jack''O Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (660, NULL, NULL, 'Shamrock''s Luck');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (661, NULL, NULL, 'Sky Blue Santa');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (662, NULL, NULL, 'Slime Tanktop');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (663, NULL, NULL, 'Snowboard Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (664, NULL, NULL, 'Snowman Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (665, NULL, NULL, 'Solean Fancy');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (666, NULL, NULL, 'Sorceror''s Soul');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (667, NULL, NULL, 'Spanish EL Fan');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (668, NULL, NULL, 'Steam Professor');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (669, NULL, NULL, 'Stellar Patriot');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (670, NULL, NULL, 'Strawberry Delight');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (671, NULL, NULL, 'Stylish Shamrock');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (672, NULL, NULL, 'Stylish Sphinx Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (673, NULL, NULL, 'Summer BOOP');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (674, NULL, NULL, 'Summer Love Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (675, NULL, NULL, 'Summer''s Eve');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (676, NULL, NULL, 'Summer''s Galore');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (677, NULL, NULL, 'Summer Solstice');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (678, NULL, NULL, 'Summer Tribal Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (679, NULL, NULL, 'Sunset Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (680, NULL, NULL, 'Sweet Halloween');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (681, NULL, NULL, 'Teal Elderine Uniform');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (682, NULL, NULL, 'Tila Tequila');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (683, NULL, NULL, 'Tomb Fox Tanktop');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (684, NULL, NULL, 'Toymaker''s Apprentice');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (685, NULL, NULL, 'Toy Soldier Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (686, NULL, NULL, 'T-Pain Shirt');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (687, NULL, NULL, 'Track Suit Top');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (688, NULL, NULL, 'Traditional Chinese Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (689, NULL, NULL, 'Traditional Hanbok');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (690, NULL, NULL, 'Traditional Kimono I');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (691, NULL, NULL, 'Traditional Kimono II');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (692, NULL, NULL, 'Traditional Kimono III');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (693, NULL, NULL, 'Traditional Tagalog');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (694, NULL, NULL, 'Trendy Bunny Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (695, NULL, NULL, 'Tribal Future Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (696, NULL, NULL, 'Tropical Hawaiian Swimsuit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (697, NULL, NULL, 'Turbulent Seas');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (698, NULL, NULL, 'Turkey EL Fan');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (699, NULL, NULL, 'Turquoise Valentine Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (700, NULL, NULL, 'Valentine''s Day Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (701, NULL, NULL, 'Vampiric Embrace');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (702, NULL, NULL, 'Veteran''s Day');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (703, NULL, NULL, 'Victorian Steampunk');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (704, NULL, NULL, 'Visual Kei');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (705, NULL, NULL, 'Warrior Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (706, NULL, NULL, 'Wedding Costume');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (707, NULL, NULL, 'White Knight');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (708, NULL, NULL, 'Winter Knight Outfit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (709, NULL, NULL, 'White Ninja Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (710, NULL, NULL, 'White Santa Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (711, NULL, NULL, 'White Tiger');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (712, NULL, NULL, 'White Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (713, NULL, NULL, 'White Wedding');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (714, NULL, NULL, 'Fall BOOP');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (715, NULL, NULL, 'Winter Wonderland');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (716, NULL, NULL, 'Yellow Dragon Warrior Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (717, NULL, NULL, 'Yellow Tuxedo/Dress');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (718, NULL, NULL, 'Zombie Hunter');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (658, NULL, NULL, 'Red Sensei Suit');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (721, NULL, NULL, 'Anniversary Slime Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (722, NULL, NULL, 'Anniversary Candy Head');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (723, NULL, NULL, 'Dark Folcalorian Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (724, NULL, NULL, 'White Folcalorian Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (725, NULL, NULL, 'Hot Pink Crystal Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (726, NULL, NULL, 'Ice Pink Crystal Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (727, NULL, NULL, 'Golden Isyan Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (728, NULL, NULL, 'Dark Dwarven Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (729, NULL, NULL, 'Dichromatic Elven Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (730, NULL, NULL, 'Emerald Elven Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (731, NULL, NULL, 'White Isyan Crown');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (732, NULL, NULL, 'Demogorgon''s Hug');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (733, NULL, NULL, 'Infernal Furnace');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (734, NULL, NULL, 'Baphe Metous'' Masque');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (735, NULL, NULL, 'Pesti Lence''s Masque');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (736, NULL, NULL, 'Tail of Calamity');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (737, NULL, NULL, 'Tail of Illicium');
INSERT INTO store_item.store_item_view (id, description, icon, name) VALUES (738, NULL, NULL, 'Infernal Casket');


--
-- Name: store_item_view_id_seq; Type: SEQUENCE SET; Schema: store_item; Owner: docker
--

SELECT pg_catalog.setval('store_item.store_item_view_id_seq', 738, true);


--
-- Name: store_item_view store_item_view_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_item_view
    ADD CONSTRAINT store_item_view_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

