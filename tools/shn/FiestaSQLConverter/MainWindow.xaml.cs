﻿using SHN_SQL_Converter;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace FiestaSQLConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private string game_version = "0.00.000";
        private string client_folder;
        public string ClientFolder
        {
            get
            {
                return client_folder;
            }
            set
            {
                client_folder = value;

                find_game_version(client_folder);

                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ClientFolder"));
            }
        }

        private string server_folder;
        public string ServerFolder
        {
            get
            {
                return server_folder;
            }
            set
            {
                server_folder = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerFolder"));
            }
        }

        private string patch_folder;
        public string PatchFolder
        {
            get
            {
                return patch_folder;
            }
            set
            {
                patch_folder = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PatchFolder"));
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            ClientFolder = "D:\\FiestaOnlineNA";
            ServerFolder = "..\\..\\..\\..\\..\\data\\server-files";
            PatchFolder = "..\\..\\..\\..\\..\\data\\patches";
            folder_grid.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProgressChanged(object sender, ProgressChangedEventArgs arg)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                progress_bar.Value = arg.ProgressPercentage;
                progress_label.Content = progress_bar.Value.ToString() + "%";
            }));
        }
        private void OnMessageChanged(object sender, MessageChangedArgs arg)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                progress_file_label.Content = arg.Message;
            }));
        }

        private async void Button_Generate(object sender, RoutedEventArgs e)
        {
            SHNToSQLConverter p = new SHNToSQLConverter
            {
                input_client_folder = Path.Combine(ClientFolder, "ressystem"),
                input_server_folder = ServerFolder,
                output_folder = game_version,
                patch_folder = PatchFolder
            };

            p.ProgressChanged += OnProgressChanged;
            p.MessageChanged += OnMessageChanged;

            Task generate_task = Task.Run(() =>
            {
                p.GenerateFiles();
            })
                .ContinueWith(task =>
                {
                    p.GenerateConstraintsFile();
                })
                .ContinueWith(task =>
                {
                    generate_button.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        generate_button.IsEnabled = true;
                        progress_file_label.Content = "Done !";

                        Process.Start(Path.Combine(Directory.GetCurrentDirectory(), "" + game_version));
                    }));
                });

            generate_button.IsEnabled = false;

            await generate_task;
        }

        private void ClientBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                ValidateNames = false,
                CheckFileExists = false,
                CheckPathExists = true,

                FileName = "Folder"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string directory_path = openFileDialog.FileName;

                var directory_info = new FileInfo(directory_path);

                if (directory_info.Directory.Exists)
                {
                    ClientFolder = directory_info.DirectoryName;
                }
            }
        }

        private void find_game_version(string directory_name)
        {
            ushort[] version = Utils.GetFiestaVersion(directory_name);

            if (version != null)
            {
                game_version = string.Format("{0:D1}.{1:D2}.{2:D3}", version[0], version[1], version[2]);
                game_version_label.Content = game_version;
            }
        }

        private void ServerBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                ValidateNames = false,
                CheckFileExists = false,
                CheckPathExists = true,

                FileName = "Folder"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string directory_path = openFileDialog.FileName;

                var directory_info = new FileInfo(directory_path);

                if (directory_info.Directory.Exists)
                {
                    ServerFolder = directory_info.DirectoryName;
                }
            }
        }

        private void PatchBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                ValidateNames = false,
                CheckFileExists = false,
                CheckPathExists = true,

                FileName = "Folder"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string directory_path = openFileDialog.FileName;

                var directory_info = new FileInfo(directory_path);

                if (directory_info.Directory.Exists)
                {
                    PatchFolder = directory_info.DirectoryName;
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            CreateSQLFromFile window = new CreateSQLFromFile();

            window.ShowDialog();
        }

        private void Generate_Server_Click(object sender, RoutedEventArgs e)
        {
            ShineTable table = new ShineTable();
            table.Load(@"D:\Gamigo 2016 January Shine Untouched\World\ItemDropTable.txt");

            var t = table.table;
        }
    }
}
