﻿using SHN_SQL_Converter;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace FiestaSQLConverter
{
    /// <summary>
    /// Interaction logic for CreateSQLFromFile.xaml
    /// </summary>
    public partial class CreateSQLFromFile : Window, INotifyPropertyChanged
    {
        private string selected_file;
        public string SelectedFile
        {
            get
            {
                return selected_file;
            }
            set
            {
                selected_file = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedFile"));
            }
        }

        private string table_name;
        public string TableName
        {
            get
            {
                return table_name;
            }
            set
            {
                table_name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TableName"));
            }
        }

        private string schema_name;
        public string SchemaName
        {
            get
            {
                return schema_name;
            }
            set
            {
                schema_name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SchemaName"));
            }
        }

        private bool generate_id = true;
        public bool GenerateID
        {
            get
            {
                return generate_id;
            }
            set
            {
                generate_id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("GenerateID"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public CreateSQLFromFile()
        {
            InitializeComponent();

            folder_grid.DataContext = this;
        }

        private void Button_Browse_Fiesta_Folder(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                ValidateNames = false,
                CheckFileExists = false,
                CheckPathExists = true,

                FileName = "Folder"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string file_path = openFileDialog.FileName;

                var file_info = new FileInfo(file_path);

                if (file_info.Exists)
                {
                    SelectedFile = file_info.FullName;
                }
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Task generate_task = Task.Run(() =>
            {
                SHNToSQLConverter.GenerateGenericFile(SelectedFile, SchemaName, TableName, GenerateID);
            })
                .ContinueWith(task =>
                {
                    create_button.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        create_button.IsEnabled = true;

                        Process.Start(Path.Combine(Directory.GetCurrentDirectory(), "generic"));
                    }));
                });

            create_button.IsEnabled = false;

            await generate_task;
        }
    }
}
