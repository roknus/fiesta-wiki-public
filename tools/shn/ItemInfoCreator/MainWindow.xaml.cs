﻿using iQuest;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace ItemInfoCreator
{
    public class ItemPosition
    {
        public string Name { get; set; }
        public int ISET { get; set; }

        public ItemPosition(string name, int iset)
        {
            Name = name;
            ISET = iset;
        }
        public override string ToString()
        {
            return Name;
        }
    }

    public class ItemPositionList
    {
        private CollectionView _positions = null;
        public CollectionView Positions
        {
            get
            {
                return _positions;
            }
        }

        public ItemPositionList(IList<ItemPosition> list)
        {
            _positions = new CollectionView(list);
        }
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SHNFile item_info_input = null;
        private SHNFile item_info_view_input = null;
        private SHNFile item_info_output = null;
        private SHNFile item_info_view_output = null;
        private SHNFile item_info_server_output = null;

        private string item_info_input_path = @"D:\Gamigo\ressystem\ItemInfo_Gamigo.shn";
        private string item_info_view_input_path = @"D:\Gamigo\ressystem\ItemViewInfo_Gamigo.shn";
        private string item_info_output_path = @"\\192.168.0.250\Shine\ItemInfo.shn";
        private string item_info_view_output_path = @"\\192.168.0.250\Shine\View\ItemViewInfo.shn";
        private string item_info_server_output_path = @"\\192.168.0.250\Shine\ItemInfoServer.shn";

        public ItemPositionList positions = null;

        public MainWindow()
        {
            InitializeComponent();

            CreatePositionCombobox();

            item_info_input = new SHNFile(item_info_input_path, false);
            item_info_view_input = new SHNFile(item_info_view_input_path, false);
            item_info_output = new SHNFile(item_info_output_path, false);
            item_info_view_output = new SHNFile(item_info_view_output_path, false);
            item_info_server_output = new SHNFile(item_info_server_output_path, false);

            {
                DataColumn[] primary_keys = new DataColumn[1];
                primary_keys[0] = item_info_input.table.Columns["ID"];
                item_info_input.table.PrimaryKey = primary_keys;

                DataView dv = new DataView();
                item_info_input.table.TableName = "Input Table";
                dv.Table = item_info_input.table;
                dv.RowFilter = "ItemAuctionGroup=36";
                item_info_input_grid.ItemsSource = dv;
                //item_info_input_grid.DataContext = item_info_input.table;
            }

            item_info_view_grid.DataContext = item_info_view_input.table.DefaultView;

            {
                DataColumn[] primary_keys = new DataColumn[1];
                primary_keys[0] = item_info_output.table.Columns["ID"];
                item_info_output.table.PrimaryKey = primary_keys;

                item_info_output_grid.DataContext = item_info_output.table;
            }

            item_info_server_grid.DataContext = item_info_server_output.table;

            item_info_view_output_grid.DataContext = item_info_view_output.table;

            item_exist.Visibility = Visibility.Hidden;
        }

        private void CreatePositionCombobox()
        {
            IList<ItemPosition> list = new List<ItemPosition>
            {
                //new ItemPosition("Minion", 1),
                //new ItemPosition("Minion Both", 3),
                new ItemPosition("Tail", 4),
                new ItemPosition("Back", 5),
                //new ItemPosition("Weapon Skin 1", 6),
                //new ItemPosition("Weapon Skin 2", 7),
                //new ItemPosition("Weapon Skin 3", 8),
                new ItemPosition("Hat", 9),
                new ItemPosition("Face", 10),
                new ItemPosition("Top", 11),
                new ItemPosition("Pants", 12),
                new ItemPosition("Shoes", 13),
                new ItemPosition("Pants 2", 14),
                new ItemPosition("Costume", 15)
            };

            positions = new ItemPositionList(list);

            item_position_combobox.DataContext = positions;
        }

        private void item_info_input_grid_CurrentCellChanged(object sender, System.EventArgs e)
        {
            item_exist.Visibility = Visibility.Hidden;

            DataRowView row_view_selected = item_info_input_grid.CurrentItem as DataRowView;

            if (row_view_selected == null)
                return;

            DataRow row_selected = row_view_selected.Row;

            var item_view = (from i in item_info_view_input.table.AsEnumerable()
                             where (i["InxName"] as string).Equals(row_selected["InxName"] as string)
                             select i).FirstOrDefault();

            item_info_view_grid.SelectedIndex = item_info_view_input.table.Rows.IndexOf(item_view);
            if (item_info_view_grid.SelectedItem != null)
                item_info_view_grid.ScrollIntoView(item_info_view_grid.SelectedItem);

            item_name.Content = row_selected["Name"];

            DataRow exist = item_info_output.table.Rows.Find(row_selected["ID"]);

            if (exist != null)
            {
                create_button.IsEnabled = false;
                item_exist.Visibility = Visibility.Visible;
            }
            else
            {
                create_button.IsEnabled = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row_view_selected = item_info_input_grid.SelectedItem as DataRowView;

            if (row_view_selected == null)
                return;

            DataRow row_selected = row_view_selected.Row;

            // Item Info
            {
                row_selected["BuyPrice"] = 1;
                row_selected["SellPrice"] = 1000000;

                var new_item_info = item_info_output.table.Rows.Add((object[])row_selected.ItemArray.Clone());

                item_info_output_grid.SelectedIndex = item_info_output.table.Rows.IndexOf(new_item_info);
                if (item_info_output_grid.SelectedItem != null)
                    item_info_output_grid.ScrollIntoView(item_info_output_grid.SelectedItem);
            }

            // Item Server Info
            {
                DataRow new_row = item_info_server_output.table.NewRow();

                ItemPosition item_position = item_position_combobox.SelectedItem as ItemPosition;

                new_row["ID"] = row_selected["ID"];
                new_row["InxName"] = row_selected["InxName"];
                new_row["MarketIndex"] = "";
                new_row["City"] = 0;
                new_row["DropGroupA"] = "-";
                new_row["DropGroupB"] = "-";
                new_row["RandomOptionDropGroup"] = "-";
                new_row["Vanish"] = 60000;
                new_row["looting"] = 20000;
                new_row["DropRateKilledByMob"] = 0;
                new_row["DropRateKilledByPlayer"] = 0;
                new_row["ISET_Index"] = item_position.ISET;
                new_row["ItemSort_Index"] = "IS_BODYACC";
                new_row["KQItem"] = 0;
                new_row["PK_KQ_USE"] = 0;
                new_row["KQ_Item_Drop"] = 0;
                new_row["PreventAttack"] = 0;

                item_info_server_output.table.Rows.Add(new_row);

                item_info_server_grid.SelectedIndex = item_info_server_output.table.Rows.IndexOf(new_row);
                if (item_info_server_grid.SelectedItem != null)
                    item_info_server_grid.ScrollIntoView(item_info_server_grid.SelectedItem);
            }

            /// Item View Info
            {
                DataRowView row_item_view_selected = item_info_view_grid.SelectedItem as DataRowView;

                if (row_view_selected == null)
                    throw new System.Exception("Missing item in the ItemViewInfo");

                var new_item_view_info = item_info_view_output.table.Rows.Add((object[])row_item_view_selected.Row.ItemArray.Clone());

                item_info_view_output_grid.SelectedIndex = item_info_view_output.table.Rows.IndexOf(new_item_view_info);
                if (item_info_view_output_grid.SelectedItem != null)
                    item_info_view_output_grid.ScrollIntoView(item_info_view_output_grid.SelectedItem);
            }
        }

        private void save_button_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < item_info_output_grid.Columns.Count; i++)
            {
                item_info_output.displayToReal.Add(item_info_output_grid.Columns[i].DisplayIndex, i); //lists all displayed
            }
            bool succ = item_info_output.Save(item_info_output_path);
            item_info_output.displayToReal.Clear();

            for (int i = 0; i < item_info_view_output_grid.Columns.Count; i++)
            {
                item_info_view_output.displayToReal.Add(item_info_view_output_grid.Columns[i].DisplayIndex, i); //lists all displayed
            }
            bool succ_view = item_info_view_output.Save(item_info_view_output_path);
            item_info_view_output.displayToReal.Clear();

            for (int i = 0; i < item_info_server_grid.Columns.Count; i++)
            {
                item_info_server_output.displayToReal.Add(item_info_server_grid.Columns[i].DisplayIndex, i); //lists all displayed
            }
            bool succ_server = item_info_server_output.Save(item_info_server_output_path);
            item_info_server_output.displayToReal.Clear();
        }
    }
}
