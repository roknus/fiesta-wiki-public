﻿using iQuest;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter
{
    abstract class SHNFileBase : ISHNFile
    {
        public string Filename { get; set; }
        public string Namespace { get; set; } = "public";
        public string Tablename { get; set; }
        public DataTable Table { get; protected set; }
        public DataSet DataSet { get; set; }
        public FileType Type { get; set; } = FileType.Client;
        public bool Temporary { get; set; } = false;

        public virtual void Load(string input_folder)
        {
            SHNFile quest_dialog = new SHNFile(Path.Combine(input_folder, Filename));

            Table = quest_dialog.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;
        }

        public virtual void Patch(string patch_folder) { }
        public virtual void CreateConstraints() { }
    }
}
