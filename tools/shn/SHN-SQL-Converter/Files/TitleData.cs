﻿using iQuest;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class TitleData : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "title0");
                Utils.RemoveEmptyString(dr, "title1");
                Utils.RemoveEmptyString(dr, "title2");
                Utils.RemoveEmptyString(dr, "title3");
            }
        }

        public override void Patch(string patch_folder)
        {
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"unused_titles_patch.csv"), Table.Columns);

            Utils.PatchTable_Delete(Table, patch.table, "type");

            {
                DataRow r = Table.AsEnumerable()
                    .Where(t => (uint)t["type"] == 57)
                    .FirstOrDefault();

                r["title1"] = DBNull.Value;
                r["title2"] = DBNull.Value;
                r["title3"] = DBNull.Value;
            }
        }
    }
}
