﻿using iQuest;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class QuestData : SHNFileBase
    {
        private enum RewardType {
            Exp = 0,
            Money = 1,
            Item = 2,
            Fame = 4
        };

        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;
        }

        private bool IsQuestActive(DataRow row)
        {
            var id = row["ID"];
            var previous_quest_id = row["PrevQuestID"];

            byte quest_type = (byte)row["Type"];
            if (((quest_type >> 1) & 1) == 0)
            {
                return false;
            }
            else if (previous_quest_id != DBNull.Value && (ushort)previous_quest_id != (ushort)id)
            {
                DataRow[] r = Table.Select(string.Format("ID = {0}", (ushort)previous_quest_id));
                if (r != null && r.Length > 0)
                {
                    if (r.Length > 1)
                        throw new Exception("Should only have one previous quest");

                    return IsQuestActive(r[0]);
                }
            }

            return true;
        }

        private bool IsQuestDoable(DataRow row)
        {
            var previous_quest_id = row["PrevQuestID"];

            if (previous_quest_id != DBNull.Value)
            {
                if ((ushort)previous_quest_id == (ushort)row["ID"])
                    return false;

                DataRow[] r = Table.Select(string.Format("ID = {0}", (ushort)previous_quest_id));

                if (r.Length == 0)
                {
                    return false;
                }
                else
                {
                    return IsQuestDoable(r[0]);
                }
            }

            return true;
        }

        private uint GetQuestRewardType(DataRow row, RewardType type)
        {
            var quest_reward_table = DataSet.Tables["quest_reward"];
            var result = from quest_reward in quest_reward_table.AsEnumerable()
                          where quest_reward["ID"].Equals(row["ID"]) && (uint)quest_reward["RewardType"] == (uint)type
                          select (uint)quest_reward["Flag"];

            if (result.Count() > 1)
                throw new Exception(string.Format("There should only be 1 {0} reward", type));

            return result.DefaultIfEmpty<uint>(0).First();
        }

        public override void Patch(string patch_folder)
        {
            /////////////////////////////////////////////////////////////////////////////
            /// Clean columns
            /////////////////////////////////////////////////////////////////////////////
            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyUShort(dr, "PrevQuestID");
                Utils.RemoveEmptyUShort(dr, "ItemID");
            }

            /////////////////////////////////////////////////////////////////////////////
            /// Add isActive column
            /////////////////////////////////////////////////////////////////////////////
            DataColumn active_column = new DataColumn("isActive", typeof(byte))
            {
                DefaultValue = 1
            };
            Table.Columns.Add(active_column);

            List<DataRow> rowsToRemove = new List<DataRow>();

            foreach (DataRow dr in Table.Rows)
            {
                if (!IsQuestDoable(dr))
                {
                    // The previous quest doesn't exist, to avoid invalid foreign key just remove this quest
                    rowsToRemove.Add(dr);
                    continue;
                }

                dr["isActive"] = IsQuestActive(dr) ? 1 : 0;

                var previous_quest_id = dr["PrevQuestID"];
                if (previous_quest_id != DBNull.Value)
                {
                    DataRow[] r = Table.Select(string.Format("ID = {0}", (ushort)previous_quest_id));

                    if (r.Length == 0)
                    {
                        throw new Exception("Previous quest doesn't exist it should have been removed");
                    }
                    else
                    {
                        byte previousQuestStartClassID = (byte)r[0]["StartClassID"];
                        if (previousQuestStartClassID != 0)
                        {
                            dr["StartClassID"] = previousQuestStartClassID;
                        }
                    }
                }
            }

            foreach (var r in rowsToRemove)
            {
                Table.Rows.Remove(r);
            }

            Func<string, RewardType, bool> addRewardColumn = (col_name, col_type) =>
            {
                DataColumn column = new DataColumn(col_name, typeof(uint))
                {
                    DefaultValue = 0
                };
                Table.Columns.Add(column);

                foreach (DataRow dr in Table.Rows)
                {
                    dr[col_name] = GetQuestRewardType(dr, col_type);
                }

                return true;
            };

            addRewardColumn("exp", RewardType.Exp);
            addRewardColumn("fame", RewardType.Fame);
            addRewardColumn("money", RewardType.Money);
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Tablename, "DescId",
                "quest_dialog",
                "quest_dialog_fk"
                );
            Utils.AddForeignKey(
                DataSet,
                Tablename, "NameId",
                "quest_dialog",
                "quest_title_fk"
                );
            Utils.AddForeignKey(
                DataSet,
                Tablename, "PrevQuestID",
                Tablename,
                "previous_quest_fk"
                );
        }
    }
}
