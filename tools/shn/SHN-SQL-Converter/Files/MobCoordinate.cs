﻿using iQuest;
using System;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class MobCoordinate : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            LoadingData loading_data = null;
            try
            {
                loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
                shn.ReadColumns(loading_data);
            }
            catch (Exception)
            {
                throw;
            }

            Table.Columns["mob_id"].DataType = typeof(uint);

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["MC_ID"];
            Table.PrimaryKey = primary_keys;

            shn.ReadRows(loading_data);
        }

        public override void Patch(string patch_folder)
        {
            Utils.PrepareForeignKey(
                DataSet,
                Tablename, "mob_id",
                "mob_info"
                );
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Tablename, "mob_id",
                "mob_info",
                "mob_info_fk"
                );
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "MapName" },
                "map_info", new string[] { "MapName" },
                "map_id");
        }
    }
}
