﻿
namespace SHN_SQL_Converter.Files
{
    class LuckyCapsuleReward : SHNFileBase
    {
        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                "lucky_capsule_reward", new string[] { "Item_Inx" },
                "item_info", new string[] { "InxName" },
                "item_fk",
                false);
        }
    }
}
