﻿using iQuest;
using System;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class AbStateView : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile();

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            LoadingData loading_data;
            try
            {
                loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
                shn.ReadColumns(loading_data);
            }
            catch (Exception)
            {
                throw;
            }

            Table.Columns["inxName"].Unique = true;

            shn.ReadRows(loading_data);

            // Replaced by the auto generated unique_id because those ID have some duplicates
            Table.Columns.Remove("ID");
        }
    }
}
