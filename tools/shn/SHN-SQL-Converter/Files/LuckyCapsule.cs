﻿using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class LuckyCapsule : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            Table = new DataTable();
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            Table.Columns.Add("ID", typeof(uint));
            Table.Columns.Add("Inxname", typeof(string));
            Table.Columns.Add("ServerID", typeof(ushort));
            Table.Columns.Add("Type", typeof(byte));

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;
        }

        public override void Patch(string patch_folder)
        {
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"lucky_capsule.csv"), Table.Columns, new string[] { "ID", "Inxname", "ServerID", "Type" });

            (from c in patch.table.AsEnumerable()
             select c)
             .ToList()
             .ForEach(
                c =>
                {
                    var r = Table.NewRow();
                    r["ID"] = c["ID"];
                    r["Inxname"] = c["Inxname"];
                    r["ServerID"] = c["ServerID"];
                    r["Type"] = c["Type"];

                    Table.Rows.Add(r);
                });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "Inxname" },
                "item_info", new string[] { "InxName" },
                "capsule_item_fk");
        }
    }
}