﻿using iQuest;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class QuestEndItem : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile();

            LoadingData loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
            shn.ReadColumns(loading_data);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            Table.Columns["ItemID"].DataType = typeof(uint);

            shn.ReadRows(loading_data);
        }

        public override void Patch(string patch_folder)
        {
            /////////////////////////////////////////////////////////////////////////////////
            // Remove when quest unavailable
            (from row in Table.AsEnumerable()
                        select row["ID"])
                        .Except(from row in DataSet.Tables["quest_data"].AsEnumerable()
                                select row["ID"])
                        .ToList()
                        .ForEach(id =>
                        {
                            Table.Select(string.Format("ID = {0}", (ushort)id)).ToList().ForEach(row => Table.Rows.Remove(row));
                        });
            /////////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////////////////////////
            // Remove disabled quest items
            (from row in Table.AsEnumerable()
             where (byte)row["Type"] % 2 == 0
             select row).ToList().ForEach(row => Table.Rows.Remove(row));
            /////////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////////////////////////
            // Remove if there is duplicate EndQuestItem
            {
                var quest_action_table = DataSet.Tables["quest_action"];

                for (int i = quest_action_table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow quest_action = quest_action_table.Rows[i];

                    DataRow[] selectedRows =
                       Table
                            .AsEnumerable()
                            .Where(quest_end_item =>
                            {
                                return quest_end_item["ID"].Equals(quest_action["ID"]) && quest_end_item["ItemID"].Equals(quest_action["ResultTarget"]);
                            })
                            .ToArray();

                    if (selectedRows.Count() > 1)
                    {
                        for (int row_index = 1; row_index < selectedRows.Count(); row_index++)
                        {
                            Table.Rows.Remove(selectedRows[row_index]);
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Table.TableName, "ID",
                "quest_data",
                "quest_fk"
                );
        }
    }
}
