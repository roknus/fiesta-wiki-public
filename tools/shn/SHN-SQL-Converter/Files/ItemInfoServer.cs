﻿using iQuest;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class ItemInfoServer : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile();

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            LoadingData loading_data;
            try
            {
                loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
                shn.ReadColumns(loading_data);
            }
            catch (Exception)
            {
                throw;
            }

            // Set default value since we are going to generate new row during patching
            Table.Columns["MarketIndex"].DefaultValue = string.Empty;
            Table.Columns["City"].DefaultValue = 0;
            Table.Columns["DropGroupA"].DefaultValue = string.Empty;
            Table.Columns["DropGroupB"].DefaultValue = string.Empty;
            Table.Columns["RandomOptionDropGroup"].DefaultValue = string.Empty;
            Table.Columns["Vanish"].DefaultValue = 0;
            Table.Columns["looting"].DefaultValue = 0;
            Table.Columns["DropRateKilledByMob"].DefaultValue = 0;
            Table.Columns["DropRateKilledByPlayer"].DefaultValue = 0;
            Table.Columns["ISET_Index"].DefaultValue = 0;
            Table.Columns["ItemSort_Index"].DefaultValue = string.Empty;
            Table.Columns["KQItem"].DefaultValue = 0;
            Table.Columns["PK_KQ_USE"].DefaultValue = 0;
            Table.Columns["KQ_Item_Drop"].DefaultValue = 0;
            Table.Columns["PreventAttack"].DefaultValue = 0;

            shn.ReadRows(loading_data);

            Table.Columns.Add("ValueUnsure", typeof(bool));
            Table.Columns.Remove("ID");

            foreach (DataRow dr in Table.Rows)
            {
                dr["ValueUnsure"] = false;
                Utils.RemoveEmptyString(dr, "RandomOptionDropGroup");
            }
        }

        public override void Patch(string patch_folder)
        {

            /////////////////////////////////////////////////////////////////////////////////
            // Remove if there is duplicate items
            {
                for (int i = Table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow item_info = Table.Rows[i];

                    DataRow[] selectedRows =
                       Table
                            .AsEnumerable()
                            .Where(item_info_2 =>
                            {
                                return item_info["InxName"].Equals(item_info_2["InxName"]);
                            })
                            .ToArray();

                    if (selectedRows.Count() > 1)
                    {
                        for (int row_index = 1; row_index < selectedRows.Count(); row_index++)
                        {
                            Table.Rows.Remove(selectedRows[row_index]);
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
            
            /// Create new entries for missing items
            DataTable item_info_table = DataSet.Tables["item_info"];

            var result = (from item_info in item_info_table.AsEnumerable()
                          join item_info_server in Table.AsEnumerable() on item_info["InxName"] equals item_info_server["InxName"] into tmp
                          from i in tmp.DefaultIfEmpty()
                          where i == null
                          select item_info).ToList();

            foreach (DataRow r in result)
            {
                var new_row = Table.NewRow();
                new_row["InxName"] = r["InxName"];
                Table.Rows.Add(new_row);
            }

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "DropGroupA");
                Utils.RemoveEmptyString(dr, "DropGroupB");
            }

            /// Patch with CSV
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"item_info_server_patch.csv"), Table.Columns, new string[] { "InxName", "RandomOptionDropGroup", "ValueUnsure" });

            Utils.PatchTable_Update(Table, patch.table, "InxName", new string[] { "RandomOptionDropGroup", "ValueUnsure" });
        }
    }
}
