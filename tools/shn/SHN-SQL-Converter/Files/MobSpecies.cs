﻿using iQuest;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class MobSpecies : SHNFileBase
    {
        private DataTable OriginalTable = null;

        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(false);

            LoadingData loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
            shn.ReadColumns(loading_data);

            OriginalTable = shn.table;

            // Set to Uint to compare with the primary key of mob_info
            OriginalTable.Columns["ID"].DataType = typeof(uint);

            shn.ReadRows(loading_data);

            Table = new DataTable("mob_species", "public");
            DataColumn primary_key_col = Table.Columns.Add("id", typeof(int));
            Table.Columns.Add("species_name", typeof(string));

            primary_key_col.AutoIncrement = true;
            Table.PrimaryKey = new DataColumn[] { primary_key_col };
        }

        public override void Patch(string patch_folder)
        {
            DataTable mob_infos = DataSet.Tables["mob_info"];

            // Get all species id
            var species_id = from r in OriginalTable.AsEnumerable()
                              select r["id"];

            // Get all mob_infos that are not in the species table
            (from c in mob_infos.AsEnumerable()
             where !species_id.Contains(c["id"])
             select c)
                .ToList()
                .ForEach(missing_mob =>
                {
                    // Then create default rows in the species table
                    DataRow row = OriginalTable.NewRow();
                    row["ID"] = missing_mob["ID"];
                    row["MobName"] = missing_mob["InxName"];

                    OriginalTable.Rows.Add(row);
                });

            OriginalTable.AsEnumerable()
                .ToList()
                .ForEach(s =>
                {
                    Utils.RemoveEmptyString(s, "UnkCol0");
                    if (s["UnkCol0"] == DBNull.Value)
                    {
                        s["UnkCol0"] = "_" + s["MobName"];
                    }
                });

            (from c in OriginalTable.AsEnumerable()
             select c["UnkCol0"].ToString())
             .Distinct()
             .ToList()
             .ForEach(c =>
             {
                 DataRow row = Table.NewRow();
                 row["species_name"] = c;
                 Table.Rows.Add(row);
             });

            DataColumn primary_key_column = Table.PrimaryKey[0];
            DataColumn fk_col = mob_infos.Columns.Add("mob_species_fk", primary_key_column.DataType);

            Utils.AddForeignKey(DataSet,
                "mob_info", "mob_species_fk",
                "mob_species", "id");

            var results = from m in mob_infos.AsEnumerable()
                          join os in OriginalTable.AsEnumerable() on m["ID"] equals os["ID"]
                          select new { m, os };

            (from x in results
             join s in Table.AsEnumerable() on x.os["UnkCol0"] equals s["species_name"]
             select new { x.m, s })
             .ToList()
             .ForEach(x =>
             {
                 x.m["mob_species_fk"] = x.s["id"];
             });

            // Create species group with the levels keys
            var species_groups = (from m in mob_infos.AsEnumerable()
                                  select m)
             .GroupBy(
                m => (int)m["mob_species_fk"],
                m => (uint)m["Level"],
                (name, mobs) => new
                {
                    Key = name,
                    Count = mobs.Count(),
                    Min = mobs.Min(),
                    Max = mobs.Max()
                });
        }
    }
}
