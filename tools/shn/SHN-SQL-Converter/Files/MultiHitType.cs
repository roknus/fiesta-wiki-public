﻿using iQuest;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class MultiHitType : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile quest_dialog = new SHNFile(Path.Combine(input_folder, Filename));

            Table = quest_dialog.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "AbIndex");
            }
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "AbIndex" },
                "ab_state", new string[] { "InxName" },
                "abstate_fk");
        }
    }
}
