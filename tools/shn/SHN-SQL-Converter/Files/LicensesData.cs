﻿using iQuest;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class LicensesData : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(true);

            LoadingData loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
            shn.ReadColumns(loading_data);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            Table.Columns["MobID"].DataType = typeof(uint);
            Table.Columns.Add("species_id", typeof(int));

            shn.ReadRows(loading_data);
        }

        public override void Patch(string patch_folder)
        {
            DataTable mob_infos = DataSet.Tables["mob_info"];

            (from m in mob_infos.AsEnumerable()
             join lic in Table.AsEnumerable() on m["ID"] equals lic["MobID"]
             select new { m, lic }).ToList().ForEach(r =>
             {
                 r.lic["species_id"] = r.m["mob_species_fk"];
             });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Tablename, "MobID",
                "mob_info",
                "mob_info_fk"
                );

            Utils.AddForeignKey(
                DataSet,
                Tablename, "species_id",
                "mob_species",
                "mob_species_fk"
                );
        }
    }
}
