﻿using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace SHN_SQL_Converter.Files
{
    class ActiveSkillGroup : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            Table = new DataTable
            {
                Namespace = Namespace,
                TableName = Tablename
            };
        }

        public override void Patch(string patch_folder)
        {
            DataTable active_skill_group_join = DataSet.Tables["active_skill_group_join"];

            var col = Table.Columns.Add("id", active_skill_group_join.Columns["ActiveSkillGroupIndex"].DataType);

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = col;
            Table.PrimaryKey = primary_keys;

            /*(from j in active_skill_group_join.AsEnumerable()
             select j)
             .Select(r =>
             {
                 var parentTable = DataSet.Tables["active_skill_group"];
                 Regex condition_regexp = new Regex(@"(\d*)$");
                 var inxname = condition_regexp.Match(r["InxName"] as string);

                 var skill_group_index = r["ActiveSkillGroupIndex"];

                 return new { inxname, skill_group_index };
             })
             .Distinct()
             .ToList()
             .ForEach(group_id =>
             {
                 var row = Table.NewRow();
                 row["id"] = group_id.skill_group_index;
                 Table.Rows.Add(row);
             });*/

            (from j in active_skill_group_join.AsEnumerable()
             select j["ActiveSkillGroupIndex"])
             .Distinct()
             .ToList()
             .ForEach(group_id =>
             {
                 DataRow row = Table.NewRow();
                 row["id"] = group_id;
                 Table.Rows.Add(row);
             });

            Table.PrimaryKey = new DataColumn[] { Table.Columns["id"] };
        }
    }
}
