﻿using iQuest;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class GradeItemOption : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile();

            LoadingData loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
            shn.ReadColumns(loading_data);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            Table.Columns["ItemIndex"].Unique = true;

            shn.ReadRows(loading_data);
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                "grade_item_option", new string[] { "ItemIndex" },
                "item_info", new string[] { "InxName" },
                "item_fk");
        }
    }
}
