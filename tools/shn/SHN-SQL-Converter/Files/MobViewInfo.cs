﻿using iQuest;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class MobViewInfo : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "Texture");
                Utils.RemoveEmptyString(dr, "MobPortrait");
            }
        }

        public override void Patch(string patch_folder)
        {
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"mob_view_info_patch.csv"), Table.Columns);

            Utils.PatchTable_Update(Table, patch.table, "id", new string[] { "mobportrait" });

            /////////////////////////////////////////////////////////////////////////////////
            // Remove if there is duplicate mobs
            {
                for (int i = Table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow r = Table.Rows[i];

                    DataRow[] selectedRows =
                       Table
                            .AsEnumerable()
                            .Where(r2 =>
                            {
                                return r["ID"].Equals(r2["ID"]);
                            })
                            .ToArray();

                    if (selectedRows.Count() > 1)
                    {
                        for (int row_index = 1; row_index < selectedRows.Count(); row_index++)
                        {
                            Table.Rows.Remove(selectedRows[row_index]);
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "InxName" },
                "mob_info", new string[] { "InxName" },
                "mob_info_fk");
        }
    }
}
