﻿using iQuest;
using System;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class QuestDialog : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            LoadingData loading_data = null;
            try
            {
                loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
                shn.ReadColumns(loading_data);
            }
            catch (Exception)
            {
                throw;
            }

            Table.Columns["ID"].Unique = true;
            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;

            shn.ReadRows(loading_data);

            foreach (DataRow dr in Table.Rows)
            {
                string dialog = dr["Dialog"] as string;

                dialog = dialog.Replace("[LINE]", "");
                dialog = dialog.Replace("[SHOW_REWARD]", "");
                dialog = dialog.Trim();

                dr["Dialog"] = dialog;
            }
        }
    }
}
