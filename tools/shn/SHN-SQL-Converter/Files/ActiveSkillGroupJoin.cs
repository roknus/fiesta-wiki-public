﻿
namespace SHN_SQL_Converter.Files
{
    class ActiveSkillGroupJoin : SHNFileBase
    {
        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "InxName" },
                "active_skill", new string[] { "InxName" },
                "active_skill_fk");

            Utils.AddForeignKey(
                DataSet,
                Tablename, "activeskillgroupindex",
                "active_skill_group",
                "skill_group_fk");
        }
    }
}
