﻿using iQuest;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SHN_SQL_Converter.Files
{
    class ItemActionCondition : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ConditionID"];
            Table.PrimaryKey = primary_keys;
        }

        public override void Patch(string patch_folder)
        {
            var skill_fk_column = Table.Columns.Add("skill_group_fk", DataSet.Tables["active_skill_group"].PrimaryKey[0].DataType);
            skill_fk_column.DefaultValue = DBNull.Value;

            (from cond in Table.AsEnumerable()
             select cond)
             .ToList()
             .ForEach(condition =>
             {
                 Regex condition_regexp = new Regex(@"^\[(\d*):(\d*)\]$");
                 var values = condition["ConditionActivity"] as uint[];

                 if (values == null)
                     return;

                 switch (values[0])
                 {
                     case 7:
                         {
                             var skill_group_id = values[1];

                             if (DataSet.Tables["active_skill_group"].AsEnumerable().Where(r => (uint)r["id"] == skill_group_id).Count() == 0)
                             {
                                 // If the skill group doesn't exist then don't create foreign key
                                 break;
                             }

                             condition["skill_group_fk"] = values[1];
                             break;
                         }
                 }
             });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Tablename, "skill_group_fk",
                "active_skill_group",
                "skill_group_fk");
        }
    }
}
