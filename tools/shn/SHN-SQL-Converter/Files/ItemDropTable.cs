﻿using System;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class ItemDropTable : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            ShineTable shn = new ShineTable();
            shn.Load(Path.Combine(input_folder, Filename));

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;
        }

        public override void Patch(string patch_folder)
        {
            foreach (DataRow dr in Table.Rows)
            {
                for (int i = 1; i <= 45; i++)
                {
                    string dropNameCol = string.Format("DrItem{0}", i);
                    string dropRateCol = string.Format("DrItem{0}R", i);

                    Utils.RemoveEmptyString(dr, dropNameCol);

                    // If the item has 0 drop rate, remove it from drop table
                    if (dr[dropRateCol].Equals(0))
                    {
                        dr[dropNameCol] = DBNull.Value;
                    }
                }

                if (!dr["DrItem41"].Equals("ClosedCollectCard"))
                {
                    dr["DrItem41"] = DBNull.Value;
                }
            }
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                "item_drop_table", new string[] { "MobId" },
                "mob_info", new string[] { "InxName" },
                "mob_fk");
        }
    }
}
