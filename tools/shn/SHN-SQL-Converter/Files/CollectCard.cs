﻿using iQuest;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class CollectCard : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "CC_CardMobGroup" },
                "collect_card_group_desc", new string[] { "CC_CardMobGroup" },
                "card_mob_group_id");
            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "CC_ItemInx" },
                "collect_card_title", new string[] { "CC_ItemInx" },
                "card_title_id");
        }
    }
}
