﻿using iQuest;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class ActiveSkill : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "DemandSk");
                Utils.RemoveEmptyString(dr, "StaNameA");
                Utils.RemoveEmptyString(dr, "StaNameB");
                Utils.RemoveEmptyString(dr, "StaNameC");
                Utils.RemoveEmptyString(dr, "StaNameD");
            }
        }

        public override void Patch(string patch_folder)
        {
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"active_skill_patch.csv"), Table.Columns);

            Utils.PatchTable_Update(Table, patch.table, "InxName", new string[] { "step", "demandsk", "useclass" });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "DemandSk" },
                Tablename, new string[] { "InxName" },
                "previous_skill_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "StaNameA" },
                "ab_state", new string[] { "InxName" },
                "stanamea_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "StaNameB" },
                "ab_state", new string[] { "InxName" },
                "stanameb_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "StaNameC" },
                "ab_state", new string[] { "InxName" },
                "stanamec_fk");
        }
    }
}
