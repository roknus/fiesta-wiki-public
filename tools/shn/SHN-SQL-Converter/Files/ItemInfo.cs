﻿using iQuest;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class ItemInfo : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            LoadingData loading_data = null;
            try
            {
                loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
                shn.ReadColumns(loading_data);
            }
            catch (Exception)
            {
                throw;
            }

            // Item info server file use uint32 for id, this is needed for join
            Table.Columns["ID"].DataType = typeof(uint);

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;

            shn.ReadRows(loading_data);

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "SetItemIndex");
            }
        }

        public override void Patch(string patch_folder)
        {
            /////////////////////////////////////////////////////////////////////////////////
            // Remove if there is duplicate items
            {
                for (int i = Table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow item_info = Table.Rows[i];

                    DataRow[] selectedRows =
                       Table
                            .AsEnumerable()
                            .Where(item_info_2 =>
                            {
                                return item_info["InxName"].Equals(item_info_2["InxName"]);
                            })
                            .ToArray();

                    if (selectedRows.Count() > 1)
                    {
                        for (int row_index = 1; row_index < selectedRows.Count(); row_index++)
                        {
                            Table.Rows.Remove(selectedRows[row_index]);
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
            
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"item_info_patch.csv"), Table.Columns);

            Utils.PatchTable_Update(Table, patch.table, "id", new string[] { "itemauctiongroup" });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "InxName" },
                "item_info_server", new string[] { "InxName" },
                "item_info_server_fk");

            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "SetItemIndex" },
                "item_set", new string[] { "SetItemIndex" },
                "item_set_id");

            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "InxName" },
                "grade_item_option", new string[] { "ItemIndex" },
                "grade_item_option_id");

            Utils.AddForeignKey(
                DataSet,
                Table.TableName, 
                "UseClass",
                "use_class",
                "use_class_fk");
        }
    }
}
