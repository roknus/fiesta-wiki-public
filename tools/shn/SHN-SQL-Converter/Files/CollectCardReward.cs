﻿using iQuest;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class CollectCardReward : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;
        }
    }
}
