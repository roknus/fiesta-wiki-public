﻿using iQuest;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class ActiveSkillView : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "Descript");
                Utils.RemoveEmptyString(dr, "Function");
            }
        }
    }
}
