﻿using System.IO;

namespace SHN_SQL_Converter.Files
{
    class ItemDropGroup : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            ShineTable shn = new ShineTable(new string[] { "ItemGroupIdx", "ItemID" });
            shn.Load(Path.Combine(input_folder, Filename));

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;
        }
    }
}
