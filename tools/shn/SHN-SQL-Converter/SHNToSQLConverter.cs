﻿using iQuest;
using SHN_SQL_Converter.Files;
using SHN_SQL_Converter.JoinTables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter
{
    public class MessageChangedArgs : EventArgs
    {
        public string Message { get; }

        public MessageChangedArgs(string fileName)
        {
            Message = fileName;
        }
    }

    public class SHNToSQLConverter
    {
        DataSet fiesta_data_set = new DataSet();

        public string input_client_folder;
        public string input_server_folder;
        public string input_world_folder { get { return Path.Combine(input_server_folder, @"World"); } }
        public string patch_folder { get; set; }
        public string output_folder { get; set; }

        private int current_file_index = 1;
        private int total_files = 1;
        private double CurrentProgress
        {
            get
            {
                return 100.0 / total_files * current_file_index++;
            }
        }

        public event ProgressChangedEventHandler ProgressChanged;
        public event EventHandler<MessageChangedArgs> MessageChanged;

        private void Progess(double progress)
        {
            ProgressChanged?.Invoke(this, new ProgressChangedEventArgs((int)progress, null));
        }

        private void SerializingFile(string filename)
        {
            MessageChanged?.Invoke(this, new MessageChangedArgs("Converting : " + filename));
        }

        private void CreatingTable(string table_name)
        {
            MessageChanged?.Invoke(this, new MessageChangedArgs("Creating table : " + table_name));
        }

        private void CreatingJoinTable(string table_name)
        {
            MessageChanged?.Invoke(this, new MessageChangedArgs("Creating join table : " + table_name));
        }

        public void GenerateFiles()
        {
            if (!Directory.Exists(output_folder))
            {
                Directory.CreateDirectory(output_folder);
            }

            List<ISHNFile> files = new List<ISHNFile>
            {
                // Common
                new UseClassTypeInfo { Filename = "UseClassTypeInfo.shn", Tablename = "use_class" },
                new ClassName { Filename = "ClassName.shn", Tablename = "class_name" },

                // Mobs
                new MobInfo { Filename = "MobInfo.shn", Tablename = "mob_info" },
                new MobLoca { Filename = "Loca/MobLoca.shn", Tablename = "mob_loca", Temporary = true },
                new MobSpecies { Filename = "MobSpecies.shn", Tablename = "mob_species" },
                new MobViewInfo { Filename = "MobViewInfo.shn", Tablename = "mob_view_info" },
                new MobInfoServer { Filename = "MobInfoServer.shn", Tablename = "mob_info_server", Type = FileType.Server},
                new MobCoordinate { Filename = "MobCoordinate.shn", Tablename = "mob_coordinate" },

                // Quests
                new QuestData { Filename = "QuestData.shn", Tablename = "quest_data" },
                new Files.QuestReward { Filename = "QuestReward.shn", Tablename = "quest_reward" },
                new QuestEndNpc { Filename = "QuestEndNpc.shn", Tablename = "quest_end_npc" },
                new QuestAction { Filename = "QuestAction.shn", Tablename = "quest_action" },
                new QuestEndItem { Filename = "QuestEndItem.shn", Tablename = "quest_end_item" },
                new QuestDialog { Filename = "QuestDialog.shn", Tablename = "quest_dialog" },

                // Maps
                new MapInfo { Filename = "MapInfo.shn", Tablename = "map_info" },

                // Licenses
                new LicensesData { Filename = "WeaponTitleData.shn", Tablename = "license_data" },

                // Drops
                new ItemDropTable { Filename = "ItemDropTable.txt", Tablename = "item_drop_table", Type = FileType.World },
                new ItemDropGroup { Filename = "ItemDropGroup.txt", Tablename = "item_drop_group", Type = FileType.World },

                // AbState
                new AbState { Filename = "AbState.shn", Tablename = "ab_state" },
                new AbStateView { Filename = "AbStateView.shn", Tablename = "ab_state_view" },
                new SubAbState { Filename = "SubAbState.shn", Tablename = "sub_ab_state" },

                // Skills
                new ActiveSkill { Filename = "ActiveSkill.shn", Tablename = "active_skill" },
                new ActiveSkillGroupJoin { Filename = "ActiveSkillGroup.shn", Tablename = "active_skill_group_join" },
                new ActiveSkillGroup { Tablename = "active_skill_group" },
                new ActiveSkillView { Filename = "ActiveSkillView.shn", Tablename = "active_skill_view" },
                new PassiveSkill { Filename = "PassiveSkill.shn", Tablename = "passive_skill" },
                new PassiveSkillView { Filename = "PassiveSkillView.shn", Tablename = "passive_skill_view" },
                new PassiveSkillAbState { Filename = "PSkillSetAbstate.shn", Tablename = "passive_skill_ab_state" },
                new MultiHitType { Filename = "MultiHitType.shn", Tablename = "multi_hit_type", Type = FileType.Server },

                // Items
                new ItemInfo { Filename = "ItemInfo.shn", Tablename = "item_info" },
                new RandomOption { Filename = "RandomOption.shn", Tablename = "random_option" },
                new ItemInfoServer { Filename = "ItemInfoServer.shn", Tablename = "item_info_server", Type = FileType.Server },
                new GradeItemOption { Filename = "GradeItemOption.shn", Tablename = "grade_item_option" },
                new ItemViewInfo { Filename = "ItemViewInfo.shn", Tablename = "item_view_info" },
                new SetEffect { Filename = "SetEffect.shn", Tablename = "set_effect" },
                new ItemSet { Filename = "SetItemName.shn", Tablename = "item_set" },
                new ItemAction { Filename = "ItemAction.shn", Tablename = "item_action" },
                new ItemActionEffect { Filename = "ItemActionEffect.shn", Tablename = "item_action_effect" },
                new ItemActionEffectDesc { Filename = "ItemActionEffectDesc.shn", Tablename = "item_action_effect_desc" },
                new ItemActionCondition { Filename = "ItemActionCondition.shn", Tablename = "item_action_condition" }, //  (depends on ActiveSkillGroup)
                new UpgradeInfo { Filename = "UpgradeInfo.shn", Tablename = "upgrade_info" },
                new BraceletsUpgradeInfo { Filename = "BRAccUpgradeInfo.shn", Tablename = "upgrade_info_bracelets" },

                // Production
                new Production { Filename = "Produce.shn", Tablename = "production" },

                // Titles
                new TitleData { Filename = "CharacterTitleData.shn", Tablename = "title_data" },
                new TitleStateView { Filename = "CharacterTitleStateView.shn", Tablename = "title_state_view" },

                // Cards
                new CollectCard { Filename = "CollectCard.shn", Tablename = "collect_card" },
                new CollectCardGroupDesc { Filename = "CollectCardGroupDesc.shn", Tablename = "collect_card_group_desc" },
                new CollectCardReward { Filename = "CollectCardReward.shn", Tablename = "collect_card_reward" },
                new CollectCardTitle { Filename = "CollectCardTitle.shn", Tablename = "collect_card_title" },
                new CollectCardView { Filename = "CollectCardView.shn", Tablename = "collect_card_view" },
                new CollectCardMobGroup { Filename = "CollectCardMobGroup.shn", Tablename = "collect_card_mob_group", Type = FileType.Server },
                new CollectCardDropRate { Filename = "CollectCardDropRate.shn", Tablename = "collect_card_drop_rate", Type = FileType.Server },

                // Lucky House
                new LuckyCapsule { Namespace = "lucky_capsule", Tablename = "lucky_capsule" },
                new LuckyCapsuleGroupRate { Filename = "LCGroupRate.shn", Namespace = "lucky_capsule", Tablename = "lucky_capsule_group_rate", Type = FileType.Server },
                new LuckyCapsuleReward { Filename = "LCReward.shn", Namespace = "lucky_capsule", Tablename = "lucky_capsule_reward", Type = FileType.Server }
            };

            List<IJoinTable> join_tables = new List<IJoinTable>
            {
                // Mobs
                new ItemInfoServerRandomOption { Tablename = "item_info_server_random_option_join" },
                new SetEffectItemAction { Tablename = "set_effect_item_action" },
                new SetEffectItemActionEffectDesc { Tablename = "set_effect_item_action_effect_desc" },
                new Licenses { Tablename = "licenses" },
                new DropTableItemInfo { Tablename = "item_info_drop_group_join" },
                new ItemDropTableGroup { Tablename = "item_drop_group_join" },
                new LuckyCapsuleGroupReward { Tablename = "lucky_capsule_group_reward_join" },
                new MultiHitJoin { Tablename = "multi_hit_join" }
            };

            total_files = files.Count + join_tables.Count;
            current_file_index = 1;

            foreach (ISHNFile file in files)
            {
                CreatingTable(file.Filename);

                file.DataSet = fiesta_data_set;

                switch (file.Type)
                {
                    case FileType.Client:
                        file.Load(input_client_folder);
                        break;
                    case FileType.Server:
                        file.Load(input_server_folder);
                        break;
                    case FileType.World:
                        file.Load(input_world_folder);
                        break;
                }

                fiesta_data_set.Tables.Add(file.Table);
            }

            foreach (ISHNFile file in files)
            {
                file.Patch(patch_folder);

                Progess(CurrentProgress);
            }

            for(int i = files.Count - 1; i >= 0; i--)
            {
                MessageChanged?.Invoke(this, new MessageChangedArgs("Removing temporary tables"));

                if (files[i].Temporary)
                {
                    fiesta_data_set.Tables.Remove(files[i].Table);
                    files.RemoveAt(i);
                }
            }

            foreach (ISHNFile file in files)
            {
                CreatingTable(file.Filename);

                file.CreateConstraints();
            }

            foreach (IJoinTable jt in join_tables)
            {
                CreatingTable(jt.Tablename);

                jt.DataSet = fiesta_data_set;

                jt.Create();

                jt.Load();

                Progess(CurrentProgress);
            }

            foreach (ISHNFile file in files)
            {
                GenerateTable(file.Tablename);
            }

            foreach (IJoinTable jt in join_tables)
            {
                GenerateTable(jt.Tablename);
            }
        }

        public void GenerateConstraintsFile()
        {
            using (StreamWriter file = new StreamWriter(Path.Combine(output_folder, @"y-constraints.sql")))
            {
                Utils.CreateConstraints(fiesta_data_set, file);
            }
        }

        public static void GenerateGenericFile(string filename, string schema_name, string table_name, bool generate_id)
        {
            if (!Directory.Exists("generic"))
            {
                Directory.CreateDirectory("generic");
            }

            using (StreamWriter file = new StreamWriter(Path.Combine(@"generic", table_name + ".sql")))
            {
                SHNFile shn = new SHNFile(filename, generate_id);

                DataTable table = shn.table;
                table.Namespace = schema_name;
                table.TableName = table_name;

                Utils.CreateTABLE(table, file);

                Utils.InsertTable(table, file);
            }
        }

        private void GenerateTable(string table_name)
        {
            SerializingFile(table_name);

            string filename = Path.Combine(output_folder, table_name);
            using (StreamWriter file = new StreamWriter(Path.ChangeExtension(filename, ".sql")))
            {
                DataTable table = fiesta_data_set.Tables[table_name];
                if (table == null)
                {
                    throw new UnknownShnTable();
                }

                Utils.CreateTABLE(table, file);

                Utils.InsertTable(table, file);
            }
        }
    }
}
