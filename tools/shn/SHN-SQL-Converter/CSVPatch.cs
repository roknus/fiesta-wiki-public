﻿using System;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter
{
    class CSVPatch
    {
        public DataTable table = new DataTable();

        public void Load(string filename, DataColumnCollection source_col, string[] columns_to_load = null)
        {
            using (StreamReader reader = new StreamReader(filename))
            {
                var line = reader.ReadLine();

                char delimiter = filename.EndsWith(".tsv") ? '\t' : ',';
                string[] headers = line.Split(delimiter);

                // if no specific column has been requested to be loaded, load everything
                if (columns_to_load == null)
                    columns_to_load = headers;

                DataColumnCollection columns = table.Columns;

                foreach (string col_name in columns_to_load)
                {
                    if (!source_col.Contains(col_name))
                        throw new Exception("CSV col name '" + col_name + "' not found");

                    DataColumn col = source_col[col_name];

                    columns.Add(col.ColumnName, col.DataType);
                }

                while (!reader.EndOfStream)
                {
                    string row_line = reader.ReadLine();
                    string[] values = row_line.Split(delimiter);

                    DataRow row = table.NewRow();

                    for (int i = 0; i < columns_to_load.Length; i++)
                    {
                        DataColumn col = columns[columns_to_load[i]];
                        row.SetField(col.ColumnName, Convert.ChangeType(values[Array.IndexOf(headers, columns_to_load[i])], col.DataType));
                    }

                    table.Rows.Add(row);
                }
            }
        }
    }
}
