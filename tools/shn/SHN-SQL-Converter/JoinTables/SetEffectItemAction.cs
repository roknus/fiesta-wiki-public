﻿namespace SHN_SQL_Converter.JoinTables
{
    class SetEffectItemAction : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                DataSet.Tables["set_effect"],
                DataSet.Tables["item_action"],
                Tablename,
                "public"
                );
        }

        public override void Load()
        {
            Utils.AddJoinColumn<ushort>(
                DataSet,
                DataSet.Tables["set_effect"],
                DataSet.Tables["item_action"],
                "ItemActionID",
                "ItemActionID",
                Tablename
                );
        }
    }
}
