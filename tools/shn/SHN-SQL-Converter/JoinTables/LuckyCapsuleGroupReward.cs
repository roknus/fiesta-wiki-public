﻿namespace SHN_SQL_Converter.JoinTables
{
    class LuckyCapsuleGroupReward : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                   DataSet.Tables["lucky_capsule_reward"],
                   DataSet.Tables["lucky_capsule_group_rate"],
                   Tablename,
                   "lucky_capsule"
                   );
        }

        public override void Load()
        {
            Utils.AddJoinColumn<byte>(
                DataSet,
                DataSet.Tables["lucky_capsule_reward"],
                DataSet.Tables["lucky_capsule_group_rate"],
                "LCR_Group",
                "LCR_Group",
                Tablename
                );
        }
    }
}
