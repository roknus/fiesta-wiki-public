﻿namespace SHN_SQL_Converter.JoinTables
{
    class ItemInfoServerRandomOption : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                DataSet.Tables["item_info_server"],
                DataSet.Tables["random_option"],
                Tablename,
                "public"
                );

        }

        public override void Load()
        {
            Utils.AddJoinColumn<string>(
                DataSet,
                DataSet.Tables["item_info_server"],
                DataSet.Tables["random_option"],
                "RandomOptionDropGroup",
                "DropItemIndex",
                Tablename
                );
        }
    }
}
