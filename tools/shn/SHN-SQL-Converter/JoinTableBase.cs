﻿using System.Data;

namespace SHN_SQL_Converter.JoinTables
{
    abstract class JoinTableBase : IJoinTable
    {
        public string Tablename { get; set; }

        public DataTable Table { get; protected set; }

        public DataSet DataSet { get; set; }

        public abstract void Create();
        public abstract void Load();
    }
}