﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SHN_SQL_Converter
{
    public class ShineTable
    {
        private char delimiter = '\t';

        private string[] unique_keys = new string[] { };

        public DataTable table = new DataTable();

        struct column
        {
            public string type;
            public string name;
        }

        public ShineTable(string[] unique_keys = null)
        {
            this.unique_keys = unique_keys;
        }

        public void Load(string filename, bool generate_id = true)
        {
            using (StreamReader reader = new StreamReader(filename))
            {
                while (!reader.EndOfStream)
                {
                    string[] values = ReadLine(reader);

                    if (values == null)
                        continue;

                    switch (values[0].ToLower())
                    {
                        case "#table":
                            ReadTableRow(values.Skip(1).ToArray());
                            break;
                        case "#columntype":
                            {
                                string[] column_name = ReadLine(reader);

                                if (!column_name[0].Equals("#columnname", StringComparison.OrdinalIgnoreCase))
                                    throw new Exception("Missing column name row");

                                ReadColumns(values.Zip(column_name, (t, n) => new column { type = t, name = n }).Skip(1).ToList(), generate_id);
                                break;
                            }
                        case "#columnname":
                            throw new Exception("This should have been handled earlier");
                        case "#record":
                            ReadRecordRow(values.Skip(1).ToArray(), generate_id);
                            break;
                    }
                }
            }
        }

        private string[] ReadLine(StreamReader reader)
        {
            string row_line = reader.ReadLine();

            var end_char = row_line.IndexOf(';');
            if (end_char != -1)
                row_line = row_line.Substring(0, end_char);

            char[] chars_to_trim = { '\t', '\n', ' ' };

            row_line = row_line.TrimEnd(chars_to_trim);

            if (row_line.Length == 0)
                return null;

            return row_line.Split(delimiter);
        }

        private Type GetType(string type_name)
        {
            switch (type_name.ToLower())
            {
                case var found when new Regex(@"(string).*").IsMatch(found):
                case "index":
                    return typeof(string);
                case "word":
                    return typeof(short);
                case "byte":
                    return typeof(byte);
                case "dword":
                    return typeof(int);
            }

            throw new Exception("No type found");
        }

        private void ReadTableRow(string[] data)
        {
            table.TableName = data[0];
        }

        private void ReadColumns(List<column> columns_data, bool generate_id)
        {
            DataColumnCollection columns = table.Columns;

            if (generate_id)
            {
                var primary_key_col = columns.Add("ID", typeof(uint));
                primary_key_col.AutoIncrement = true;
                primary_key_col.AutoIncrementSeed = 1;
                primary_key_col.AutoIncrementStep = 1;

                DataColumn[] primary_keys = new DataColumn[1];
                primary_keys[0] = primary_key_col;
                table.PrimaryKey = primary_keys;
            }

            foreach (column col in columns_data)
            {
                var new_column = columns.Add(col.name, GetType(col.type));

                if (unique_keys != null && unique_keys.Contains(col.name))
                {
                    new_column.Unique = true;
                }
            }
        }

        private void ReadRecordRow(string[] data, bool generate_id)
        {
            DataColumnCollection columns = table.Columns;
            DataRow row = table.NewRow();

            // Avoid the primary key auto generated
            var starting_column_index = generate_id ? 1 : 0;

            for (int i = 0; i < data.Length; i++)
            {
                DataColumn col = columns[starting_column_index + i];
                row.SetField(col.ColumnName, Convert.ChangeType(data[i], col.DataType));
            }

            try
            {
                table.Rows.Add(row);
            }
            catch (ConstraintException)
            {
                // Do nothing just skip
            }
        }
    }
}
