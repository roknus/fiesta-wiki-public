﻿using System;
using System.Runtime.Serialization;

namespace SHN_SQL_Converter
{
    [Serializable]
    internal class UnknownShnTable : Exception
    {
        public UnknownShnTable()
        {
        }

        public UnknownShnTable(string message) : base(message)
        {
        }

        public UnknownShnTable(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnknownShnTable(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}