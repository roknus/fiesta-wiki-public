﻿using System.Data;

namespace SHN_SQL_Converter
{
    interface IJoinTable
    {
        public string Tablename { get; }
        public DataTable Table { get; }
        public DataSet DataSet { get; set; }

        public void Create();
        public void Load();
    }
}
