// auto attack
internal static int CalculateBashDamage(GameObject attacker, GameObject target, bool isCrit = false)
        {
            var damage = 0.0D;

            var minWC = (double) attacker.Stats.CurrentMinDmg + (isCrit ? attacker.Stats.CriticalMinDmg : 0);
            var maxWC = (double) attacker.Stats.CurrentMaxDmg + (isCrit ? attacker.Stats.CriticalMaxDmg : 0);

            var minAtkPower = minWC;
            var maxAtkPower = maxWC;

            var attackPower = Mathd.Rnd.Next((int) (maxAtkPower - minAtkPower)) + minAtkPower;
            var defensePower = target.Stats.CurrentDef + (double) target.Stats.CurrentEND;

            if (defensePower < 1.0D)
                defensePower = 1.0D;

            damage = (attacker.Level + 1) / defensePower * attackPower;
            return (int) damage;
        }

// skill
internal static int CalculateSkillDamage(GameObject caster, GameObject target, ActiveSkillInstance skill, HitType hitType, bool isCrit = false)
{
    var damage = 0.0D;

    if (hitType == HitType.HT_PY)
    {
        var minWC = caster.Stats.CurrentMinDmg + (double) skill.Stats.CurrentMinDmg + (isCrit ? caster.Stats.CriticalMinDmg : 0);
        var maxWC = caster.Stats.CurrentMaxDmg + (double) skill.Stats.CurrentMaxDmg + (isCrit ? caster.Stats.CriticalMaxDmg : 0);
        var minEmpBonus = 0.0D;
        var maxEmpBonus = 0.0D;

        var minAtkPower = minWC + minEmpBonus;
        var maxAtkPower = maxWC + maxEmpBonus;

        var attackPower = Mathd.Rnd.Next((int) (maxAtkPower - minAtkPower)) + minAtkPower;
        var defensePower = target.Stats.CurrentDef + (double) target.Stats.CurrentEND;

        if (defensePower < 1.0D)
            defensePower = 1.0D;

        damage = (caster.Level + 1) / defensePower * attackPower;
        return (int) damage;
    }

    if (hitType == HitType.HT_MA)
    {
        var minMA = caster.Stats.CurrentMinMDmg + (double) skill.Stats.CurrentMinMDmg + (isCrit ? caster.Stats.CriticalMinMDmg : 0);
        var maxMA = caster.Stats.CurrentMaxMDmg + (double) skill.Stats.CurrentMaxMDmg + (isCrit ? caster.Stats.CriticalMaxMDmg : 0);
        var minEmpBonus = 0.0D;
        var maxEmpBonus = 0.0D;

        var minAtkPower = minMA + minEmpBonus;
        var maxAtkPower = maxMA + maxEmpBonus;

        var attackPower = Mathd.Rnd.Next((int) (maxAtkPower - minAtkPower)) + minAtkPower;
        var defensePower = target.Stats.CurrentMDef + (double) target.Stats.CurrentSPR;

        if (defensePower < 1.0D)
            defensePower = 1.0D;

        damage = (caster.Level + 1) / defensePower * attackPower;
        return (int) damage;
    }

    return (int) damage;
}