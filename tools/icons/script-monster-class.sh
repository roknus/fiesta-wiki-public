#!/bin/bash

inputPath="$1/resmenu"

outputPath="./icons"

if [ ! -d "$outputPath" ]; then
	mkdir icons
fi

if [ ! -d "${outputPath}/monster-class" ]; then
	mkdir icons/monster-class
fi
	
convert -crop 16x16+0+0		+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-undead.webp"
convert -crop 16x16+16+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-human.webp"
convert -crop 16x16+32+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-beast.webp"
convert -crop 16x16+48+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-summoned-beast.webp"
convert -crop 16x16+64+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-elemental.webp"
convert -crop 16x16+80+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-spirit.webp"
convert -crop 16x16+96+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-collectible.webp"
convert -crop 16x16+112+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-npc.webp"
convert -crop 16x16+128+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-demon.webp"
convert -crop 16x16+144+0 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-devildom.webp"
		
convert -crop 16x16+0+16	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-general.webp"
convert -crop 16x16+16+16 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-elite.webp"
convert -crop 16x16+32+16 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-field-boss.webp"
convert -crop 16x16+48+16 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-boss.webp"
convert -crop 16x16+64+16 	+repage 	"${inputPath}/game/Maininterface_3.tga" 	"${outputPath}/monster-class/monster-icon-worldboss.webp"