#!/bin/bash

inputPath="/mnt/f/FiestaOnlineNA/resmenu"

outputPath="./icons"

if [ ! -d "$outputPath" ]; then
	mkdir icons
fi

if [ ! -d "${outputPath}/money-icon" ]; then
	mkdir icons/money-icon
fi

./magick convert -flip -crop 16x16+187+286 +repage "${inputPath}/game/Maininterface.TGA"	"${outputPath}/money-icon/money-icon-gem.png"
./magick convert -flip -crop 16x16+204+286 +repage "${inputPath}/game/Maininterface.TGA" 	"${outputPath}/money-icon/money-icon-gold.png"
./magick convert -flip -crop 16x16+221+286 +repage "${inputPath}/game/Maininterface.TGA" 	"${outputPath}/money-icon/money-icon-silver.png"
./magick convert -flip -crop 16x16+238+286 +repage "${inputPath}/game/Maininterface.TGA" 	"${outputPath}/money-icon/money-icon-copper.png"
