SET input-path=D:\Gamigo\resmenu

SET output-path=D:\LostTemple\fiesta\tools\icons

if not exist "%output-path%\monster-class\" mkdir %output-path%\monster-class
	
magick convert -flip -crop 16x16+0+0	+repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-undead.png"
magick convert -flip -crop 16x16+16+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-human.png"
magick convert -flip -crop 16x16+32+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-beast.png"
magick convert -flip -crop 16x16+48+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-summoned-beast.png"
magick convert -flip -crop 16x16+64+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-elemental.png"
magick convert -flip -crop 16x16+80+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-spirit.png"
magick convert -flip -crop 16x16+96+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-collectible.png"
magick convert -flip -crop 16x16+112+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-npc.png"
magick convert -flip -crop 16x16+128+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-demon.png"
magick convert -flip -crop 16x16+144+0 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-devildom.png"

magick convert -flip -crop 16x16+0+16	+repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-general.png"
magick convert -flip -crop 16x16+16+16 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-elite.png"
magick convert -flip -crop 16x16+32+16 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-field-boss.png"
magick convert -flip -crop 16x16+48+16 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-boss.png"
magick convert -flip -crop 16x16+64+16 +repage "%input-path%\game\Maininterface_3.TGA" "%output-path%\monster-class\monster-icon-worldboss.png"