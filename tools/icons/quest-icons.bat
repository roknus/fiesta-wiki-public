SET input-path=D:\Gamigo\resmenu

SET output-path=D:\LostTemple\fiesta\tools\icons

if not exist "%output-path%\quest-icon\" mkdir %output-path%\quest-icon

magick convert -flip -crop 16x16+496+65 +repage "%input-path%\game\Maininterface.TGA" 		"%output-path%\quest-icon\quest-icon-daily.png"
magick convert -flip -crop 13x16+85+384 +repage "%input-path%\game\Maininterface.TGA" 		"%output-path%\quest-icon\quest-icon-normal.png"
magick convert -flip -crop 15x15+84+400 +repage "%input-path%\game\Maininterface.TGA" 		"%output-path%\quest-icon\quest-icon-event.png"
magick convert -flip -crop 15x15+100+384 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\quest-icon\quest-icon-epic.png"
magick convert -flip -crop 15x15+100+400 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\quest-icon\quest-icon-chaos.png"
magick convert -flip -crop 11x15+118+384 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\quest-icon\quest-icon-class.png"
