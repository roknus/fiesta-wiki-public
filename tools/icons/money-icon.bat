SET input-path=D:\Gamigo\resmenu

SET output-path=D:\LostTemple\fiesta\tools\icons

if not exist "%output-path%\money-icon\" mkdir %output-path%\money-icon

magick convert -flip -crop 16x16+187+286 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\money-icon\money-icon-gem.png"
magick convert -flip -crop 16x16+204+286 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\money-icon\money-icon-gold.png"
magick convert -flip -crop 16x16+221+286 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\money-icon\money-icon-silver.png"
magick convert -flip -crop 16x16+238+286 +repage "%input-path%\game\Maininterface.TGA" 	"%output-path%\money-icon\money-icon-copper.png"
