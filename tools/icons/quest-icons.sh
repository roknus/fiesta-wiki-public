#!/bin/bash

inputPath="/mnt/f/FiestaOnlineNA/resmenu"

outputPath="./icons"

if [ ! -d "$outputPath" ]; then
	mkdir icons
fi

if [ ! -d "${outputPath}/quest-icon" ]; then
	mkdir icons/quest-icon
fi

./magick convert -flip -crop 16x16+496+65 +repage 	"${inputPath}/game/Maininterface.TGA" 	"${outputPath}/quest-icon/quest-icon-daily.png"
./magick convert -flip -crop 13x16+85+384 +repage 	"${inputPath}/game/Maininterface.TGA" 	"${outputPath}/quest-icon/quest-icon-normal.png"
./magick convert -flip -crop 15x15+84+400 +repage 	"${inputPath}/game/Maininterface.TGA" 	"${outputPath}/quest-icon/quest-icon-event.png"
./magick convert -flip -crop 15x15+100+384 +repage 	"${inputPath}/game/Maininterface.TGA" 	"${outputPath}/quest-icon/quest-icon-epic.png"
./magick convert -flip -crop 15x15+100+400 +repage 	"${inputPath}/game/Maininterface.TGA" 	"${outputPath}/quest-icon/quest-icon-chaos.png"
./magick convert -flip -crop 11x15+118+384 +repage 	"${inputPath}/game/Maininterface.TGA" 	"${outputPath}/quest-icon/quest-icon-class.png"
